module.exports = {
  plugins: {
    'postcss-pxtorem': {
      //结果为：设计稿元素尺寸/16，比如元素宽320px,最终页面会换算成 20rem
      // rootValue: 192,
      rootValue: 37.5,
      propList: ['*'], // 需要被转换的属性
      selectorBlackList: [] // 不进行px转换的选择器
    }
  }
}
