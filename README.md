# 路由结构

## 系统管理

    - 用户管理
    - 菜单管理
    - 角色管理
    - 部门管理

## 应用管理

    - 隔离点基本信息 /yq-hotel
    	- 隔离点信息管理 /yq-hotel/baseInfo
    	- 隔离点房间管理 /yq-hotel/room
    - 安排预定管理 /yq-reserve
    	- 隔离点预定 /yq-reserve/hotelReserve
    	- 人员接收跟踪 /yq-reserve/personReceive
    - 隔离人员管理 /yq-personnel
    	- 信息登记核实 /yq-personnel/infoRegistra
    	- 心身风险评估 /yq-personnel/healthAssessment
    	- 办理入住 /yq-personnel/checkIn
    	- 日常医学检测	/yq-personnel/medicalMonitor
    	- 离店管理 /yq-personnel/checkOutManage
    - 统计查询 /yq-totalSearch
    	- 隔离人员综合查询 /yq-totalSearch/searchUser
    	- 报表存档管理 /yq-totalSearch/reportManage
    - 监测预警 /yq-supervise
    	- 隔离到期预警 /yq-supervise/expirationAlert
    	- 中高危人员监测 /yq-supervise/userSupervise
    	- 待办事项查询 /yq-supervise/todoList

## 全局方法

src/assets/js/proto.js

## 全局组件

src/plugins/components.js

## 组件说明文档

src/components/组件说明书.md

## 封装组件文档

https://note.youdao.com/s/A2pj3Ebp 此文档较老，建议直接在 components 文件夹下查看代码注释，更加清晰
