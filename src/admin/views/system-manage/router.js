// Created by lifei on 2020/5/21--16:21.
export default {
	path: "/sys-manage",
	component: () => import("~/layout/index.vue"),
	children: [
		{
			path: "dictionary",
			meta: { title: "字典管理" },
			component: () => import("~/admin/views/system-manage/authority/dict.vue")
		},
		{
			path: "log",
			meta: { title: "日志管理" },
			component: () => import("~/admin/views/system-manage/monitor/log.vue")
		},
		{
			path: "login-log",
			meta: { title: "登录日志" },
			component: () => import("~/admin/views/system-manage/monitor/login-log.vue")
		},
		{
			path: "user",
			meta: { title: "用户管理" },
			component: () => import("~/admin/views/system-manage/authority/user.vue")
		},
		{
			path: "user-info",
			meta: { title: "用户信息" },
			component: () => import("~/admin/views/system-manage/authority/info.vue")
		},
		{
			path: "edit-password",
			meta: { title: "修改密码" },
			component: () =>
				import("~/admin/views/system-manage/authority/edit-password.vue")
		},
		{
			path: "dept",
			meta: { title: "部门管理" },
			component: () => import("~/admin/views/system-manage/authority/dept.vue")
		},
		{
			path: "role",
			meta: { title: "角色管理" },
			component: () => import("~/admin/views/system-manage/authority/role.vue")
		},
		{
			path: "menu",
			meta: { title: "菜单管理" },
			component: () => import("~/admin/views/system-manage/authority/menu.vue")
		},
		{
			path: "file",
			meta: { title: "文件管理" },
			component: () => import("~/admin/views/system-manage/monitor/file.vue")
		},
		{
			path: "online-user",
			meta: { title: "在线用户" },
			component: () =>
				import("~/admin/views/system-manage/monitor/online-user.vue")
		},
		{
			path: "task-dispatch",
			meta: { title: "任务调度" },
			component: () =>
				import("~/admin/views/system-manage/sys-conf/task-dispatch.vue")
		},
		{
			path: "interface-log",
			meta: { title: "接口调用日志" },
			component: () =>
				import("~/admin/views/system-manage/monitor/interfacelog.vue")
		},
		{
			path: "division",
			meta: { title: "行政区划" },
			component: () =>
				import("~/admin/views/system-manage/authority/division.vue")
		},
		{
			path: "message",
			meta: { title: "模板配置" },
			component: () =>
				import("~/admin/views/system-manage/authority/message-template.vue")
		},
		{
			path: "service",
			meta: { title: "服务管理" },
			component: () => import("~/admin/views/system-manage/monitor/service.vue")
		},
		//短信日志管理
		{
			path: "sms",
			meta: { title: "短信日志管理" },
			component: () => import("~/admin/views/system-manage/monitor/sms.vue")
		}
	]
};
