

import request from '@/plugins/axios'

export function fetchList(query) {
  return request({
    url: '/auth/online',
    method: 'get',
    params: query
  })
}

export function exportData(filter) {
  return request({
    url: '/auth/online/download?&filter = ' + filter,
    method: 'get',
    responseType: 'blob'
  })
}

export function kickOut(obj) {
  return request({
    url: '/auth/online',
    method: 'delete',
    data: obj
  })
}





