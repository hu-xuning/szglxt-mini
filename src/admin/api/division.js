import request from '@/plugins/axios'

export function getDivision(param) {
  return request({
    url: '/admin/division/tree',
    params: param,
    method: 'get'
  })
}
export function getDivisionAll(param) {
  return request({
    url: '/admin/division/treeAll',
    params: param,
    method: 'get'
  })
}

export function addObj(obj) {
  return request({
    url: '/admin/division',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/admin/division/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/admin/division/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/admin/division',
    method: 'put',
    data: obj
  })
}

export function getObjByCode(id) {
  return request({
    url: '/admin/division/getObjByCode/' + id,
    method: 'get'
  })
}
