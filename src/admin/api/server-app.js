import request from '@/plugins/axios'

export function appPage(query) {
    return request({
        url: '/api/devops/appPage',
        method: 'get',
        params: query
    })
}

export function appEdit(data) {
    return request({
        url: '/api/devops/appEdit',
        method: 'post',
        data: data
    })
}

export function appDel(query) {
    return request({
        url: '/api/devops/appDel',
        method: 'get',
        params: query
    })
}
