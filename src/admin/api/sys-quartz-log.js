

import request from '@/plugins/axios'

export function logFetchList(query) {
  return request({
    url: '/admin/task/jobs/logs',
    method: 'get',
    params: query
  })
}


export function downloadLogObj(obj) {
  return request({
    url: '/admin/task/jobs/logs/download',
    method: 'get',
    data: obj
  })
}

export function addObj(obj) {
  return request({
    url: '/generator/sysquartzlog',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/generator/sysquartzlog/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/generator/sysquartzlog/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/generator/sysquartzlog',
    method: 'put',
    data: obj
  })
}
