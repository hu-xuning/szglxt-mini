

import request from '@/plugins/axios'

export function fetchList(query) {
    return request({
        url: '/admin/sys-file/page',
        method: 'get',
        params: query
    })
}

export function addObj(obj) {
    return request({
        url: '/admin/sys-file/',
        method: 'post',
        data: obj
    })
}

export function delObj(id) {
    return request({
        url: '/admin/sys-file/' + id,
        method: 'delete'
    })
}

export function downObj(bucket, fileName) {
    return request({
        url: '/admin/sys-file/' + bucket + '/' + fileName,
        method: 'get',
        responseType: 'blob'
    })
}

export function downObjById(id) {
    return request({
        url: '/admin/sys-file/fileById/' + id,
        method: 'get',
        responseType: 'blob'
    })
}



