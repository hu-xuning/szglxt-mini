

import request from '@/plugins/axios'

export function fetchList (query) {
    return request({
        url: '/api/sms/page',
        method: 'get',
        params: query
    })
}

export function delObj (id) {
    return request({
        url: '/api/sms/' + id,
        method: 'delete'
    })
}

export function addObj (obj) {
    return request({
        url: '/api/sms/batchSave',
        method: 'post',
        data: obj
    })
}

export function getObj (id) {
    return request({
        url: '/api/sms/' + id,
        method: 'get'
    })
}

export function putObj (obj) {
    return request({
        url: '/api/sms',
        method: 'put',
        data: obj
    })
}

