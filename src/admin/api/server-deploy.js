import request from '@/plugins/axios'

export function serverPage(query) {
    return request({
        url: '/api/devops/serverPage',
        method: 'get',
        params: query
    })
}

export function serverEdit(data) {
    return request({
        url: '/api/devops/serverEdit',
        method: 'post',
        data: data
    })
}

export function testConnect(query) {
    return request({
        url: '/api/devops/testConnect',
        method: 'get',
        params: query
    })
}

export function serverDel(query) {
    return request({
        url: '/api/devops/serverDel',
        method: 'get',
        params: query
    })
}

export function exportExcel(query) {
    return request({
        url: '/api/devops/exportExcel',
        method: 'get',
        params: query
    })
}
