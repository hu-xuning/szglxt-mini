

import request from '@/plugins/axios'

export function fetchList(query) {
  return request({
    url: '/admin/task/jobs',
    method: 'get',
    params: query
  })
}

export function downloadObj(obj) {
  return request({
    url: '/admin/task/jobs/download',
    method: 'get',
    data: obj
  })
}


export function addObj(obj) {
  return request({
    url: '/admin/task/jobs',
    method: 'post',
    data: obj
  })
}

export function putObj(obj) {
  return request({
    url: '/admin/task/jobs',
    method: 'put',
    data: obj
  })
}


export function getObj(id) {
  return request({
    url: '/admin/task/jobs/' + id,
    method: 'put'
  })
}


export function executionObj(id) {
  return request({
    url: '/admin/task/jobs/exec/' + id,
    method: 'put'
  })
}

export function delObj(ids) {
  return request({
    url: '/admin/task/jobs/',
    method: 'delete',
    data: ids
  })
}


