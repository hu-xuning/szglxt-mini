

import request from '@/plugins/axios'

export function checkID (query) {
    return request({
        url: "/admin/user/card/query",
        method: "get",
        params: query
    })
}

export function fetchList (query) {
  return request({
    url: '/admin/user/page',
    method: 'get',
    params: query
  })
}

export function addObj (obj) {
  return request({
    url: '/admin/user',
    method: 'post',
    data: obj
  })
}

export function getObj (id) {
  return request({
    url: '/admin/user/' + id,
    method: 'get'
  })
}

export function delObj (id) {
  return request({
    url: '/admin/user/' + id,
    method: 'delete'
  })
}
export function putObj (obj) {
  return request({
    url: '/admin/user/edit',
    method: 'put',
    data: obj
  })
}
export function putUser (obj) {
  return request({
    url: '/admin/user',
    method: 'put',
    data: obj
  })
}

export function getDetails (obj) {
  return request({
    url: '/admin/user/details/' + obj,
    method: 'get'
  })
}

//密码重置
export function putPassword (obj) {
  return request({
    url: '/admin/user/passwordReset',
    method: 'put',
    data: obj
  })
}

export function tokenBase() {
    return request({
        url: '/admin/user/tokenBase',
        method: 'get'
    })
}


//获取隔离点下拉
export function getHotelList() {
  return request({
      url: '/jdglhotelbaseinfo/listHotel',
      method: 'get'
  })
}