import request from '@/plugins/axios'
export function download(url, params) {
    return request({
      url: url,
      method: 'get',
      params:Object.assign({},params, { indices: false }),
      responseType: 'blob'
    })
  }

export function downloadPost(url, params) {
    return request({
        url: url,
        method: 'post',
        data:Object.assign({},params, { indices: false }),
        responseType: 'blob'
    })
}
