import request from '@/plugins/axios'

export function fetchList(query) {
    return request({
        url: '/api/message/pageMyMessage',
        method: 'get',
        params: query
    })
}

export function getUnReasMsg(query) {
    return request({
        url: '/api/message/listMyMessageUnRead',
        method: 'get',
        params: query
    })
}

export function readMsg(uuid) {
    return request({
        url: '/api/message/readMessage/' + uuid,
        method: 'get',
    })
}

export function fetchTemplateList(query) {
    return request({
        url: '/api/messageConfig/getMessageConfigList',
        method: 'get',
        params: query
    })
}


export function addTemplate(obj) {
    return request({
        url: '/api/messageConfig/saveMessageConfig',
        method: 'post',
        data: obj
    })
}

export function getObj(id) {
    return request({
        url: '/admin/user/' + id,
        method: 'get'
    })
}

export function delTemplate(id) {
    return request({
        url: '/api/messageConfig/removeMessageConfig/' + id,
        method: 'post'
    })
}

export function putTemplate(obj) {
    return request({
        url: '/api/messageConfig/saveMessageConfig',
        method: 'post',
        data: obj
    })
}

export function getDetails(obj) {
    return request({
        url: '/admin/user/details/' + obj,
        method: 'get'
    })
}


export function readMsgBatch(obj) {
    return request({
        url: '/api/message/readMessage/batch',
        method: 'post',
        data: obj
    })
}

export function getMessageTypes() {
    return request({
        url: '/api/message/getMessageTypes',
        method: 'get',
    })
}

//密码重置
export function putPassword(obj) {
    return request({
        url: '/admin/user/passwordReset',
        method: 'put',
        data: obj
    })
}
