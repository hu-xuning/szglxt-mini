import request from '@/plugins/axios'

export function getLocalInfo(query) {
    return request({
        url: '/api/monitor/getLocalInfo',
        method: 'get',
        params: query
    })
}

export function getRemoteInfo(query) {
    return request({
        url: '/api/monitor/getRemoteInfo',
        method: 'get',
        params: query
    })
}