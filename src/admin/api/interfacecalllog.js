import request from '@/plugins/axios'

export function fetchList(query) {
  return request({
    url: '/admin/interfacecalllog/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/admin/interfacecalllog',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/admin/interfacecalllog/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/admin/interfacecalllog/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/admin/interfacecalllog',
    method: 'put',
    data: obj
  })
}

export function getInterTest(obj) {
  return request({
    url: '/admin/interfacecalllog/getInterTest',
    method: 'get',
  })
}
