/*
 * @Author: cqg
 * @Date: 2021-06-30 21:33:43
 * @LastEditTime: 2021-07-06 21:21:31
 * @Description: file content
 * @LastEditors: cqg
 */
import request from "@/plugins/axios";

//房间分页查询
export function getHotel() {
  return request({
    url: "/api/house/statical/hotel",
    method: "get",
  });
}

//通过id查询
export function getHouse(params) {
  return request({
    url: `/api/house/statical/house`,
    method: "get",
    params,
  });
}
//获取房间人员列表
export function getPersonInHouse(houseId) {
  return request({
    url: `/jdglhotelmoveinto/house/${houseId}`,
    method: "get",
  });
}
//给后端传入二维码扫描返回路径
export function getIszUrl(params) {
  return request({
    url: `/jdglhotelmoveinto/qr`,
    method: "get",
    params,
  });
}
//办理入住
export function enter(data) {
  return request({
    url: `/jdglhotelmoveinto/enter`,
    method: "post",
    data,
  });
}

//一次性获取酒店所有楼层房间回显看板接口
export function getAllInfo(params) {
  return request({
    url: `/api/house/statical/panel`,
    method: "get",
    params,
  });
}

//房间信息弹窗
export function getPopUp(houseId) {
  return request({
    url: `/api/house/statical/panel/info/${houseId}`,
    method: "get",
  });
}

//房间信息弹窗
export function getPopUpNews(params) {
  return request({
    url: `/api/person/statical/panel/list`,
    method: "get",
    params,
  });
}

//房间信息弹窗分页
export function getPopUpFenY(params) {
  return request({
    url: `/api/person/statical/panel/daily/page`,
    method: "get",
    params,
  });
}
//房间信息弹窗分页
export function getKanban(params) {
  return request({
    url: `/api/house/statical/isolation/kanban`,
    method: "get",
    params,
  });
}
