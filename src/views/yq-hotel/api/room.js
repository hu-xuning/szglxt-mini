import request from "@/plugins/axios";

//房间分页查询
export function page(query) {
  return request({
    url: "/jdglhotelhouseinfo/page",
    method: "get",
    params: query,
  });
}
//通过id查询
export function getObj(id) {
  return request({
    url: `/jdglhotelhouseinfo/${id}`,
    method: "get",
  });
}
//新增房间
export function addOrEdit(data, type) {
  return request({
    url: "/jdglhotelhouseinfo",
    method: type == "add" ? "post" : "put",
    data,
  });
}

//删除房间
export function delObj(id) {
  return request({
    url: "/jdglhotelhouseinfo/" + id,
    method: "delete",
  });
}
//隔离点下拉,isSelf:是否带权限
export function getListHotel() {
  return request({
    url: "/jdglhotelhouseinfo/listHotel",
    method: "get",
  });
}
//房屋入住记录
export function pageMoveHouseRecord(params) {
  return request({
    url: "/jdglhotelhouseinfo/pageMoveHouseRecordVO",
    method: "get",
    params,
  });
}
