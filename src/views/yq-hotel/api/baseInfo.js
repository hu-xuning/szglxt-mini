import request from "@/plugins/axios";

//隔离点信息分页查询
export function pageAll(query) {
  return request({
    url: "/jdglhotelbaseinfo/pageAll",
    method: "get",
    params: query,
  });
}
//通过id查询
export function getObj(id) {
  return request({
    url: "/jdglhotelbaseinfo/" + id,
    method: "get",
  });
}
//新增或修改隔离点基本信息表
export function addOrEdit(data, type) {
  return request({
    url: "/jdglhotelbaseinfo",
    method: type == "add" ? "post" : "put",
    data,
  });
}
//删除隔离点基本信息
export function delObj(id) {
  return request({
    url: "/jdglhotelbaseinfo/" + id,
    method: "delete",
  });
}
//查询可选用户
export function getAllUser(realName) {
  return request({
    url: "/admin/user/getAllUser?realName=" + realName,
    method: "get",
  });
}

export function getAreaList (query) {
  return request({
    url: '/admin/division/tree',
    method: 'get',
    params: query
  })
}