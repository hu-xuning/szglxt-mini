
export const tableOption = {
    column: [
      /*   {
            'label': '所属市',
            'prop': 'city',
            'dict_type': 'city'
        },
        {
            'label': '所属街道',
            'prop': 'street',
            'dict_type': 'street'
        },
        {
            'label': '所属社区',
            'prop': 'community',
            'dict_type': 'community'
        }, */
        {
            'type': 'input',
            'label': '隔离点名称',
            'prop': 'hotelName'
        },
        {
            'type': 'select',
            'label': '隔离点类型',
            'prop': 'hotelType',
            'minWidth': 140,
            'dict_type': 'hotel_type',
        }, {
            'type': 'input',
            'label': '隔离房间总数',
            'prop': 'fjs'
        },   {
            'type': 'input',
            'label': '隔离人员总数',
            'prop': 'personCount'
        },{

            'type': 'input',
            'label': '待消杀房间数',
            'prop': 'xsfjs'
        }, {

            'label': '已预订房间数',
            'prop': 'ydfjs'
        }, {

            'type': 'input',
            'label': '已锁定房间数',
            'prop': 'sdfjs'
        }, {

            'type': 'input',
            'label': '剩余可用房间数',
            'prop': 'syfjs'
        }
    ],
    tableData: {
        "hotelBaseInfoId": "b64245e31b4e2ec0ba54cb9d39948478",
        "hotelName": "test",
        "hotelStreet": "新安",
        "hotelAddr": "1",
        "hotelType": "11",
        "floorNum": "12",
        "livingFloor": "12",
        "isMonitor": 0,
        "isSecurity": 0,
        "houseTotalNum": 0,
        "hotelContact": "a",
        "hotelPhone": "12",
        "useStartTime": "2021-06-04 17:23:20",
        "useEndTime": "2021-06-26 00:00:00",
        "remark": "",
        "belongto": "440306011",
        "creapersid": 123,
        "creapers": "hadmin",
        "creatime": "2021-06-04 17:23:34",
        "modipersid": 123,
        "modipers": "hadmin", "moditime": "2021-06-16 11:24:44", "isValid": 1, "hotelUsers": [{
            "hotelUserId": "daa7ffde1db6e8d82c17da63a40e5386",
            "userId": 357,
            "hotelBaseInfoId": "b64245e31b4e2ec0ba54cb9d39948478",
            "name": "谢宁",
            "cardId": "",
            "telPhone": "无",
            "post": "",
            "personType": "41",
            "remark": "", "creapersid": null, "creapers": "hadmin", "creatime": "2021-06-16 11:24:44", "modipersid": null, "modipers": "hadmin", "moditime": "2021-06-16 11:24:44", "isValid": 1
        }], "fjzs": "0", "ydfjs": "0", "syfjs": "0", "bucketName": null, "fileName": null, "uploadFileName": null, "filePath": null, "qrCodePrefix": "https://ldxxcj.baoan.gov.cn/bafyt/index.html#/bafyt/index"
    }
}