import request from '@/plugins/axios'

//分页
export function queryList(query){
    return request({
        url: '/hotel/jdglhotelbaseinfo/page',
        method: 'get',
        params: query
    })
}