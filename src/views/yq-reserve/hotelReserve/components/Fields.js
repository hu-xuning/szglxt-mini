export const hotelInfo =
 [
    {
        'label': '隔离点名称',
        'prop': 'hotelName'
    },
    {
        'label': '所属区划',
        'prop': 'hotelStreet',
    },
    {
        'label': '隔离点类型',
        'prop': 'hotelType',
        'dict':'hotel_type'
    }, {
        'label': '详细地址',
        'prop': 'hotelAddr'
    }, {
        'label': '楼层数量',
        'prop': 'floorNum'
    }, {
        'label': '隔离人员居住楼层',
        'prop': 'livingFloor'
    }, {
        'label': '楼道及周边是否安装视频监控',
        'prop': 'isMonitor',
    }, {
        'label': '是否配备楼道安保人员',
        'prop': 'isSecurity',
        'dict':"shifou"
    }, {
        'type': 'input',
        'label': '隔离房间总数',
        'prop': 'fjs'
    }, {
        'label': '隔离点方联系人',
        'prop': 'hotelContact'
    }, {
        'label': '隔离点方联系电话',
        'prop': 'hotelPhone'
    }, {
        'label': '使用开始时间',
        'prop': 'useStartTime',
    }, {
        'label': '使用终止时间',
        'prop': 'useEndTime',
    }, {
        'label': '备注',
        'prop': 'remark'
    }, {
        'label': '工作人员使用房间数',
        'prop': 'gzrs'
    }, {
        'label': '隔离人员使用房间数',
        'prop': 'glrs'
    }, {
        'label': '待消杀房间数',
        'prop': 'xsfjs'
    }, {
        'label': '已预订房间数',
        'prop': 'ydfjs'
    }, {
        'label': '已锁定房间数',
        'prop': 'sdfjs'
    }, {
        'label': '剩余可用房间数',
        'prop': 'syfjs'
    }
]

