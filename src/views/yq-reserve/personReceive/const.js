
export const tableOption = {
    column: [
        {
            label: "隔离点名称",
            prop: "hotelName",
        },
        {
            'label': '所属市',
            'prop': 'city',
            'dict_type': 'city'
        },
        {
            'label': '所属区',
            'prop': 'area',
            'dict_type': 'area'
        },
        {
            'label': '所属街道',
            'prop': 'street',
            'dict_type': 'street'
        },
        {
            'label': '所属社区',
            'prop': 'community',
            'dict_type': 'community'
        },
        {
            label: "详细地址",
            prop: "hotelAddr",
        },
        {
            label: "预订批次",
            prop: "batchNum",
        },
        {
            label: "预订间数",
            prop: "bookRoomNum",
        },
        {
            label: "预订人数",
            prop: "bookPersonNum",
        },
        {
            label: "预入住时间",
            prop: "bookMoveTime",
        },
        {
            label: "已入住人数",
            prop: "moveNum",
        },
        {
            label: "已入住间数",
            prop: "moveRoomNum",
        },
        {
            label: "接收人",
            prop: "receivePerson",
        },
        {
            type: "datetime",
            label: "接收时间",
            prop: "receiveTime",
        },
        {
            label: "预定员",
            prop: "arrangePerson",
        },
        {
            label: "安排时间",
            prop: "arrangeTime",
        }
    ],
    filterList: [
        { label: '预订批次', inputType: 'select', name: 'batchNum', children: [] },//批次接口请求
        // {
        //     label: "安排时间",
        //     inputType: "datetimerange",
        //     name: "arrangeTime"
        // },
        {
            label: "接收时间",
            inputType: "datetimerange",
            name: ["receiveTimeStart","receiveTimeEnd"]
        },
        {
            label: "预入住时间",
            inputType: "datetimerange",
            name: ["bookMoveTimeStart", "bookMoveTimeEnd"]
        }
    ]
}