import request from "@/plugins/axios";
export function getNavList () {
	return request({
        url: "/api/isolate/statical/division",
        method: "get",
        params: {
          id: '123'
        }
    });
}

export function getStatusList (query) {
	return request({
	   url: '/apis/bmsoft/isolate/statical',
	   method: 'get',
	   params: query
	})
}