import request from '@/plugins/axios'

export function messagePage(query) {
    return request({
        url: '/api/sysinfo/pageByUserId',
        method: 'get',
        params: query
    })
}

export function getSysInfoNum() {
    return request({
        url: '/api/sysinfo/getSysInfoNum',
        method: 'post'
    })
}
export function getDeptAndUsers() {
    return request({
        url: '/api/sysinfo/getDeptAndUserAll',
        method: 'post'
    })
}

export function batchDeleteSysInfo(data){
    return request({
        url: '/api/sysinfo/batchDeleteSysInfo',
        method: 'delete',
        data: data
    })
}

export function batchReadSysInfo(data){
    return request({
        url: '/api/sysinfo/batchReadSysInfo',
        method: 'post',
        data: data
    })
}

export function getSysInfoByMsgId(id) {
    return request({
        url: '/api/sysinfo/getSysInfoByMsgId/'+ id,
        method: 'get'
    })
}

export function sendMsg(data){
    return request({
        url: '/api/sysinfo/sendMsg',
        method: 'post',
        data: data
    })
}

export function getDeptInfo(data){
    return request({
        url: '/api/sysinfo/getDeptInfo',
        method: 'post',
        data: data
    })
}

export function getUserInfoByUserName(data){
    return request({
        url: '/api/sysinfo/getUserInfoByUserName',
        method: 'post',
        data: data
    })
}
