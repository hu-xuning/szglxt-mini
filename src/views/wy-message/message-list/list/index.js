import messageTable from "../component/message-table";
import { getSysInfoNum, sendMsg } from "../api/message";
import SelectUsers from "./SelectUsers";
export default {
	components: {
		messageTable,
		SelectUsers
	},
	created() {
		this.keys = "";
		this.handleMessagesTotal();
	},
	data() {
		return {
			activeName: "second",
			messagesTotal: {},
			dialogVisible: false,
			messageAdd: {
				deptList: [],
				sysInfo: {
					messageType: "",
					messageHeader: "",
					msgContent: ""
				},
				userIdList: []
			},
			deptList: [],
			userIdList: [],
			rules: {
				messageType: [
					{ required: true, message: "请选择消息类型", trigger: "blur" }
				],
				messageHeader: [
					{ required: true, message: "请输入消息标题", trigger: "blur" }
				],
				msgContent: [
					{ required: true, message: "请输入消息内容", trigger: "blur" }
				]
			},
			deptName: [],
			realName: [],
			keys: "" // 显示 选择了哪些人
		};
	},
	computed: {
		messageTypes() {
			return this.getDict("message_type");
		}
	},
	watch: {
		activeName(val) {
			this.$refs[`tab_${val}`].getListData();
		}
	},
	methods: {
		handleAdd() {
			this.dialogVisible = true;
		},
		handleMessagesTotal() {
			getSysInfoNum().then(res => {
				if (res.data.code === 0) {
					this.messagesTotal = res.data.data;
					this.$store.commit("changeMessagesTotal", res.data.data);
					this.$refs['tab_second'].getListData()
				}
			});
		},
		beforeClose(done) {
			done();
			this.handleCancel();
		},
		handleCancel() {
			this.keys = "";
			this.dialogVisible = false;
			this.messageAdd.sysInfo = {};
			this.messageAdd.deptList = [];
			this.messageAdd.userIdList = [];
		},
		handleCommit() {
			this.$refs["messageDetails"].validate(valid => {
				if (valid) {
					if (
						this.messageAdd.deptList.length < 1 &&
						this.messageAdd.userIdList.length < 1
					) {
						this.$message.error("接收人不能为空！");
						return;
					}
					sendMsg(this.messageAdd).then(res => {
						this.handleCancel();
						this.$message({
							message: "保存成功！",
							type: "success"
						});
						this.handleMessagesTotal();
						this.$forceUpdate();
					});
				} else {
					this.$message.error("保存失败！");
					return false;
				}
			});
		},
		xzlxr() {
			this.$refs.SelectUsers.show();
		},
		Userload(CheckedKeys) {
			this.messageAdd.userIdList = CheckedKeys.list;
			this.keys = CheckedKeys.keys;
		}
	}
};
