export default {
	path: "/wy-message",
	component: () => import("~/layout/index"),
	children: [
		{
			path: "/message-list/list",
			name: "message_list",
			meta: { title: "消息列表" },
			component: () => import("~/views/wy-message/message-list/list/index.vue")
		},
		{
			path: "/message-list/details",
			name: "message_details",
			meta: { title: "我的消息详情" },
			component: () =>
				import("~/views/wy-message/message-list/edit/message-details.vue")
		}
	]
};
