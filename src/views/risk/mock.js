export default echartData = [
    {
        "groundTime": "2021-07-15",
        "groundNum": 145,
        "details": [
            {
                "originCountry": "美国洛杉矶",
                "personNum": 145
            }
        ]
    },
    {
        "groundTime": "2021-07-16",
        "groundNum": 315,
        "details": [
            {
                "originCountry": "美国洛杉矶",
                "personNum": 315
            }
        ]
    },
    {
        "groundTime": "2021-07-17",
        "groundNum": 121,
        "details": [
            {
                "originCountry": "美国洛杉矶",
                "personNum": 121
            }
        ]
    },
    {
        "groundTime": "2021-07-18",
        "groundNum": 206,
        "details": [
            {
                "originCountry": "新加坡",
                "personNum": 80
            },
            {
                "originCountry": "日本东京",
                "personNum": 126
            }
        ]
    },
    {
        "groundTime": "2021-07-19",
        "groundNum": 23,
        "details": [
            {
                "originCountry": "日本东京",
                "personNum": 23
            }
        ]
    },
    {
        "groundTime": "2021-07-20",
        "groundNum": 214,
        "details": [
            {
                "originCountry": "印度尼西亚雅加达",
                "personNum": 214
            }
        ]
    },
    {
        "groundTime": "2021-07-21",
        "groundNum": 245,
        "details": [
            {
                "originCountry": "美国洛杉矶",
                "personNum": 245
            }
        ]
    },
    {
        "groundTime": "2021-07-22",
        "groundNum": 411,
        "details": [
            {
                "originCountry": "美国洛杉矶",
                "personNum": 411
            }
        ]
    },
    {
        "groundTime": "2021-07-23",
        "groundNum": 101,
        "details": [
            {
                "originCountry": "泰国曼谷",
                "personNum": 101
            }
        ]
    },
    {
        "groundTime": "2021-07-24",
        "groundNum": 316,
        "details": [
            {
                "originCountry": "泰国曼谷",
                "personNum": 316
            }
        ]
    },
    {
        "groundTime": "2021-07-25",
        "groundNum": 49,
        "details": [
            {
                "originCountry": "泰国曼谷",
                "personNum": 49
            }
        ]
    },
    {
        "groundTime": "2021-07-25",
        "groundNum": 49,
        "details": [
            {
                "originCountry": "泰国曼谷",
                "personNum": 49
            }
        ]
    },
    {
        "groundTime": "2021-07-26",
        "groundNum": 149,
        "details": [
            {
                "originCountry": "泰国曼谷",
                "personNum": 149
            }
        ]
    },
    {
        "groundTime": "2021-07-27",
        "groundNum": 225,
        "details": [
            {
                "originCountry": "泰国曼谷",
                "personNum": 225
            }
        ]
    },
    {
        "groundTime": "2021-07-28",
        "groundNum": 271,
        "details": [
            {
                "originCountry": "泰国曼谷",
                "personNum": 271
            }
        ]
    },
    {
        "groundTime": "2021-07-29",
        "groundNum": 210,
        "details": [
            {
                "originCountry": "印度尼西亚雅加达",
                "personNum": 210
            }
        ]
    },
    {
        "groundTime": "2021-07-30",
        "groundNum": 245,
        "details": [
            {
                "originCountry": "美国洛杉矶",
                "personNum": 245
            }
        ]
    }
]