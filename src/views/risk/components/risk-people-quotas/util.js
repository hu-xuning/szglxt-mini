import Router from '@/router'

export const DEFAULT_QUOTA_CLICK = {
    openPeoplePage (item, data) {
        Router.push({
            path: `/riskList`,
            query: {
                title: `人员详情(${item.label})`,
                zzIds: item.zzIds,
                dataType: item.dataType
            }
        })
    },
    openReportPage (item, data) {
        Router.push({
            path: `/riskDetail`,
            query: {
                dataType: item.dataType
            }
        })
    }
}