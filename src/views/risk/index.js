/*
 * @Descripttion:
 * @version:
 * @Author: tangyanqing
 * @Date: 2021-07-12 09:58:42
 * @LastEditors: Lxc
 * @LastEditTime: 2021-07-13 12:13:18
 */
import request from '@/plugins/axios'



export function staticalquery() {
    return request({
        url: '/risk/statical/query',
        method: 'get',
    })
}

export function staticPersonInfo(type, pageInfo) {
    return request({
        url: `/risk/statical/toPersonInfo/${type}`,
        method: 'get',
        params: pageInfo
    })
}

// 航班转运人数发展趋势（前14天+未来3天）【含来源国家、人数】
export function flightTrendData() {
    return request({
        url: `/apis/bmsoft/statistics/flight/17day`,
        method: 'get',
    })
}

// 获取某天航班隔离人员分配情况，（参数：日期yyyy-mm-dd）
export function flightIsolateByDate(date) {
    return request({
        url: `/flight/flightIsolate`,
        method:  'get',
        params: {
            dateTime: date
        }
    })
}

export function loadSymptomData (type, pageInfo) {
    return request({
        url: `/healthy/symptom/list?type=${type}`,
        method: 'get',
        params: pageInfo
    })
}

export function loadDiseaseData (type, pageInfo) {
    return request({
        url: `/healthy/basics/list?type=${type}`,
        method: 'get',
        params: pageInfo
    })
}

export function loadPsychicData (type, pageInfo) {
    return request({
        url: `/healthy/psychology/list?type=${type}`,
        method: 'get',
        params: pageInfo
    })
}

/**
 * 查询隔离点用户
*/
export function loadPeopleByHotel (hotelId, type, pageInfo) {
    let url = null
    if (type == 20) {
        url = '/apis/bmsoft/statistics/caring-staff/list'
    } else if (type == 30) {
        url = '/apis/bmsoft/statistics/import-staff/list'
    } else if (type == 13) {
        url = '/apis/bmsoft/statistics/top-symptoms-staff/list'
    }
    if (!url) {
        return Promise.reject()
    }
    return request({
        url,
        method: 'get',
        params: Object.assign({
            hotelId
        }, {
            pageNum: pageInfo.current,
            pageSize: pageInfo.size
        })
    })
}
