import request from "@/plugins/axios";

//查询
export function queryAll(params) {
  return request({
    url: "/statistical/query/personnel/dynamic",
    method: "get",
    params
  });
}
//导出
export function exportAll(obj) {
  return request({
    url: "/statistical/export/personnel/dynamic",
    method: "get",
    data: obj,
    responseType: 'arraybuffer'
  });
}