import request from "@/plugins/axios";

//查询
export function queryAll(params) {
  return request({
    url: "/personUnlawfulAct/query/escaped",
    method: "get",
	params
  });
}

//搜索查询
export function querySearch(params) {
  return request({
    url: "/personUnlawfulAct/query/escaped",
    method: "get",
	params
  });
}
//导出
export function exportAll(obj) {
  return request({
    url: "/personUnlawfulAct/export/escaped",
    method: "get",
    data: obj,
    responseType: 'arraybuffer'
  });
}