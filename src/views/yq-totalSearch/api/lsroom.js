import request from "@/plugins/axios";

//查询
export function queryAll() {
  return request({
    url: "/statistical/query/roomDynamic",
    method: "get",
	// data: jsonObj
  });
}


//查询
export function querySearch(params) {
  return request({
    url: "/statistical/query/roomDynamic",
    method: "get",
	params
  });
}
//导出
export function exportAll(useStartTime) {
  return request({
    url: "/statistical/export/roomDynamic",
    method: "get",
    data: obj,
    responseType: 'arraybuffer'
  });
}