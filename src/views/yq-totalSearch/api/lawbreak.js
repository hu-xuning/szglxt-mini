import request from "@/plugins/axios";

//查询
export function queryAll(params) {
  return request({
    url: "/personUnlawfulAct/query/info",
    method: "get",
	params
  });
}

//搜索查询
export function querySearch(params) {
  return request({
    url: "/personUnlawfulAct/query/info",
    method: "get",
    params
  });
}
//导出
export function exportAll(obj) {
  return request({
    url: "/personUnlawfulAct/export/info",
    method: "get",
    data: obj,
    responseType: 'arraybuffer'
  });
}