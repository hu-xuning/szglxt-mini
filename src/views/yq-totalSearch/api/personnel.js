import request from "@/plugins/axios";

//查询
export function queryAll(params) {
  return request({
    url: "/statistical/query/staffHealth",
    method: "get",
	params
  });
}
//导出
export function exportAll(obj) {
  return request({
    url: "/statistical/export/staffHealth",
    method: "get",
    data: obj,
    responseType: 'arraybuffer'
  });
}