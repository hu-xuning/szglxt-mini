import request from "@/plugins/axios";

//查询
export function queryAll(params) {
  return request({
    url: "/statistical/page/personnel/abnormal",
    method: "get",
	params
  });
}

//搜索查询
export function querySearch(params) {
  return request({
    url: "/statistical/page/personnel/abnormal",
    method: "get",
	params
  });
}
//导出
export function exportAll(obj) {
  return request({
    url: "/statistical/export/personnel/abnormal",
    method: "get",
    data: obj,
    responseType: 'arraybuffer'
  });
}