import request from "@/plugins/axios";
// 获取总览数据
export function getPublicTotal () {
	return request({
		url: "/main/getJdglPublicInfo",
		method: "get"
	});
}
// 总览次级数据
export function getPublicSubInfo (id) {
	return request({
		url: `/main/getJdglPublicInfoByArea/${id}`,
		method: "get"
	});
}

// 酒店总览数据
export function getHotelTotal () {
	return request({
		url: "/main/getJdglHotelInfo",
		method: "get"
	});
}

// 酒店风险提示
export function getHotelTotalWarning () {
	return request({
		url: "/main/getJdglHotelInfoByRisk",
		method: "get"
	});
}


// 隔离人员数据
export function getPersonTotal () {
	return request({
		url: "/main/getJdglPersonInfo",
		method: "get"
	});
}

// 工作人员数据
export function getStaffTotal () {
	return request({
		url: "/main/getJdglWorkPersonInfo",
		method: "get"
	});
}