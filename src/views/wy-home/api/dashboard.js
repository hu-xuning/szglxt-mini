import request from "@/plugins/axios";

export function getFinanceList(query) {
	return request({
		url: "/api/index/getFinanceList",
		method: "get",
		params: query
	});
}

export function getContractRemind(query) {
	return request({
		url: "/api/rentcontract/remind2",
		method: "get",
		params: query
	});
}

/**
 * 获取待办列表
 */
export const getTodoList = params => {
	return request({
		url: "/api/flowbusimain/page2?current=1&size=7",
		method: "get"
	});
};

export function getBudgetApply(busiId) {
	return request({
		url: "/api/budgetaddapply/getBudgetApply/" + busiId,
		method: "get"
	});
}
