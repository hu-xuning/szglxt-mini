// Created by lifei on 2020/7/18--8:35.
export default {
	path: "/dashboard",
	component: () => import("~/layout/index"),
	children: [
		{
			path: "",
			meta: { title: "首页" },
			component: () => import("~/views/wy-home/index")
		}
	]
};
