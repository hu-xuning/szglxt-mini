/*
 * @Author: xjt
 * @Date: 2021-01-19 11:35:28
 * @LastEditTime: 2021-06-25 16:19:15
 * @Description: 首页模块角色配置
 * 中心领导首页角色 - ROLE_LEADER_HOMEPAGE
		综合部首页角色 - ROLE_TOTAL_HOMEPAGE
		产权部首页角色 - ROLE_BUILD_HOMEPAGE
		经营部首页角色 - ROLE_OPERA_HOMEPAGE
		物业部首页角色 - ROLE_PROPER_HOMEPAGE
		财务部首页角色 - ROLE_FINANC_HOMEPAGE
		管理所首页角色 - ROLE_MANAGE_HOMEPAGE
		产业引进部门首页角色 - ROLE_OTHER_HOMEPAGE
 * @LastEditors: xjt
 */

// 我的消息 - MyMessage [公共]
// 我的待办 - Todo [公共]
// 合同信息 - BaseContract [除了物业部门以外所有要显示的合同信息]
// 运营合同信息 - OperateContract [物业部门展示的合同信息]
// 项目规划数量 - ProjectData [panel]
// 财务核心数据 - FinanceData
// 物业核心数据 - PropertyInfo
import store from "@/store/index";

//角色分配组件
const componentNames = {
	// 中心领导首页角色
	ROLE_LEADER_HOMEPAGE: [
		"MyMessage",
		"BaseContract",
		"FinanceData",
		"PropertyInfo"
	],
	// 综合部首页角色
	ROLE_TOTAL_HOMEPAGE: ["MyMessage", "BaseContract", "PropertyInfo"],
	// 产权部首页角色
	ROLE_BUILD_HOMEPAGE: ["MyMessage", "ProjectData", "PropertyInfo"],
	// 经营部首页角色
	ROLE_OPERA_HOMEPAGE: [
		"MyMessage",
		"BaseContract",
		"FinanceData",
		"PropertyInfo"
	],
	// 物业部首页角色
	ROLE_PROPER_HOMEPAGE: [
		"MyMessage",
		"OperateContract",
		"PropertyInfo"
	],
	// 财务部首页角色
	ROLE_FINANC_HOMEPAGE: [
		"MyMessage",
		"BaseContract",
		"ProjectData",
		"FinanceData"
	],
	// 管理所首页角色
	ROLE_MANAGE_HOMEPAGE: ["MyMessage", "BaseContract", "PropertyInfo"],
	// 产业引进部门首页角色
	ROLE_TOTAL_HOMEPAGE: ["MyMessage", "BaseContract", "PropertyInfo"],
	// 默认
	DEFAULT: ["MyMessage", "Todo"]
};

// 组件优先级设置
const componentsSort = {
	// 我的消息
	MyMessage: 0,
	// 我的待办
	Todo: 10,
	// 合同信息
	BaseContract: 20,
	// 物业合同信息
	OperateContract: 30,
	// 物业规划数据
	ProjectData: 40,
	// 物业核心数据
	PropertyInfo: 50,
	// 财务核心数据
	FinanceData: 999
};

// 获取组件名称
export const getComponents = roles => {
	const { roleCode } = store.state.user.user;
	if (!roleCode || !roleCode.length) return componentNames.DEFAULT;
	let component = roleCode.reduce(
		(total, item) => total.concat(componentNames[item]),
		[]
	);
	let components = Array.from(new Set(component));
	return components.sort((a, b) => componentsSort[a] - componentsSort[b]);
};
