/*
 * @Author: xjt
 * @Date: 2021-06-28 16:32:26
 * @LastEditTime: 2021-06-29 20:35:46
 * @Description: 数据对照组
 * @LastEditors: xjt
 */
export default {
	person: {
		// 人员状态
		status: {
			"add_count": "当天入住",
			"remove_count": "期满解除",
			"gohome_count": "转居家隔离",
			"hospital_count": "转医院治疗观察",
		},
		// 人员类别
		category: {
			"flight_count": "航班入境",
			"hongkong_count": "香港入境",
			"local_touch_count": "本土密接者",
			"local_secondtouch_count": "本土次密接者",
			"local_keynote_count": "本土重点人员",
		},
		// 人员身份
		identy: {
			"inland_count": "内地籍",
			"identity_hongkong_count": "香港籍",
			"macao_count": "澳门籍",
			"taiwan_count": "台籍",
			"out_count": "外籍",
		},
		// 特殊人群
		special: {
			"age70": "70岁以上",
			"age14": "14岁以下",
			"basic_count": "基础疾病",
			"heart_count": "心理疾病",
			"disease_count": "残疾人",
			"gravida_count": "孕产妇",
		},
		// 风险提示
		warning: 'monitor',
	},
	place: {
		// 隔离点类型
		category: {
			touch_count: '密切接触者隔离点',
			international_count: '国际航班入境人员隔离点',
			hongkong_count: '香港入境及其他来深人员隔离点',
			shenzhen_count: '经深圳转运到市外人员隔离点',
			out_count: '境外航班机组人员隔离点'
		},
		// 区划
		area: {},
		total: {
			hotel_count: '隔离点数',
			house_count: '总房间数',
		},
		belongHotel: "belongHotel"
	},
	staff: {
		total: {
			jduser_count: '总量',
			now_work: '当班'
		},
		category: {
			gldzb_count: "隔离点专班",
			abry_count: "安保人员",
			jdgly_count: "街道管理员",
			bjry_count: "保洁人员",
			qgly_count: "区管理员",
			xsry_count: "消杀人员",
			swzz_count: "市委组织部管理员",
			ylry_count: "医疗服务人员",
			zbld_count: "专班领导",
			xlfdry_count: "心理疏导人员",
			xxgl_count: "信息管理专员",
			qt_count: "其他人员",
		},
		health: {
			inspection_count: "核酸合格",
			fristneedle_count: "第一针疫苗",
			secondneedle_count: "第二针疫苗",
		},
		warning: 'monitor',
	}
}
