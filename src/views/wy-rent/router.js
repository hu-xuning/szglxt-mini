/*
 * @Author: cqg
 * @Date: 2020-11-18 15:21:41
 * @LastEditors: cqg
 * @LastEditTime: 2021-04-26 10:38:40
 * @Description: 租赁管理路由
 */
export default {
	path: "/wy-rent",
	component: () => import("~/layout/index.vue"),
	children: [
		{
			path: "price/list", //价格列表
			meta: { title: "调价记录" },
			component: () => import("~/views/wy-rent/price/list.vue")
		},
		{
			path: "price/add", //定价
			meta: { title: "新增评估价" },
			component: () => import("~/views/wy-rent/price/add.vue")
		},
		{
			path: "price/import", //定价
			meta: { title: "批量导入" },
			component: () => import("~/views/wy-rent/price/import.vue")
		},
		{
			path: "price/graphic-list", //图形显示
			meta: { title: "价格评估" },
			component: () => import("~/views/wy-rent/price/graphic-list.vue")
		},
		{
			path: "contract", //合同列表
			meta: { title: "合同管理" },
			component: () => import("~/views/wy-rent/contract/list.vue")
		},
		{
			path: "printNotification", //合同列表
			meta: { title: "入住通知打印" },
			component: () =>
				import("~/views/wy-rent/printNotification/printNotification.vue")
		},

		{
			path: "contract/list-history", //合同历史管理
			meta: { title: "合同历史管理" },
			component: () => import("~/views/wy-rent/contract/list-history.vue")
		},
		{
			path: "contract/edit", //合同修改
			meta: { title: "合同编辑" },
			component: () => import("~/views/wy-rent/contract/edit/edit.vue")
		},
		{
			path: "contract/view", //合同修改
			meta: { title: "合同查看" },
			component: () => import("~/views/wy-rent/contract/view.vue")
		},

		{
			path: "contract-acceptance", //
			meta: { title: "合同接收列表" },
			component: () =>
				import("~/views/wy-rent/contract-acceptance/list/index.vue")
		},
		{
			path: "contract/ocr", //ocr录入
			meta: { title: "OCR录入" },
			component: () => import("~/views/wy-rent/contract/edit/ocr.vue")
		}
	]
};
