import { getPriceInfo } from "../api/rent-price";

export default {
	name: "price-info",
	data() {
		return {
			rentPricebatchId: "",
			rentPiceId: "",
			priceBatch: "",
			batch: "",
			assessDate: null,
			validDate: null,
			assessOrg: "",
			priceInfo: [],
			fileList: [],
			dialogVisible: false,
			tableData: []
		};
	},
	mounted: function() {
		//this.getInfo(this.rentPiceId);
	},
	methods: {
		//加载数据
		show(rentPriceId) {
			//保存批次信息

			this.dialogVisible = true;
			//this.$refs.dialogPriceInfo.show=true;
			//alert(JSON.stringify(this.$refs));

			getPriceInfo(rentPriceId)
				.then(response => {
					//alert("11"+JSON.stringify(response));
					if (response.data.code == 0) {
						//this.$alert("提交成功");
						//详细数据
						//alert("11"+JSON.stringify(response));
						let batchInfo = response.data.data.batch;
						if (batchInfo) {
							this.rentPricebatchId = batchInfo.rentPricebatchId;
							//alert(this.rentPricebatchId);
							this.batch = batchInfo.batch;
							this.assessDate = batchInfo.assessDate;
							this.validDate = batchInfo.validDate;
							this.assessOrg = batchInfo.assessOrg;
						}
						let priceInfo = response.data.data.price;
						if (priceInfo) {
							this.tableData = [priceInfo];
						}

						this.$refs.myApply.getFiles(this.rentPricebatchId);

						//this.dialogVisible=true;
						//this.$alert(JSON.stringify(this.tableData));
					} else {
						this.$alert(
							"服务端返回错误：" + response.data.code + response.data.message
						);
					}
				})
				.catch(reason => {
					this.$alert("出错了" + JSON.stringify(reason));
				});
		}
	}
};
