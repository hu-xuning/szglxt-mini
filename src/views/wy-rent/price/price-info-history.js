import {getPriceInfoHistory} from '../api/rent-price'

export default {
    name: "price-info-history",

    data() {
        return {
            roomId:"",
            propertyName:"",
            buildingArea: null,
            priceInfo: {},
            dialogVisible:false
        }
    },
    mounted:
        function () {
            //this.getInfo(this.rentPiceId);
        },
    methods: {
        //加载数据
        show(roomId) {
            //保存批次信息

            this.dialogVisible=true;
            this.roomId=roomId;
            //this.$refs.dialogPriceInfo.show=true;
            //alert(JSON.stringify(this.$refs));


            getPriceInfoHistory(roomId).then(
                response=>{
                    //alert("11"+JSON.stringify(response));
                    if(response.data.code==0){
                        //this.$alert("提交成功");
                        //详细数据
                        //alert("11"+JSON.stringify(response));
                        let roomInfo=response.data.data.room
                        if(roomInfo) {
                            this.propertyName=roomInfo.property_name;
                            this.buildingArea = roomInfo.building_area;
                        }
                        this.priceInfo=response.data.data.price;
                    }
                    else {
                        this.$alert("服务端返回错误："+response.data.code+response.data.message);
                    }
                }
            ).catch(reason=>{
                    this.$alert("出错了"+JSON.stringify(reason));

                }
            );
        }

    }
}
