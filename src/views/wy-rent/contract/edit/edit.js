import {
	addObj,
	getObj,
	putObj,
	getGenerateFeeObj,
	saveUrl,
	presend,
	uploadContractFile,
	getProjectIdByRoomId
} from "@/views/wy-rent/api/rent-contract.js";
import { getInspectionResultNum } from "@/views/wy-authorization/use-card/api/use-card.js";
import { fetchList } from "@/views/wy-rent/api/rent-contract-acceptance";
import { getObj as getClientManage } from "@/views/wy-client/api/clientlessee";
import clientSelect from "@/components/comm-select/client-select";
import roomSelect from "@/components/comm-select/room-select";
import templateUpload from "./components/templateUpload";
import priceTotal from "./components/priceTotal";
import { calc } from "@/utils";
import moment from "moment";
import patrolRecordDialog from "../components/patrolRecord.vue";
import { Promise } from "core-js";

export default {
	components: {
		clientSelect,
		roomSelect,
		templateUpload,
		priceTotal,
		patrolRecordDialog
	},
	beforeMount() {
		//判断编辑和新增
		const { id, type, subtype, scene } = this.$route.query;
		if (id) {
			this.formData.rentContractId = id;
			// 如果是变更场景，禁止部分内容的编辑
			scene === "change" && (this.unEdit = true);
			// 获取合同数据
			this.showContractData(id);
		} else {
			if (!type || !subtype) {
				this.$router.back();
				return;
			}
			this.type = type;
			this.subType = subtype;
			this.formData.type = type;
			this.formData.subType = subtype;
		}
	},
	data() {
		return {
			// 禁止编辑（部分内容的，用于局部场景）
			unEdit: false,
			// 合同主类
			type: "",
			// 合同子类
			subType: "",
			// 合同基础信息表单
			formData: {
				customerId: "", //承租方id
				customerName: "", //承租方名称
				rentContractId: "", //合同id
				contractName: "", //合同名称
				contractCode: "", //合同编码
				contractUrl: "", //合同url
				signType: "", //签定方式
				signDate: "", //合同签订日期
				feeTypeValue: 1, //自定义收费时间
				feeDate: "", //应收日
				startDate: "", //合同开始时间
				endDate: "", //合同结束时间
				rentStartDate: "", //起租日期
				freeFeeStart: "",
				freeFeeEnd: "",
				type: "", //合同类型
				subType: "", //合同细类
				buildingArea: "", //建筑总面积
				rentAmount: "", //总月租金
				feeType: "", //计费方式
				approveType: "", //审批方式
				administrativeOffice: "", //管理所
				importDept: "", //产业引进部门
				address: "", //地址
				remarks: "", //备注
				category: 1 //租赁合同标志
			},
			// 承租方类型
			clientLesseeType: "",
			//承租方信息
			propForm: {
				clientLesseeType: "",
				clientLesseeName: ""
			},
			// 基础信息
			baseInfo: {
				// 承租方
				lessee: "",
				// 供应商
				provider: "",
				// 工商证件
				commercialCertificate: "",
				// 个人证件
				personageCertificate: ""
			},
			//物业信息
			propertyInformation: [],
			//租金信息
			rules_rentFeeInformation: {
				rentFeeInformation: [],
				rules: {
					startDate: [
						{ required: true, message: "请选择开始时间", trigger: "blur" },
						{ validator: this.validateStartDate, trigger: "blur" }
					],
					endDate: [
						{ required: true, message: "请选择结束时间", trigger: "blur" },
						{ validator: this.validateEndDate, trigger: "blur" }
					],
					priceFinal: [
						{ required: true, message: "请输入月租金", trigger: "blur" }
					]
				}
			},
			// 免租时间
			freeFees: [],
			//其他费用
			contractFee: [],
			// 上传附件列表
			uploadFileList: [],
			//费用预览-待生成
			receivableFees: [],
			//费用预览-已生成
			receivableFees2: [],
			propertyName: "",
			// 是否是续租(续租需要提前上传附件)
			isRelet: false
		};
	},
	computed: {
		// 步骤列表
		milepost() {
			return [
				{ title: "选择模板", validate: this.handleChooseTemplate },
				{ title: "合同基础信息填写", validate: this.contractValidate },
				{ title: "上传文件", validate: this.uploadValidate },
				{ title: "智能计费", validate: this.handleCompleted }
			];
		},
		// 是否显示自定义收费周期
		isFeeTypeValue() {
			return this.formData.feeType == "6";
		},
		//收费项目
		charge_project_type() {
			return this.getDict("charge_project_type");
		},
		//调价方式
		rentPriceRejustType() {
			return this.getDict("rent_price_rejust_type");
		},
		//证件类型
		rentCertificateType() {
			return this.getDict("rent_certificate_type");
		},
		//签订方式
		rentSigningMethod() {
			return this.getDict("rent_signing_method");
		},
		//收取方式
		rentFeeType() {
			return this.getDict("rent_fee_type");
		},
		//审批方式
		rentContractApproveType() {
			return this.getDict("rent_contract_approve_type");
		},
		//管理所
		administrativeOffice() {
			return this.getDict("administrative_office");
		},
		//承租方类型
		lesseeType() {
			return this.getDict("lessee_type");
		},
		//供应商类型
		providerType() {
			return this.getDict("provider_type");
		},
		//个人证件类型
		personageCertificateType() {
			return this.getDict("personage_certificate_type");
		},
		//工商证件类型
		commercialCertificateType() {
			return this.getDict("commercial_certificate_type");
		},
		// 合同细类名称
		rentContractSubTypeName() {
			if (!this.subType) return "";
			let subTypes = this.getDict("rent_contract_sub_type");
			let subType = subTypes.find(item => item.value == this.subType);
			return subType ? subType.label : "";
		},
		//合同类型名称
		rentContractTypeName() {
			if (!this.type) return "";
			let types = this.getDict("rent_contract_type");
			let type = types.find(item => item.value == this.type);
			return type ? type.label : "";
		},
		//合同表单校验规则
		rules_formData() {
			return {
				customerId: [
					{ required: true, message: "承租方不能为空", trigger: "blur" }
				],
				signType: [
					{ required: true, message: "签订方式不能为空", trigger: "blur" }
				],
				signDate: [
					{ required: true, message: "合同签订日期不能为空", trigger: "blur" }
				],
				startDate: [
					{ required: true, message: "合同开始日期不能为空", trigger: "blur" }
				],
				endDate: [
					{ required: true, message: "合同结束日期不能为空", trigger: "blur" }
				],
				rentStartDate: [
					{ required: true, message: "起租日期不能为空", trigger: "blur" }
				],
				feeType: [
					{ required: true, message: "请选择收取方式", trigger: "blur" }
				],
				rentAmount: [
					{ required: true, message: "请输入月租金", trigger: "blur" }
				],
				feeDate: [
					{
						required: true,

						message: "请输入1到31之间的数字代表每月收租时间",
						trigger: "blur"
					}
				]
			};
		},
		chooseDialogAttrs() {
			return {
				title: "物业信息",
				placeholder: "请输入项目名称或物业名称或楼栋名称或曾用名",
				url: `/api/rentprice/get-byname`,
				width: "1200px",
				searchKey: "propertyName",
				defaultParams: { type: 1, contractId: this.formData.rentContractId },
				showData: [
					{
						prop: "projectName",
						label: "项目名称",
						width: "130"
					},
					{
						prop: "buildingName",
						label: "楼栋名称",
						width: "130"
					},
					{
						prop: "floor",
						label: "楼层",
						width: "100"
					},
					{
						prop: "propertyName",
						label: "物业名称",
						width: "140"
					},

					{
						prop: "buildingArea",
						label: "建筑面积",
						width: "100"
					},
					{
						prop: "manageUnit",
						label: "管理所",
						width: "200",
						formatter: this.dataFormatter
					}
				]
			};
		},
		// 是否在合同基础编辑显示上传组件
		showBaseFileUpload() {
			return this.rules_rentFeeInformation.rentFeeInformation.some(
				item => !item.hasFee && item.ifUploadFile == 0
			);
		},
		// 物业大类
		propertyTypes() {
			return this.getDict("property_types");
		},
		// 物业小类
		useTypesFilter() {
			return this.getDict("property_use_types");
		},
		// 物业细类
		subdivides() {
			return this.getDict("project_subdivide");
		}
	},
	watch: {
		// 物业信息列表监听
		"formData.rentAmount"(val) {
			if (this.rules_rentFeeInformation.rentFeeInformation.length == 1) {
				this.rules_rentFeeInformation.rentFeeInformation = [
					{
						...this.rules_rentFeeInformation.rentFeeInformation[0],
						priceFinal: val
					}
				];
			}
		},
		// 月租金监听
		freeFees() {
			if (this.freeFees.length > 1) {
				const [freeFeeStart, freeFeeEnd] = this.freeFees;
				this.formData.freeFeeStart = this.getDate(freeFeeStart);
				this.formData.freeFeeEnd = this.getDate(freeFeeEnd);
				//起租日期等免租结束加1
				if (
					this.formData.freeFeeEnd != null &&
					this.formData.freeFeeEnd != ""
				) {
					let tempDate = Date.parse(this.formData.freeFeeEnd);
					tempDate = tempDate + 1000 * 60 * 60 * 24;
					let tempDate2 = new Date(tempDate);
					this.formData.rentStartDate = this.getDate(tempDate2);
				}
			}
		}
	},
	methods: {
		// 一、选择模板
		async handleChooseTemplate() {
			let { fileList, nameForm } = this.$refs.templateUpload;
			//1、判断有无文件上传
			if (fileList.length) {
				// 合同附件上传
				let formData = new FormData();
				formData.append("file", fileList[0].raw);
				formData.append("module", "rentContract");
				formData.append("busId", this.$route.query.id || "");
				const res = await uploadContractFile(formData);
				if (res.data.code == 0) {
					this.formData.contractUrl = res.data.data.url;
					this.formData.contractName = nameForm.contractName;
				}
			}
			// 2、判断当前是新增还是编辑
			// 编辑
			if (this.$route.query.id) {
				const { rentContractId, contractUrl, type, subType } = this.formData;
				const { contractName } = nameForm;
				let response = await saveUrl({
					rentContractId,
					contractName,
					contractUrl,
					type,
					subType
				});
				if (response.data.code == 0) {
					this.formData.contractName = nameForm.contractName;
					this.$alert("请阅读合同信息，根据实际情况编辑合同内容");
					this.$refs.steps.next();
				}
			} else {
				// 新增
				let requestData = {
					contractName: nameForm.contractName,
					contractUrl: this.formData.contractUrl,
					type: this.type,
					subType: this.subType,
					category: 1
				};
				const response = await addObj(requestData);
				if (response.data.code == 0) {
					this.$alert("合同初始信息创建成功，请补充合同详细信息");
					this.formData.contractName = nameForm.contractName;
					this.formData.rentContractId = response.data.data;
					this.showContractData(response.data.data, true);
					this.$refs.steps.next();
				}
			}
			return false;
		},
		// 二、合同基础信息填写
		async contractValidate() {
			const validateArr = [
				new Promise((resolve, reject) => {
					!this.formData.customerId || !this.formData.customerName
						? reject("请选择承租方")
						: resolve();
				}),
				new Promise((resolve, reject) => {
					let result =
						!this.propertyInformation.length ||
						!this.rules_rentFeeInformation.rentFeeInformation.length;
					result ? reject("物业信息和租金标准都至少填写一条数据") : resolve();
				}),
				new Promise((resolve, reject) => {
					if (this.propertyInformation.some(item => item.projectType == "")) {
						reject("物业信息物业大类不能为空");
					} else if (
						this.propertyInformation.some(
							item => item.projectSubclassType == ""
						)
					) {
						reject("物业信息物业小类不能为空");
					}
					resolve();
				}),
				new Promise((resolve, reject) => {
					this.$refs.formData.validate(res =>
						res ? resolve() : reject("合同信息验证不通过，请按要求填写")
					);
				}),
				new Promise(async (resolve, reject) => {
					this.$refs.rules_rentFeeInformation.validate(res =>
						res ? resolve() : reject("租金标准验证不通过，请按要求填写")
					);
				}),
				new Promise((resolve, reject) => {
					this.formData.startDate > this.formData.endDate
						? reject("合同结束日期不能小于开始日期")
						: resolve();
				}),
				new Promise((resolve, reject) => {
					let res = this.rules_rentFeeInformation.rentFeeInformation.some(
						i => i.startDate > i.endDate
					);
					res ? reject("租金标准中区间的结束日期不能小于开始日期") : resolve();
				}),
				// new Promise((resolve, reject) => {
				// 	let res =
				// 		this.isRelet &&
				// 		this.rules_rentFeeInformation.rentFeeInformation.some(
				// 			item => !item.hasFee && !item.ifUploadFile
				// 		) &&
				// 		!this.uploadFileList.length;

				// 	if (res) {
				// 		reject("附件不能为空");
				// 	}
				// 	resolve();
				// }),
				new Promise((resolve, reject) => {
					let result =
						isNaN(this.formData.feeDate) ||
						parseInt(this.formData.feeDate) > 31 ||
						parseInt(this.formData.feeDate) < 1;
					result ? reject("应收日必须是1-31之间的数字") : resolve();
				}),

				new Promise((resolve, reject) => {
					let result =
						!this.contractFee.length ||
						this.contractFee.some(e => e.feeitemId !== "2");
					result
						? reject("其他款项下的“租赁押金”费用项目至少要有一条数据")
						: resolve();
				}),

				new Promise((resolve, reject) => {
					let result =
						!this.contractFee.length ||
						this.contractFee.some(e => e.feeitemId !== "2");
					result
						? reject("其他款项下的“租赁押金”费用项目至少要有一条数据")
						: resolve();
				}),
				new Promise((resolve, reject) => {
					let result = this.contractFee.some(
						e => e.feeitemId == "2" && [null, ""].includes(e.amount)
					);
					result ? reject("其他款项下的租赁押金的金额不能为空") : resolve();
				})
			];
			Promise.all(validateArr)

				.then(async res => {
					this.$confirm(
						"按照财务要求，合同变更，如租金价格调整，需要上传领导批示或者会议纪要等附件，是否有附件需要上传？",
						"提示",
						{
							confirmButtonText: "确定",
							cancelButtonText: "取消",
							type: "warning"
						}
					)
						.then(() => {
							return false;
						})
						.catch(async () => {
							// 如果有附件的话，先上传附件
							if (this.uploadFileList.length) {
								this.$refs.reletContractFileUpload.submit();
							} else {
								let requestData = {
									...this.formData,
									rentContractDetails: this.rules_rentFeeInformation
										.rentFeeInformation,
									rentContractProfee: this.contractFee,
									rentContractRooms: this.propertyInformation,
									category: 1
								};
								try {
									const putResult = await putObj(requestData);
									if (putResult.data.code == 0) {
										this.$refs.steps.next();
									}
								} catch (error) {
									this.$message.error("提交失败");
								}
							}
						});
					return false;
				})
				.catch(err => {
					err && this.$message.error(err);
				});
		},
		// 三、上传文件
		uploadValidate() {
			if (this.uploadFileList.length) {
				this.$refs.rentContractFileUpload.submit();
			} else {
				this.preViewFee();
				this.$refs.steps.next();
			}
		},
		// 四、上传完成
		async handleCompleted() {
			const response = await presend(this.formData.rentContractId);
			if (response.data.code == 0) {
				this.$router.push("/wy-rent/contract");
			}
			return false;
		},
		// 合同基础信息新增附件
		async reletUploadCompleted() {
			let newRentFeeInformation = this.rules_rentFeeInformation.rentFeeInformation.map(
				item => {
					item.ifUploadFile = item.hasFee ? item.ifUploadFile : 1;
					return item;
				}
			);
			try {
				const putResult = await putObj({
					...this.formData,
					rentContractDetails: newRentFeeInformation,
					rentContractProfee: this.contractFee,
					rentContractRooms: this.propertyInformation,
					category: 1
				});
				if (putResult.data.code == 0) {
					this.$refs.steps.next();
				}
			} catch (error) {
				this.$message.error("提交失败");
			}
		},
		// 获取合同数据
		async showContractData(id, isAdd = false) {
			// 获取合同数据
			const res = await getObj(id);
			if (res.data.code == 0) {
				const { data } = res.data;
				this.formData = { ...this.formData, ...data };

				// // 租金信息
				// this.rules_rentFeeInformation.rentFeeInformation = this.unEdit
				// 	? data.rentContractDetails.map(item => ({
				// 			...item,
				// 			disabled: true
				// 	  }))
				// 	: data.rentContractDetails;
				this.rules_rentFeeInformation.rentFeeInformation =
					data.rentContractDetails;
				// 判断是否变更
				this.isRelet =
					this.rules_rentFeeInformation.rentFeeInformation.length &&
					this.rules_rentFeeInformation.rentFeeInformation.some(
						item => item.hasFee
					);

				!this.rules_rentFeeInformation.rentFeeInformation.length &&
					this.addFeeInformation(this.isRelet);
				// 物业信息
				this.propertyInformation = data.rentContractRooms;
				// 其他费用{}
				this.contractFee = data.rentContractProfees;
				// 初始化合同模板组件
				this.$route.query.id &&
					this.$refs.templateUpload.init({
						contractName: data.contractName,
						pdfUrl: window.location.origin + data.contractUrl,
						previewType: data.contractUrl.split(".").pop()
					});

				// 编辑场景设置表单的合同类型
				if (!this.type) {
					this.type = data.type;
				}
				if (!this.subType) {
					this.subType = data.subType;
				}
			}

			//免租期
			this.freeFees = [];
			if (this.formData.freeFeeStart) {
				this.freeFees[0] = this.formData.freeFeeStart;
			}
			if (this.formData.freeFeeEnd) {
				this.freeFees[1] = this.formData.freeFeeEnd;
			}

			//获得承租方信息
			if (this.formData.customerId != null) {
				const propForm = await getClientManage(this.formData.customerId);
				this.propForm = propForm.data.data;
				this.clientLesseeType = propForm.data.data.clientLesseeType;
			}
			this.showlessee();
		},
		// 获取费用预览数据
		async preViewFee(id) {
			//待生成费用
			const res = await getGenerateFeeObj(this.formData.rentContractId);
			if (res.data.code == 0) {
				this.receivableFees = res.data.data;
			}

			await fetchList({
				rentContractId: this.formData.rentContractId,
				category: 1,
				size: 200
			}).then(response => {
				if (response.data.code == 0) {
					//if(response.data.data.length>0) {
					this.receivableFees2 = response.data.data.records;
				}
			});
			//已生成费用
		},
		//展示证件类型
		showlessee() {
			const { clientLesseeType, providerType, certificateType } = this.propForm;
			let lessee = this.lesseeType.find(item => item.value == clientLesseeType);
			let provider = this.providerType.find(item => item.value == providerType);
			let personageCertificate = this.personageCertificateType.find(
				item => item.value == certificateType
			);
			let commercialCertificate = this.commercialCertificateType.find(
				item => item.value == certificateType
			);
			this.baseInfo = {
				lessee: lessee ? lessee.label : "",
				provider: provider ? provider.label : "",
				personageCertificate: personageCertificate
					? personageCertificate.label
					: "",
				commercialCertificate: commercialCertificate
					? commercialCertificate.label
					: ""
			};
		},
		// 添加租金标准区间
		addFeeInformation() {
			this.rules_rentFeeInformation.rentFeeInformation.push({
				startDate: "",
				endDate: "",
				priceFinal: this.rules_rentFeeInformation.rentFeeInformation.length
					? this.rules_rentFeeInformation.rentFeeInformation[0].priceFinal
					: "",
				remarks: "",
				hasFee: !this.isRelet,
				ifUploadFile: 0
			});
			if (this.rules_rentFeeInformation.rentFeeInformation.length > 1) {
				this.$message({
					duration: 6000,
					message: "新增租金标准区间操作建议填写该条租金标准区间说明",
					type: "warning"
				});
			}
		},
		// 添加其他费用
		selectFeeItem() {
			this.contractFee.push({
				feeitemId: "",
				chargeProjectName: "",
				feeStadardCode: "",
				feeStadard: "",
				amount: ""
			});
		},
		// 监听承租方选择
		async handleClientChoose(selectedData) {
			this.propForm = selectedData[0];
			this.formData.customerId = this.propForm.clientLesseeId;
			this.formData.customerName = this.propForm.clientLesseeName;
			//产业引进部门
			this.formData.importDept = this.propForm.introduceUnit;
			//这里要重新查一下，因为列表返回的字段不全
			if (this.formData.customerId != null) {
				const propForm = await getClientManage(this.formData.customerId);
				this.propForm = propForm.data.data;
				this.clientLesseeType = propForm.data.data.clientLesseeType;
			}
			let rps = await getInspectionResultNum(this.propForm.clientLesseeName);
			if (rps.data.data.code !== 0) {
				this.$confirm(
					"该单位（个人）存在物业巡查不达标的记录，是否查看详情？",
					"提示",
					{
						confirmButtonText: "是",
						cancelButtonText: "否",
						type: "warning"
					}
				).then(async () => {
					this.$refs.patrolRecordDialog.show({
						useUnit: this.propForm.clientLesseeName
					});
				});
			}
			this.showlessee();
		},
		// 监听物业选择
		handlePropertyChoose(selectedData) {
			this.formData.buildingArea = 0;
			let dataList = selectedData.map(item => {
				const {
					roomId,
					buildingArea,
					propertyName,
					price,
					manageUnit,
					projectType,
					projectSubclassType,
					projectSubdivide
				} = item;
				return {
					roomId,
					buildingArea,
					propertyName,
					assessPrice: price,
					projectType,
					projectSubclassType,
					projectSubdivide,
					price: "",
					remarks: "",
					manageUnit
				};
			});
			let propertyInformation = [...this.propertyInformation, ...dataList];
			this.propertyInformation = propertyInformation;

			this.dualAdministrativeOffice();
		},
		dualAdministrativeOffice() {
			if (
				this.propertyInformation != null &&
				this.propertyInformation.length > 0
			) {
				this.formData.buildingArea = this.propertyInformation.reduce(
					(total, item) => total + item.buildingArea,
					0
				);

				let roomId = this.propertyInformation[0].roomId;
				getProjectIdByRoomId(roomId).then(response => {
					let pid = "";
					if (response.data.code == 0) {
						this.formData.administrativeOffice = response.data.data.manageUnit;
					}
				});
			}
		},

		// 第三步附件上传完成回调
		uloadCompleted(response) {
			this.$message.success("附件提交成功");
			this.preViewFee();
			this.$refs.steps.next();
		},
		// 列表删除监听
		deleteList(index, type, subType = "") {
			if (subType) {
				this[type][subType].splice(index, 1);
			} else {
				this[type].splice(index, 1);
			}

			//删除物业时，重新选管理时所
			this.dualAdministrativeOffice();
		},
		//将date类型转换为yyyy-MM-dd
		getDate(strDate) {
			var date = new Date(strDate);
			var returnValue = date.getFullYear();
			if (date.getMonth() + 1 < 10) {
				returnValue += "-0" + (date.getMonth() + 1);
			} else {
				returnValue += "-" + (date.getMonth() + 1);
			}
			if (date.getDate() < 10) {
				returnValue += "-0" + date.getDate();
			} else {
				returnValue += "-" + date.getDate();
			}
			return returnValue;
		},
		// 租金标准时间校验-开始时间
		validateStartDate(rule, value, callback) {
			let index = rule.field.split(".")[1];
			let dateList = this.rules_rentFeeInformation.rentFeeInformation;
			// 判断结束时间是否存在并且当前时间是否大于结束时间
			if (
				dateList[index].endDate &&
				moment(value).isAfter(dateList[index].endDate)
			) {
				return callback(new Error("开始时间不能大于结束时间"));
			}
			// 判断数组的长度是否大于1，判断时间是否和重叠
			if (
				dateList.length > 1 &&
				dateList.some(
					(item, current) =>
						current != index &&
						moment(value).isBetween(item.startDate, item.endDate, null, "[]")
				)
			) {
				return callback(new Error("时间区间不能重叠"));
			}
			return callback();
		},
		// 租金标准时间校验-结束时间
		validateEndDate(rule, value, callback) {
			let index = rule.field.split(".")[1];
			let dateList = this.rules_rentFeeInformation.rentFeeInformation;
			if (
				dateList[index].startDate &&
				moment(value).isBefore(dateList[index].startDate)
			) {
				return callback(new Error("结束时间不能小于开始时间"));
			}
			if (
				dateList.length > 1 &&
				dateList.some(
					(item, current) =>
						current != index &&
						moment(value).isBetween(item.startDate, item.endDate, null, "[]")
				)
			) {
				return callback(new Error("时间区间不能重叠"));
			}
			return callback();
		},
		// 物业改变时，自动计算月租金
		changeRentAmount() {
			let rentAmount = "";
			let rentAmountMax = 0;
			if (this.propertyInformation.length) {
				rentAmount = this.propertyInformation.reduce((total, item) => {
					let itemRentAmount = calc.Mul(
						item.price || 0,
						item.buildingArea || 0
					);
					return calc.Add(total, itemRentAmount, 2);
				}, 0);
				rentAmountMax = this.propertyInformation.reduce((total, item) => {
					return item.price
						? calc.Add(total, item.buildingArea * 10000, 2)
						: total;
				}, 0);
			}
			this.formData.rentAmount =
				rentAmount > rentAmountMax ? rentAmountMax : rentAmount;
			console.log(this.formData.rentAmount);
		},
		//合同起始日期、结束日期、月租金， 监听联动租金标准事件
		changeRentStandard(value, code) {
			if (code == "rentStartDate") {
				this.formData.startDate = this.formData.rentStartDate;
			}
			let codeObj = {
				rentStartDate: "startDate",
				endDate: "endDate",
				rentAmount: "priceFinal"
			};
			if (this.rules_rentFeeInformation.rentFeeInformation.length == 1) {
				this.$set(
					this.rules_rentFeeInformation.rentFeeInformation[0],
					[codeObj[code]],
					value
				);
			}
		},
		// 数据格式化
		dataFormatter(row, column, callValue) {
			let obj = this.getDict("administrative_office").find(
				p => p.value == callValue
			);
			return obj ? obj.label : "";
		},
		// 物业表格类型改变监听
		changePropertyType(type, value, index) {
			if (type === 0) {
				this.$set(this.propertyInformation, index, {
					...this.propertyInformation[index],
					projectSubclassType: "",
					projectSubdivide: ""
				});
			}
			if (type === 1) {
				this.$set(this.propertyInformation, index, {
					...this.propertyInformation[index],
					projectSubdivide: ""
				});
			}
		}
	}
};
