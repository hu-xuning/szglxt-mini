import {
	fetchList,
	delObj,
	recordHis,
	renew,
	excute,
	send
} from "../api/rent-contract.js";
import CacheParams from "@/assets/vue-mixin/CacheParams";
import typeSelect from "./type-select.vue";
import contractBack from "./back.vue";
import contractView from "./view.vue";
import { getInspectionResultNum } from "@/views/wy-authorization/use-card/api/use-card.js";
import patrolRecordDialog from "./components/patrolRecord";
import contractFileAdd from "@/components/business/contract-file-add";

export default {
	name: "contract-manage",
	mixins: [CacheParams],
	components: {
		typeSelect,
		contractBack,
		contractView,
		patrolRecordDialog,
		contractFileAdd
	},
	provide() {
		return {
			initList: this.getListData
		};
	},
	mounted() {
		const { warningType } = this.$route.query;
		if (warningType) {
			this.params.warningType = warningType;
			this.$refs.dataList.setFilter(new Map([["warningType", warningType]]));
		}
		this.getListData();
	},
	data() {
		return {
			tableData: [],
			// 请求参数
			params: {
				current: 1,
				size: 10,
				category: 1,
				propertyNames: ""
			},
			// 分页
			pager: {
				total: 0, // 总页数
				current: 1, // 当前页数
				size: 10 // 每页显示多少条
			},
			// 页面loading
			loading: false,
			filterList: [
				// { label: "楼栋曾用名称", inputType: "input", name: "usedName" },
				{
					label: "合同签订日期",
					inputType: "daterange",
					name: ["signDateStart", "signDateEnd"]
				},
				{
					label: "合同状态",
					inputType: "dict",
					dictName: "rent_contract_status",
					name: "status",
					defaultValue: ["03"],
					multiple: true
				},
				{
					label: "租赁周期",
					inputType: "daterange",
					name: ["startDate", "endDate"]
				},
				{
					label: "管理所",
					inputType: "dict",
					dictName: "administrative_office",
					name: "administrativeOffice",
					multiple: true
				},
				{
					label: "合同终止日期",
					inputType: "daterange",
					name: ["backDateStart", "backDateEnd"]
				}
			],
			NewIcon: require("@/assets/images/new.png")
		};
	},
	computed: {
		// 查看权限
		canLook() {
			return this.getPermissions("rentcontract_get");
		},
		// 编辑权限
		canEdit() {
			return this.getPermissions("rentcontract_edit");
		},
		// 菜单列表
		menuList() {
			let list = [];
			if (this.canEdit) {
				list = [{ add: "合同录入" }, { ocr: "OCR录入" }];
			}
			this.canLook && list.push("export");
			return list;
		}
	},
	methods: {
		getListData(params = {}) {
			this.loading = true;
			let requestData = { ...this.params, ...params };
			fetchList(requestData)
				.then(res => {
					const { current, size, total, records } = res.data.data;
					this.tableData = records;
					this.params = requestData;
					this.pager = { total, current, size };
				})
				.finally(() => {
					this.loading = false;
				});
		},
		// 操作监听
		handleBtnClick(type, row) {
			switch (type) {
				case "view":
					this.$refs.contractView.show(row.rentContractId);
					break;
				case "edit":
					this.$router.push({
						path: "/wy-rent/contract/edit",
						query: { id: row.rentContractId, scene: type }
					});
					break;
				case "delete":
					this.confirm("删除确认", "您确定要删除该数据吗？", async () => {
						const resp = await delObj(row.rentContractId);
						if (resp.data.code == 0) {
							this.suc("删除成功");
							this.getListData();
						} else {
							this.err("删除失败" + resp.data.code + resp.data.message);
						}
					});
					break;
				case "change":
					this.change(row, type);
					break;
				case "renew":
					this.renew(row, type);
					break;
				case "back":
					this.$refs.contractBack.show(row.rentContractId);
					break;
				// 发送
				case "send":
					this.$confirm("确认发送吗？").then(async () => {
						const response = await send(row.rentContractId);
						if (response.data.code == 0) {
							this.$alert("合同发送成功");
							this.getListData();
						} else {
							this.$alert("合同发送失败" + response.data.msg);
						}
					});
					break;
				//执行，从审核通过变成，执行中
				case "excute":
					this.$confirm(
						"执行操作后，合同状态为执行中，关联的物业状态变为出租，确认执行吗？"
					).then(async () => {
						const response = await excute(row.rentContractId);
						if (response.data.code == 0) {
							this.$alert("合同执行成功");
							this.getListData();
						} else {
							this.$alert("合同执行失败" + response.data.msg);
						}
					});
					break;
				// 追加附件
				case "additional":
					this.$refs.contractFileAdd.show(row.rentContractId, "rent-contract");
					break;
				default:
					break;
			}
		},
		//变更
		change(row, type) {
			this.$confirm(
				"发起变更后，原合同将备份为历史合同，新合同将提交财务部重新核算并生成费用，确认要变更吗?"
			).then(async () => {
				const resp = await recordHis(row.rentContractId);
				if (resp.data.code == 0) {
					this.$router.push({
						path: "/wy-rent/contract/edit",
						query: { id: row.rentContractId, scene: type }
					});
				} else {
					this.err("发起变更失败" + resp.data.code + resp.data.message);
				}
			});
		},
		// 续签
		renew(row, type) {
			this.$confirm(
				"发起续签后，原合同将终止，新合同将提交财务部重新核算并生成费用，确认要续签吗"
			).then(async () => {
				let rps = await getInspectionResultNum(row.customerName);
				if (rps.data.data.code !== 0) {
					return this.getDisqualificationList(row);
				}
				renew(row.rentContractId).then(resp => {
					if (resp.data.code == 0) {
						let id = resp.data.data.rentContractId;
						this.$router.push({
							path: "/wy-rent/contract/edit",
							query: { id, scene: type }
						});
					} else {
						this.err("发起续签失败" + resp.data.code + resp.data.message);
					}
				});
			});
		},
		//查验是否有巡查不合格记录
		getDisqualificationList(row) {
			this.$confirm("该单位存在物业巡查不达标的记录，是否查看详情？", "提示", {
				confirmButtonText: "是",
				cancelButtonText: "否",
				type: "warning"
			})
				.then(() => {
					this.$refs.patrolRecordDialog.show({
						useUnit: row.customerName
					});
				})
				.catch(() => {
					renew(row.rentContractId).then(resp => {
						if (resp.data.code == 0) {
							let id = resp.data.data.rentContractId;
							this.$router.push({
								path: "/wy-rent/contract/edit",
								query: { id, scene: type }
							});
						} else {
							this.err("发起续签失败" + resp.data.code + resp.data.message);
						}
					});
				});
		},
		// 菜单点击监听
		handleMenuClick(type) {
			switch (type) {
				case "ocr":
				case "add":
					this.$refs.typeSelect1.show(type);
					break;
				case "export":
					this.publicExport(
						"合同列表",
						"/api/rentcontract/rentcontractList-download",
						this.params
					);
					break;
				default:
					this.$message(type);
					break;
			}
		},
		resetList () {
			this.editDataShow = false
			this.getListData()
		},
	}
};
