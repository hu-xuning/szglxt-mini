import { back, getObj } from "../api/rent-contract.js";
import { mapState } from "vuex";

export default {
	name: "contract-back",
	data() {
		return {
			dialogVisible: false,
			//合同终止类型，下拉值
			//contractEndType:[],
			rentContractBackId: "",

			contract: {
				rentContractId: "",
				contractCode: "",
				customerName: "",
				propertyNames: "",
				signDate: null
			},
			formData: {
				//rentContractBackId:null,
				endType: null,
				backDate: null,
				// backPerson: null,
				// backCertificateNum: "",
				remarks: "",
				feeComplete: 0
			},

			rules: {
				endType: [
					{
						required: true,
						message: "请选择终止类型",
						trigger: "blur"
					}
				],
				backDate: [
					{
						required: true,
						message: "请选择退租日期",
						trigger: "blur"
					}
				],
				// backPerson: [
				// 	{ required: true, message: "请输入退租人", trigger: "blur" }
				// ],
				// backCertificateNum: [
				// 	{
				// 		required: true,
				// 		message: "请输入退租人证件号",
				// 		trigger: "blur"
				// 	},
				// 	{
				// 		min: 8,
				// 		max: 18,
				// 		message: "证件长度是 8 到 18 个字符"
				// 	}
				// ],
				// feeComplete: [
				// 	{
				// 		required: true,
				// 		message: "请确认已结清所有费用",
				// 		trigger: "blur"
				// 	}
				// ]
			},

			fileList: []
		};
	},

	computed: mapState({
		//调价方式
		contractEndType: state => {
			let data =
				state.dict.find(item => item.type === "contract_end_type") || {};
			return data.children || [];
		}
	}),
	methods: {
		//加载数据
		show(rentContractId) {
			this.dialogVisible = true;
			//alert(rentContractId);

			getObj(rentContractId).then(response => {
				//console.log(response)
				if (response.data.code == 0) {
					this.contract = response.data.data;
				} else {
					this.$alert(
						"读取合同服务端返回错误：" +
							response.data.code +
							response.data.message
					);
				}
			});
		},

		//退租
		back() {
			this.$refs["form"].validate(valid => {
				if (valid) {
					//先上传附件
					if (this.$refs.myUpload.fileList.length > 0) {
						this.$refs.myUpload.submit("contract-back", null);
					} else {
						this.back2();
					}
				}
			});
		},

		onSuccess(data) {
			if (data.code == 0) {
				let id = data.data;
				this.rentContractBackId = id;
				this.back2();
			} else {
				this.err(
					"上传附件失败," + response.data.code + "," + response.data.msg
				);
			}
		},
		onError(err) {
			this.err("上传附件出错" + err);
			this.$alert(err);
		},

		back2() {
			this.$confirm("确认退租吗?").then(() => {
				// this.formData.feeComplete = this.formData.feeComplete ? 1 : 0;

				back({
					...this.formData,
					rentContractId: this.contract.rentContractId,
					rentContractBackId: this.rentContractBackId
				}).then(resp => {
					if (resp.data.code == 0) {
						this.$alert("退租成功");
						this.dialogVisible = false;
						//this.$parent.getListData();
						this.$emit('resetList')
					} else {
						this.err("退租失败" + resp.data.code + resp.data.message);
					}

				});
			});
		}
	}
};
