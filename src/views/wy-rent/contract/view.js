import { getObj, getContractApproveList } from "../api/rent-contract.js";
// hxf-0731-引入vuex
import approvalRecord from "@/components/approval-record";
import contractChange from "./components/contractChange.vue";
import contractDetails from "./components/contractDetails.vue";
import contractRenew from "./components/contractRenew.vue";
import contractBack from "./components/contractBack.vue";
import contractPreview from "@/components/contractPreview";
import contractInformation from "./components/contractInformation";
import propertyPreview from "@/views/wy-info/property-manage/list/components/preview";

export default {
	name: "contract-view",
	components: {
		contractDetails,
		approvalRecord,
		contractChange,
		contractRenew,
		contractBack,
		contractPreview,
		propertyPreview,
		contractInformation
	},
	data() {
		return {
			viewDialogVisible: false,
			// 审批记录
			recordingLlist: [],
			// tab
			activeName: "first",
			formData: {}, //合同总信息
			rentContractId: "", //合同id
			// 合同名称
			contractName: "",
			// 合同预览地址
			pdfUrl: "",
			//字典项匹配
			lessee: "",
			provider: "",
			commercialCertificate: "",
			personageCertificate: "",
			subType: "", //合同细类
			type: "", //合同类型
			signType: "", //签订方式
			feeType: "", //计租方式
			approveType: "", //审批方式
			administrativeOffice: "" //管理所
		};
	},
	computed: {
		//调价方式
		rent_price_rejust_type() {
			return this.getDict("rent_price_rejust_type");
		},
		//证件类型
		rent_certificate_type() {
			return this.getDict("rent_certificate_type");
		},
		//签订方式
		rent_signing_method() {
			return this.getDict("rent_signing_method");
		},
		//收费方式
		rent_fee_type() {
			return this.getDict("rent_fee_type");
		},
		//审批方式
		rent_contract_approve_type() {
			return this.getDict("rent_contract_approve_type");
		},
		//管理所
		administrative_office() {
			return this.getDict("administrative_office");
		},
		lessee_type() {
			return this.getDict("lessee_type");
		},
		provider_type() {
			return this.getDict("provider_type");
		},
		personage_certificate_type() {
			return this.getDict("personage_certificate_type");
		},
		commercial_certificate_type() {
			return this.getDict("commercial_certificate_type");
		},
		rent_contract_sub_type() {
			return this.getDict("rent_contract_sub_type");
		},
		rent_contract_type() {
			return this.getDict("rent_contract_type");
		}
	},
	methods: {
		//获取审批列表
		async getContractApproveList(rentContractId) {
			let category = 1;
			const res = await getContractApproveList(rentContractId, category);
			if (res.data.code == 0) this.recordingLlist = res.data.data;
			else this.$alert("加载变更记录服务端服务错误" + res.code);
		},
		//查询表单数据
		async show(contractId) {
			//改为了弹出框模式
			this.viewDialogVisible = true;
			this.rentContractId = contractId;
			const response = await getObj(this.rentContractId, this.category);
			let formData = response.data.data;
			this.formData = formData;

			//合同信息
			if (this.$refs.contractInformation) {
				this.$refs.contractInformation.loadContractList(contractId);
			}
			//收费明细
			if (this.$refs.contractDetails && this.$refs.contractDetails.preViewFee) {
				this.$refs.contractDetails.preViewFee(contractId, this.formData);
			}
			if (this.$refs.contractChange && this.$refs.contractChange.loadHisList) {
				this.$refs.contractChange.loadHisList(contractId);
			}
			if (this.$refs.contractRenew && this.$refs.contractRenew.loadRenewList) {
				this.$refs.contractRenew.loadRenewList(contractId);
			}
			if (this.$refs.contractBack && this.$refs.contractBack.loadBackInfo) {
				this.$refs.contractBack.loadBackInfo(contractId);
			}
			this.getContractApproveList(contractId);
		},
		// 获取字典的label名称
		getDictLabel(dictName, val) {
			if (!dictName || !val) return "";
			let item = this[dictName].find(i => i.value == val);
			return item?.label || "";
		},
		close() {
			this.viewDialogVisible = false;
			// 审批记录
			this.recordingLlist = [];
			this.activeName = "first";
			this.formData = {}; //合同总信息
			this.rentContractId = ""; //合同id
			// 合同名称
			this.contractName = "";
			// 合同预览地址
			this.pdfUrl = "";
			//字典项匹配
			this.lessee = "";
			this.provider = "";
			this.commercialCertificate = "";
			this.personageCertificate = "";
			this.subType = ""; //合同细类
			this.type = ""; //合同类型
			this.signType = ""; //签订方式
			this.feeType = ""; //计租方式
			this.approveType = ""; //审批方式
			this.administrativeOffice = ""; //管理所
		},
		// 数据格式化
		dataFormatter(row, column, cellValue) {
			switch (column.property) {
				case "projectType":
					return this.$options.filters.dict(cellValue, "property_types") || "";
				case "projectSubclassType":
					return (
						this.$options.filters.dict(cellValue, "property_use_types") || ""
					);
				case "projectSubdivide":
					return (
						this.$options.filters.dict(cellValue, "project_subdivide") || ""
					);
				default:
					return "";
			}
		}
	}
};
