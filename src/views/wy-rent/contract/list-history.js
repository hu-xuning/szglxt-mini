// import pager from '@/assets/vue-mixin/pager'
import { fetchListHistory } from "../api/rent-contract.js";
import contractView from "./view.vue";

export default {
	name: "contract-history",
	// mixins: [pager],
	components: { contractView },
	provide() {
		return {
			initList: this.getListData
		};
	},
	beforeMount() {
		if (this.$route.query.warningType)
			this.params.warningType = this.$route.query.warningType;
		this.getListData();
	},
	data() {
		return {
			tableData: [],
			// 请求参数
			params: {
				current: 1,
				size: 10,
				category: 1,
				propertyNames: ""
			},
			// 选中的数据
			multipleSelection: [],
			// 分页
			pager: {
				total: 0, // 总页数
				current: 1, // 当前页数
				size: 10 // 每页显示多少条
			},
			// 页面loading
			loading: false,
			filterList: [
				{
					label: "合同类型",
					inputType: "dict",
					dictName: "rent_contract_type",
					name: "type",
					multiple: true
				},
				{
					label: "合同细类",
					inputType: "dict",
					dictName: "rent_contract_sub_type",
					name: "subType",
					multiple: true
				},

				{
					label: "合同状态",
					inputType: "select",
					name: "status",
					multiple: true,
					children: [
						{
							label: "已终止",
							value: "04"
						},
						{
							label: "已续签",
							value: "06"
						}
					]
				},
				{
					label: "合同签订日期",
					inputType: "daterange",
					name: ["signDateStart", "signDateEnd"]
				},
				{
					label: "合同结束日期",
					inputType: "daterange",
					name: ["endDateStart", "endDateEnd"]
				},
				{
					label: "起租日期",
					inputType: "daterange",
					name: ["rentStartDateStart", "rentStartDateEnd"]
				},
				{
					label: "租赁周期",
					inputType: "daterange",
					name: ["startDate", "endDate"]
				},
				{
					label: "统计类别",
					inputType: "select",
					name: "warningType",
					children: [
						{ label: "即将到期", value: "01" },
						{ label: "待续签", value: "02" },
						{ label: "本月新签", value: "03" },
						{ label: "本月终止", value: "04" },
						{ label: "执行中", value: "05" },
						{ label: "已终止", value: "06" }
					],
					multiple: false
				}
			],

			NewIcon: require("@/assets/images/new.png")
		};
	},

	methods: {
		getListData(params = {}) {
			this.loading = true;
			let requestData = { ...this.params, ...params };
			fetchListHistory(requestData)
				.then(res => {
					const { current, size, total, records } = res.data.data;
					this.tableData = records;
					this.params = requestData;
					this.pager = { total, current, size };
				})
				.finally(() => {
					this.loading = false;
				});
		},
		exportData() {
			this.publicExport(
				"合同列表",
				"/api/rentcontract/rentcontractList-download-history",
				this.params
			);
		},
		view(row) {
			this.$refs.contractView.show(row.rentContractId);
		},
		// 菜单点击监听
		handleMenuClick(type) {
			switch (type) {
				case "export":
					this.exportData();
					break;
				default:
					this.$message(type);
					break;
			}
		}
	}
};
