import request from "@/plugins/axios";

//租赁合同模块
export function fetchList(query) {
	return request({
		url: "/api/rentcontract/page",
		method: "get",
		params: query
	});
}

//租赁合同模块历史
export function fetchListHistory(query) {
	return request({
		url: "/api/rentcontract/page-history",
		method: "get",
		params: query
	});
}

//续签记录，同一个parentId下
export function getRenewList(rentContractId) {
	return request({
		//url: '/api/rentcontract/renewlist?rentContractId='+rentContractId,
		url: "/api/rentcontract/renewlist/" + rentContractId,
		method: "get"
	});
}

export function addObj(obj) {
	//alert("a");
	return request({
		url: "/api/rentcontract",
		method: "post",
		data: obj
	});
}

export function getObj(id) {
	return request({
		url: "/api/rentcontract/" + id,
		method: "get"
	});
}

export function delObj(id) {
	return request({
		url: "/api/rentcontract/" + id,
		method: "delete"
	});
}

export function putObj(obj) {
	//alert(JSON.stringify(obj));
	return request({
		url: "/api/rentcontract",
		method: "put",
		data: obj
	});
}
export function getGenerateFeeObj(id) {
	return request({
		url: "/api/rentcontract/generatefee-all/" + id,
		method: "get"
	});
}

//改为待发送
export function presend(id) {
	return request({
		url: "/api/rentcontract/presend/" + id,
		method: "get"
	});
}

//改为已发送
export function send(id) {
	return request({
		url: "/api/rentcontract/send/" + id,
		method: "get"
	});
}

//文件上传
export function uploadContractFile(obj) {
	return request({
		url: "/admin/sys-file/upload",
		method: "post",
		data: obj
	});
}
//保存PDFUrl
export function saveUrl(obj) {
	return request({
		url: "/api/rentcontract/saveUrlAndContractNameById",
		method: "post",
		data: obj
	});
}
//变更时记录历史
export function recordHis(id) {
	//alert(JSON.stringify(obj));
	return request({
		//url: '/api/rentcontract/record-his?rentContractId='+id,
		url: "/api/rentcontract/record-his/" + id,
		method: "put"
	});
}

//续签
export function renew(id) {
	//alert(JSON.stringify(id));+
	return request({
		//url: '/api/rentcontract/renew?rentContractId='+id,
		url: "/api/rentcontract/renew/" + id,
		method: "put"
	});
}

//退租
export function back(obj) {
	//alert(JSON.stringify(id));
	return request({
		url: "/api/rentcontract/back",
		method: "put",
		data: obj
	});
}

export function getHisList(contractId) {
	//alert(contractId);
	return request({
		//url: '/api/rentcontracthis/list?rentContractId='+contractId,
		url: "/api/rentcontracthis/list/" + contractId,
		method: "get"
	});
}

//退租记录
export function getContractBack(contractId) {
	//alert(contractId);
	return request({
		url: "/api/rentcontractback/getbycontractid/" + contractId,
		method: "get"
	});
}

//审批记录
export function getContractApproveList(contractId) {
	//alert(contractId);
	return request({
		url: "/api/commonapprovalrecord/" + contractId,
		method: "get"
	});
}
//查看历史合同信息
export function getHistoryContract(rentContractHisId) {
	//alert(contractId);
	return request({
		url: "/api/rentcontracthis/" + rentContractHisId,
		method: "get"
	});
}

//执行。从13改成03
export function excute(id) {
	//alert(JSON.stringify(id));
	return request({
		url: "/api/rentcontract/excute/" + id,
		method: "put"
	});
}

//根据roomId获取房间信息
export function getProjectIdByRoomId(roomId) {
	return request({
		url: "/api/collectionreceivable/getProjectIdByRoomId/" + roomId,
		method: "get"
	});
}
//根据roomId获取房间信息(其他物业)
export function getOtherProperty(roomId) {
	return request({
		url: "/api/otherproperty/" + roomId,
		method: "get"
	});
}

//判断合同打印状态
export function getRentContractPageList(rentContractId) {
	return request({
		url: `/api/rentcontract/checkin-notice/${rentContractId}`,
		method: "get"
	});
}

//产业用房合同分页查询
export function assessIndustrial(query) {
	return request({
		url: "/api/rentcontract/assessIndustrial/page",
		method: "get",
		params: query
	});
}
