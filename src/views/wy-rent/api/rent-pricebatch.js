import request from "~/plugins/axios";

//保存定价 包括批次，附件
export function add(query) {
	//alert(JSON.stringify(query));
	return request({
		url: "/api/rentpricebatch",
		method: "post",
		data: query
	});
}

//导入用单独的方法
export function batchImport(query) {
	//alert(JSON.stringify(query));
	return request({
		url: "/api/rentpricebatch/batch-import",
		method: "post",
		data: query,
		config: {
			headers: { "Content-Type": "multipart/form-data" }
		}
	});
}

/**
 * 新增价格评估数据
 * @param {Obj}} query
 */
export function addRentPriceData(data) {
	return request({
		url: "/api/rentpricebatch",
		method: "post",
		data: data
	});
}

// 导出
export function downloadPost(params) {
	return request({
		url: "/api/rentpricebatch/import-check-download2",
		method: "post",
		data: params,
		responseType: "blob"
	});
}
