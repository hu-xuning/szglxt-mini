import request from '@/plugins/axios'

export function fetchList(query) {
    return request({
        url: '/api/rentcontractreceivable/page',
        method: 'get',
        params: query
    })
}

//合同基本信息
export function getRentContractPageList(query) {
    return request({
        url: '/api/rentcontractreceivable/getRentContractPageList',
        method: 'get',
        params: query
    })
}


//核算数据（租金）
export function check(contractId) {
    return request({
        url: '/api/rentcontractreceivable/checkList/'+contractId,
        method: 'get'
    })
}
//核算数据（押金）
export function generateProFee(contractId) {
    return request({
        url: '/api/rentcontractreceivable/generateProFee/'+contractId,
        method: 'get'
    })
}
//核算数据提交
export function checkSubmit(obj) {
    return request({
        url: '/api/rentcontractreceivable/checkSubmit',
        method: 'post',
        data: obj
    })
}
//接收或拒绝
export function addReceive(obj) {
    return request({
        url: '/api/rentcontractreceivable/receive',
        method: 'post',
        data: obj
    })
}

//接收并发送管理所
export function addReceiveToManageUnit(obj){
	return request({
		url: '/api/rentcontractreceivable/receiveToManageUnit',
		method: 'post',
		data: obj
	})
}
//接收或拒绝
export function rentContractList() {
    return request({
        url: '/api/rentcontractreceivable/rentContractList',
        method: 'get'
    })
}

