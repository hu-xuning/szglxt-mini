import request from '~/plugins/axios'

//查询历史定价
export function fetchPriceList(query) {
    //alert(JSON.stringify(query))
    return request({
        url: '/api/rentprice/houserentprice-list',
        method: 'get',
        params: query
    })
}

//查询最新定价
export function fetchPriceListNow(query) {
    //alert(JSON.stringify(query))
    return request({
        url: '/api/rentprice/houserentprice-list-now',
        method: 'get',
        params: query
    })
}

//小区列表
export function fetchProjectList() {
    return request({
        url: '/api/rentprice/project-list',
        method: 'get',
    })
}

//楼栋列表
export function fetchBuildingList(query) {
    return request({
        url: '/api/rentprice/building-list',
        method: 'get',
        params: query
    })
}

export function getPriceInfo(id) {
    //alert(id);
    return request({
        url: '/api/rentprice/'+id,
        method: 'get'
    })
}

//房间的定价历史
export function getPriceInfoHistory(roomId){
    return request({
        //url: '/api/rentprice/get-byroomid?roomId='+roomId,
        url: '/api/rentprice/get-byroomid/'+roomId,
        method: 'get'
    })
}

//搜索物业
export function getRoomPriceByName(query){
    return request({
        url: '/api/rentprice/get-byname',
        params: query,
        method: 'get'
    })
}

//按类型费用项目
export function getFeeItem(typeId){
    return request({
        url: '/api/financemanagecharge/manageChargeList/'+typeId,
        method: 'get'
    })
}

//搜索授权单位
export function searchUnitList(unitType,searchKeyword, currentPage, pageSize) {
    return request({
        url: '/api/applyunit/page?unitTypeList='+unitType+'&keyword=' + searchKeyword + "&current=" + currentPage + "&size=" + pageSize,
        method: 'get',

    })
}

//搜索授权单位
export function getUnitList(params) {
    return request({
        url: '/api/applyunit/page',
        method: 'get',
        params
    })
}





