import pager from "@/assets/vue-mixin/pager";
import vPager from "@/components/list-page/v-pager";
import {
	fetchList,
	getRentContractPageList
} from "@/views/wy-rent/api/rent-contract-acceptance.js";

import editCheck from "../edit/edit-check";
import editReceive from "../edit/edit-receive";
import { downloadBlob } from "@/utils";
import { download } from "@/admin/api/common";
import { console } from "vuedraggable/src/util/helper";
import { mapGetters, mapState } from "vuex";
import contractView from "@/views/wy-rent/contract/view.vue";
import BaseLoad from "@/assets/vue-mixin/BaseLoad";
// import { threadId } from 'worker_threads'
export default {
	mixins: [pager, BaseLoad],
	components: {
		vPager,
		editCheck,
		editReceive,
		contractView
	},

	data() {
		return {
			contract_receive: false,
			contract_finance_submit: false,
			contract_manageunit_submit: false,
			left: 0,
			height: 0,
			leftSearch: "",
			leftSearchForm: {},
			selectList: [],
			tabPosition: "left",
			tableData: [],
			selectionList: [],
			tableLoading: false,
			searchForm: { rentContractId: "xxx" },
			totalSearchData: [
				{
					label: "收款项目",
					value: "chargeProjectName",
					type: "select",
					model: "chargeProjectName",
					children: []
				},
				{
					label: "费用周期",
					value: "costPeriodStart",
					type: "daterange",
					model: "costPeriodStart",
					children: []
				},
				{
					label: "应收日期",
					value: "receivableDate",
					type: "daterange",
					model: "receivableDate",
					children: []
				},
				{
					label: "应收款状态",
					value: "receivableStatus",
					type: "select",
					model: "receivableStatus",
					children: []
				}
			],
			searchDataFrom: {},
			leftPager: {
				tabTotal: 0,
				tabPageSize: 2,
				tabCurrentPage: 1
			},

			rentData: [],
			flag: false,
			btnText: "未接收",
			operText: "接收",
			clickCheck: false,
			activeIndex: -1,
			NewIcon: require("@/assets/images/new.png")
		};
	},

	computed: {
		//...mapGetters(['permissions']),

		...mapState({
			charge_project_type: state => {
				let data =
					state.dict.find(item => item.type === "charge_project_type") || {};
				let list = data.children || [];
				return data.children || [];
			},
			receivable_status: state => {
				let data =
					state.dict.find(item => item.type === "payable_status") || {};
				let list = data.children || [];
				return data.children || [];
			}
		}),
		tabPageParams: function() {
			return {
				current: this.leftPager.tabCurrentPage,
				size: this.leftPager.tabPageSize
			};
		}
	},
	mounted() {
		this.$set(this.totalSearchData[0], "children", this.charge_project_type);
		this.$set(this.totalSearchData[3], "children", this.receivable_status);
		this.searchByKey();
		this.getTabListData();
		alert("11");
	},

	methods: {
		selectChange() {
			this.getTabListData();
		},
		showData(contractId) {
			this.$refs.contractView.show(contractId);
			//this.$parent.$router.push({path: "/wy-rent/contract/view?id=" + contractId});
		},

		//  绑定输入框回车触发该事件以及点击确定后传值
		searchByKey(val) {
			if (this.searchDataFrom.chargeProjectName) {
				let sum = "";
				this.searchDataFrom.chargeProjectNames.map(item => {
					if (sum.length > 0) {
						sum += "," + item.value;
					} else {
						sum = item.value;
					}
				});
				this.searchForm.chargeProjectName = sum;
			}
			if (this.searchDataFrom.receivableStatus) {
				let sum = "";
				this.searchDataFrom.receivableStatuss.map(item => {
					if (sum.length > 0) {
						sum += "," + item.value;
					} else {
						sum = item.value;
					}
				});
				this.searchForm.receivableStatus = sum;
			} else {
				delete this.searchForm.receivableStatus;
			}

			if (this.searchDataFrom.costPeriodStart) {
				this.searchForm.costPeriodStart = this.searchDataFrom.costPeriodStart;
			} else {
				delete this.searchForm.costPeriodStart;
			}
			if (this.searchDataFrom.receivableDate) {
				this.searchForm.receivableDate = this.searchDataFrom.receivableDate;
			} else {
				delete this.searchForm.receivableDate;
			}

			if (val) {
				this.searchForm.projectName = val;
			} else {
				delete this.searchForm.projectName;
			}

			this.resetList();
		},
		downExcel() {
			var time = new Date();
			let name =
				time.getFullYear() +
				"-" +
				(time.getMonth() + 1) +
				"-" +
				time.getDate() +
				" " +
				time.getHours() +
				":" +
				time.getMinutes() +
				":" +
				time.getSeconds() +
				"-" +
				"应收列表.xlsx";

			console.log(this.searchForm.projectName);
			let url = "/api/rentcontractreceivable/download";
			download(url, Object.assign(this.searchForm)).then(response => {
				downloadBlob(response.data, name);
			});
		},
		//  绑定重置按钮
		searchReset() {
			let rentContractId = this.searchForm.rentContractId;
			this.searchForm = {};
			this.searchForm.rentContractId = rentContractId;
			this.resetList();
		},
		getListData() {
			fetchList(Object.assign({}, this.pageParams, this.searchForm)).then(
				resp => {
					this.setListData(resp.data);
				}
			);
		},
		restartSelectionList() {
			//不要带过去，让其自动生成
			this.selectionList[0].createBy = null;
			this.selectionList[0].createName = null;
			this.selectionList[0].createTime = null;
			this.selectionList[0].operateBy = null;
			this.selectionList[0].operateName = null;
			this.selectionList[0].operateTime = null;
		},
		//  分页的方法-将接口中拿到的数据，存到分页中
		setTabListData(resp) {
			let data = resp.data;
			this.leftPager.tabTotal = data.total;
			this.rentData = data.records;
			this.handleTabClick();
		},
		//  分页的方法
		changeTabPageSize(val) {
			this.leftPager.tabCurrentPage = 1;
			this.leftPager.tabPageSize = val;
			this.getTabListData();
		},
		//  分页的方法
		changeTabCurrentPage(val) {
			this.leftPager.tabCurrentPage = val;
			this.getTabListData();
		},
		//  分页的方法-接口中拿到的数据
		getTabListData() {
			//接口，根据实际接口改一下
			getRentContractPageList(
				Object.assign({}, this.tabPageParams, this.leftSearchForm)
			).then(resp => {
				//alert(JSON.stringify(resp))
				this.setTabListData(resp.data);
			});
		},
		handleInputSearch() {
			if (this.leftSearch) {
				this.leftSearchForm.rentContractName = this.leftSearch;
				// console.log('this.leftSearchForm---',this.$refs.notCheck, this.activeIndex)
				if (this.activeIndex !== -1) {
					this.$refs.notCheck[this.activeIndex].innerHTML = "未接收";
					this.$refs.notCheck[this.activeIndex].style.backgroundColor = "#fff";
				}
				this.clickCheck = false;
			} else {
				delete this.leftSearchForm.rentContractName;
			}
			this.tabResetList();
		},
		tabResetList() {
			this.leftPager.tabCurrentPage = 1;
			this.getTabListData();
		},
		editCheck(item) {
			this.$refs.editCheck.show(item);
		},
		editReceive(item) {
			//this.activeIndex = index
			this.$refs.editReceive.show(item);
		},
		handleTabClick(tab) {
			if (tab === undefined || tab === null || tab === "") {
				if (this.rentData.length > 0) {
					this.searchForm.rentContractId = this.rentData[0].rentContractId;
					this.resetList();
				}
			} else {
				this.searchForm.rentContractId = this.rentData[
					tab.index
				].rentContractId;
				this.resetList();
			}
		},
		handleClickCheck() {
			this.$refs.notCheck[this.activeIndex].innerHTML = "已接收";
			this.$refs.notCheck[this.activeIndex].style.backgroundColor = "#1563d1";
			this.clickCheck = true;
		},
		//查看
		showReceivableData(item) {
			this.$refs.showActual.show(item);
		}
	},
	filters: {
		mySubString(val) {
			if (val != null) {
				return val.substring(0, 7);
			}
			return "";
		},
		ellipsis(value) {
			if (!value) {
				return "";
			} else if (value.length > 8) {
				return value.slice(0, 8) + "...";
			} else {
				return value;
			}
		}
	}
};
