export const tableOption = {
    "align": "center",
    "index": true,
    "border": true,
    "column": [{
        "prop": "reimbursementId",
        "type": "input",
        "label": "报销Id",
        "search": true
    }, {"prop": "approveTemplateId", "type": "input", "label": "审批模板id", "search": true}, {
        "prop": "reimbursementName",
        "type": "input",
        "label": "报销名称",
        "search": true
    }, {"prop": "applyTime", "type": "input", "label": "申请时间", "search": true}, {
        "prop": "applyPerson",
        "type": "input",
        "label": "申请人",
        "search": true
    }, {"prop": "approveTemplate", "type": "input", "label": "审批模板", "search": true}, {
        "prop": "status",
        "type": "input",
        "label": "审批状态",
        "search": true
    }, {"prop": "remarks", "type": "input", "label": "备注", "search": true}, {
        "prop": "isDeleted",
        "type": "input",
        "label": "是否删除",
        "search": true
    }, {"prop": "createBy", "type": "input", "label": "创建人ID", "search": true}, {
        "prop": "createName",
        "type": "input",
        "label": "创建人姓名",
        "search": true
    }, {"prop": "createTime", "type": "input", "label": "创建时间", "search": true}, {
        "prop": "operateBy",
        "type": "input",
        "label": "操作人ID",
        "search": true
    }, {"prop": "operateName", "type": "input", "label": "操作人姓名", "search": true}, {
        "prop": "operateTime",
        "type": "input",
        "label": "操作时间",
        "search": true
    }],
    "gutter": 0,
    "stripe": true,
    "menuBtn": true,
    "viewBtn": true,
    "emptyBtn": true,
    "emptySize": "medium",
    "emptyText": "清空",
    "menuAlign": "center",
    "submitBtn": true,
    "indexLabel": "序号",
    "labelWidth": 120,
    "submitSize": "medium",
    "submitText": "提交",
    "labelSuffix": "：",
    "menuPosition": "center",
    "labelPosition": "left",
    "searchMenuSpan": 6
}
