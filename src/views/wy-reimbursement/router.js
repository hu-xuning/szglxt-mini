// Created by chenc
export default {
	path: "/reimbursement",
	component: () => import("~/layout/index.vue"),
	children: [
		{
			path: "/reimbursement/new",
			meta: { title: "报销申请列表" },
			component: () =>
				import("~/views/wy-reimbursement/views/new-reimbursement.vue")
		},
		{
			path: "/reimbursement/new/edit",
			meta: { title: "编辑" },
			component: () =>
				import("~/views/wy-reimbursement/views/new-reimbursement-edit.vue")
		},
		{
			path: "/reimbursement/new/add",
			meta: { title: "新增" },
			component: () =>
				import("~/views/wy-reimbursement/views/new-reimbursement-edit.vue")
		},
		{
			path: "/reimbursement/manage",
			meta: { title: "报销管理列表" },
			component: () =>
				import("~/views/wy-reimbursement/views/reimbursement-manage.vue")
		}
	]
};
