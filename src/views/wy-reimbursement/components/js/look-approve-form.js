// import { getObj} from '@/views/wy-reimbursement/api/approveform'
export default {
	name: "approve-form",
	props: ["approveData"],
	data() {
		return {
			approveFormTable: this.approveData,
			todayTime: ""
		};
	},
	mounted() {
		this.getTodayTime();

		this.queryOne();
	},
	methods: {
		queryOne() {
			this.approveData.approveTemplateId;
		},

		getTodayTime() {
			var time = new Date();
			this.todayTime =
				time.getFullYear() +
				"年" +
				(time.getMonth() + 1) +
				"月" +
				time.getDate() +
				"日";
		}
	}
};
