import { mapState } from "vuex";
export default {
	name: "payment-approve-form",
	props: {
		printData: {
			type: Object,
			default: () => ({})
		}
	},
	computed: {
		...mapState({
			user: state => state.user.user
		})
	},
	data() {
		return {
			todayTime: "",
			person: {},
			rules: {
				approvalName: [
					{ required: true, message: "请输入名称", trigger: "blur" }
				],
				reqPricet: [
					{
						required: true,
						message: "本次申请付款额不能为空且必须为数字值",
						trigger: "blur"
					},
					{
						pattern: /(^(([1-9]+\d*)|(0{1}))(.\d{1,2})?)|(^-([1-9]+\d*(\.\d{1,2})?|0\.(0[1-9]{1}|[1-9]{1}\d?)))/,
						message: "请输入正确的金额,最多只有两位小数",
						trigger: "blur"
					}
				],
				contractPrice: [
					{
						required: true,
						message: "合同价格不能为空且必须为数字值",
						trigger: "blur"
					},
					{
						pattern: /(^(([1-9]+\d*)|(0{1}))(.\d{1,2})?)|(^-([1-9]+\d*(\.\d{1,2})?|0\.(0[1-9]{1}|[1-9]{1}\d?)))/,
						message: "请输入正确的金额,最多只有两位小数",
						trigger: "blur"
					}
				],
				paymentStatus: [
					{ required: true, message: "已付款情况不能为空", trigger: "blur" }
				],
				balance: [
					{ required: true, message: "余款不能为空", trigger: "blur" },
					{
						pattern: /(^(([1-9]+\d*)|(0{1}))(.\d{1,2})?)|(^-([1-9]+\d*(\.\d{1,2})?|0\.(0[1-9]{1}|[1-9]{1}\d?)))/,
						message: "请输入正确的金额,最多只有两位小数",
						trigger: "blur"
					}
				],
				businessAgent: [
					{ required: true, message: "请输入业务经办人", trigger: "blur" },
					{ min: 1, max: 25, message: "长度在 1 到 25 个字符", trigger: "blur" }
				],
				departmentHead: [
					{ required: true, message: "请输入部门负责人", trigger: "blur" },
					{ min: 1, max: 25, message: "长度在 1 到 25 个字符", trigger: "blur" }
				],
				chargeApprove: [
					{ required: true, message: "分管领导审批不能为空", trigger: "blur" },
					{ min: 1, max: 64, message: "长度在 1 到 25 个字符", trigger: "blur" }
				]
			}
		};
	},
	mounted() {
		this.getTodayTime();
		this.queryOne();
	},
	methods: {
		handleTotalPrices() {
			var regPos = /(^(([1-9]+\d*)|(0{1}))(.\d{1,2})?)|(^-([1-9]+\d*(\.\d{1,2})?|0\.(0[1-9]{1}|[1-9]{1}\d?)))/;
			if (!regPos.test(this.printData.reqPricet)) {
				this.$message.error("本次申请付款额不能小于0且不能包含特殊字符！");
			}
		},
		handlecontractPrice() {
			var regPos = /(^(([1-9]+\d*)|(0{1}))(.\d{1,2})?)|(^-([1-9]+\d*(\.\d{1,2})?|0\.(0[1-9]{1}|[1-9]{1}\d?)))/;
			if (!regPos.test(this.printData.contractPrice)) {
				this.$message.error("合同价格不能小于0且不能包含特殊字符！");
			}
		},
		handlebalance() {
			var regPos = /(^(([1-9]+\d*)|(0{1}))(.\d{1,2})?)|(^-([1-9]+\d*(\.\d{1,2})?|0\.(0[1-9]{1}|[1-9]{1}\d?)))/;
			if (!regPos.test(this.printData.balance)) {
				this.$message.error("余款不能小于0且不能包含特殊字符！");
			}
		},
		queryOne() {},
		getTodayTime() {
			var time = new Date();
			this.todayTime =
				time.getFullYear() +
				"年" +
				(time.getMonth() + 1) +
				"月" +
				time.getDate() +
				"日";
			console.log(this.todayTime);
		},
		check() {
			this.$refs.form.validate(valid => {
				if (valid) {
					this.$emit("commit");
				} else {
					return false;
				}
			});
		}
	}
};
