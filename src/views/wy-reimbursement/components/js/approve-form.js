export default {
	name: "approve-form",
	props: {
		printData: {
			type: [Array, Object],
			default: () => ({})
		}
	},
	data() {
		return {
			todayTime: "",
			ruleform: {
				handler: "",
				reqStartTime: "",
				reqPricet: "",
				expenditureContent: "",
				departmentOpinion: "",
				leaderOpinion: ""
			},
			rules: {
				handler: [{ required: true, message: "请输入经手人", trigger: "blur" }],
				reqStartTime: [
					{ required: true, message: "请选择申请时间", trigger: "blur" }
				],
				reqPricet: [
					{ required: true, message: "金额不能为空" },
					{
						pattern: /(^(([1-9]+\d*)|(0{1}))(.\d{1,2})?)|(^-([1-9]+\d*(\.\d{1,2})?|0\.(0[1-9]{1}|[1-9]{1}\d?)))/,
						message: "请输入正确的金额,最多只有两位小数",
						trigger: "blur"
					}
				],
				expenditureContent: [
					{ required: true, message: "开支内容不能为空", trigger: "blur" }
				],
				departmentOpinion: [
					{
						required: true,
						message: "部门（所）领导意见不能为空",
						trigger: "blur"
					}
				],
				leaderOpinion: [
					{ required: true, message: "分管领导意见不能为空", trigger: "blur" }
				]
			}
		};
	},
	mounted() {
		this.getTodayTime();
	},
	methods: {
		handleTotalPrices() {
			var regPos = /^\d+(\.\d+)?$/;
			if (!regPos.test(this.printData.reqPricet)) {
				this.$message.error("金额不能小于0且不能包含特殊字符！");
			}
		},
		getTodayTime() {
			var time = new Date();
			this.todayTime =
				time.getFullYear() +
				"年" +
				(time.getMonth() + 1) +
				"月" +
				time.getDate() +
				"日";
		},
		check() {
			this.$refs.form.validate(valid => {
				if (valid) {
					this.$emit("commit");
				} else {
					return false;
				}
			});
		}
	}
};
