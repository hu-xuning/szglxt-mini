// import { getObj} from '@/views/wy-reimbursement/api/fundsupervisionpaymentform'
export default {
	name: "payment-approve-form",
	props: ["fundSupervisionPaymentFormData"],
	data() {
		return {
			fundSupervisionPaymentFormTable: this.fundSupervisionPaymentFormData,
			todayTime: "",
			//取得父组件传过来得值
			approveTemplateId: this.rid,
			options: [
				{
					label: "-=请选择=-",
					value: -1
				},
				{
					label: "未完成",
					value: 0
				},
				{
					label: "已完成",
					value: 1
				}
			]
		};
	},
	mounted() {
		this.getTodayTime();
		this.queryOne();
	},
	methods: {
		//qh+:根据编号查询一条数据
		queryOne() {
			console.log("-------------->" + this.approveTemplateId);
			// getObj(this.approveTemplateId).then(response=>{
			//     console.log("查询成功")
			//     this.approveFormTable = response.data.data
			// })
		},
		getTodayTime() {
			var time = new Date();
			this.todayTime =
				time.getFullYear() +
				"年" +
				(time.getMonth() + 1) +
				"月" +
				time.getDate() +
				"日";
			console.log(this.todayTime);
		}
	}
};
