
export default {

    name: "payment-approve-form",
    props: ["paymentApproveData"],
    data() {
        return {

            paymentApproveFormTable:this.paymentApproveData,
            todayTime: "",
            approveTemplateId:this.rid,
        }
    },
    mounted(){
        this.getTodayTime()

    },
    methods: {
        queryOne(){
            console.log("-------------->"+this.approveTemplateId)

        },
        getTodayTime(){
            var time = new Date()
            this.todayTime = time.getFullYear() + "年" + (time.getMonth()+1) + "月" +time.getDate() + "日"
        },
    }
}
