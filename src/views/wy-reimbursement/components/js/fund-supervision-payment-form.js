import {mapState} from "vuex";
export default {
    name: "payment-approve-form",
    props: {
        printData:{
            type: Array,
            default:() => {
                return {}
            }
        }
    },
    computed:{
        ...mapState({
            user: state => state.user.user,
        }),
    },
    data() {
        return {
            todayTime: "",
            //取得父组件传过来得值
            options: [
                {
                    label: "-=请选择=-",
                    value: "-1"
                },
                {
                    label: "未完成",
                    value: "0"
                },
                {
                    label: "已完成",
                    value: "1"
                },
            ],
            person:{
            },
            rules: {
                approvalName: [
                    { required: true, message: '请输入工程名称', trigger: 'blur' },
                ],
                jobSchedule: [
                    { required: true, message: '请选择工程进度', trigger: 'blur' },

                ],
                auditUnit: [
                    {   required: true, message: '请输入审计单位', trigger: 'blur' },
                ],
                reqPricet: [
                    {   required: true, message: '请输入本次申请付款额且为数字', trigger: 'blur' },
                    { pattern: /^\d+(\.\d{1,2})?$/, message: "请输入正确的金额,最多只有两位小数", trigger: "blur" },
                ],
                approvalNumber: [
                    {   required: true, message: '请输入批准文号', trigger: 'blur' },
                ],
                investmentAmount: [
                    {   required: true, message: '请输入投资额且为数字', trigger: 'blur' },
                    { pattern: /^\d+(\.\d{1,2})?$/, message: "请输入正确的金额,最多只有两位小数", trigger: "blur" },
                ],
                agentContractPrice: [
                    {   required: true, message: '请输入代建合同金额且为数字', trigger: 'blur' },
                    { pattern: /^\d+(\.\d{1,2})?$/, message: "请输入正确的金额,最多只有两位小数", trigger: "blur" },

                ],
                bidPrice: [
                    {   required: true, message: '请输入中标价且为数字', trigger: 'blur' },
                    { pattern: /^\d+(\.\d{1,2})?$/, message: "请输入正确的金额,最多只有两位小数", trigger: "blur" },
                ],
                budgetPrice: [
                    {   required: true, message: '请输入预算价且为数字', trigger: 'blur' },
                    { pattern: /^\d+(\.\d{1,2})?$/, message: "请输入正确的金额,最多只有两位小数", trigger: "blur" },
                ],
                settlementPrice: [
                    {   required: true, message: '请输入结算价且为数字', trigger: 'blur' },
                    { pattern: /^\d+(\.\d{1,2})?$/, message: "请输入正确的金额,最多只有两位小数", trigger: "blur" },
                ],
                paymentStatus: [
                    {   required: true, message: '请输入已付款情况', trigger: 'blur' },
                ],
                balance: [
                    {   required: true, message: '请输入工程余款且为数字', trigger: 'blur' },
                    { pattern: /^\d+(\.\d{1,2})?$/, message: "请输入正确的金额,最多只有两位小数", trigger: "blur" },
                ],
                businessAgent: [
                    {   required: true, message: '请输入业务经办人', trigger: 'blur' },
                ],
                departmentHead: [
                    {   required: true, message: '请输入部门负责人', trigger: 'blur' },
                ],
                chargeApprove: [
                    {   required: true, message: '请输入分管领导审批', trigger: 'blur' },
                ],
            }
        }
    },
    mounted(){
        this.getTodayTime()
        this.queryOne()
    },
    methods: {
        handleTotalPrices(){
            var regPos =  /(^(([1-9]+\d*)|(0{1}))(.\d{1,2})?)|(^-([1-9]+\d*(\.\d{1,2})?|0\.(0[1-9]{1}|[1-9]{1}\d?)))/;
            if (!regPos.test(this.printData.reqPricet)){
                this.$message.error('本次申请付款额不能小于0且不能包含特殊字符！')
            }
        },
        handlebalance(){
            var regPos = /(^(([1-9]+\d*)|(0{1}))(.\d{1,2})?)|(^-([1-9]+\d*(\.\d{1,2})?|0\.(0[1-9]{1}|[1-9]{1}\d?)))/;
            if (!regPos.test(this.printData.balance)){
                this.$message.error('工程余款不能小于0且不能包含特殊字符！')
            }
        },
        handleapprovalNumber(){
        },
        handleinvestmentAmount(){
            var regPos =  /(^(([1-9]+\d*)|(0{1}))(.\d{1,2})?)|(^-([1-9]+\d*(\.\d{1,2})?|0\.(0[1-9]{1}|[1-9]{1}\d?)))/;
            if (!regPos.test(this.printData.investmentAmount)){
                this.$message.error('投资额不能小于0且不能包含特殊字符！')
            }
        },
        handleagentContractPrice(){
            var regPos = /(^(([1-9]+\d*)|(0{1}))(.\d{1,2})?)|(^-([1-9]+\d*(\.\d{1,2})?|0\.(0[1-9]{1}|[1-9]{1}\d?)))/;
            if (!regPos.test(this.printData.agentContractPrice)){
                this.$message.error('代建合同金额不能小于0且不能包含特殊字符！')
            }
        },
        handbidPrice(){

            var regPos = /(^(([1-9]+\d*)|(0{1}))(.\d{1,2})?)|(^-([1-9]+\d*(\.\d{1,2})?|0\.(0[1-9]{1}|[1-9]{1}\d?)))/;
            if (!regPos.test(this.printData.bidPrice)){
                this.$message.error('中标价不能小于0且不能包含特殊字符！')
            }
        },
        handbudgetPrice(){
            var regPos =  /(^(([1-9]+\d*)|(0{1}))(.\d{1,2})?)|(^-([1-9]+\d*(\.\d{1,2})?|0\.(0[1-9]{1}|[1-9]{1}\d?)))/;
            if (!regPos.test(this.printData.budgetPrice)){
                this.$message.error('预算价不能小于0且不能包含特殊字符！')
            }
        },
        handsettlementPrice(){
            var regPos =  /(^(([1-9]+\d*)|(0{1}))(.\d{1,2})?)|(^-([1-9]+\d*(\.\d{1,2})?|0\.(0[1-9]{1}|[1-9]{1}\d?)))/;
            if (!regPos.test(this.printData.settlementPrice)){
                this.$message.error('结算价不能小于0且不能包含特殊字符！')
            }
        },
        //qh+:根据编号查询一条数据
        queryOne(){
        },
        getTodayTime(){
            var time = new Date()
            this.todayTime = time.getFullYear() + "年" + (time.getMonth()+1) + "月" +time.getDate() + "日"
        },
        check(){
            if(parseFloat(this.printData.reqPricet) >parseFloat(this.printData.settlementPrice)){
                this.$message.error('申请付款额必须小于等于结算价');
                return ;
            }
            if(parseFloat(this.printData.bidPrice) >parseFloat(this.printData.budgetPrice)){
                console.log("bidPrice=="+this.printData.bidPrice +"=== budgetPrice :"+this.printData.budgetPrice)
                this.$message.error("中标价必须小于等于预算价")
                return;
            }
            this.$refs.form.validate((valid) => {
                if (valid) {
                    this.$emit('commit')
                } else {
                    return false;
                }
            });
        }
    }
}
