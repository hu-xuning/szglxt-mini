import request from '@/plugins/axios'

export function fetchList(query) {
  return request({
    url: '/api/reimbursement/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/api/reimbursement',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/api/reimbursement/' + id,
    method: 'get'
  })
}

export function delObj(obj) {
  return request({
    url: '/api/reimbursement/',
    method: 'delete',
    params:obj
  })
}

export function putObj(obj) {
  return request({
    url: '/api/reimbursement',
    method: 'put',
    data: obj
  })
}

export function getNewId() {
    return request({
        url: '/api/reimbursement/getNewId',
        method: 'get'
    })
}
