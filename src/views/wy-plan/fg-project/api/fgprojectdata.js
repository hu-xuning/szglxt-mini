import request from '@/plugins/axios'

export function fetchList(query) {
  return request({
    url: '/api/fgprojectdata/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/api/fgprojectdata',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/api/fgprojectdata/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/api/fgprojectdata/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/api/fgprojectdata',
    method: 'put',
    data: obj
  })
}

export function download(id) {
  return request({
    url: '/api/fgprojectdata/download/'+id,
    method: 'get'
  })
}

export function updateSyncFgData() {
  return request({
    url: '/api/fgprojectdata/updateSyncFgData',
    method: 'get',
  })
}