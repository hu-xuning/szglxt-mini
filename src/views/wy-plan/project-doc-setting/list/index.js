import sortTree from "@/components/sort-tree";
import { milestoneTemplatePage } from "../api/docSetting";

export default {
	name: "resource-manage",
	components: { sortTree },
	provide() {
		return {
			initList: this.getListData
		};
	},
	beforeMount() {
		this.getListData();
	},
	data() {
		return {
			// hxf-0829-表单绑定
			tableData: [],
			// 请求参数
			params: {
				current: 1,
				size: 10
			},
			// 分页
			pager: {
				total: 0, // 总页数
				current: 1, // 当前页数
				size: 10 // 每页显示多少条
			},
			// 页面loading
			loading: false
		};
	},
	methods: {
		// hxf-0829-新增或编辑跳转
		handleSettingClick() {
			this.$router.push({ name: "project_doc_setting_add" });
		},
		handleSettingClickEdit(row) {
			this.$router.push({
				name: "project_doc_setting_edit",
				query: { id: row.archivesId }
			});
		},
		// hxf-0908-分页获取
		getListData(params = {}) {
			this.loading = true;
			let requestData = { ...this.params, ...params };
			milestoneTemplatePage(requestData)
				.then(res => {
					const { current, size, total, records } = res.data.data;
					this.tableData = records;
					this.params = requestData;
					this.pager = { total, current, size };
				})
				.finally(() => {
					this.loading = false;
				});
		},
		// 菜单点击监听
		handleMenuClick(type) {
			switch (type) {
				case "add":
					this.handleSettingClick();
					break;
				default:
					this.$message(type);
					break;
			}
		}
	}
};
