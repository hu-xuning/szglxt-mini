import {
	milestonetemplate,
	getMilestonetemplate,
	milestonetemplateEdit
} from "../api/docSetting";
// hxf-0829-修改项目档案设置编辑与新增
export default {
	name: "doc-edit",
	data() {
		return {
			// hxf-0907-附件信息是否必填
			RequiredOptions: [
				{
					value: "1",
					label: "是"
				},
				{
					value: "2",
					label: "否"
				}
			],
			// hxf-0907-新增标识
			isAdd: true,
			// hxf-0908-弹框默认为false
			dialogVisible: false,
			// hxf-0908-弹框资料
			bulletInformation: {
				milepostName: "",
				remarks: "",
				sortNumber: "1"
			},
			// hxf-0908-基本信息校验
			formSettingRules: {
				archivesName: [
					{ required: true, message: "请输入模板名称", trigger: "blur" },
					{ min: 0, max: 30, message: "长度在0-30个字符串", trigger: "blur" }
				]
			},
			// hxf-0908-弹框资料校验
			bulletInformationRules: {
				milepostName: [
					{ required: true, message: "请输入里程碑名称", trigger: "blur" },
					{ min: 0, max: 30, message: "长度在0-30个字符串", trigger: "blur" }
				]
			},
			// hxf-0909-提交form表单
			projectDocForm: {
				// hxf-0829-基本信息
				milestoneTemplate: {
					archivesName: "",
					remarks: ""
				},
				// hxf-0909-里程碑表格
				milestoneConfigVoList: []
			},
			// hxf-0909-存入id
			archivesId: "",
			// hxf-0909-里程碑表格
			editableTabsValue: "0",
			tabIndex: 0
		};
	},
	methods: {
		// hxf-0907-附件信息编辑
		handleClickEdit(row) {
			this.$set(row, "newAdd", true);
			this.$forceUpdate();
		},
		// hxf-0907-附件信息删除
		handleClickDelete(index, rows) {
			rows.splice(index, 1);

			//删除后重新排序
			rows = rows.map((r, index) => {
				r.sort = index + 1;
			});
		},
		// hxf-0829-更新
		handleUpdate() {
			this.$refs["doc-setting"].validate(valid => {
				if (valid) {
					this.projectDocForm.milestoneConfigVoList.map((item, index) => {
						this.projectDocForm.milestoneConfigVoList[index].milestoneName =
							item.title;
						this.projectDocForm.milestoneConfigVoList[
							index
						].milestoneMaterials = item.content;
						this.projectDocForm.milestoneConfigVoList[index].milestoneSort =
							index + 1;
					});
					if (this.projectDocForm.milestoneConfigVoList.length > 0) {
						let errFlag = false;
						this.projectDocForm.milestoneConfigVoList.map((item, index) => {
							if (
								item.milestoneMaterials.length <= 0 ||
								(item.milestoneMaterials[index] &&
									item.milestoneMaterials[index].materialsName === "")
							) {
								errFlag = true;
							}
						});
						if (errFlag) {
							this.$message.error("请完善里程碑信息！");
							return;
						}
						milestonetemplateEdit(this.projectDocForm).then(res => {
							if (res.data.code === 0) {
								this.closeNowRouter();
								this.$message.success("更新成功！");
							}
						});
					} else {
						milestonetemplateEdit(this.projectDocForm).then(res => {
							if (res.data.code === 0) {
								this.closeNowRouter();
								this.$message.success("更新成功！");
							}
						});
					}
				} else {
					this.$message.error("请完善必填信息！");
					return false;
				}
			});
		},
		// hxf-0829-保存
		handlePreservation() {
			this.$refs["doc-setting"].validate(valid => {
				if (valid) {
					this.projectDocForm.milestoneConfigVoList.map((item, index) => {
						this.projectDocForm.milestoneConfigVoList[index].milestoneName =
							item.title;
						this.projectDocForm.milestoneConfigVoList[
							index
						].milestoneMaterials = item.content;
						this.projectDocForm.milestoneConfigVoList[index].milestoneSort =
							index + 1;
					});
					if (this.projectDocForm.milestoneConfigVoList.length > 0) {
						let errFlag = false;
						this.projectDocForm.milestoneConfigVoList.forEach((item,index) => {
							if(item.milestoneMaterials.length <= 0 || item.milestoneMaterials.some(i => i.materialsName === '')){
								errFlag = true;
							}
						})
						if (errFlag) {
							this.$message.error("请完善里程碑信息！");
							return;
						}
						milestonetemplate(this.projectDocForm).then(res => {
							if (res.data.code === 0) {
								this.closeNowRouter();
								this.$message.success("添加成功！");
							}
						});
					} else {
						milestonetemplate(this.projectDocForm).then(res => {
							if (res.data.code === 0) {
								this.closeNowRouter();
								this.$message.success("添加成功！");
							}
						});
					}
				} else {
					this.$message.error("请完善必填信息！");
					return false;
				}
			});
		},
		// hxf-0907-附件添加
		superInforAdd() {
			let index = this.projectDocForm.milestoneConfigVoList.length - 1;
			if (this.projectDocForm.milestoneConfigVoList.length > 0) {
				if (
					this.projectDocForm.milestoneConfigVoList[index].content.length === 0
				) {
					this.$message.error("请完善已添加的里程碑信息！");
				} else {
					this.dialogVisible = true;
				}
			} else {
				this.dialogVisible = true;
			}
			//hxf-若为编辑，则排序号不重新拍
			if (this.projectDocForm.milestoneConfigVoList.length > 0) {
				this.bulletInformation.sortNumber =
					this.projectDocForm.milestoneConfigVoList.length + 1;
			}
		},
		// hxf-0908-X弹框关闭
		beforeClose(done) {
			done();
		},
		// hxf-0908-弹框信息确认
		handleDialogClick(targetName) {
			this.$refs["dialog-form"].validate(valid => {
				if (valid) {
					let newTabName = ++this.tabIndex + "";
					this.projectDocForm.milestoneConfigVoList.push({
						title: this.bulletInformation.milepostName,
						name: newTabName,
						sortNumber: this.bulletInformation.sortNumber,
						content: []
					});
					this.editableTabsValue = newTabName;
					this.dialogVisible = false;
					this.$nextTick(function() {
						// this.bulletInformation = {}
						this.bulletInformation.milepostName = "";
						this.bulletInformation.remarks = "";
					});
					this.$message.success("添加成功！");
					this.bulletInformation.sortNumber =
						Number(this.bulletInformation.sortNumber) + 1;
				} else {
					this.$message.error("请完善必填信息！");
					return false;
				}
			});
		},
		// hxf-0908-删除弹框tabs
		removeTab(targetName) {
			this.$confirm("是否确认删除里程碑?", "提示", {
				confirmButtonText: "确定",
				cancelButtonText: "取消",
				type: "warning"
			})
				.then(() => {
					let tabs = this.projectDocForm.milestoneConfigVoList;
					let activeName = this.editableTabsValue;
					if (activeName === targetName) {
						tabs.forEach((tab, index) => {
							if (tab.name === targetName) {
								let nextTab = tabs[index + 1] || tabs[index - 1];
								if (nextTab) {
									activeName = nextTab.name;
								}
							}
						});
					}
					this.editableTabsValue = activeName;
					this.projectDocForm.milestoneConfigVoList = tabs.filter(
						tab => tab.name !== targetName
					);

					this.$message.success("删除成功！");
				})
				.catch(() => {
					this.$message({
						type: "info",
						message: "已取消删除"
					});
				});
		},
		// hxf-0908-里程碑详细信息添加
		milestoneDetailsAdd(index) {
			this.projectDocForm.milestoneConfigVoList[index].content.push({
				archivesId: "",
				materialsName: "",
				/*isReuse: '0',
                isRequired: '0',*/
				// sort: this.projectDocForm.milestoneConfigVoList[index].sortNumber,
				sort:
					this.projectDocForm.milestoneConfigVoList[index].content.length + 1,
				newAdd: true
			});
			/*if (this.projectDocForm.milestoneConfigVoList[index].content.length === 1) {
                this.projectDocForm.milestoneConfigVoList[index].content[0].isReuse = '0'
            }*/
		},
		closeNowRouter() {
			this.$store.commit("delLastRoute", this.$route.path);
		},
		// hxf-0909-编辑时初始获取初始数据
		handleGetMilestonetemplate(id) {
			if (id !== "" && id !== null && id !== undefined) {
				getMilestonetemplate(id).then(res => {
					if (res.data.code === 0) {
						this.projectDocForm = res.data.data;
						this.projectDocForm.milestoneConfigVoList.map((item, index) => {
							this.projectDocForm.milestoneConfigVoList[index].title =
								item.milestoneName;
							this.projectDocForm.milestoneConfigVoList[index].content =
								item.milestoneMaterials;
							this.projectDocForm.milestoneConfigVoList[
								index
							].name = index.toString();
							this.tabIndex = index.toString();
							// this.projectDocForm.milestoneConfigVoList[index].content[0].isReuse = '0'
						});
					}
				});
			}
		}
	},
	created() {
		this.archivesId = this.$route.query.id;
		if (this.$route.meta.title !== "新增") {
			this.isAdd = false;
		}
	},
	mounted() {
		this.handleGetMilestonetemplate(this.archivesId);
	}
};
