import request from '@/plugins/axios'

export function milestoneTemplatePage(query) {
    return request({
        url: '/api/milestonetemplate/page',
        method: 'get',
        params: query
    })
}

export function milestonetemplate(data) {
    return request({
        url: '/api/milestonetemplate',
        method: 'post',
        data: data
    })
}

export function getMilestonetemplate(id) {
    return request({
        url: '/api/milestonetemplate/' + id,
        method: 'get'
    })
}

export function milestonetemplateEdit(data) {
    return request({
        url: '/api/milestonetemplate',
        method: 'put',
        data: data
    })
}
