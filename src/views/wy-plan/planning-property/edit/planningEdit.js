import {
	planprojectAdd,
	planprojectEdit,
	getPlanproject,
	validateProject,
	validateLandCode,
	validateProjectCountryCode
} from "../api/planning";
import {
	doubleValidate,
	validatePhone,
	isInt,
	mapVerification
} from "~/utils/validate.js";
import { mapState, mapGetters } from "vuex";
import positionDialog from "@/components/positionDialog";
import BaseLoad from "@/assets/vue-mixin/BaseLoad";
import { validateFacRoomName } from "~/views/wy-info/project-manage/api/project_manage";
import projectMilestones from "../components/project-milestones";
let moment = require("moment");
export default {
	name: "planning-edit",
	mixins: [BaseLoad],
	components: { positionDialog, projectMilestones },
	created() {
		if (this.$route.meta.title !== "新增") {
			this.isAdd = false;
		}
		// console.log(this.$route.meta.title);
		this.planUses = this.getDict("property_use_types");
		this.ownershipNatureTypes = this.getDict("asset-ownership-nature");
		this.landUses = this.getDict("land_uses");
		this.manageUnits = this.getDict("administrative_office");
		this.initialTypes = this.getDict("project_init_type");
		this.operations = this.getDict("property_operations");
		this.planningForm.landInfo.cityCode = this.cityAssembly[0].value;
		this.planningForm.landInfo.countyCode = this.districtCollection[0].value;
	},
	mounted() {
		this.isEdit = this.$route.query.isEdit;
		this.handleGetPlanproject();
		if (!this.isEdit) {
			let obj = {
				tempId: this.planningForm.propertyBasic.isUpdate,
				projectId: ""
			};
			this.$refs.milestones.handleClickToUpdateItem(obj);
			// hxf-0910-tab选中项
			this.$refs.milestones.handleClick({ index: 0 });
		}
	},
	computed: {
		// 物业类型
		propertyTypes() {
			return this.getDict("property_types");
		},
		projectSubdivides() {
			return this.getDict("project_subdivide");
		},
		// 物业用途（所有）
		propertyUseTypes() {
			return this.getDict("property_use_types");
		},
		facilityLevels() {
			return this.getDict("facility_levels");
		},
		facilityUnits() {
			return this.getDict("facility_units");
		},
		projectType() {
			return this.getDict("project_types");
		},
		projectStreet() {
			return this.getDict("streets");
		},
		transferModeOption() {
			return this.getDict("transfer_types");
		},
		communityNames() {
			return this.getDict("community_names");
		},
		propertySources() {
			return this.getDict("property_source");
		},
		statisticTypes() {
			return this.getDict("statistic_types");
		},
		...mapGetters({
			cityAssembly: "getCityAssembly",
			districtCollection: "getDistrictCollection"
		})
	},
	data() {
		let floorNumValidator = (rule, value, callback) => {
			const re = /^-?[1-9]\d*$/;
			const rsCheck = re.test(value);
			if (!rsCheck) {
				callback(new Error("请输入-20 至 200之间非0的整数"));
			} else {
				if (value > 200) {
					return callback(new Error("最大值不超过200"));
				} else if (value < -20) {
					return callback(new Error("最小值不能小于-20"));
				} else {
					callback();
				}
			}
		};
		return {
			planningForm: {
				// hxf-0828-项目信息
				propertyBasic: {
					projectId: "",
					projectCode: "",
					projectCountryCode: "",
					projectName: "",
					projectArea: "",
					projectType: "",
					// projectStreet: '',
					isUpdate: "1",
					stage: "",
					constructionUnit: "",
					contactName: "",
					contactPhone: "",
					remarks: "",
					mapCoordinates: ""
				},
				mapCoordinates: "",
				usedName: "",
				// hxf-0828-配套信息
				supportingFacilities: [],
				// hxf-0909-资料信息
				projectMilestones: [],
				landInfo: {
					address: "",
					addressInfo: "",
					cityCode: "",
					community: "",
					countyCode: "",
					manageUnit: "",
					landContractNum: "",
					landId: "",
					landUse: "",
					parcelArea: "",
					parcelNum: "",
					remarks: "",
					street: "",
					investmentIntensity: "",
					areaCode: "",
					areaName: ""
				}
			},
			// hxf-0928-土地信息校验
			landInfo_rules: {
				parcelNum: [
					{ required: false, message: "请输入宗地号", trigger: "blur" },
					{ min: 0, max: 30, message: "长度在0-30个字符串", trigger: "blur" }
				],
				parcelArea: [
					{ required: false, message: "请输入宗地面积(m²)", trigger: "blur" },
					{ validator: doubleValidate, trigger: "blur" }
				],
				landUse: [
					{ required: false, message: "请输入土地用途", trigger: "blur" }
				],
				landContractNum: [
					{ required: false, message: "请输入土地合同号", trigger: "blur" },
					{ min: 0, max: 100, message: "长度在0-100个字符串", trigger: "blur" }
				],
				cityCode: [
					{
						required: true,
						validator: (rules, value, callback) => {
							//区县
							let county = this.planningForm.landInfo.countyCode;
							//   街道
							let street = this.planningForm.landInfo.street;
							//   社区
							let community = this.planningForm.landInfo.community;
							//   管理所
							let addressInfo = this.planningForm.landInfo.addressInfo;
							if (!value || !county || !street || !community || !addressInfo) {
								return callback(new Error("物业地址不能为空"));
							}
							return callback();
						},
						trigger: "blur"
					}
				],
				addressInfo: [
					{ required: true, message: "地址不能为空", trigger: "blur" },
					{ min: 0, max: 30, message: "长度在0-30个字符串", trigger: "blur" }
				]
			},
			// hxf-0831-项目信息校验
			propertyBasic_rules: {
				projectName: [
					{ required: true, message: "请输入项目名称", trigger: "blur" },
					{ min: 0, max: 30, message: "长度在0-30个字符串", trigger: "blur" }
				],
				projectArea: [
					{ required: true, message: "请输入项目面积", trigger: "blur" },
					{ validator: doubleValidate, trigger: "blur" }
				],
				contactPhone: [
					{ required: false },
					{ validator: validatePhone, trigger: "blur" }
				],
				projectType: [
					{ required: true, message: "请输入物业大类", trigger: "blur" },
					{ min: 0, max: 30, message: "长度在0-30个字符串", trigger: "blur" }
				],
				// manageUnit: [
				// 	{ required: true, message: "请选择管理权属", trigger: "change" }
				// ],
				projectCountryCode: [
					{ required: false, message: "请输入项目国家编号", trigger: "blur" },
					{ min: 0, max: 100, message: "长度在0-100个字符串", trigger: "blur" }
				],
				mapCoordinates: [
					{ required: false, message: "请输入地图坐标", trigger: "blur" },
					{ min: 0, max: 50, message: "长度在0-50个字符串", trigger: "blur" },
					{ validator: mapVerification, trigger: "blur" }
				]
			},
			// hxf-0929-配套信息校验
			bulletInformation_rules: {
				// buildingName: [
				// 	{ required: true, message: "请输入楼栋名称", trigger: "blur" },
				// 	{ min: 0, max: 25, message: "长度在0-25个字符串", trigger: "blur" }
				// ],
				// floorNum: [
				// 	{ required: true, message: "请输入所属楼层", trigger: "blur" },
				// 	{ validator: floorNumValidator, trigger: "blur" }
				// ],
				propertyName: [
					{ required: true, message: "请输入物业名称", trigger: "blur" },
					{ min: 0, max: 30, message: "长度在0-30个字符串", trigger: "blur" }
				],
				usedName: [
					{ required: false, message: "请输入曾用名", trigger: "blur" },
					{ min: 0, max: 30, message: "长度在0-30个字符串", trigger: "blur" }
				],
				floorHeight: [
					{ required: false, message: "请输入层高", trigger: "blur" },
					{ validator: doubleValidate, trigger: "blur" }
				],
				clearHeight: [
					{ required: false, message: "请输入净高", trigger: "blur" },
					{ validator: doubleValidate, trigger: "blur" }
				],
				loadBearing: [
					{ required: false, message: "请输入承重", trigger: "blur" },
					{ validator: doubleValidate, trigger: "blur" }
				],
				thickness: [
					{ required: false, message: "请输入楼板厚度", trigger: "blur" },
					{ validator: doubleValidate, trigger: "blur" }
				],
				address: [
					{ required: true, message: "请输入物业地址", trigger: "blur" },
					{ min: 0, max: 50, message: "长度在0-50个字符串", trigger: "blur" }
				],
				initialType: [
					{ required: false, message: "请选择初始类别", trigger: "blur" }
				],
				planUse: [
					{ required: true, message: "请输入规划用途", trigger: "blur" },
					{ min: 0, max: 100, message: "长度在0-100个字符串", trigger: "blur" }
				],
				parkingSpotNum: [
					{ required: false, message: "请输入停车位数", trigger: "blur" },
					{ validator: isInt, trigger: "blur" }
				],
				totalBuildings: [
					{ required: true, message: "请输入总栋数", trigger: "blur" },
					{ validator: isInt, trigger: "blur" }
				],
				useLandArea: [
					{ required: true, message: "请输入用地面积", trigger: "blur" },
					{ validator: doubleValidate, trigger: "blur" }
				],
				floorSpace: [
					{ required: false, message: "请输入占地面积", trigger: "blur" },
					{ validator: doubleValidate, trigger: "blur" }
				],
				buildArea: [
					{ required: false, message: "请输入建筑面积", trigger: "blur" },
					{ validator: doubleValidate, trigger: "blur" }
				],
				insideArea: [
					{ required: false, message: "请输入套内面积", trigger: "blur" },
					{ validator: doubleValidate, trigger: "blur" }
				],
				operation: [
					{ required: true, message: "请选择经营情况", trigger: "blur" }
				],
				useArea: [
					{ required: false, message: "请输入使用面积", trigger: "blur" },
					{ validator: doubleValidate, trigger: "blur" }
				],
				unmanagedArea: [
					{ required: false, message: "请输入非经营面积", trigger: "blur" },
					{ validator: doubleValidate, trigger: "blur" }
				],
				managedArea: [
					{ required: false, message: "请输入经营面积", trigger: "blur" },
					{ validator: doubleValidate, trigger: "blur" }
				],
				noApportionedArea: [
					{ required: false, message: "请输入分摊公用面积", trigger: "blur" },
					{ validator: doubleValidate, trigger: "blur" }
				],
				buildPrice: [
					{ required: false, message: "请输入建购价款", trigger: "blur" },
					{ validator: doubleValidate, trigger: "blur" }
				],
				obligee: [
					{ required: true, message: "请输入权利人", trigger: "blur" },
					{ min: 0, max: 30, message: "长度在0-30个字符串", trigger: "blur" }
				],
				ownershipNature: [
					{ required: true, message: "请选择权利性质", trigger: "blur" }
				],
				endTime: [
					{ required: true, message: "请选择竣工日期", trigger: "blur" }
				],
				propertySource: [
					{ required: true, message: "请选择物业来源", trigger: "blur" }
				],
				mapCoordinates: [
					{ required: false, message: "请输入地图坐标", trigger: "blur" },
					{ min: 0, max: 30, message: "长度在0-30个字符串", trigger: "blur" },
					{ validator: mapVerification, trigger: "blur" }
				],
				projectType: [
					{ required: true, message: "请输入物业大类", trigger: "blur" },
					{ min: 0, max: 30, message: "长度在0-30个字符串", trigger: "blur" }
				],
				projectSubclassType: [
					{ required: true, message: "请输入物业小类", trigger: "blur" },
					{ min: 0, max: 30, message: "长度在0-30个字符串", trigger: "blur" }
				],
				transferMode: [
					{ required: true, message: "请输入移交方式", trigger: "blur" },
					{ min: 0, max: 30, message: "长度在0-30个字符串", trigger: "blur" }
				],
				projectSubdivide: [
					{ required: false, message: "请选择物业细类", trigger: "blur" }
				],
				facilityNumber: [
					{ required: false, message: "请输入公服数量", trigger: "blur" },
					{ validator: doubleValidate, trigger: "blur" }
				],
				facilityUnit: [
					{ required: false, message: "请选择公服单位", trigger: "blur" }
				],
				facilityLevel: [
					{ required: false, message: "请选择公服层级", trigger: "blur" }
				],
				// radius: [
				// 	{ required: true, message: "请输入覆盖半径", trigger: "blur" },
				// 	{ validator: doubleValidate, trigger: "blur" }
				// ],
				statisticType: [
					{ required: true, message: "请选择物业统计类别", trigger: "blur" }
				  ],
			},
			//hxf-0831-新增标识
			isEdit: "",
			// stage: '',
			isAdd: true,
			//里程碑
			dialogVisible: false,
			milestoneSortIndex: 0,
			projectId: "",
			isThereAMilestone: "",
			materialsName: true,
			bulletInformation: {
				buildingName: "", //楼栋名称 jiaj
				floorNum: "", //楼层号 jiaj
				propertyName: "",
				usedName: "",
				address: "",
				initialType: "",
				planUse: "",
				parkingSpotNum: "",
				totalBuildings: "",
				useLandArea: "",
				floorSpace: "",
				buildArea: "",
				operation: "20",
				insideArea: "",
				useArea: "",
				unmanagedArea: "",
				managedArea: "",
				noApportionedArea: "",
				buildPrice: "",
				obligee: "",
				ownershipNature: "",
				endTime: "",
				propertySource: "",
				mapCoordinates: "",
				projectType: "", //物业类型 jiaj
				projectSubclassType: "", //物业用途 jiaj
				transferType: "", //移交方式 jiaj
				transferUnit: "", //移交单位 jiaj
				receiveTime: "", //移交时间 jiaj
				transferDesc: "", //移交描述 jiaj
				remarks: "",
				projectSubdivide: "", //物业细类
				facilityNumber: "",
				facilityUnit: "",
				facilityLevel: "",
				radius: "",
				floorHeight: "",
				loadBearing: "",
				thickness: "",
				clearHeight:"",
				statisticType:"1"
			},
			bulletInformationFlag: true,
			bulletInformationIndex: 0,
			planUses: [],
			initialTypes: [],
			ownershipNatureTypes: [],
			landUses: [],
			operations: [],
			manageUnits: [],
			communityNameStreet: [],
			propertyUseTypesFilter: [] //当前物业类型能选择的值,

		};
	},
	methods: {
		//里程碑赋值
		fileBusId(data) {
			this.planningForm.projectMilestones = data;
		},
		//里程碑校验
		isMaterialsName(data) {
			this.materialsName = data;
		},
		//jiaj 根据物业类型，获取对应的物业用途
		changeProjectType(val) {
			//重置物业用途
			this.propertyUseTypesFilter = [];
			if (val) {
				this.bulletInformation.projectSubclassType = "";
				//过滤物业用途
				this.propertyUseTypesFilter = this.propertyUseTypes.filter(
					ut => ut.value.indexOf(val) == 0
				);
			}
		},
		// hxf-0929-配套信息编辑
		handleClickEdit(scope) {
			this.dialogVisible = true;
			this.bulletInformation = scope.row;
			this.bulletInformationFlag = false;
			this.bulletInformationIndex = scope.$index;
			//初始化物业用途（根据物业类型） jiaj
			this.propertyUseTypesFilter = this.propertyUseTypes.filter(
				ut => ut.value.indexOf(this.bulletInformation.projectType) == 0
			);
		},
		// hxf-0828-配套信息删除
		handleClickDelete(index, rows) {
			rows.splice(index, 1);
		},
		// hxf-0828-多选选择方法
		handleSelectionChange(val) {
			this.multipleSelection = val;
		},
		submitData() {
			if (this.isAdd) {
				this.handlePreservation();
			} else {
				this.handleUpdate();
			}
		},
		// hxf-0828-更新
		handleUpdate() {
			this.planningForm.landInfo.areaCode =
				this.planningForm.landInfo.cityCode +
				"-" +
				this.planningForm.landInfo.countyCode;
			this.planningForm.landInfo.address =
				this.getCardTypeValue(
					this.planningForm.landInfo.cityCode,
					this.cityAssembly
				) +
				this.getCardTypeValue(
					this.planningForm.landInfo.countyCode,
					this.districtCollection
				) +
				this.getCardTypeValue(
					this.planningForm.landInfo.street,
					this.projectStreet
				) +
				this.getCardTypeValue(
					this.planningForm.landInfo.community,
					this.communityNames
				);

			//给对象赋值
			this.planningForm.mapCoordinates = this.planningForm.propertyBasic.mapCoordinates;
			this.planningForm.manageUnit = this.planningForm.propertyBasic.manageUnit;
			//校验里程碑名称是否已填
			if (!this.materialsName) {
				this.$message.error(
					"资料信息中材料名称为必填项，请将材料名称补充完整！"
				);
				return false;
			}
			//验证土地信息表单
			this.$refs["planningFormLand"].validate(valid => {
				if (valid) {
					this.$refs["planningForm"].validate(valid => {
						if (valid) {
							this.planningForm.propertyBasic.projectId = this.isEdit;
							let areaCode =
								this.planningForm.landInfo.cityCode +
								"-" +
								this.planningForm.landInfo.countyCode +
								"-" +
								this.planningForm.landInfo.street +
								"-" +
								this.planningForm.landInfo.community;
							validateProject({
								addressInfo: this.planningForm.landInfo.addressInfo,
								areaCode: areaCode,
								propertyName: this.planningForm.propertyBasic.projectName,
								projectId: this.projectId,
								propertyRight:'1'
							}).then(res => {
								if (res.data.data === 1) {
									this.$message.error("该项目名称已存在，请重新输入！");
								} else {
									this.loadAction("保存中");
									planprojectEdit(this.planningForm)
										.then(res => {
											this.loadClose();
											this.$message.success("保存成功！");
											this.closeNowRouter();
										})
										.catch(e => {
											console.log(e);
											this.loadClose();
											this.$message.error("保存失败");
										});
								}
							});
						} else {
							this.$message.error("项目信息表单校验失败，请检查");
							return false;
						}
					});
				} else {
					this.$message.error("土地信息表单校验失败，请检查");
					return false;
				}
			});
		},
		// hxf-0828-保存
		handlePreservation() {
			this.planningForm.landInfo.areaCode =
				this.planningForm.landInfo.cityCode +
				"-" +
				this.planningForm.landInfo.countyCode;
			this.planningForm.landInfo.address =
				this.getCardTypeValue(
					this.planningForm.landInfo.cityCode,
					this.cityAssembly
				) +
				this.getCardTypeValue(
					this.planningForm.landInfo.countyCode,
					this.districtCollection
				) +
				this.getCardTypeValue(
					this.planningForm.landInfo.street,
					this.projectStreet
				) +
				this.getCardTypeValue(
					this.planningForm.landInfo.community,
					this.communityNames
				);

			//给对象赋值
			this.planningForm.mapCoordinates = this.planningForm.propertyBasic.mapCoordinates;
			this.planningForm.manageUnit = this.planningForm.propertyBasic.manageUnit;
			//校验里程碑名称是否已填
			if (!this.materialsName) {
				this.$message.error(
					"资料信息中材料名称为必填项，请将材料名称补充完整！"
				);
				return false;
			}
			this.$refs["planningFormLand"].validate(valid => {
				if (valid) {
					this.$refs["planningForm"].validate(valid => {
						if (valid) {
							let areaCode =
								this.planningForm.landInfo.cityCode +
								"-" +
								this.planningForm.landInfo.countyCode +
								"-" +
								this.planningForm.landInfo.street +
								"-" +
								this.planningForm.landInfo.community;
							validateProject({
								addressInfo: this.planningForm.landInfo.addressInfo,
								areaCode: areaCode,
								propertyName: this.planningForm.propertyBasic.projectName,
								propertyRight:'1'
							}).then(res => {
								if (res.data.data === 1) {
									this.$message.error("该项目名称已存在，请重新输入！");
								} else {
									this.loadAction("保存中");
									planprojectAdd(this.planningForm)
										.then(res => {
											this.loadClose();
											this.$message.success("保存成功！");
											this.closeNowRouter();
										})
										.catch(e => {
											console.log(e);
											this.loadClose();
											this.$message.error("保存失败");
										});
								}
							});
						} else {
							this.$message.error("项目信息表单校验失败，请检查");
							return false;
						}
					});
				} else {
					this.$message.error("土地信息表单校验失败，请检查");
					return false;
				}
			});
		},
		closeNowRouter() {
			this.$store.commit("delLastRoute", this.$route.path);
		},
		// hxf-0901-回显
		handleGetPlanproject() {
			if (this.isEdit) {
				getPlanproject(this.isEdit).then(res => {
					if (res.data.code === 0) {
						this.planningForm = res.data.data;
						//给对象赋值-地图坐标
						this.$set(
							this.planningForm.propertyBasic,
							"manageUnit",
							this.planningForm.manageUnit
						);
						this.$set(
							this.planningForm.propertyBasic,
							"mapCoordinates",
							this.planningForm.mapCoordinates
						);
						if (this.planningForm.projectMilestones.length > 0) {
							this.milestoneSortIndex = this.planningForm.projectMilestones.findIndex(
								pm => pm.milestoneName == this.planningForm.propertyBasic.stage
							);
							if (this.milestoneSortIndex == -1) {
								this.milestoneSortIndex = 0;
							}
							this.milestoneSort = (this.milestoneSortIndex + 1).toString();
							let obj = {
								tempId: this.planningForm.propertyBasic.isUpdate,
								projectId: this.isEdit
							};
							this.$refs.milestones.handleClickToUpdateItem(obj);
						} else {
							let obj = {
								tempId: this.planningForm.propertyBasic.isUpdate,
								projectId: ""
							};
							this.$refs.milestones.handleClickToUpdateItem(obj);
						}
						this.$refs.milestones.handleClick({
							index: this.milestoneSortIndex
						});
						this.$forceUpdate();
						this.isThereAMilestone = res.data.data.propertyBasic.isUpdate;
						this.projectId = res.data.data.propertyBasic.projectId;
						this.initCommunityName();
					}
				});
			}
		},
		// hxf-0901-验证项目编号是否存在
		landCodeOnly(value) {
			validateLandCode({
				landCode: value,
				landId: this.planningForm.landInfo.landId || ""
			}).then(res => {
				if (res.data.data !== 0) {
					this.$message.warning("宗地号已存在，请确认是否继续操作");
				}
			});
		},
		validateProjectCountryCode(value) {
			validateProjectCountryCode(value, this.projectId || "").then(res => {
				if (res.data.data !== 0) {
					this.$message.error("项目国家编码已存在，请重新输入");
					this.planningForm.propertyBasic.projectCountryCode = "";
				}
			});
		},
		// hxf-0928-添加
		superInforAdd() {
			this.dialogVisible = true;
			this.bulletInformation = {
				buildingName: "", //楼栋名称 jiaj
				floorNum: "", //楼层号 jiaj
				propertyName: "",
				usedName: "",
				address: this.getAddressStr(),
				initialType: "",
				parkingSpotNum: "",
				floorSpace: "",
				buildArea: "",
				operation: "20",
				insideArea: "",
				unmanagedArea: "",
				managedArea: "",
				noApportionedArea: "",
				projectType: "", //物业类型 jiaj
				projectSubclassType: "", //物业用途 jiaj
				transferType: "", //移交方式 jiaj
				transferUnit: "", //移交单位 jiaj
				receiveTime: "", //移交时间 jiaj
				transferDesc: "", //移交描述 jiaj
				remarks: "",
				projectSubdivide: "", //物业细类
				facilityNumber: "",
				facilityUnit: "",
				facilityLevel: "",
				radius: "",
				floorHeight: "",
				loadBearing: "",
				thickness: "",
				clearHeight:"",
				statisticType:"1"
			};
			this.bulletInformationFlag = true;
			this.bulletInformationIndex = 0;
		},
		// 获取地址信息
		getAddressStr() {
			const {
				cityCode,
				countyCode,
				street,
				community,
				addressInfo
			} = this.planningForm.landInfo;
			let cityName = this.getLabel(cityCode, this.cityAssembly);
			let countyName = this.getLabel(countyCode, this.districtCollection);
			let streetName = this.getLabel(street, this.projectStreet);
			let communityName = this.getLabel(community, this.communityNameStreet);
			return `${cityName}${countyName}${streetName}${communityName}${addressInfo}`;
		},
		// 获取label名称
		getLabel(value, arr) {
			if (value == "") return "";
			let item = arr.find(i => i.value == value);
			return item?.label || "";
		},
		// hxf-0928-弹框取消
		handleDialogClear() {
			this.dialogVisible = false;
			this.bulletInformation = {};
		},
		// hxf-0928-弹框确认
		handleDialogClick() {
			this.$refs["bulletInformation-form"].validate(valid => {
				if (valid) {
					if (this.bulletInformationFlag) {
						this.planningForm.supportingFacilities.push(this.bulletInformation);
						this.handleDialogClear();
					} else {
						this.planningForm.supportingFacilities[
							this.bulletInformationIndex
						] = this.bulletInformation;
						this.handleDialogClear();
					}
				} else {
					this.$message.error("保存失败！");
					return false;
				}
			});
		},
		// hxf-0914-建筑面积失焦校验
		handleFloorAreaBlur() {
			if (this.bulletInformation.buildArea) {
				if (
					Number(this.bulletInformation.buildArea) <
					Number(this.bulletInformation.insideArea)
				) {
					this.$message.error("套内面积不能大于建筑面积！");
					this.bulletInformation.insideArea = "";
				}
			}
		},
		dataFormat(row, column, cellValue) {
			if (!cellValue) {
				return "";
			}
			return moment(cellValue).format("YYYY-MM-DD");
		},
		getCardTypeValue(num, sum) {
			let arr = sum.filter(e => e.value === num);
			if (arr.length > 0) {
				return arr[0].label;
			} else {
				return num;
			}
		},
		// 监听地图坐标监听
		resetPosition(data) {
			let position = JSON.parse(data);
			if (position.type == "location") {
				console.log(position.value);
				if (position.value.length) {
					this.planningForm.propertyBasic.mapCoordinates = position.value.join(
						","
					);
				}
				this.planningForm.propertyBasic.mapObject = position.mapObject
					? JSON.stringify(position.mapObject)
					: "";
			} else {
				//planningForm.propertyBasic.fenceCoordinates
				this.planningForm.propertyBasic = {
					...this.planningForm.propertyBasic,
					fenceCoordinates: JSON.stringify(position.value)
				};
			}
		},
		// hxf-1009-区改变清空街道和社区
		handleChangeCountyCode() {
			this.planningForm.landInfo.street = "";
			this.planningForm.landInfo.community = "";
			this.planningForm.landInfo.addressInfo = "";
		},
		// hxf-1009-街道改变清空社区
		handleChangeStreet() {
			//从街道数据字典中获取社区数据
			this.initCommunityName();
			this.planningForm.landInfo.community = "";
			this.planningForm.landInfo.addressInfo = "";
		},
		//社区改变清空门牌号
		handleChangeCommunity() {
			this.planningForm.landInfo.addressInfo = "";
		},
		initCommunityName() {
			this.communityNameStreet.length = 0;
			let selectStreet = this.planningForm.landInfo.street;
			if (
				selectStreet != "" &&
				this.communityNames != null &&
				this.communityNames.length > 0
			) {
				this.communityNameStreet = this.communityNames.filter(
					cn => cn.value.indexOf(selectStreet) != -1
				);
			}
		},
		handleValidateProject(value) {
			if (
				this.planningForm.landInfo.cityCode === "" ||
				this.planningForm.landInfo.countyCode === "" ||
				this.planningForm.landInfo.street === "" ||
				this.planningForm.landInfo.community === ""
			) {
				this.$message.error("请将市区街道社区填写完整后再填写详细地址！");
				this.planningForm.landInfo.addressInfo = "";
			} else {
				let areaCode =
					this.planningForm.landInfo.cityCode +
					"-" +
					this.planningForm.landInfo.countyCode +
					"-" +
					this.planningForm.landInfo.street +
					"-" +
					this.planningForm.landInfo.community;
				validateProject({
					addressInfo: value,
					areaCode: areaCode,
					projectId: this.projectId,
					propertyRight:'1'
				}).then(res => {
					if (res.data.data === 2) {
						this.$message.warning("该地址已存在！");
					}
				});
			}
		},
		//验证物业名称唯一
		propertyNameOnly(value) {
			validateFacRoomName({
				roomName: value,
				facId: this.bulletInformation.facilitiesId || ""
			}).then(res => {
				if (res.data.data !== 0) {
					this.$message.error("物业名称已存在，请重新输入！");
					this.bulletInformation.propertyName = "";
				}
			});
		},
		// 经营面积失焦校验
		handleAreaCompared() {
			if (
				Number(this.bulletInformation.unmanagedArea) +
					Number(this.bulletInformation.managedArea) >
				Number(this.bulletInformation.buildArea)
			) {
				this.$message.error("经营面积+非经营面积之和不能大于建筑面积！");
				this.bulletInformation.managedArea = "";
			}
		},
		//分摊共用面积
		handleNoAppor() {
			if (
				Number(this.bulletInformation.noApportionedArea) >
				Number(this.bulletInformation.buildArea)
			) {
				this.$message.error("分摊公用面积不能大于建筑面积！");
				this.bulletInformation.noApportionedArea = "";
			}
		},
		//物业小类发生变化时，重置物业细类的值。
		changeProjectSubclassType(val) {
			this.bulletInformation.projectSubdivide = "";
		},
		autoSetPropertyName() {
			let rname = this.planningForm.propertyBasic.projectName;
			if (this.bulletInformation.buildingName.length > 0) {
				rname += this.bulletInformation.buildingName;
			}
			if (this.bulletInformation.floorNum.length > 0) {
				rname += this.bulletInformation.floorNum + "层";
			}
			if (rname.length > 0) {
				this.bulletInformation.propertyName = rname;
			}
		}
	},
	filters: {
		ellipsis(value) {
			if (!value) {
				return "";
			} else if (value.length > 6) {
				return value.slice(0, 6) + "...";
			} else {
				return value;
			}
		}
	}
};
