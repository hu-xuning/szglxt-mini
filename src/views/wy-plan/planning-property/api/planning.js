import request from '@/plugins/axios'

export function planProjectPage(query) {
    return request({
        url: '/api/planproject/page',
        method: 'get',
        params: query
    })
}

export function planprojectAdd(data) {
    return request({
        url: '/api/planproject',
        method: 'post',
        data: data
    })
}

export function planprojectEdit(data) {
    return request({
        url: '/api/planproject',
        method: 'put',
        data: data
    })
}
export function getPlanproject(id) {
    return request({
        url: '/api/planproject/' + id,
        method: 'get'
    })
}
export function validateprojectCode(param) {
    return request({
        url: '/api/propertybasic/validateProjectCode',
        method: 'get',
        params: param
    })
}
export function planToUnderConstruction(param) {
    return request({
        url: '/api/propertybasic/planToUnderConstruction',
        method: 'post',
        data: param
    })
}

export function initMilestones(query) {
    return request({
        url: '/api/planproject/initMilestones',
        method: 'get',
        params: query
    })
}


export function validateProject(query){
    return request({
        url: '/api/rentproperty/validateProject',
        method: 'get',
        params: query
    })
}
//验证宗地号是否重复
export function validateLandCode(query){
    return request({
        url: '/api/landinfo/validateLandCode',
        method: 'get',
        params: query
    })
}

/**
 * 验证项目国家编码是否存在
 * @param query
 */
export function validateProjectCountryCode(code, id) {
    return request({
        url: '/api/propertybasic/validateProjectCountryCode',
        method: 'get',
        params: {"projectCountryCode": code, "projectId": id}
    })
}


export function get(projectId){
    return request({
        url: '/api/propertybasic/' + projectId,
        method: 'get'
    })
}
