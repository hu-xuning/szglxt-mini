export default {
	path: "/wy-plan",
	component: () => import("~/layout/index"),
	children: [
		{
			path: "project-doc-setting",
			meta: { title: "项目档案设置" },
			component: () =>
				import("~/views/wy-plan/project-doc-setting/list/index.vue")
		},
		{
			path: "project-doc-setting/edit",
			name: "project_doc_setting_add",
			meta: { title: "新增" },
			component: () =>
				import("~/views/wy-plan/project-doc-setting/edit/index.vue")
		},
		{
			path: "project-doc-setting/edit",
			name: "project_doc_setting_edit",
			meta: { title: "编辑" },
			component: () =>
				import("~/views/wy-plan/project-doc-setting/edit/index.vue")
		},
		{
			path: "planning-property",
			meta: { title: "规划物业" },
			component: () =>
				import("~/views/wy-plan/planning-property/list/index.vue")
		},
		{
			path: "planning-property/add",
			name: "planning_add",
			meta: { title: "新增" },
			component: () =>
				import("~/views/wy-plan/planning-property/edit/planningEdit.vue")
		},
		{
			path: "planning-property/edit",
			name: "planning_edit",
			meta: { title: "编辑" },
			component: () =>
				import("~/views/wy-plan/planning-property/edit/planningEdit.vue")
		},
		{
			path: "projects-under-construction",
			meta: { title: "在建项目" },
			component: () =>
				import("~/views/wy-plan/projects-under-construction/list/index.vue")
		},
		{
			path: "projects-under-construction/add",
			name: "construction_add",
			meta: { title: "新增" },
			component: () =>
				import(
					"~/views/wy-plan/projects-under-construction/edit/constructionEdit.vue"
				)
		},
		{
			path: "projects-under-construction/edit",
			name: "construction_edit",
			meta: { title: "编辑" },
			component: () =>
				import(
					"~/views/wy-plan/projects-under-construction/edit/constructionEdit.vue"
				)
		},
		{
			path: "fg-project",
			meta: { title: "发改立项信息查看" },
			component: () => import("~/views/wy-plan/fg-project/list/index.vue")
		},
		{
			path: "planning-property/look",
			name: "planning_look",
			component: () =>
				import(
					"~/views/wy-plan/planning-property/newLook/planning-property_look.vue"
				),
			meta: { title: "规划项目查看" }
		},
		{
			path: "projects-under-construction/look",
			name: "construction_look",
			component: () =>
				import(
					"~/views/wy-plan/projects-under-construction/newLook/construction_look.vue"
				),
			meta: { title: "在建项目查看" }
		}
	]
};
