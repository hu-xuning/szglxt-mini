import { mapGetters } from "vuex";
import BaseLoad from "@/assets/vue-mixin/BaseLoad";
import supportFacilityInformation from "./supporting-facilities-look";
import { get } from "../api/construction.js";
import { checkFiles } from "@/api/operate-construction-contract";
import { download } from "~/admin/api/common";
import { downloadBlob } from "~/utils/index";
import positionDialog from "@/components/positionDialog";
import { initMilestones } from "@/views/wy-plan/planning-property/api/planning";
export default {
	name: "construction_look",
	mixins: [BaseLoad],
	created() {
		// 如果是内嵌页面预览隐藏头部和左侧菜单
		const { projectId, frame } = this.$route.query;
		frame && this.$store.commit("resetWebViewStyle", true);
		this.isEdit = projectId;
		this.initCitySelector();
		this.projectStreet = this.getDict("streets");
		this.communityNames = this.getDict("community_names");
		this.landUses = this.getDict("land_uses");
		this.isUpdates = this.getDict("milestones_isUpdate");
		this.projectType = this.getDict("project_types");
		this.manageUnits = this.getDict("administrative_office");
		this.propertyTypes = this.getDict("property_types");
		this.propertyUseTypes = this.getDict("property_use_types");
		this.transferModeOption = this.getDict("transfer_types");
		this.propertyChannels = this.getDict("property_channel");
		this.propForm.landInfo.cityCode = this.cityAssembly[0].value;
		this.propForm.landInfo.countyCode = this.districtCollection[0].value;
	},
	computed: {
		...mapGetters({
			cityAssembly: "getCityAssembly",
			districtCollection: "getDistrictCollection"
		})
	},
	components: {
		supportFacilityInformation,
		positionDialog
	},
	data() {
		return {
			propForm: {
				landInfo: {
					parcelNum: "",
					parcelArea: "",
					landUse: "",
					landContractNum: "",
					cityCode: "",
					countyCode: "",
					street: "",
					community: "",
					addressInfo: ""
				},
				propertyBasic: {
					projectCode: "",
					projectName: "",
					projectArea: "",
					projectType: "",
					manageUnit: "",
					isUpdate: "",
					constructionUnit: "",
					contactName: "",
					contactPhone: "",
					stage: "",
					mapCoordinates: "",
					remarks: ""
				},
				supportingFacilities: [],
				projectMilestones: [],
				totalBuilding: 0
			},
			isEdit: "",
			projectId: "",
			selected: "",
			milestoneSort: "1",
			milestoneSortIndex: 0,
			addSource: true,
			isAdd: true,
			isUpdates: [],
			multipleSelection: [],
			projectStreet: [],
			landUses: [],
			communityNames: [],
			communityNameStreet: [],
			projectType: [],
			manageUnits: [],
			propertyTypes: [],
			propertyUseTypes: [],
			transferModeOption: [],
			propertyChannels: []
		};
	},
	methods: {
		//社区联动
		initCommunityName() {
			this.communityNameStreet.length = 0;
			let selectStreet = this.propForm.landInfo.street;
			if (
				selectStreet != "" &&
				this.communityNames != null &&
				this.communityNames.length > 0
			) {
				this.communityNameStreet = this.communityNames.filter(
					cn => cn.value.indexOf(selectStreet) != -1
				);
			}
		},
		//配套信息查看
		handleClickLook(item) {
			this.$refs.supportFacilityInformation.showDialog(item);
		},
		//资料信息选择序号列表
		handleSelectionChange(val) {
			this.multipleSelection = val;
		},
		//对应value取label
		getCardTypeValue(num, sum) {
			let arr = sum.filter(e => e.value === num);
			if (arr.length > 0) {
				return arr[0].label;
			} else {
				return num;
			}
		},
		// 初始化
		initCitySelector() {
			const loading = this.$loading({
				lock: true,
				text: "数据加载中"
			});
			get(this.isEdit)
				.then(rep => {
					this.isAdd = false;
					if (rep.data.data.propertyBasic !== null) {
						this.propForm.propertyBasic = rep.data.data.propertyBasic;
						this.propForm.usedName = rep.data.data.usedName;
						this.propForm.totalBuilding = rep.data.data.totalBuilding;
						this.projectId = rep.data.data.propertyBasic.projectId;
						if (this.propForm.propertyBasic.addSource === "1") {
							this.addSource = false;
						}
					}
					if (rep.data.data.projectMilestones !== null) {
						this.propForm.projectMilestones = rep.data.data.projectMilestones;
						if (this.propForm.projectMilestones.length > 0) {
							this.milestoneSortIndex = this.propForm.projectMilestones.findIndex(
								pm => pm.milestoneName == this.propForm.propertyBasic.stage
							);
							if (this.milestoneSortIndex === -1) {
								this.milestoneSortIndex = 0;
							}
							this.milestoneSort = (this.milestoneSortIndex + 1).toString();
						} else {
							let obj = {
								tempId: this.propForm.propertyBasic.isUpdate,
								projectId: ""
							};
							initMilestones(obj).then(res => {
								if (res.data.code === 0) {
									this.propForm.projectMilestones = res.data.data;
								} else {
									this.$message.error("未获取到里程碑信息！");
								}
							});
						}
					}
					if (rep.data.data.landInfo !== null) {
						this.propForm.landInfo = rep.data.data.landInfo;
						this.initCommunityName();
					}
					if (rep.data.data.supportingFacilities !== null) {
						this.propForm.supportingFacilities =
							rep.data.data.supportingFacilities;
					}
					this.$set(
						this.propForm.propertyBasic,
						"manageUnit",
						rep.data.data.manageUnit
					);
					this.$set(
						this.propForm.propertyBasic,
						"mapCoordinates",
						rep.data.data.mapCoordinates
					);
					loading.close();
				})
				.catch(e => {
					console.log(e);
					loading.close();
					this.$message({
						type: "error",
						message: "数据获取失败!"
					});
				});
		},
		//关闭
		closeTab() {
			this.$store.commit("delLastRoute", this.$route.path);
		},
		//资料信息文件下载
		handleDownload(item) {
			console.log(item);
			checkFiles(item.fileId).then(resp => {
				if (resp.data.code === 0) {
					download("/admin/sys-file/download/" + item.fileId, null).then(
						response => {
							downloadBlob(response.data, item.original);
						}
					);
				} else {
					this.$message({
						message: `文件不存在！`,
						type: "warning"
					});
				}
			});
		}
	}
};
