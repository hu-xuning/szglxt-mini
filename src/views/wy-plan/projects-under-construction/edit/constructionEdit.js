import {
	doubleValidate,
	validatePhone,
	isInt,
	mapVerification
} from "~/utils/validate.js";
import {
	underconstructionAdd,
	underconstructionEdit,
	getUnderconstruction,
	validateprojectCode,
	validateProject
} from "../api/construction";
import { mapGetters, mapState } from "vuex";
import positionDialog from "@/components/positionDialog";
import BaseLoad from "@/assets/vue-mixin/BaseLoad";
import {
	validateFacRoomName,
	validateProjectCountryCode
} from "~/views/wy-info/project-manage/api/project_manage";
import { validateLandCode } from "@/views/wy-plan/planning-property/api/planning.js";
import projectMilestones from "@/views/wy-plan/planning-property/components/project-milestones";import moment from "moment";
export default {
	name: "project-construction",
	mixins: [BaseLoad],
	components: { positionDialog, projectMilestones },
	data() {
		let floorNumValidator = (rule, value, callback) => {
			const re = /^-?[1-9]\d*$/;
			const rsCheck = re.test(value);
			if (!rsCheck) {
				callback(new Error("请输入-20 至 200之间非0的整数"));
			} else {
				if (value > 200) {
					return callback(new Error("最大值不超过200"));
				} else if (value < -20) {
					return callback(new Error("最小值不能小于-20"));
				} else {
					callback();
				}
			}
		};
		return {
			// hxf-0828-项目信息
			formPlanning: {
				// hxf-0901-项目信息
				propertyBasic: {
					projectId: "",
					projectCode: "",
					projectName: "",
					projectCountryCode: "",
					projectArea: "",
					projectType: "",
					projectStreet: "",
					isUpdate: "2",
					stage: ""
				},
				/*// hxf-0901-项目信息-项目地址
                projectAddr: '',*/
				// hxf-0901-配套信息
				supportingFacilities: [],
				// hxf-0910-资料信息
				projectMilestones: [],
				usedName: "",
				landInfo: {
					address: "",
					addressInfo: "",
					cityCode: "",
					community: "",
					countyCode: "",
					manageUnit: "",
					landContractNum: "",
					landId: "",
					landUse: "",
					parcelArea: "",
					parcelNum: "",
					remarks: "",
					street: "",
					areaCode: "",
					areaName: ""
				}
			},
			// hxf-0929-土地信息校验
			landInfo_rules: {
				parcelNum: [
					{ required: false, message: "请输入宗地号", trigger: "blur" },
					{ min: 0, max: 30, message: "长度在0-30个字符串", trigger: "blur" }
				],
				parcelArea: [
					{ required: false, message: "请输入宗地面积(m²)", trigger: "blur" },
					{ validator: doubleValidate, trigger: "blur" }
				],
				landUse: [
					{ required: false, message: "请输入土地用途", trigger: "blur" }
				],
				landContractNum: [
					{ required: false, message: "请输入土地合同编号", trigger: "blur" },
					{ min: 0, max: 100, message: "长度在0-100个字符串", trigger: "blur" }
				],
				cityCode: [
					{
						required: true,
						validator: (rules, value, callback) => {
							//区县
							let county = this.formPlanning.landInfo.countyCode;
							//   街道
							let street = this.formPlanning.landInfo.street;
							//   社区
							let community = this.formPlanning.landInfo.community;
							//   管理所
							let addressInfo = this.formPlanning.landInfo.addressInfo;
							if (!value || !county || !street || !community || !addressInfo) {
								return callback(new Error("物业地址不能为空"));
							}
							return callback();
						},
						trigger: "blur"
					}
				],
				addressInfo: [
					{ required: true, message: "地址不能为空", trigger: "blur" },
					{ min: 0, max: 50, message: "长度在0-50个字符串", trigger: "blur" }
				]
			},
			// hxf-0831-项目信息校验
			propertyBasic_rules: {
				// projectCode: [
				//     { required: true, message: "请输入项目编号", trigger: "blur" },
				//     { min: 0, max: 30, message: "长度在0-30个字符串", trigger: "blur" },
				//     { validator: validateprojectCode, trigger: "change" },
				// ],
				projectName: [
					{ required: true, message: "请输入项目名称", trigger: "blur" },
					{ min: 0, max: 30, message: "长度在0-30个字符串", trigger: "blur" }
				],
				projectArea: [
					{ required: true, message: "请输入项目面积", trigger: "blur" },
					{ validator: doubleValidate, trigger: "blur" }
				],
				contactPhone: [
					{ required: false },
					{ validator: validatePhone, trigger: "blur" }
				],
				mapCoordinates: [
					{ required: false, message: "请输入地图坐标", trigger: "blur" },
					{ min: 0, max: 50, message: "长度在0-50个字符串", trigger: "blur" },
					{ validator: mapVerification, trigger: "blur" }
				],
				projectCountryCode: [
					{ required: false, message: "请输入项目国家编号", trigger: "blur" },
					{ min: 0, max: 100, message: "长度在0-100个字符串", trigger: "blur" }
				],
				projectType: [
					{ required: true, message: "请输入物业大类", trigger: "blur" },
					{ min: 0, max: 30, message: "长度在0-30个字符串", trigger: "blur" }
				],
				/*manageUnit: [
					{ required: true, message: "请选择管理权属", trigger: "change" }
				]*/ /*,
                projectStreet: [
                    { required: true, message: "请输入项目街道", trigger: "blur" },
                    { min: 0, max: 30, message: "长度在0-30个字符串", trigger: "blur" },
                ],
                stage: [
                    { required: true, message: "请输入当前阶段", trigger: "blur" },
                    { min: 0, max: 30, message: "长度在0-30个字符串", trigger: "blur" },
                ],
                projectAddr: [{ validator: validateprojectAddr, trigger: 'blur' }]*/
			},
			// hxf-0929-配套信息校验
			bulletInformation_rules: {
				// buildingName: [
				// 	{ required: true, message: "请输入楼栋名称", trigger: "blur" },
				// 	{ min: 0, max: 25, message: "长度在0-25个字符串", trigger: "blur" }
				// ],
				// floorNum: [
				// 	{ required: true, message: "请输入楼层", trigger: "blur" },
				// 	{ validator: floorNumValidator, trigger: "blur" }
				// ],
				propertyName: [
					{ required: true, message: "请输入物业名称", trigger: "blur" },
					{ min: 0, max: 30, message: "长度在0-30个字符串", trigger: "blur" }
				],
				usedName: [
					{ required: false, message: "请输入曾用名", trigger: "blur" },
					{ min: 0, max: 30, message: "长度在0-30个字符串", trigger: "blur" }
				],
				address: [
					{ required: true, message: "请输入物业地址", trigger: "blur" },
					{ min: 0, max: 50, message: "长度在0-50个字符串", trigger: "blur" }
				],
				initialType: [
					{ required: false, message: "请选择初始类别", trigger: "blur" }
				],
				planUse: [
					{ required: true, message: "请输入规划用途", trigger: "blur" },
					{ min: 0, max: 100, message: "长度在0-100个字符串", trigger: "blur" }
				],
				parkingSpotNum: [
					{ required: false, message: "请输入停车位数", trigger: "blur" },
					{ validator: isInt, trigger: "blur" }
				],
				totalBuildings: [
					{ required: true, message: "请输入总栋数", trigger: "blur" },
					{ validator: isInt, trigger: "blur" }
				],
				useLandArea: [
					{ required: true, message: "请输入用地面积", trigger: "blur" },
					{ validator: doubleValidate, trigger: "blur" }
				],
				floorSpace: [
					{ required: false, message: "请输入占地面积", trigger: "blur" },
					{ validator: doubleValidate, trigger: "blur" }
				],
				buildArea: [
					{ required: false, message: "请输入建筑面积", trigger: "blur" },
					{ validator: doubleValidate, trigger: "blur" }
				],
				insideArea: [
					{ required: false, message: "请输入套内面积", trigger: "blur" },
					{ validator: doubleValidate, trigger: "blur" }
				],
				operation: [
					{ required: true, message: "请选择经营类型", trigger: "blur" }
				],
				useArea: [
					{ required: false, message: "请输入使用面积", trigger: "blur" },
					{ validator: doubleValidate, trigger: "blur" }
				],
				unmanagedArea: [
					{ required: false, message: "请输入非经营面积", trigger: "blur" },
					{ validator: doubleValidate, trigger: "blur" }
				],
				managedArea: [
					{ required: false, message: "请输入经营面积", trigger: "blur" },
					{ validator: doubleValidate, trigger: "blur" }
				],
				noApportionedArea: [
					{ required: false, message: "请输入不分摊面积", trigger: "blur" },
					{ validator: doubleValidate, trigger: "blur" }
				],
				buildPrice: [
					{ required: false, message: "请输入建购价款", trigger: "blur" },
					{ validator: doubleValidate, trigger: "blur" }
				],
				obligee: [
					{ required: true, message: "请输入权利人", trigger: "blur" },
					{ min: 0, max: 30, message: "长度在0-30个字符串", trigger: "blur" }
				],
				ownershipNature: [
					{ required: true, message: "请选择权利性质", trigger: "blur" }
				],
				endTime: [
					{ required: true, message: "请选择竣工日期", trigger: "blur" }
				],
				propertySource: [
					{ required: true, message: "请选择项目来源", trigger: "blur" }
				],
				mapCoordinates: [
					{ required: false, message: "请输入地图坐标", trigger: "blur" },
					{ min: 0, max: 30, message: "长度在0-30个字符串", trigger: "blur" },
					{ validator: mapVerification, trigger: "blur" }
				],
				projectType: [
					{ required: true, message: "请输入物业类型", trigger: "blur" },
					{ min: 0, max: 30, message: "长度在0-30个字符串", trigger: "blur" }
				],
				projectSubclassType: [
					{ required: true, message: "请输入物业用途", trigger: "blur" },
					{ min: 0, max: 30, message: "长度在0-30个字符串", trigger: "blur" }
				],
				transferMode: [
					{ required: true, message: "请输入移交方式", trigger: "blur" },
					{ min: 0, max: 30, message: "长度在0-30个字符串", trigger: "blur" }
				],
				facilityNumber: [
					{ required: false, message: "请输入公服数量", trigger: "blur" },
					{ validator: doubleValidate, trigger: "blur" }
				],
				facilityUnit: [
					{ required: false, message: "请选择公服单位", trigger: "blur" }
				],
				facilityLevel: [
					{ required: false, message: "请选择公服层级", trigger: "blur" }
				],
				// radius: [
				// 	{ required: true, message: "请输入覆盖半径", trigger: "blur" },
				// 	{ validator: doubleValidate, trigger: "blur" }
				// ],
				floorHeight: [
					{ required: false, message: "请输入层高", trigger: "blur" },
					{ validator: doubleValidate, trigger: "blur" }
				],
				clearHeight: [
					{ required: false, message: "请输入净高", trigger: "blur" },
					{ validator: doubleValidate, trigger: "blur" }
				],
				loadBearing: [
					{ required: false, message: "请输入承重", trigger: "blur" },
					{ validator: doubleValidate, trigger: "blur" }
				],
				thickness: [
					{ required: false, message: "请输入楼板厚度", trigger: "blur" },
					{ validator: doubleValidate, trigger: "blur" }
				],
				statisticType: [
					{ required: true, message: "请选择物业统计类别", trigger: "blur" }
				  ],
			},
			//hxf-0831-新增标识
			isEdit: "",
			isAdd: true,
			milestoneSort: "1",
			milestoneSortIndex: 0,
			isThereAMilestone: "",
			projectId: "",
			materialsName: true,
			// activeIndex: 0,
			selected: "",
			addSource: true,
			dialogVisible: false,
			bulletInformation: {
				propertyName: "",
				facilitiesId: "",
				usedName: "",
				address: "",
				initialType: "",
				planUse: "",
				parkingSpotNum: "",
				totalBuildings: "",
				useLandArea: "",
				floorSpace: "",
				buildArea: "",
				insideArea: "",
				operation: "20",
				useArea: "",
				unmanagedArea: "",
				managedArea: "",
				noApportionedArea: "",
				buildPrice: "",
				obligee: "",
				ownershipNature: "",
				endTime: "",
				propertySource: "",
				mapCoordinates: "",
				projectType: "", //物业类型 jiaj
				projectSubclassType: "", //物业用途 jiaj
				transferType: "", //移交方式 jiaj
				transferUnit: "", //移交单位 jiaj
				receiveTime: "", //移交时间 jiaj
				transferDesc: "", //移交描述 jiaj
				remarks: "",
				projectSubdivide: "", //物业细类 jiaj
				facilityNumber: "",
				facilityUnit: "",
				facilityLevel: "",
				radius: "",
				floorHeight: "",
				loadBearing: "",
				thickness: "",
				clearHeight:"",
				statisticType:"1"
			},
			bulletInformationFlag: true,
			bulletInformationIndex: 0,
			communityNameStreet: [],
			propertyUseTypesFilter: []
		};
	},
	created() {
		if (this.$route.meta.title !== "新增") {
			this.isAdd = false;
		}
		this.formPlanning.landInfo.cityCode = this.cityAssembly[0].value;
		this.formPlanning.landInfo.countyCode = this.districtCollection[0].value;
	},
	mounted() {
		this.isEdit = this.$route.query.isEdit;
		this.handleGetPlanproject();
		if (!this.isEdit) {
			let obj = {
				tempId: this.formPlanning.propertyBasic.isUpdate,
				projectId: ""
			};
			this.$refs.milestones.handleClickToUpdateItem(obj);
			// hxf-0910-tab选中项
			this.$refs.milestones.handleClick({ index: 0 });
		}
	},
	computed: {
		planUses() {
			return this.getDict("property_use_types");
		},
		initialTypes() {
			return this.getDict("project_init_type");
		},
		ownershipNatureTypes() {
			return this.getDict("asset-ownership-nature");
		},
		landUses() {
			return this.getDict("land_uses");
		},
		isUpdates() {
			return this.getDict("milestones_isUpdate");
		},
		propertyTypes() {
			return this.getDict("property_types");
		},
		operations() {
			return this.getDict("property_operations");
		},
		propertyUseTypes() {
			return this.getDict("property_use_types");
		},
		projectSubdivides() {
			return this.getDict("project_subdivide");
		},
		facilityLevels() {
			return this.getDict("facility_levels");
		},
		facilityUnits() {
			return this.getDict("facility_units");
		},
		projectType() {
			return this.getDict("project_types");
		},
		projectStreet() {
			return this.getDict("streets");
		},
		transferModeOption() {
			return this.getDict("transfer_types");
		},
		communityNames() {
			return this.getDict("community_names");
		},
		propertySources() {
			return this.getDict("property_source");
		},
		manageUnits() {
			return this.getDict("administrative_office");
		},
		statisticTypes() {
			return this.getDict("statistic_types");
		},
		...mapGetters({
			cityAssembly: "getCityAssembly",
			districtCollection: "getDistrictCollection"
		})
	},
	watch: {
		"formPlanning.propertyBasic.isUpdate": function() {
			if (this.isThereAMilestone === this.formPlanning.propertyBasic.isUpdate) {
				this.addSource &&
					this.$refs.milestones.handleClickToUpdateItem({
						tempId: this.formPlanning.propertyBasic.isUpdate,
						projectId: this.projectId
					});
			} else {
				this.addSource &&
					this.$refs.milestones.handleClickToUpdateItem({
						tempId: this.formPlanning.propertyBasic.isUpdate,
						projectId: ""
					});
			}
		}
	},
	methods: {
		//里程碑赋值
		fileBusId(data) {
			this.formPlanning.projectMilestones = data;
		},
		//里程碑校验
		isMaterialsName(data) {
			this.materialsName = data;
		},
		//jiaj 根据物业类型，获取对应的物业用途
		changeProjectType(val) {
			//重置物业用途
			this.propertyUseTypesFilter = [];
			if (val) {
				this.bulletInformation.projectSubclassType = "";
				this.bulletInformation.projectSubdivide = "";
				//过滤物业用途
				this.propertyUseTypesFilter = this.propertyUseTypes.filter(
					ut => ut.value.indexOf(val) == 0
				);
			}
		},
		changeProjectSubclassType() {
			this.bulletInformation.projectSubdivide = "";
		},

		// hxf-0828-配套信息删除
		handleClickDelete(index, rows) {
			rows.splice(index, 1);
		},
		// hxf-0828-更新
		handleUpdate() {
			this.formPlanning.landInfo.areaCode =
				this.formPlanning.landInfo.cityCode +
				"-" +
				this.formPlanning.landInfo.countyCode;
			this.formPlanning.landInfo.address =
				this.getCardTypeValue(
					this.formPlanning.landInfo.cityCode,
					this.cityAssembly
				) +
				this.getCardTypeValue(
					this.formPlanning.landInfo.countyCode,
					this.districtCollection
				) +
				this.getCardTypeValue(
					this.formPlanning.landInfo.street,
					this.projectStreet
				) +
				this.getCardTypeValue(
					this.formPlanning.landInfo.community,
					this.communityNames
				);

			//赋值坐标以及管理权属
			this.formPlanning.mapCoordinates = this.formPlanning.propertyBasic.mapCoordinates;
			this.formPlanning.manageUnit = this.formPlanning.propertyBasic.manageUnit;
			//校验里程碑名称是否已填
			if (!this.materialsName) {
				this.$message.error(
					"资料信息中材料名称为必填项，请将材料名称补充完整！"
				);
				return false;
			}
			this.$refs["formConstructionLand"].validate(valid => {
				if (valid) {
					this.$refs["formConstruction"].validate(valid => {
						if (valid) {
							this.formPlanning.propertyBasic.projectId = this.isEdit;

							let areaCode =
								this.formPlanning.landInfo.cityCode +
								"-" +
								this.formPlanning.landInfo.countyCode +
								"-" +
								this.formPlanning.landInfo.street +
								"-" +
								this.formPlanning.landInfo.community;
							//判断项目名称+地址是否存在
							validateProject({
								addressInfo: this.formPlanning.landInfo.addressInfo,
								areaCode: areaCode,
								propertyName: this.formPlanning.propertyBasic.projectName,
								projectId: this.projectId,
								propertyRight:'1'
							}).then(res => {
								if (res.data.data === 1) {
									this.$message.error("该项目名称已存在，请重新输入！");
								} else {
									this.loadAction("保存中");
									underconstructionEdit(this.formPlanning)
										.then(res => {
											this.loadClose();
											this.$message.success("保存成功！");
											this.closeNowRouter();
										})
										.catch(e => {
											console.log(e);
											this.loadClose();
											this.$message.error("保存失败");
										});
								}
							});
						} else {
							this.$message.error("保存失败！");
							return false;
						}
					});
				} else {
					this.$message.error("保存失败！");
					return false;
				}
			});
		},
		// hxf-0828-保存
		handlePreservation() {
			this.formPlanning.landInfo.areaCode =
				this.formPlanning.landInfo.cityCode +
				"-" +
				this.formPlanning.landInfo.countyCode;
			this.formPlanning.landInfo.address =
				this.getCardTypeValue(
					this.formPlanning.landInfo.cityCode,
					this.cityAssembly
				) +
				this.getCardTypeValue(
					this.formPlanning.landInfo.countyCode,
					this.districtCollection
				) +
				this.getCardTypeValue(
					this.formPlanning.landInfo.street,
					this.projectStreet
				) +
				this.getCardTypeValue(
					this.formPlanning.landInfo.community,
					this.communityNames
				);

			//赋值坐标以及管理权属
			this.formPlanning.mapCoordinates = this.formPlanning.propertyBasic.mapCoordinates;
			this.formPlanning.manageUnit = this.formPlanning.propertyBasic.manageUnit;
			//校验里程碑名称是否已填
			if (!this.materialsName) {
				this.$message.error(
					"资料信息中材料名称为必填项，请将材料名称补充完整！"
				);
				return false;
			}
			const formConstructionLand = new Promise((resolve, reject) => {
				this.$refs["formConstructionLand"].validate(valid => {
					valid ? resolve() : reject("土地信息表单校验失败，请检查");
				});
			});
			const formConstruction = new Promise((resolve, reject) => {
				this.$refs["formConstruction"].validate(valid => {
					valid ? resolve() : reject("项目信息表单校验失败，请检查");
				});
			});

			Promise.all([formConstructionLand, formConstruction])
				.then(() => {
					let areaCode =
						this.formPlanning.landInfo.cityCode +
						"-" +
						this.formPlanning.landInfo.countyCode +
						"-" +
						this.formPlanning.landInfo.street +
						"-" +
						this.formPlanning.landInfo.community;
					validateProject({
						addressInfo: this.formPlanning.landInfo.addressInfo,
						areaCode: areaCode,
						propertyName: this.formPlanning.propertyBasic.projectName,
						propertyRight:'1'
					}).then(res => {
						if (res.data.data === 1) {
							this.$message.error("该项目名称已存在，请重新输入！");
						} else {
							this.loadAction("保存中");
							underconstructionAdd(this.formPlanning)
								.then(res => {
									this.loadClose();
									this.$message.success("保存成功！");
									this.closeNowRouter();
								})
								.catch(e => {
									console.log(e);
									this.loadClose();
									this.$message.error("保存失败");
								});
						}
					});
				})
				.catch(error => {
					this.$message.error(error);
				});
		},
		closeNowRouter() {
			this.$store.commit("delLastRoute", this.$route.path);
		},
		// hxf-0901-验证项目编号是否存在
		projectCodeOnly(value, callback) {
			validateprojectCode({
				projectCode: value,
				projectId: this.isEdit || ""
			}).then(res => {
				if (res.data.data === 0) {
					return callback();
				} else {
					return callback(new Error("项目编号已存在，请重新输入！"));
				}
			});
		},
		// hxf-0901-回显
		handleGetPlanproject() {
			if (this.isEdit) {
				getUnderconstruction(this.isEdit).then(res => {
					if (res.data.code === 0) {
						this.formPlanning = res.data.data;

						//赋值坐标以及管理权属
						this.$set(
							this.formPlanning.propertyBasic,
							"manageUnit",
							this.formPlanning.manageUnit
						);
						this.$set(
							this.formPlanning.propertyBasic,
							"mapCoordinates",
							this.formPlanning.mapCoordinates
						);

						// this.stage = this.formPlanning.propertyBasic.stage
						if (this.formPlanning.propertyBasic.addSource === "1") {
							this.addSource = false;
						}
						if (this.formPlanning.projectMilestones.length > 0) {
							this.milestoneSortIndex = this.formPlanning.projectMilestones.findIndex(
								pm => pm.milestoneName == this.formPlanning.propertyBasic.stage
							);
							if (this.milestoneSortIndex == -1) {
								this.milestoneSortIndex = 0;
							}
							this.milestoneSort = (this.milestoneSortIndex + 1).toString();
							let obj = {
								tempId: this.formPlanning.propertyBasic.isUpdate,
								projectId: this.isEdit
							};
							this.$refs.milestones.handleClickToUpdateItem(obj);
						} else {
							let obj = {
								tempId: this.formPlanning.propertyBasic.isUpdate,
								projectId: ""
							};
							this.$refs.milestones.handleClickToUpdateItem(obj);
						}
						this.$refs.milestones.handleClick({
							index: this.milestoneSortIndex
						});
						this.$forceUpdate();
						this.isThereAMilestone = res.data.data.propertyBasic.isUpdate;
						this.projectId = res.data.data.propertyBasic.projectId;
						this.initCommunityName();
					}
				});
			}
		},
		// hxf-0929-添加
		superInforAdd() {
			this.dialogVisible = true;
			this.bulletInformation = {
				buildingName: "", //楼栋名称 jiaj
				floorNum: "", //楼层号 jiaj
				propertyName: "",
				usedName: "",
				address: this.getAddressStr(),
				initialType: "",
				parkingSpotNum: "",
				floorSpace: "",
				buildArea: "",
				insideArea: "",
				operation: "20",
				unmanagedArea: "",
				managedArea: "",
				noApportionedArea: "",
				projectType: "", //物业类型 jiaj
				projectSubclassType: "", //物业用途 jiaj
				transferType: "", //移交方式 jiaj
				transferUnit: "", //移交单位 jiaj
				receiveTime: "", //移交时间 jiaj
				transferDesc: "", //移交描述 jiaj
				remarks: "",
				projectSubdivide: "",
				facilityNumber: "",
				facilityUnit: "",
				facilityLevel: "",
				radius: "",
				floorHeight: "",
				loadBearing: "",
				thickness: "",
				clearHeight:"",
				statisticType:"1"
			};
			this.bulletInformationFlag = true;
			this.bulletInformationIndex = 0;
		},
		// 获取地址信息
		getAddressStr() {
			const {
				cityCode,
				countyCode,
				street,
				community,
				addressInfo
			} = this.formPlanning.landInfo;
			let cityName = this.getLabel(cityCode, this.cityAssembly);
			let countyName = this.getLabel(countyCode, this.districtCollection);
			let streetName = this.getLabel(street, this.projectStreet);
			let communityName = this.getLabel(community, this.communityNameStreet);
			return `${cityName}${countyName}${streetName}${communityName}${addressInfo}`;
		},
		// 获取label名称
		getLabel(value, arr) {
			if (value == "") return "";
			let item = arr.find(i => i.value == value);
			return item?.label || "";
		},
		// hxf-0929-配套信息编辑
		handleClickEdit(scope) {
			this.dialogVisible = true;
			this.bulletInformation = scope.row;
			this.bulletInformationFlag = false;
			this.bulletInformationIndex = scope.$index;
			this.propertyUseTypesFilter = this.propertyUseTypes.filter(
				ut => ut.value.indexOf(this.bulletInformation.projectType) == 0
			);
		},
		handleUnitConstractionArea() {
			if (this.bulletInformation.buildArea) {
				if (
					Number(this.bulletInformation.insideArea) >
					Number(this.bulletInformation.buildArea)
				) {
					this.$message.error("套内面积不能大于建筑面积！");
					this.bulletInformation.insideArea = "";
				}
			}
		},
		// hxf-0929-弹框取消
		handleDialogClear() {
			this.dialogVisible = false;
			// for(let i in this.bulletInformation) {
			//     this.bulletInformation[i] = ''
			// }

			//还原的
			this.bulletInformation = {};
		},
		// hxf-0929-弹框确认
		handleDialogClick() {
			this.$refs["bulletInformation-form"].validate(valid => {
				if (valid) {
					if (this.bulletInformationFlag) {
						this.formPlanning.supportingFacilities.push(this.bulletInformation);
						this.handleDialogClear();
					} else {
						this.formPlanning.supportingFacilities[
							this.bulletInformationIndex
						] = this.bulletInformation;
						this.handleDialogClear();
					}
				} else {
					this.$message.error("保存失败！");
					return false;
				}
			});
		},
		// hxf-0914-建筑面积失焦校验
		handleFloorAreaBlur(row) {
			if (Number(row.floorArea) < Number(row.floorSpace)) {
				this.$message.error("建筑面积不能小于占地面积！");
				row.floorArea = "";
			}
		},
		handleFloorSpaceBlur(row) {
			if (row.floorArea !== "") {
				if (Number(row.floorArea) < Number(row.floorSpace)) {
					this.$message.error("占地面积不能大于建筑面积！");
					row.floorSpace = "";
				}
			}
		},
		dataFormat: function(row, column, cellValue) {
			if (!cellValue) {
				return "";
			}
			return moment(cellValue).format("YYYY-MM-DD");
		},
		getCardTypeValue(num, sum) {
			let arr = sum.filter(e => e.value === num);
			if (arr.length > 0) {
				return arr[0].label;
			} else {
				return num;
			}
		},
		// 监听地图坐标监听
		resetPosition(data) {
			let position = JSON.parse(data);
			if (position.type == "location") {
				if (position.value.length) {
					this.formPlanning.propertyBasic = {
						...this.formPlanning.propertyBasic,
						mapObject: position.mapObject
							? JSON.stringify(position.mapObject)
							: "",
						mapCoordinates: position.join(",")
					};
				}
			} else {
				this.formPlanning.propertyBasic = {
					...this.formPlanning.propertyBasic,
					fenceCoordinates: JSON.stringify(position.value)
				};
			}
		},
		// hxf-1009-区改变清空街道和社区
		handleChangeCountyCode() {
			this.formPlanning.landInfo.street = "";
			this.formPlanning.landInfo.community = "";
			this.formPlanning.landInfo.addressInfo = "";
		},
		// hxf-1009-街道改变清空社区
		handleChangeStreet() {
			this.formPlanning.landInfo.community = "";
			this.formPlanning.landInfo.addressInfo = "";
			this.initCommunityName();
		},
		handleChangeCommunity() {
			this.formPlanning.landInfo.addressInfo = "";
		},
		initCommunityName() {
			this.communityNameStreet.length = 0;
			let selectStreet = this.formPlanning.landInfo.street;
			if (
				selectStreet != "" &&
				this.communityNames != null &&
				this.communityNames.length > 0
			) {
				this.communityNameStreet = this.communityNames.filter(
					cn => cn.value.indexOf(selectStreet) != -1
				);
			}
		},
		handleValidateProject(value) {
			if (
				this.formPlanning.landInfo.cityCode === "" ||
				this.formPlanning.landInfo.countyCode === "" ||
				this.formPlanning.landInfo.street === "" ||
				this.formPlanning.landInfo.community === ""
			) {
				this.$message.error("请将市区街道社区填写完整后再填写详细地址！");
				this.formPlanning.landInfo.addressInfo = "";
			} else {
				let areaCode =
					this.formPlanning.landInfo.cityCode +
					"-" +
					this.formPlanning.landInfo.countyCode +
					"-" +
					this.formPlanning.landInfo.street +
					"-" +
					this.formPlanning.landInfo.community;
				validateProject({ addressInfo: value, areaCode: areaCode,propertyRight:'1' }).then(
					res => {
						if (res.data.data === 2) {
							this.$message.warning("该地址已存在！");
						}
					}
				);
			}
		},
		// 验证宗地号是否存在
		landCodeOnly(value) {
			validateLandCode({
				landCode: value,
				landId: this.formPlanning.landInfo.landId || ""
			}).then(res => {
				if (res.data.data !== 0) {
					this.$message.warning("宗地号已存在，请确认是否继续操作");
				}
			});
		},
		//验证物业名称唯一
		propertyNameOnly(value) {
			validateFacRoomName({
				roomName: value,
				facId: this.bulletInformation.facilitiesId || ""
			}).then(res => {
				if (res.data.data !== 0) {
					this.$message.error("物业名称已存在，请重新输入！");
					this.bulletInformation.propertyName = "";
				}
			});
		},
		// 经营面积失焦校验
		handleAreaCompared() {
			if (
				Number(this.bulletInformation.unmanagedArea) +
					Number(this.bulletInformation.managedArea) >
				Number(this.bulletInformation.buildArea)
			) {
				this.$message.error("经营面积+非经营面积之和不能大于建筑面积！");
				this.bulletInformation.managedArea = "";
			}
		},
		//分摊共用面积
		handleNoAppor() {
			if (
				Number(this.bulletInformation.noApportionedArea) >
				Number(this.bulletInformation.buildArea)
			) {
				this.$message.error("分摊公用面积不能大于建筑面积！");
				this.bulletInformation.noApportionedArea = "";
			}
		},
		validateProjectCountryCode(value) {
			validateProjectCountryCode(value, this.projectId || "").then(res => {
				if (res.data.data !== 0) {
					this.$message.error("项目国家编码已存在，请重新输入");
					this.formPlanning.propertyBasic.projectCountryCode = "";
				}
			});
		},
		autoSetPropertyName() {
			let rname = this.formPlanning.propertyBasic.projectName;
			if (this.bulletInformation.buildingName.length > 0) {
				rname += this.bulletInformation.buildingName;
			}
			if (this.bulletInformation.floorNum.length > 0) {
				rname += this.bulletInformation.floorNum + "层";
			}
			if (rname.length > 0) {
				this.bulletInformation.propertyName = rname;
			}
		}
	},
	filters: {
		ellipsis(value) {
			if (!value) {
				return "";
			} else if (value.length > 6) {
				return value.slice(0, 6) + "...";
			} else {
				return value;
			}
		}
	}
};
