import request from '@/plugins/axios'

export function underconstructionPage(query) {
    return request({
        url: '/api/underconstruction/page',
        method: 'get',
        params: query
    })
}

export function underconstructionAdd(data) {
    return request({
        url: '/api/underconstruction',
        method: 'post',
        data: data
    })
}

export function underconstructionEdit(data) {
    return request({
        url: '/api/underconstruction',
        method: 'put',
        data: data
    })
}
export function getUnderconstruction(id) {
    return request({
        url: '/api/underconstruction/' + id,
        method: 'get'
    })
}
export function validateprojectCode(param) {
    return request({
        url: '/api/propertybasic/validateProjectCode',
        method: 'get',
        params: param
    })
}
export function turnOver(param) {
    return request({
        url: '/api/propertybasic/turnOver',
        method: 'post',
        data: param
    })
}
export function initMilestones(query) {
    return request({
        url: '/api/underconstruction/initMilestones',
        method: 'get',
        params: query
    })
}

export function validateProject(query){
    return request({
        url: '/api/rentproperty/validateProject',
        method: 'get',
        params: query
    })
}
export function get(projectId){
    return request({
        url: '/api/propertybasic/' + projectId,
        method: 'get'
    })
}
