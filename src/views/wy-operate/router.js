// 修缮管理
const expairRoutes = [
	{
		path: "repair/question",
		meta: { title: "修缮问题反馈" },
		component: () =>
			import("~/views/wy-operate/views/repair/question/index.vue")
	},
	{
		path: "repair/budget",
		meta: { title: "预算登记" },
		component: () => import("~/views/wy-operate/views/repair/budget/index.vue")
	},
	{
		path: "repair/contract",
		meta: { title: "合同录入" },
		component: () =>
			import("~/views/wy-operate/views/repair/contract/index.vue")
	},
	{
		path: "repair/contract/add",
		meta: { title: "合同录入新增" },
		component: () =>
			import("~/views/wy-operate/views/repair/contract/add/index.vue")
	},
	{
		path: "repair/contract/ocr",
		meta: { title: "合同录入ocr新增" },
		component: () =>
			import("~/views/wy-operate/views/repair/contract/ocr/index.vue")
	},
	{
		path: "repair/payment",
		meta: { title: "付款登记" },
		component: () => import("~/views/wy-operate/views/repair/payment/index.vue")
	},
	{
		path: "repair/accept",
		meta: { title: "验收登记" },
		component: () => import("~/views/wy-operate/views/repair/accept/index.vue")
	},
	{
		path: "repair/final-accounts",
		meta: { title: "结算登记" },
		component: () =>
			import("~/views/wy-operate/views/repair/final-accounts/index.vue")
	},
	{
		path: "repair/overview",
		meta: { title: "修缮概览" },
		component: () =>
			import("~/views/wy-operate/views/repair/overview/index.vue")
	},
	{
		path: "repair/department",
		meta: { title: "施工单位管理" },
		// 新的直接复用客户管理-供应商管理
		component: () => import("~/views/wy-client/views/clientprovider/index.vue")
		// 老的单独查供应商的页面
		//component: () => import("~/views/wy-operate/views/repair/department/index.vue")
	}
];

// 装修管理
const decorateRoutes = [
	{
		path: "decorate/decoration",
		meta: { title: "过程监督" },
		component: () =>
			import("~/views/wy-operate/views/decorate/decoration/index.vue")
	},
	{
		path: "decorate/accept",
		meta: { title: "验收登记" },
		component: () =>
			import("~/views/wy-operate/views/decorate/accept/index.vue")
	}
];

// 巡查路由
const inspectionRoutes = [
	{
		path: "property-inspection/catalogue-check",
		meta: { title: "检查编目" },
		component: () =>
			import(
				"~/views/wy-operate/views/property-inspection/catalogue-check/index.vue"
			)
	},
	{
		path: "property-inspection/standard",
		meta: { title: "检查标准列表" },
		component: () =>
			import("~/views/wy-operate/views/property-inspection/standard/index.vue")
	},
	{
		path: "property-inspection/method",
		meta: { title: "检查方法" },
		component: () =>
			import("~/views/wy-operate/views/property-inspection/method/index.vue")
	},
	{
		path: "property-inspection/check-in",
		meta: { title: "巡查结果登记" },
		component: () =>
			import("~/views/wy-operate/views/property-inspection/check-in/index.vue")
	},
	{
		path: "property-inspection/look-view",
		meta: { title: "巡查结果查看" },
		component: () =>
			import("~/views/wy-operate/views/property-inspection/look-view/index.vue")
	}
];

// 代建合同&EPC
const epcRoutes = [
	{
		path: "epc/project",
		meta: { title: "工程登记" },
		component: () => import("~/views/wy-operate/views/epc/project/index.vue")
	},
	{
		path: "epc/project-info",
		meta: { title: "立项信息登记" },
		component: () =>
			import("~/views/wy-operate/views/epc/project-info/index.vue")
	},
	{
		path: "epc/tender-info",
		meta: { title: "招标信息登记" },
		component: () =>
			import("~/views/wy-operate/views/epc/tender-info/index.vue")
	},
	{
		path: "epc/contract",
		meta: { title: "合同录入" },
		component: () => import("~/views/wy-operate/views/epc/contract/index.vue")
	},
	{
		path: "epc/contract/edit",
		name: "contract_add",
		meta: { title: "新增" },
		component: () =>
			import("~/views/wy-operate/views/epc/contract/components/edit-data.vue")
	},
	{
		path: "epc/contract/edit",
		name: "contract_edit",
		meta: { title: "编辑" },
		component: () =>
			import("~/views/wy-operate/views/epc/contract/components/edit-data.vue")
	},
	{
		path: "epc/contract/look",
		name: "contract_look",
		meta: { title: "查看" },
		component: () =>
			import("~/views/wy-operate/views/epc/contract/components/show-actual.vue")
	},
	{
		path: "epc/payment",
		meta: { title: "付款登记" },
		component: () => import("~/views/wy-operate/views/epc/payment/index.vue")
	},
	{
		path: "epc/accept",
		meta: { title: "验收登记" },
		component: () => import("~/views/wy-operate/views/epc/accept/index.vue")
	},
	{
		path: "epc/final-accounts",
		meta: { title: "结算/决算登记" },
		component: () =>
			import("~/views/wy-operate/views/epc/final-accounts/index.vue")
	},
	{
		path: "epc/final-cost",
		meta: { title: "财务决算" },
		component: () => import("~/views/wy-operate/views/epc/final-cost/index.vue")
	},
	{
		path: "epc/transfer-assets",
		meta: { title: "资产移交" },
		component: () =>
			import("~/views/wy-operate/views/epc/transfer-assets/index.vue")
	},
	{
		path: "epc/overview",
		meta: { title: "工程信息概览" },
		component: () => import("~/views/wy-operate/views/epc/overview/index.vue")
	}
];

// 其他收款合同
const collectionRoutes = [
	{
		path: "collection/collection",
		meta: { title: "虚拟物业采集" },
		component: () =>
			import(
				"~/views/wy-operate/views/collection/collection/list/other_property_list.vue"
			)
	},
	{
		path: "other-property/edit",
		name: "collection_property_add",
		component: () =>
			import(
				"~/views/wy-operate/views/collection/collection/edit/other_property_form.vue"
			),
		meta: { title: "虚拟物业新增" }
	},
	{
		path: "other-property/edit",
		name: "collection_property_edit",
		component: () =>
			import(
				"~/views/wy-operate/views/collection/collection/edit/other_property_form.vue"
			),
		meta: { title: "虚拟物业编辑" }
	},
	{
		path: "other-property/look",
		name: "collection_property_look",
		component: () =>
			import(
				"~/views/wy-operate/views/collection/collection/look/other_property_look.vue"
			),
		meta: { title: "虚拟物业查看" }
	},
	{
		path: "collection/other-property",
		meta: { title: "物业复核" },
		component: () =>
			import(
				"~/views/wy-operate/views/collection/other-property/list/other_property_list.vue"
			)
	},
	{
		path: "other-property/edit",
		name: "collection_other_property_edit",
		component: () =>
			import(
				"~/views/wy-operate/views/collection/other-property/edit/other_property_form.vue"
			),
		meta: { title: "物业复核复核" }
	},
	{
		path: "other-property/look",
		name: "collection_other_property_look",
		component: () =>
			import(
				"~/views/wy-operate/views/collection/other-property/look/other_property_look.vue"
			),
		meta: { title: "物业复核查看" }
	},
	{
		path: "collection/contract",
		meta: { title: "合同录入" },
		component: () =>
			import("~/views/wy-operate/views/collection/contract/list.vue")
	},
	{
		path: "contract/list-history", //合同历史管理
		meta: { title: "合同历史管理" },
		component: () =>
			import("~/views/wy-operate/views/collection/contract/list-history.vue")
	},
	{
		path: "collection/contract/edit", //合同修改
		meta: { title: "合同编辑" },
		component: () =>
			import("~/views/wy-operate/views/collection/contract/edit.vue")
	},
	{
		path: "contract/view", //合同修改
		meta: { title: "合同查看" },
		component: () =>
			import("~/views/wy-operate/views/collection/contract/view.vue")
	},
	{
		path: "collection/collection-accept",
		meta: { title: "合同接收" },
		component: () =>
			import("~/views/wy-operate/views/collection/collection-accept/index.vue")
	}
];

// 其他付款合同
const paymentRoutes = [
	{
		path: "payment/department",
		meta: { title: "单位管理" },
		component: () =>
			import("~/views/wy-operate/views/payment/department/index.vue")
	},
	{
		path: "payment/contract",
		meta: { title: "合同录入" },
		component: () =>
			import("~/views/wy-operate/views/payment/contract/index.vue")
	},
	{
		path: "payment/payment",
		meta: { title: "付款登记" },
		component: () =>
			import("~/views/wy-operate/views/payment/payment/index.vue")
	},
	{
		path: "payment/final-accounts",
		meta: { title: "结算/决算登记" },
		component: () =>
			import("~/views/wy-operate/views/payment/final-accounts/index.vue")
	}
];

export default {
	path: "/wy-operate",
	component: () => import("@/views/wy-operate/layout/index.vue"),
	children: [
		//修缮管理
		...expairRoutes,
		// 装修管理
		...decorateRoutes,
		// 物业巡查
		...inspectionRoutes,
		// 代建合同&EPC
		...epcRoutes,
		//其他收款合同
		...collectionRoutes,
		//其他付款合同
		...paymentRoutes
	]
};
