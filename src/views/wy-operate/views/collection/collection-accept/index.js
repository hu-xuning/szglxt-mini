import {
	fetchList,
	getRentContractPageList
} from "@/views/wy-rent/api/rent-contract-acceptance.js";
import editCheck from "./edit/edit-check";
import editReceive from "./edit/edit-receive";
import contractView from "@/views/wy-rent/contract/view.vue";
export default {
	components: {
		editCheck,
		editReceive,
		contractView
	},
	provide() {
		return {
			// 更新核算列表
			initList: this.getListData,
			// 更新合同列表
			updateList: this.getTabListData
		};
	},
	data() {
		return {
			contract: {
				filterList: [
					{
						label: "核查状态",
						inputType: "select",
						name: "status",
						children: [
							{ label: "所有", value: "" },
							{ label: "待接收", value: "10" },
							{ label: "待核算", value: "11" }
						]
					}
				],
				// 列表数据
				tableData: [],
				// 请求参数
				params: {
					current: 1,
					size: 10,
					category: 2,
					propertyNames: "",
					status: ""
				},
				// 分页
				pager: {
					total: 0, // 总页数
					current: 1, // 当前页数
					size: 10 // 每页显示多少条
				},
				// 页面loading
				loading: false
			},
			// 核算列表请求参数
			account: {
				// 筛选列表
				filterList: [
					{
						label: "收款项目",
						inputType: "dict",
						dictName: "charge_project_type",
						name: "chargeProjectName"
					},
					{
						label: "费用周期",
						inputType: "daterange",
						name: ["costPeriodStart", "costPeriodEnd"]
					},
					{
						label: "应收日期",
						inputType: "daterange",
						name: "receivableDate"
					},
					{
						label: "应收状态",
						inputType: "dict",
						dictName: "payable_status",
						name: "receivableStatus"
					}
				],
				// 表格列表数据
				tableData: [],
				// 请求参数
				params: {
					current: 1,
					size: 10,
					category: 2,
					propertyNames: ""
				},
				// 分页
				pager: {
					total: 0, // 总页数
					current: 1, // 当前页数
					size: 10 // 每页显示多少条
				},
				// 页面loading
				loading: true
			},
			NewIcon: require("@/assets/images/new.png")
		};
	},
	beforeMount() {
		this.getListData();
		this.getTabListData();
	},
	methods: {
		// 切换核算列表
		changeAccountList(rentContractId) {
			this.contract.tableData = this.contract.tableData.map(item => ({
				...item,
				active: item.rentContractId == rentContractId
			}));
			this.getListData({ rentContractId, current: 1 });
		},
		//  获取合同列表
		getTabListData(params = {}) {
			let req = { ...this.contract.params, ...params };
			getRentContractPageList(req)
				.then(res => {
					const { current, size, total, records } = res.data.data;
					this.contract.tableData = records.map((item, index) => ({
						...item,
						active: index === 0
					}));
					this.contract.params = req;
					this.contract.pager = { total, current, size };
					records.length > 0
						? this.getListData({
								rentContractId: records[0].rentContractId
						  })
						: (this.account.loading = false);
				})
				.catch(() => {
					this.account.loading = false;
				})
				.finally(() => {
					this.contract.loading = false;
				});
		},
		//获取核算列表
		getListData(params = {}) {
			this.account.loading = true;
			let req = { ...this.account.params, ...params };
			fetchList(req)
				.then(res => {
					const { current, size, total, records } = res.data.data;
					this.account.tableData = records;
					this.account.params = req;
					this.account.pager = { total, current, size };
				})
				.finally(() => {
					this.account.loading = false;
				});
		}
	}
};
