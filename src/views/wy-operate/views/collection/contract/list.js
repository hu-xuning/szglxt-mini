import {
	fetchList,
	delObj,
	recordHis,
	renew,
	excute,
	send
} from "@/views/wy-rent/api/rent-contract.js";
import typeSelect from "./type-select.vue";
import contractBack from "./back.vue";
import contractView from "./view.vue";
import CacheParams from "@/assets/vue-mixin/CacheParams";

export default {
	name: "contract-manage",
	mixins: [CacheParams],
	components: { typeSelect, contractBack, contractView },
	provide() {
		return {
			initList: this.getListData
		};
	},
	mounted() {
		const { warningType } = this.$route.query;
		warningType ? this.getListData({ warningType }) : this.getListData();
	},
	data() {
		return {
			tableData: [],
			// 请求参数
			params: {
				current: 1,
				size: 10,
				category: 2,
				propertyNames: ""
			},
			// 分页
			pager: {
				total: 0, // 总页数
				current: 1, // 当前页数
				size: 10 // 每页显示多少条
			},
			// 页面loading
			loading: false,
			NewIcon: require("@/assets/images/new.png")
		};
	},
	computed: {
		filterList() {
			let list = [
				{
					label: "合同类型",
					inputType: "dict",
					dictName: "rent_contract_type",
					name: "type",
					multiple: true
				},
				{
					label: "合同细类",
					inputType: "dict",
					dictName: "rent_contract_sub_type",
					name: "subType",
					multiple: true
				},
				{
					label: "合同状态",
					inputType: "dict",
					dictName: "rent_contract_status",
					name: "status",
					multiple: true
				},
				{
					label: "合同签订日期",
					inputType: "daterange",
					name: ["signDateStart", "signDateEnd"]
				},
				{
					label: "合同结束日期",
					inputType: "daterange",
					name: ["endDateStart", "endDateEnd"]
				},
				{
					label: "起租日期",
					inputType: "daterange",
					name: ["rentStartDateStart", "rentStartDateEnd"]
				},
				{
					label: "租赁周期",
					inputType: "daterange",
					name: ["startDate", "endDate"]
				},
				{
					label: "统计类别",
					inputType: "select",
					name: "warningType",
					children: [
						{ label: "即将到期", value: "01" },
						{ label: "待续签", value: "02" },
						{ label: "本月新签", value: "03" },
						{ label: "本月终止", value: "04" },
						{ label: "执行中", value: "05" },
						{ label: "已终止", value: "06" }
					],
					multiple: true
				}
			];
			const { warningType } = this.$route.query;
			if (warningType) {
				list[list.length - 1].defaultValue = [warningType];
			}
			return list;
		}
	},
	methods: {
		getListData(params = {}) {
			this.loading = true;
			let requestData = { ...this.params, ...params };
			fetchList(requestData)
				.then(res => {
					const { current, size, total, records } = res.data.data;
					this.tableData = records;
					this.params = requestData;
					this.pager = { total, current, size };
				})
				.finally(() => {
					this.loading = false;
				});
		},

		//导出合同列表excel表格
		exportData() {
			this.publicExport(
				"合同列表",
				"/api/rentcontract/rentcontractList-download",
				this.params
			);
		},
		edit(row) {
			this.$parent.$router.push({
				path: `/wy-operate/collection/contract/edit?id=${row.rentContractId}&scene=edit`
			});
		},
		view(row) {
			this.$refs.contractView.show(row.rentContractId);
		},
		del(row) {
			if (row.status != "00") {
				this.err("只有草稿状态的合同才能删除");
			} else {
				this.confirm("删除确认", "您确定要删除该数据吗？", async () => {
					const resp = await delObj(row.rentContractId);
					if (resp.data.code == 0) {
						this.suc("删除成功");
						this.getListData();
					} else {
						this.err("删除失败" + resp.data.code + resp.data.message);
					}
				});
			}
		},
		//变更
		change(row) {
			this.$confirm(
				"发起变更后，原合同将备份为历史合同，新合同将提交财务部重新核算并生成费用，确认要变更吗?"
			).then(async () => {
				const resp = await recordHis(row.rentContractId);
				if (resp.data.code == 0) {
					this.$parent.$router.push({
						path: `/wy-operate/collection/contract/edit?id=${row.rentContractId}&scene=change`
					});
				} else {
					this.err("发起变更失败" + resp.data.code + resp.data.message);
				}
			});
		},
		// 续签
		renew(row) {
			this.$confirm(
				"发起续签后，原合同将终止，新合同将提交财务部重新核算并生成费用，确认要续签吗"
			).then(() => {
				renew(row.rentContractId).then(resp => {
					if (resp.data.code == 0) {
						let id = resp.data.data.rentContractId;

						this.$parent.$router.push({
							path: `/wy-operate/collection/contract/edit?id=${id}&scene=renew`
						});
					} else {
						this.err("发起续签失败" + resp.data.code + resp.data.message);
					}
				});
			});
		},

		back(row) {
			this.$refs.contractBack.show(row.rentContractId);
		},
		//发送
		send(row) {
			this.$confirm("确认发送吗？").then(async () => {
				const response = await send(row.rentContractId);
				if (response.data.code == 0) {
					this.$alert("合同发送成功");
					this.getListData();
				} else {
					this.$alert("合同发送失败" + response.data.msg);
				}
			});
		},

		//执行，从审核通过变成，执行中
		excute(row) {
			this.$confirm(
				"执行操作后，合同状态为执行中，关联的物业状态变为出租，确认执行吗？"
			).then(async () => {
				const response = await excute(row.rentContractId);
				if (response.data.code == 0) {
					this.$alert("合同执行成功");
					this.getListData();
				} else {
					this.$alert("合同执行失败" + response.data.msg);
				}
			});
		},
		// 菜单点击监听
		handleMenuClick(type) {
			switch (type) {
				case "add":
					this.$refs.typeSelect1.show();
					break;
				case "export":
					this.exportData();
					break;
				default:
					this.$message(type);
					break;
			}
		},
		resetList () {
			this.editDataShow = false
			this.getListData()
		},
	}
};
