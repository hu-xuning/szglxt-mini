import {
	getObj,
	getContractApproveList
} from "@/views/wy-rent/api/rent-contract.js";
import { getObj as getClientManage } from "@/views/wy-client/api/clientlessee.js";
import contractChange from "./components/contractChange.vue";
import contractDetails from "./components/contractDetails.vue";
import contractRenew from "./components/contractRenew.vue";
import contractBack from "./components/contractBack.vue";
import contractPreview from "@/components/contractPreview";
import propertyPreview from "@/views/wy-operate/views/collection/contract/components/preview";

export default {
	name: "contract-view",
	components: {
		contractDetails,
		contractChange,
		contractRenew,
		contractBack,
		contractPreview,
		propertyPreview
	},
	data() {
		return {
			viewDialogVisible: false,
			activeName: "first",
			formData: {}, //合同总信息
			rentContractId: "", //合同id
			contractName: "",
			pdfUrl: "",
			//承租方内容展示
			propForm: {
				clientLesseeType: "",
				clientLesseeName: ""
			},
			//合同内容展示
			propertyInformation: [],
			rentFeeInformation: [],
			contractFee: [],
			recordingLlist: [],
			//字典项匹配
			lessee: "",
			provider: "",
			commercialCertificate: "",
			personageCertificate: "",
			subType: "", //合同细类
			type: "", //合同类型
			signType: "", //签订方式
			feeType: "", //计租方式
			approveType: "", //审批方式
			administrativeOffice: "" //管理所
		};
	},
	computed: {
		//调价方式
		rent_price_rejust_type() {
			return this.getDict("rent_price_rejust_type");
		},
		//证件类型
		rent_certificate_type() {
			return this.getDict("rent_certificate_type");
		},
		//签订方式
		rent_signing_method() {
			return this.getDict("rent_signing_method");
		},
		//收费方式
		rent_fee_type() {
			return this.getDict("rent_fee_type");
		},
		//审批方式
		rent_contract_approve_type() {
			return this.getDict("rent_contract_approve_type");
		},
		//管理所
		administrative_office() {
			return this.getDict("administrative_office");
		},
		lessee_type() {
			return this.getDict("lessee_type");
		},
		provider_type() {
			return this.getDict("provider_type");
		},
		personage_certificate_type() {
			return this.getDict("personage_certificate_type");
		},
		commercial_certificate_type() {
			return this.getDict("commercial_certificate_type");
		},
		rent_contract_sub_type() {
			return this.getDict("rent_contract_sub_type");
		},
		rent_contract_type() {
			return this.getDict("rent_contract_type");
		}
	},

	methods: {
		//获取审批列表
		async getContractApproveList(rentContractId) {
			let category = 1;
			const res = await getContractApproveList(rentContractId, category);
			if (res.data.code == 0) this.recordingLlist = res.data.data;
			else this.$alert("加载变更记录服务端服务错误" + res.code);
		},
		//查询表单数据
		async show(contractId) {
			//改为了弹出框模式
			this.viewDialogVisible = true;

			this.rentContractId = contractId;
			const response = await getObj(this.rentContractId, this.category);

			let formData = response.data.data;
			this.formData = formData;
			this.rentFeeInformation = formData.rentContractDetails;
			this.propertyInformation = formData.rentContractRooms;
			this.contractFee = formData.rentContractProfees;

			//合同预览
			this.pdfUrl = formData.contractUrl;
			this.contractName = formData.contractName;

			//获得承租方信息
			if (this.formData.customerId) {
				const propForm = await getClientManage(
					this.formData.customerId,
					this.category
				);
				this.propForm = propForm.data.data;
				this.showlessee();
			}
			//查看附件
			this.$refs.rentContractFileUpload.getFiles(this.formData.rentContractId);

			//5个子表
			//收费明细
			if (
				this.$refs.contractDetails &&
				this.$refs.contractDetails.preViewFee &&
				this.rentFeeInformation &&
				this.rentFeeInformation.length > 0
			) {
				this.$refs.contractDetails.preViewFee(contractId, this.formData);
			}
			if (this.$refs.contractChange && this.$refs.contractChange.loadHisList) {
				this.$refs.contractChange.loadHisList(contractId);
			}
			if (this.$refs.contractRenew && this.$refs.contractRenew.loadRenewList) {
				this.$refs.contractRenew.loadRenewList(contractId);
			}
			if (this.$refs.contractBack && this.$refs.contractBack.loadBackInfo) {
				this.$refs.contractBack.loadBackInfo(contractId);
			}
			this.getContractApproveList(contractId);
		},
		// 获取字典的label名称
		getDictLabel(dictName, val) {
			if (!dictName || !val) return "";
			let item = this[dictName].find(i => i.value == val);
			return item?.label || "";
		},
		//展示证件类型
		showlessee() {
			this.lessee = this.getDictLabel(
				"lessee_type",
				this.propForm?.clientLesseeType
			);
			this.provider = this.getDictLabel(
				"provider_type",
				this.propForm?.providerType
			);
			this.personageCertificate = "身份证";
			this.commercialCertificate = this.getDictLabel(
				"commercial_certificate_type",
				this.propForm?.certificateType
			);
			this.signType = this.getDictLabel(
				"rent_signing_method",
				this.propForm?.signType
			);
			this.feeType = this.getDictLabel("rent_fee_type", this.propForm?.feeType);
			this.approveType = this.getDictLabel(
				"rent_contract_approve_type",
				this.propForm?.approveType
			);
			this.administrativeOffice = this.getDictLabel(
				"administrative_office",
				this.propForm?.administrativeOffice
			);
			this.subType = this.getDictLabel(
				"rent_contract_sub_type",
				this.propForm?.subType
			);
			this.type = this.getDictLabel("rent_contract_type", this.propForm?.type);
		},
		close() {
			this.viewDialogVisible = false;
			this.propForm = {
				clientLesseeType: "",
				clientLesseeName: ""
			};
			this.activeName = "first";
			this.formData = {};
			this.rentContractId = ""; //合同id
			this.contractName = "";
			this.pdfUrl = "";
			this.propertyInformation = [];
			this.rentFeeInformation = [];
			this.contractFee = [];
			this.recordingLlist = [];
			this.lessee = "";
			this.provider = "";
			this.commercialCertificate = "";
			this.personageCertificate = "";
			this.subType = ""; //合同细类
			this.type = ""; //合同类型
			this.signType = ""; //签订方式
			this.feeType = ""; //计租方式
			this.approveType = ""; //审批方式
			this.administrativeOffice = ""; //管理所
		}
	}
};
