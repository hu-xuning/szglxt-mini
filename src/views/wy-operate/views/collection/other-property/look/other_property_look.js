import {get, getFiles, checkFiles} from '../api/other_property'
import {getDict} from '~/utils'
import { mapState,mapGetters } from 'vuex'

export default {
    name: 'project_look',
    computed: {
        ...mapState({
            // 是否内嵌页面场景
            isWebView: state => state.isWebView
        }),
        belong_ch: function(){
            const belongs = getDict('property_ownerships')
            let val = this.propForm.otherProperty.belong
            if(val && belongs.length > 0){
                const pts = belongs.find(t => t.value == val)
                return pts ? pts.label : ''
            }
            return ''
        },
        street_ch: function(){
            const street = getDict('streets')
            let val = this.propForm.otherProperty.projectStreet
            if(val && street.length > 0){
                const pts = street.find(t => t.value == val)
                return pts ? pts.label : ''
            }
            return ''
        },
        communityName_ch: function(){
            const street = getDict('community_names')
            let val = this.propForm.otherProperty.communityName
            if(val && street.length > 0){
                const pts = street.find(t => t.value == val)
                return pts ? pts.label : ''
            }
            return ''
        },
        manageUnit_ch: function(){
            const manageUnits = getDict('administrative_office')
            let val = this.propForm.otherProperty.manageUnit
            if(val && manageUnits.length > 0){
                const pts = manageUnits.find(t => t.value == val)
                return pts ? pts.label : ''
            }
            return ''
        },
        ...mapGetters({
            cityAssembly: 'getCityAssembly',
            districtCollection: 'getDistrictCollection',
        })
    },
    created(){
        const loading = this.$loading({
            lock: true,
            text: '数据加载中'
        });
         // 如果是内嵌页面预览隐藏头部和左侧菜单
        const { otherId, frame } =  this.$route.query
        frame && this.$store.commit('resetWebViewStyle',true)

        this.otherId = otherId
        //通过id获取项目信息
        get(this.otherId).then( rep => {
           this.propForm.otherProperty = rep.data.data
            this.propForm.otherProperty.cityCode = this.propForm.otherProperty.areaCode.split('-')[0]
            this.propForm.otherProperty.countyCode = this.propForm.otherProperty.areaCode.split('-')[1]

            loading.close()
        }).catch(e => {
            loading.close()
            this.$message({
                type: 'error',
                message: '数据获取失败!'
            })
        })
        getFiles(this.otherId, this.model).then(rep => {
            this.fileList = rep.data.data
        })

        this.manageUnits = getDict('administrative_office')
        this.landUses = getDict('land_uses')
        this.communityNames = getDict('community_names')
        this.streets = getDict('streets')
        this.deptNames = getDict("budget_department")
    },
    data() {
        return {
            otherId: '',
            streets: [],
            manageUnits: [],
            landUses: [],
            communityNames: [],
            deptNames: [],
            fileList: [],
            model: 'otherProperty',

            propForm:{
                otherProperty: {
                    otherId: "",
                    otherName: "",
                    propertyId: "",
                    propertyName: "",
                    projectId: "",
                    projectName: "",
                    buildArea:"",
                    manageUnit:"",
                    areaCode: "",
                    areaName: "",
                    projectStreet: "",
                    communityName:"",
                    addressInfo:"",
                    mapCoordinates:"",
                    vrUrl:"",
                    latitude:"",
                    longitude: "",
                    remarks:"",
                    landId:"",
                    parcelNum: "",
                    parcelArea: "",
                    landUse: "",
                    landContractNum: "",
                    cityCode: "",
                    countyCode:"",
                    street:"",
                    community:"",
                    landAddressInfo:"",
                    address:"",
                    propertyType:"",
                    useType:"",
                    propertyOwnership:"",
                    propertyStatus:"",
                    propertySource:"",
                    deptId: "",
                    deptName: "",
                    gatherPeople: ""
                },
            },


        }
    },
    methods: {
        closeTab() {
            //关闭当前新增页，暂时没找到好办法
            this.$store.commit("delHistoryRoute", this.$route.fullPath);
            // this.$parent.$children[2].closeTab('current')
            //跳转
            this.$router.push("/wy-operate/collection/other-property");
        },

        handleDownload(fileId) {
            checkFiles(fileId).then(resp => {
                if (resp.data.code === 0) {
                    window.open('/admin/sys-file/download/' + fileId)
                } else {
                    this.$message({
                        message: `文件不存在！`,
                        type: 'warning'
                    })
                }
            })
        },

        // hxf-1011-回显时对应value显示label
        getCardTypeValue(num, sum){
            let arr = sum.filter(e=>e.value === num)
            if (arr.length > 0) {
                return arr[0].label
            } else {
                return num
            }
        }
    }

}
