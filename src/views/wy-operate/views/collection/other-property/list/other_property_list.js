import { query } from "../api/other_property.js";
import CacheParams from "@/assets/vue-mixin/CacheParams";
let moment = require("moment");

export default {
	name: "other-propery-list",
	mixins: [CacheParams],
	provide() {
		return {
			initList: this.query
		};
	},
	mounted() {
		this.query();
	},
	data() {
		return {
			loading: false,
			tableData: [],
			// 请求参数
			params: {
				current: 1,
				size: 20,
				otherName: ""
			},
			// 选中的数据
			selectDataArr: [],
			// 分页
			pager: {
				total: 0, // 总页数
				current: 1, // 当前页数
				size: 20 // 每页显示多少条
			}
		};
	},
	computed: {
		get_button() {
			return this.getPermissions("otherproperty_get");
		},
		add_but() {
			return this.getPermissions("otherproperty_add");
		},
		edit_but() {
			return this.getPermissions("otherproperty_edit");
		},
		del_but() {
			return this.getPermissions("otherproperty_del");
		},
		exp_but() {
			return this.getPermissions("otherproperty_export");
		},
		belongs() {
			return this.getDict("administrative_office");
		},
		streets() {
			return this.getDict("streets");
		},
		deptNames() {
			return this.getDict("budget_department");
		}
	},
	methods: {
		query(params = {}) {
			this.loading = true;
			let requestData = { ...this.params, ...params };
			query(requestData)
				.then(res => {
					const { current, size, total, records } = res.data.data;
					this.tableData = records;
					this.params = requestData;
					this.pager = { total, current, size };
				})
				.finally(() => {
					this.loading = false;
				});
		},
		toLook(row) {
			this.$router.push({
				name: "collection_other_property_look",
				query: { otherId: row.otherId }
			});
		},
		toEdit(row) {
			this.$router.push({
				name: "collection_other_property_edit",
				query: { otherId: row.otherId }
			});
		},
		dateFormat(row, column, cellValue) {
			if (!cellValue) {
				return "";
			}
			return moment(cellValue).format("YYYY-MM-DD");
		},
		formateBelong(row, column, cellValue) {
			let obj = this.belongs.find(p => p.value == cellValue);
			if (obj) {
				return obj.label;
			}
			return "";
		},
		formateStreet(row, column, cellValue) {
			let obj = this.streets.find(p => p.value == cellValue);
			if (obj) {
				return obj.label;
			}
			return "";
		},
		formateDeptNames(row, column, cellValue) {
			let obj = this.deptNames.find(p => p.value == cellValue);
			if (obj) {
				return obj.label;
			}
			return "";
		},
		// 导出
		toExport() {
			this.publicExport(
				"物业复核导出",
				"/api/otherproperty/download",
				this.params
			);
		},
		// 菜单点击监听
		handleMenuClick(type) {
			switch (type) {
				case "export":
					this.toExport();
					break;
				default:
					this.$message(type);
					break;
			}
		}
	}
};
