import { mapGetters } from "vuex";
import {
    query,
    remove
} from "../api/other_property.js";
let moment = require("moment");

export default {
    name: "other-propery-list",
    computed: {
        ...mapGetters(["permissions"])
    },
		provide () {
			return {
				initList: this.query
			};
		},
    mounted() {
        this.get_button = this.permissions["otherproperty_get"];
        this.add_but = this.permissions["otherproperty_add"];
        this.edit_but = this.permissions["otherproperty_edit"];
        this.del_but = this.permissions["otherproperty_del"];
        this.exp_but = this.permissions["otherproperty_export"];
        this.belongs = this.getDict("administrative_office");
        this.streets = this.getDict("streets");
        this.deptNames = this.getDict("budget_department")
        this.query();
    },
    data() {
        return {
            add_but: false,
            belongs: [],
            streets: [],
            deptNames: [],
            edit_but: false,
            exp_but: false,
            del_but: false,
            loading: false,
            placeholder: "输入物业名称",
            tableData: [],
						// 请求参数
						params: {
							current: 1,
							size: 20,
							otherName: ""
						},
						// 选中的数据
						selectDataArr: [],
						// 分页
						pager: {
							total: 0, // 总页数
							current: 1, // 当前页数
							size: 20 // 每页显示多少条
						}
        };
    },
    methods: {
				query (params = {}) {
					this.loading = true;
					let requestData = { ...this.params, ...params };
					query(requestData)
						.then(res => {
							const { current, size, total, records } = res.data.data;
							this.tableData = records;
							this.params = requestData;
							this.pager = { total, current, size };
						})
						.finally(() => {
							this.loading = false;
						});
				},
        toAdd() {
            this.$router.push({
                name: "collection_property_add",
                query: { otherId: "" }
            });
        },
        deleteData() {
            if (this.selectDataArr.length == 0) {
                this.$alert("请勾选要删除的数据！");
                return;
            }
            let ids = this.selectDataArr.map(sd => sd.otherId);
            this.$confirm("此操作将永久删除该数据, 是否继续?", {
                confirmButtonText: "确定",
                cancelButtonText: "取消",
                type: "warning"
            })
                .then(() => {
                    remove(ids)
                        .then(rep => {
                            this.$message({
                                type: "success",
                                message: "删除成功!"
                            });
                            this.query();
                        })
                        .catch(e => {
                            console.log(e);
                            this.$message({
                                type: "success",
                                message: "删除失败!"
                            });
                        });
                })
                .catch(() => {
                    this.$message({
                        type: "info",
                        message: "已取消删除"
                    });
                });
        },
        toLook(row) {
            this.$router.push({
                name: "collection_property_look",
                query: { otherId: row.otherId }
            });
        },
        toEdit(row) {
            this.$router.push({
                name: "collection_property_edit",
                query: { otherId: row.otherId }
            });
        },
        dateFormat: function(row, column, cellValue) {
            if (!cellValue) {
                return "";
            }
            return moment(cellValue).format("YYYY-MM-DD");
        },
        formateBelong: function(row, column, cellValue) {
            let obj = this.belongs.find(p => p.value == cellValue);
            if (obj) {
                return obj.label;
            }
            return "";
        },
        formateStreet: function(row, column, cellValue) {
            let obj = this.streets.find(p => p.value == cellValue);
            if (obj) {
                return obj.label;
            }
            return "";
        },
        formateDeptNames: function(row, column, cellValue) {
            let obj = this.deptNames.find(p => p.value == cellValue);
            if (obj) {
                return obj.label;
            }
            return "";
        },
        // 导出
        toExport() {
            this.publicExport('虚拟物业导出','/api/otherproperty/download',this.params)
        },
				// 菜单点击监听
				handleMenuClick (type) {
					switch (type) {
						case "add":
							this.toAdd();
							break;
						case "export":
							this.toExport();
							break;
						case "delete":
							this.deleteData();
							break;
						default:
							this.$message(type);
							break;
					}
				}
    }
};
