//import pager from "@/assets/js/page-mixin";
import pager from '@/assets/vue-mixin/pager'
import {mapGetters} from 'vuex'
import { getDict } from '~/utils'
import {query2} from "../api/other_property.js";
let moment = require('moment')

export default {
    name: 'property-list',
    mixins: [pager],
    computed: {
        ...mapGetters(['permissions'])
    },
    mounted() {
        this.useTypes =  getDict('property_use_types')
        this.roomStatus = getDict('room_status')
        this.propertyTypes = getDict('property_types')
        this.propertyOwnerships = getDict('property_ownerships')
        this.propertySourceDict = getDict('property_source')
    },
    data() {
        return {
            propertyObject:{

            },
            dialogVisible: false,
            add_but: false,
            useTypes: [],
            roomStatus: [],
            propertyTypes: [],
            propertyOwnerships: [],
            propertySourceDict: [],
            edit_but: false,
            del_but: false,
            loading: false,
            placeholder: '输入物业名称',
            tableData: [],
            queryData: {
                propertyName: ''
            },
            selectDataArr: [],

        }
    },
    methods: {
        submit(){
            this.dialogVisible = false;
            this.$emit('getPropertyObject',this.propertyObject )
        },
        //表格行点击
        rowClick(row){
            this.propertyObject = row
        },
        show(){
        this.dialogVisible= true
        this.query()
        },

        handleClose(done) {
            this.propertyObject = {}
                    done();
          },
        query(){
            this.loading = true
            query2(Object.assign({
                current: this.pager.currentPage,
                size: this.pager.pageSize
            }, this.queryData)).then(rep => {
                if(rep.data.code === 0){
                    let repD = rep.data.data
                    this.tableData = repD.records
                    this.pager.total = repD.total
                }
                this.loading = false
            }).catch(e => {
                this.loading = false
            })
        },

        getListData() {

            query2(Object.assign({}, this.pageParams, this.queryData)).then(resp => {
                this.setListData(resp.data)
            })
        },

        selectionChange(val){
            this.selectDataArr = val
        },
        searchByKey(val){
            this.queryData.propertyName = val
            //this.query()
            this.resetList()
        },
        // changePageSize(size){
        //     this.pager.pageSize = size
        //     this.query()
        // },
        // changeCurrentPage(page){
        //     this.pager.currentPage = page
        //     this.query()
        // },

        dateFormat: function(row, column, cellValue){
            if(!cellValue){
                return ''
            }
            return moment(cellValue).format('YYYY-MM-DD')
        },
        //字典翻译，此处根据列写死的 物业用途  物业类型 产权情况 物业状态 物业来源
        dictFormate: function(row, column, cellValue){
            let columnTxt = column.property
            //物业类型
            let dict;
            if("propertyType" === columnTxt){
                dict = this.propertyTypes.find(p => p.value == cellValue)
            }
            if("useType" === columnTxt){
                dict = this.useTypes.find(p => p.value == cellValue)
            }
            if("propertyOwnership" === columnTxt){
                dict = this.propertyOwnerships.find(p => p.value == cellValue)
            }
            if("propertyStatus" === columnTxt){
                dict = this.roomStatus.find(p => p.value == cellValue)
            }
            if("propertySource" === columnTxt){
                dict = this.propertySourceDict.find(p => p.value == cellValue)
            }
            if(dict){
                return dict.label
            }
            return ''
        },
    }
}
