import request from "@/plugins/axios";

// 分页查询其它物业
export function query(query) {
    return request({
        url: "/api/otherproperty/page",
        method: "get",
        params: query
    });
}

// 查询其它物业
export function getOtherPropertyList(query) {
    return request({
        url: "/api/otherproperty/getOtherPropertyList",
        method: "get",
        params: query
    });
}

// 分页查询物业
export function query2(query) {
    return request({
        url: "/api/otherproperty/page2",
        method: "get",
        params: query
    });
}

// 查询物业
export function getPropertyList(query) {
    return request({
        url: "/api/otherproperty/getPropertyList",
        method: "get",
        params: query
    });
}

// 主键查询
export function get(otherId) {
    return request({
        url: "/api/otherproperty/" + otherId,
        method: "get"
    });
}

// 新增
export function save(data) {
    return request.post("/api/otherproperty", data);
}

// 修改
export function update(data) {
    return request({
        url: "/api/otherproperty",
        method: "put",
        data: data
    });
}

// 批量删除
export function remove(ids) {
    return request({
        url: "/api/otherproperty/delete",
        method: "post",
        data: ids
    });
}

// 单个删除
export function delObj(id) {
    return request({
        url: "/api/otherproperty/" + id,
        method: "delete"
    });
}

//文件列表接口
export function getFiles(id, moduleId) {
    return request({
        url: "/admin/sys-file/getFileList/" + id + "/" + moduleId,
        method: "get"
    });
}

//判断文件是否存在
export function checkFiles(id) {
    return request({
        url: "/admin/sys-file/checkFileIsNot/" + id,
        method: "get"
    });
}
//新增验证其他物业名称是否重复
export function validateOtherName(otherName) {
    return request({
        url: "/api/otherproperty/validateOtherName",
        method: "get",
        params: otherName
    });
}
//修改验证其他物业名称是否重复

export function validateOtherName2(params) {
    return request({
        url: "/api/otherproperty/validateOtherName2",
        method: "get",
        params
    });
}
//新增验证物业ID是否是否重复int 1存在，0 不存在
export function validatePropertyId(propertyId) {
    return request({
        url: "/api/otherproperty/validatePropertyId",
        method: "get",
        params: propertyId
    });
}
//修改验证物业ID是否是否重复int 1存在，0 不存在
export function validatePropertyId2(params) {
    return request({
        url: "/api/otherproperty/validatePropertyId2",
        method: "get",
        params
    });
}

//删除文件
export function delFiles(id, moduleId) {
    return request({
        url: "/admin/sys-file/delFileByFileId/" + id + "/" + moduleId,
        method: "delete"
    });
}
