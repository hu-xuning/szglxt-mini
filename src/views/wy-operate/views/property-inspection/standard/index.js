import { getDict } from "~/utils";
import { downloadPost } from "../../../api/check-in.js";
import { downloadBlob } from "~/utils/index.js";
import {
    pagingQuery,
    getcheckItemList,
    newStandard,
    deleteStandard,
    editInspectionCriteria
} from "@/views/wy-operate/api/standard.js";
import { getTinspectionName } from "../../../api/method.js";
export default {
    provide () {
        return {
            initList: this.getListData
        };
    },
    data () {
        return {
            // 物业项列表
            treeList: [],
            unfold: false,
            restaurants: [],
            // 弹窗表单设置
            form: {
                inspectionWay: "",
                inspectionName: "",
                projectType: "",
                sort: null,
                remarks: "",
                inspectionId: ""
            },
            propertyTypes: [],
            dialogVisible: false,
            // 表格列表数据
            tableData: [],
            // 请求参数
            params: {
                current: 1,
                size: 20,
                inspectionId: "",
                inspectionName: "",
                projectType: ""
            },
            // 选中的数据
            selectDataArr: [],
            // 分页
            pager: {
                total: 0, // 总页数
                current: 1, // 当前页数
                size: 20 // 每页显示多少条
            },

            filterList: [
                {
                    label: "物业类型",
                    inputType: "dict",
                    dictName: "property_types",
                    name: "projectType"
                }
            ],

            // 校验规则
            rules: {
                sort: [
                    {
                        required: true,
                        message: "请输入排序",
                        trigger: "blur"
                    }
                ],
                projectType: [
                    {
                        required: true,
                        message: "请选择物业类型",
                        trigger: "blur"
                    }
                ],
                inspectionName: [
                    {
                        required: true,
                        message: "请输入检查项名称",
                        trigger: "blur"
                    }
                ],
                inspectionStandard: [
                    {
                        required: true,
                        message: "请输入检查标准",
                        trigger: "blur"
                    }
                ]
            },
            title: "",
            formLabelWidth: "120px"
        };
    },
    mounted () {
        this.propertyTypes = getDict("property_types");
        this.getcheckItemList();
        this.getListData();
    },
    methods: {
        // 菜单点击监听
        handleMenuClick (type) {
            switch (type) {
                case "add":
                    this.showAddOrEdit("add");
                    break;
                case "delete":
                    this.handleDelete();
                    break;
                case "export":
                    this.exportTable();
                    break;
            }
        },
        // 全部展开
        unFoldAll () {
            this.unfold = !this.unfold;
            // 转换成树的原数据
            this.treeList.forEach((e, i) => {
                this.$refs.treeBox.store.nodesMap[
                    this.treeList[i].id
                ].expanded = this.unfold;
            });
        },
        renderContent (h, { node, data, store }) {
            return (
                <span class="custom-tree-node">
                    <span>
                        <span>{data.label}</span>
                        <span>
                            {" "}
                            {!data.projectType ? `(${data.total})` : ""}
                        </span>
                    </span>
                </span>
            );
        },
        //左侧列表
        getcheckItemList () {
            getcheckItemList()
                .then(res => {
                    this.treeList = res.data.data;
                    let num = this.treeList.length - 1;
                    this.treeList.forEach((e, i) => {
                        this.$set(this.treeList[i], "id", i + 1);
                        e.children.forEach((q, w) => {
                            num++;
                            this.$set(
                                this.treeList[i].children[w],
                                "id",
                                num + 1
                            );
                        });
                        this.propertyTypes.forEach((v, index) => {
                            if (Object.is(e.projectType, v.value)) {
                                this.treeList[i].label = v.label;
                            }
                        });
                    });
                })
                .catch(() => {
                    Object.is();
                });
        },
        // 获取右侧列表
        getListData (params = {}) {
            let requestData = { ...this.params, ...params };
            pagingQuery(requestData)
                .then(res => {
                    const { current, size, total, records } = res.data.data;
                    this.tableData = records;
                    this.params = requestData;
                    this.pager = { total, current, size };
                })
                .catch(() => { });
        },
        // 物业项情况变化
        handleNodeClick (data, e, b) {
            this.params = {};
            this.form.inspectionId = e.data.inspectionId;
            if (data.projectType) {
                this.form.inspectionName = "";
                this.form.projectType = data.projectType;
                this.params.projectType = data.projectType;
            } else {
                this.form.projectType = e.parent.data.projectType;
                this.form.inspectionName = data.label;
                this.params.inspectionId = data.inspectionId;
                this.params.projectType = e.parent.data.projectType;
            }
            this.getListData();
        },
        //打开 新增/编辑/查看   弹框  事件
        showAddOrEdit (v, row) {
            this.restaurants = []
            // 弹框控制
            this.dialogVisible = true;
            // 新增/编辑/查看  弹框title
            switch (v) {
                case "add":
                    this.title = "新增检查标准";
                    this.form.projectType && this.getTinspectionName();
                    break;
                case "edit":
                    this.form = JSON.parse(JSON.stringify(row));
                    this.title = "编辑检查标准";
                    break;
            }
        },
        // 展示检查项的全部列表(用于模糊搜索)
        getTinspectionName () {
            getTinspectionName(this.form.projectType)
                .then(res => {
                    this.restaurants = res.data.data
                    if (this.restaurants.length == 0) return (this.form.inspectionName = "");
                    let result = this.restaurants.some((item, index) => {
                        return item.value == this.form.inspectionName;
                    });
                    if (!result) this.form.inspectionName = "";
                })
                .catch(() => { });
        },
        // propertyTypesFromChange(v) {
        //     this.getTinspectionName();
        // },
        // 保存
        preservation (formName) {
            this.$refs[formName].validate(valid => {
                if (valid) {
                    if (this.title === "新增检查标准") {
                        // 如果是新增
                        newStandard(this.form)
                            .then(response => {
                                if (response.data.data == 500)
                                    return this.$message({
                                        message: response.data.data.msg,
                                        type: "warning"
                                    });

                                this.$message({
                                    message: response.data.data.msg,
                                    type: "success"
                                });
                                this.dialogVisible = false;
                                this.getListData();
                                this.getTinspectionName();
                                this.getcheckItemList();
                            })
                            .catch(() => {
                                console.log("catch");
                            });
                    } else {
                        //如果是编辑
                        editInspectionCriteria(this.form)
                            .then(response => {
                                if (response.data.data == 500)
                                    return this.$message({
                                        message: response.data.data.msg,
                                        type: "warning"
                                    });
                                this.$message({
                                    message: response.data.data.msg,
                                    type: "success"
                                });
                                this.dialogVisible = false;
                                this.getListData();
                                this.getTinspectionName();
                                this.getcheckItemList();
                            })
                            .catch(() => {
                                console.log("catch");
                            });
                    }
                } else {
                    console.log("error submit!!");
                    return false;
                }
            });
        },
        // 重置方法回调
        handleReset () {
            this.params.inspectionId = "";
            this.$refs.treeBox.setCurrentKey();
        },
        // 删除
        handleDelete (row) {
            if (this.selectDataArr.length == 0) {
                this.$alert("请勾选要删除的数据！");
                return;
            }
            let ids = this.selectDataArr.map(e => e.standardId);
            this.$confirm(`此操作将永久删除该数据, 是否继续?`, {
                confirmButtonText: "确定",
                cancelButtonText: "取消",
                type: "warning"
            })
                .then(() => {
                    deleteStandard(ids)
                        .then(res => {
                            this.getListData();
                            this.getTinspectionName();
                            this.getcheckItemList();
                            this.$message({
                                type: "success",
                                message: "删除成功!"
                            });
                        })
                        .catch(e => {
                            this.$message.error("删除失败");
                        });
                })
                .catch(() => {
                    this.$message({
                        type: "info",
                        message: "已取消删除"
                    });
                });
        },
        //导出检查标准excel表格
        //导出
        exportTable () {
            if (this.tableData.length == 0) {
                if (this.params.projectType) {
                    if (this.params.inspectionId) {
                        this.$message({
                            message: " 当前物业类型下检查项暂无可导出数据",
                            type: "warning"
                        });
                        return;
                    } else {
                        this.$message({
                            message: "当前物业类型暂无可导出数据",
                            type: "warning"
                        });
                        return;
                    }
                } else {
                    this.$message({
                        message: "暂无可导出数据",
                        type: "warning"
                    });
                    return;
                }
            }
            var time = new Date();
            var name =
                time.getFullYear() +
                "-" +
                (time.getMonth() + 1) +
                "-" +
                time.getDate() +
                " " +
                time.getHours() +
                ":" +
                time.getMinutes() +
                ":" +
                time.getSeconds() +
                "检查标准列表.xls";
            var url = "/api/inspectionstandard/exportInspectionWays";
            downloadPost(url, {
                standardIds: this.selectDataArr.map(e => e.standardId),
                inspectionId: this.params.inspectionId,
                projectType: this.params.projectType,
                inspectionName: this.params.inspectionName
            }).then(response => {
                downloadBlob(response.data, name);
            });
        }
    }
};
