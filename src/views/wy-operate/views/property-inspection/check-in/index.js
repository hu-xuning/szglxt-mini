import { mapState } from "vuex";
import {
	inspectionList,
	getEditInformation,
	propertyNameList,
	useUnitList,
	getRoomInfo,
	getCheckItemList,
	changeModifyInspectionResults,
	deleteDeleteInspectionResults,
	getRoomUnit
} from "../../../api/check-in.js";
import { validatePhoneTwo } from "~/utils/validate.js";
import reviewDialog from "./reviewDialog.vue";

export default {
	provide() {
		return {
			initList: this.getListData
		};
	},
	components: {
		reviewDialog
	},
	computed: mapState({
		user: state => state.user.user
	}),
	data() {
		return {
			options: [],
			loading: false,
			dialogTableVisible: false,
			rectifyFileList: [],
			reviewFileList: [],
			rules: {
				roomId: [
					{
						required: true,
						message: "请选择物业名称",
						trigger: "blur"
					}
				],
				projectAddr: [
					{
						required: false,
						message: "请输物业地址",
						trigger: "blur"
					}
				],
				address: [
					{
						required: true,
						message: "请输入具体位置",
						trigger: "blur"
					}
				],
				manageUnit: [
					{
						required: true,
						message: "请选择所属管理所",
						trigger: "change"
					}
				],
				inspectionPerson: [
					{
						required: true,
						message: "请输入巡查人员",
						trigger: "blur"
					},
					{
						min: 1,
						max: 6,
						message: "长度在 1 到 6个字符",
						trigger: "blur"
					}
				],
				inspectionResult: [
					{
						required: true,
						message: "请输入巡查结果",
						trigger: "blur"
					},
					{
						min: 1,
						max: 6,
						message: "长度在 1 到 6个字符",
						trigger: "blur"
					}
				],
				predictInspectionDate: [
					{
						required: true,
						message: "请选择预计巡查日期",
						trigger: "blur"
					}
				],
				inspectionDate: [
					{
						required: true,
						message: "请选择实际巡查日期",
						trigger: "change"
					}
				],
				mobile: [
					{ required: false, message: "请输入联系电话", trigger: "blur" },
					{ validator: validatePhoneTwo, trigger: "blur" }
				]
			},
			manageUnits: [],
			unitList: [],
			uploadFileList: [], // 选中文件列表
			// 弹出框标题
			title: "新增巡查结果",
			//首页表格勾选列表
			selectDataArr: [],
			//巡查结果详情表单记录
			projectType: "",
			//新增|编辑表单数据
			resultDetailsForm: {
				reportDangerRecord: [],
				inspectionLogRecheck: [], //复查
				inspectionLogRectify: {
					opinion: "",
					inspectionLogId: ""
				} //整改
			},
			searchDataFrom: {},
			currentPage: 1,
			// 过滤检索配置
			filterList: [
				{
					label: "物业类型",
					inputType: "dict",
					name: "projectType",
					dictName: "property_types",
					multiple: true
				},
				{
					label: "检查结果",
					inputType: "dict",
					name: "inspectionResult",
					dictName: "inspection_result",
					multiple: true
				},
				{
					label: "管理所",
					inputType: "dict",
					name: "manageUnit",
					dictName: "administrative_office",
					multiple: true
				},
				{
					label: "检查日期",
					inputType: "daterange",
					name: ["startDate", "endDate"]
				}
			],
			params: {
				current: 1,
				size: 10,
				propertyName: ""
			},
			//标签页title  （为了展示没什么卵用）
			tabTitle: "上报隐患记录",
			dialogVisible: false,
			pager: {
				total: 0, // 总页数
				currentPage: 1, // 当前页数
				pageSize: 10 // 每页显示多少条
			},
			// 首页列表
			tableData: []
		};
	},
	mounted() {
		this.getListData();
		this.manageUnits = this.getDict("administrative_office");
	},
	methods: {
		//  * 码值转换
		dictVal_propertyTypes(row, column, cellValue) {
			let obj = this.getDict("property_types").find(p => p.value == cellValue);
			return obj ? obj.label : "";
		},
		// 菜单点击监听
		handleMenuClick(type) {
			switch (type) {
				case "add":
					this.showDetailsOfInspectionResults("add");
					break;
				case "delete":
					this.handleDelete();
					break;
				case "export":
					this.exportTable();
					break;
			}
		},
		// 管理所格式化
		manageFormatter(row, column, cellValue) {
			const manage = this.getDict("administrative_office").find(
				item => item.value == cellValue
			);
			return manage?.label || "";
		},
		// 巡查结果过滤
		inspectionResultFormatter(cellValue) {
			const manage = this.getDict("inspection_result").find(
				item => item.value == cellValue
			);
			return manage?.label || "";
		},
		//获取巡查结果列表
		getListData(params = {}) {
			let requestData = { ...this.params, ...params };
			inspectionList(requestData).then(res => {
				const { current, size, total, records } = res.data.data;
				this.tableData = records;
				this.params = requestData;
				this.pager = { total, current, size };
				if (this.tableData.length == 0) {
					this.$message.error("没有查到相关物业巡查记录");
				}
			});
		},
		//导出
		exportTable() {
			if (this.tableData.length == 0) {
				return this.$message({
					message: "暂无可导出数据",
					type: "warning"
				});
			}
			var time = new Date();
			var name =
				time.getFullYear() +
				"-" +
				(time.getMonth() + 1) +
				"-" +
				time.getDate() +
				" " +
				time.getHours() +
				":" +
				time.getMinutes() +
				":" +
				time.getSeconds() +
				"物业巡查结果.xls";
			this.publicExport(
				"物业巡查结果",
				"/api/inspectionresult/exportInspectionResults",
				this.params
			);
		},
		// 删除
		handleDelete() {
			if (!this.selectDataArr.length) {
				return this.$alert("请勾选要删除的数据！");
			}
			let ids = this.selectDataArr.map(e => e.resultId);
			this.$confirm(`此操作将永久删除该数据, 是否继续?`, {
				type: "warning"
			})
				.then(() => {
					deleteDeleteInspectionResults(ids)
						.then(res => {
							this.getListData();
							this.$message.success("删除成功");
						})
						.catch(e => {
							this.$message.error("删除失败");
						});
				})
				.catch(() => {
					this.$message.info("已取消删除");
				});
		},

		// 打开表单弹窗
		showDetailsOfInspectionResults(v, row) {
			//获取物业名称列表
			row && this.remotePropertyNameMethod(row.propertyName);
			// 表单清空
			this.resultDetailsForm = {
				inspectionLogRecheck: [], //复查
				reportDangerRecord: [],
				inspectionLogRectify: {
					opinion: "",
					inspectionLogId: ""
				} //整改
			};
			this.uploadFileList = [];
			this.rectifyFileList = [];
			// 打开弹窗
			this.dialogVisible = true;
			switch (v) {
				case "add":
					this.resultDetailsForm.inspectionPerson = this.user.realName;
					this.title = "新增巡查结果";
					break;
				case "edit":
					this.title = "编辑巡查结果";
					break;
				case "view":
					this.title = "查看巡查结果";
					break;
			}
			if (["edit", "view"].includes(v)) {
				// 获取当前这条巡查结果的详细信息
				getEditInformation(row.resultId).then(res => {
					this.resultDetailsForm = JSON.parse(JSON.stringify(res.data.data));
					if (!this.resultDetailsForm.inspectionLogRectify) {
						this.resultDetailsForm.inspectionLogRectify = {
							opinion: "",
							inspectionLogId: ""
						}; //整改
					}
					this.$nextTick(() => {
						this.$refs.upload.getFiles(this.resultDetailsForm.resultId);
						this.$refs.rectifyUpload.getFiles(
							this.resultDetailsForm.inspectionLogRectify.inspectionLogId
						);
					});
				});
			}
		},
		// 物业巡查模糊搜索
		async remotePropertyNameMethod(query) {
			if (query) {
				this.loading = true;
				const res = await propertyNameList(query);
				this.loading = false;
				this.options = res.data.data.map(item => {
					return { ...item, value: item.roomId, label: item.value };
				});
			} else this.options = [];
		},
		// 使用单位模糊查询
		async remoteUseUnitMethod(query) {
			if (!this.loading) {
				this.loading = true;
				const res = await useUnitList(query);
				this.unitList = res.data.data.map(item => {
					const { value } = item;
					return { value };
				});
			}
			this.loading = false;
		},
		//  物业类型选择监听
		async propertyNameMonitoring(ev) {
			console.log("物业名称选择监听");
			const res = await getRoomInfo(ev);
			this.resultDetailsForm.propertyName = res.data.data.value;
			this.resultDetailsForm.projectAddr = res.data.data.projectAddr;
			this.resultDetailsForm.address = res.data.data.address;
			this.resultDetailsForm.manageUnit = res.data.data.manageUnit;
			this.resultDetailsForm.projectType = res.data.data.projectType;
			this.resultDetailsForm.projectSubclassType =
				res.data.data.projectSubclassType;
			this.resultDetailsForm.propertyStatus = res.data.data.propertyStatus;
			//获取检查项列表
			this.getCheckItemList(res.data.data.projectType);
			//获取物业使用单位
			this.getUnit(res.data.data.roomId);
		},

		async getUnit(v) {
			const res = await getRoomUnit(v);
			if (res.data.data != null) {
				this.$set(this.resultDetailsForm, "useUnit", res.data.data.unitName);
				this.$set(this.resultDetailsForm, "mobile", res.data.data.telephone);
			}
		},
		// 获取检查项列表(新增时请求隐患记录) （参数：物业类型）
		async getCheckItemList(v) {
			const res = await getCheckItemList(v);
			this.$set(this.resultDetailsForm, "reportDangerRecord", res.data.data);
		},
		//关闭弹框
		close() {
			this.$refs.resultDetailsForm.resetFields();
			this.uploadFileList = [];
			this.rectifyFileList = [];
		},
		// 保存
		save() {
			if (["新增巡查结果", "编辑巡查结果"].includes(this.title)) {
				this.$refs.resultDetailsForm.validate(valid => {
					if (valid)
						this.$refs.rectifyUpload.submit(
							"rectify",
							this.resultDetailsForm.inspectionLogId
						);
				});
			}
		},
		// 上传文件提交
		async onSuccess(response, file, fileList) {
			let params =
				this.title == "新增巡查结果"
					? { ...this.resultDetailsForm, resultId: response.data }
					: this.resultDetailsForm;
			const res = await changeModifyInspectionResults(params, this.title);
			this.dialogVisible = false;
			this.$message.success(res.data.data.msg);
			this.getListData();
		},
		// 整改信息上传文件提交
		async rectifyOnSuccess(response, file, fileList) {
			this.resultDetailsForm.inspectionLogRectify = {
				...this.resultDetailsForm.inspectionLogRectify,
				inspectionLogId: response.data
			};
			this.$refs.upload.submit("checkin", this.resultDetailsForm.resultId);
		}
		// // 点击跳转物业查看
		// handleClickProperty(row) {
		// 	getProjectIdByRoomId(row.roomId).then(res => {
		// 		let pid = "";
		// 		if (res.data.code === 0 && res.data.data) {
		// 			pid = res.data.data.projectId;
		// 		}
		// 		if (pid !== "") {
		// 			this.$router.push({
		// 				name: "property_look",
		// 				query: { id: row.roomId, projectId: pid }
		// 			});
		// 		} else {
		// 			this.$message.error("该物业不存在！");
		// 		}
		// 	});
		// }
	}
};
