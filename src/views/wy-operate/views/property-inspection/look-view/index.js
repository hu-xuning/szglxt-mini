import { getDict } from "~/utils";
import {
	inspectionList,
	getEditInformation,
	propertyNameList,
	useUnitList,
	getCheckItemList,
	changeModifyInspectionResults,
	deleteDeleteInspectionResults
} from "../../../api/check-in.js";
import { getProjectIdByRoomId } from "@/views/wy-rent/api/rent-contract";

export default {
	provide() {
		return {
			initList: this.getListData
		};
	},
	data() {
		return {
			rectifyFileList: [],
			manageUnits: [],
			//首页表格勾选列表
			selectDataArr: [],
			inspectionResults: [],
			pager: {
				total: 0, // 总页数
				currentPage: 1, // 当前页数
				pageSize: 10 // 每页显示多少条
			},
			loading: false,
			// 首页分页及条件查询依赖
			uploadFileList: [], // 选中文件列表
			searchDataFrom: {},
			title: "查看巡查结果",
			tabTitle: "上报隐患记录",
			projectType: "",
			dialogTableVisible: false,
			form: {
				name: "",
				region: "",
				date1: "",
				date2: "",
				delivery: false,
				type: [],
				resource: "",
				desc: ""
			},
			params: {
				current: 1,
				size: 10,
				propertyName: ""
			},
			// 过滤检索配置
			filterList: [
				{
					label: "物业类型",
					inputType: "dict",
					name: "projectType",
					dictName: "property_types",
					multiple: true
				},
				{
					label: "检查结果",
					inputType: "dict",
					name: "inspectionResult",
					dictName: "inspection_result",
					multiple: true
				},
				{
					label: "管理所",
					inputType: "dict",
					name: "manageUnit",
					dictName: "administrative_office",
					multiple: true
				},
				{
					label: "检查日期",
					inputType: "daterange",
					name: ["startDate", "endDate"]
				}
			],
			tableData: [],
			//新增|编辑表单数据
			resultDetailsForm: {
				reportDangerRecord: [],
				inspectionLogRecheck: [], //复查
				inspectionLogRectify: {
					opinion: "",
					inspectionLogId: ""
				} //整改
			}
		};
	},
	mounted() {
		this.getListData();
		this.manageUnits = getDict("administrative_office");
	},
	methods: {
		//  * 码值转换
		dictVal_propertyTypes(row, column, cellValue) {
			let obj = getDict("property_types").find(p => p.value == cellValue);
			return obj ? obj.label : "";
		},
		// 巡查结果过滤
		inspectionResultFormatter(cellValue) {
			const manage = this.getDict("inspection_result").find(
				item => item.value == cellValue
			);
			return manage?.label || "";
		},
		// 管理所格式化
		manageFormatter(row, column, cellValue) {
			const manage = getDict("administrative_office").find(
				item => item.value == cellValue
			);
			return manage?.label || "";
		},
		//获取巡查结果列表
		getListData(params = {}) {
			let requestData = { ...this.params, ...params };
			inspectionList(requestData)
				.then(res => {
					const { current, size, total, records } = res.data.data;
					this.tableData = records;
					this.params = requestData;
					this.pager = { total, current, size };
				})
				.catch(() => {});
		},
		//关闭弹框
		close() {
			this.$refs.resultDetailsForm.resetFields();
		},
		showDetailsOfInspectionResults(v, row) {
			this.dialogTableVisible = true;
			// 表单清空
			this.resultDetailsForm = {
				inspectionLogRecheck: [], //复查
				reportDangerRecord: [],
				inspectionLogRectify: {
					opinion: "",
					inspectionLogId: ""
				} //整改
			};
			this.uploadFileList = [];
			this.rectifyFileList = [];
			getEditInformation(row.resultId)
				.then(res => {
					// 给表单复制
					this.resultDetailsForm = JSON.parse(JSON.stringify(res.data.data));
					if (!this.resultDetailsForm.inspectionLogRectify) {
						this.resultDetailsForm.inspectionLogRectify = {
							opinion: "",
							inspectionLogId: ""
						}; //整改
					}
					this.$nextTick(() => {
						this.$refs.upload &&
							this.$refs.upload.getFiles(this.resultDetailsForm.resultId);
						this.$refs.rectifyUpload.getFiles(
							this.resultDetailsForm.inspectionLogRectify.inspectionLogId
						);
					});
				})
				.catch({});
		},
		// 点击跳转物业查看
		handleClickProperty(row) {
			getProjectIdByRoomId(row.roomId).then(res => {
				let pid = "";
				if (res.data.code === 0 && res.data.data) {
					pid = res.data.data.projectId;
				}
				if (pid !== "") {
					this.$router.push({
						name: "property_look",
						query: { id: row.roomId, projectId: pid }
					});
				} else {
					this.$message.error("该物业不存在！");
				}
			});
		}
	}
};
