/*
 * @Author: xjt
 * @Date: 2021-04-30 10:24:51
 * @LastEditTime: 2021-04-30 11:15:32
 * @Description: file content
 * @LastEditors: xjt
 */
import propertyChooseDialog from "@/views/wy-operate/components/propertyChooseDialog";
import {
	changeContract,
	checkContractRepeat,
	maxContractNumber
} from "@/views/wy-operate/api/repair";
export default {
	components: {
		propertyChooseDialog
	},
	async mounted() {
		const res = await maxContractNumber();
		this.operationContract.contractNumber = res.data.msg;
	},
	data() {
		return {
			//合同基本信息
			operationContract: {
				contractId: "", //合同id
				supplierId: "", //供应商id
				businessType: "", //业务类型
				engineeringName: "", //工程名称
				roomId: "", //物业id
				propertyName: "", //物业名称
				area: "", //物业面积
				contractType: "", //合同类型
				contractSource: "", //合同来源
				signTime: "", //签订时间
				contractNumber: "", //合同编号
				contractName: "", //合同名称
				unitType: "", //单位类型
				unitName: "", //单位名称
				startTime: "", //开工时间
				endTime: "", //竣工时间
				constructionPeriod: "", //工期
				budgetAmount: "", //预算金额
				censorshipAmount: "", //送审金额
				approvalAmount: "", //审定金额
				contractAmount: "", //合同金额
				totalAmount: "", //总金额
				payMethod: "", //付款方式
				remarks: "", //备注
				billId: "", //工单id
				warrantyPeriod: 0, //保修期-单位月
				margin: "" //保证金
			},
			//服务价目列表
			operateContractDetailList: [],

			pickerOptions: {
				disabledDate:function disabledDate(time){
					//return time.getTime() < Date.now() // 选当前时间之后的时间
					return time.getTime() > Date.now() // 选当前时间之前的时间
				}
			}
		};
	},
	computed: {
		// 校验规则
		rules() {
			return {
				engineeringName: [
					{ required: true, message: "工程名称不能为空", trigger: "blur" }
				],
				contractName: [
					{ required: true, message: "合同名称不能为空", trigger: "blur" },
					{ validator: this.checkContractName, trigger: "blur" }
				],
				contractNumber: [
					{ required: true, message: "合同编号不能为空", trigger: "blur" },
					{ validator: this.checkContractNumber, trigger: "blur" }
				],
				propertyName: [
					{ required: true, message: "物业名称不能为空", trigger: "blur" }
				],
				unitName: [
					{ required: true, message: "单位名称不能为空", trigger: "blur" }
				],
				signTime: [
					{ required: true, message: "签订日期不能为空", trigger: "blur" }
				],
				totalAmount: [
					{ required: true, message: "合同金额不能为空", trigger: "blur" },
					{
						pattern: /(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/,
						message: "请输入正确的金额",
						trigger: "blur"
					}
				],
				constructionPeriod: [
					{ required: true, message: "工期不能为空", trigger: "blur" },
					//{
					//	type: "number",
					//	min: 0,
					//	message: "请输入正确工期（必须为数字且最小为0）",
					//	trigger: "blur"
					//}
				],
				payMethod: [
					{ required: true, message: "付款方式不能为空", trigger: "blur" }
				],
				margin: [{ required: true, message: "请输入保证金", trigger: "blur" }]
			};
		}
	},
	methods: {
		// 提交新增
		save() {
			this.$refs.form.validate(valid => {
				if (valid && this.customValidate()) {
					const params = {
						operationContract: this.operationContract,
						operateContractDetailList: this.operateContractDetailList,
						obtId: this.operationContract.obtId
					};
					changeContract(params).then(res => {
						if (res.data.code == 0) {
							this.$message.success("新增成功!");
							this.$router.back();
						} else {
							this.$message.error(res.data.code);
						}
					});
				}
			});
		},
		// 自定义表单验证
		customValidate() {
			if (this.operateContractDetailList.length) {
				if (this.operateContractDetailList.some(item => item.unitPrice == "")) {
					this.$message.error("单价不能为空");
					return false;
				} else if (
					this.operateContractDetailList.some(
						item =>
							!/(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/.test(
								item.unitPrice
							)
					)
				) {
					this.$message.error("请输入正确的单价");
					return false;
				} else if (
					this.operateContractDetailList.some(item => item.costItems == "")
				) {
					this.$message.error("费用项目不能为空");
					return false;
				} else if (
					this.operateContractDetailList.some(item => item.paymentAmount == "")
				) {
					this.$message.error("付款金额不能为空");
					return false;
				} else if (
					this.operateContractDetailList.some(
						item =>
							!/(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/.test(
								item.paymentAmount
							)
					)
				) {
					this.$message.error("请输入正确的付款金额");
					return false;
				} else if (
					this.operateContractDetailList.some(
						item =>
							!/(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/.test(
								item.discountAmount
							)
					)
				) {
					this.$message.error("请输入正确的折扣金额");
					return false;
				}
			}
			return true;
		},
		// 添加物业价目明细
		addDetailList() {
			this.operateContractDetailList.push({
				contractDetailId: "", //价目表id
				contractId: "", //合同id  后台已做统一赋值为合同基本信息表id
				unitPrice: "", //单价
				costItems: "", //费用项目
				paymentAmount: "", //付款金额
				discountAmount: "", //折扣金额
				finallyAmount: "", //最终金额
				remarks: "" //备注
			});
		},
		// 删除价目明细
		releteDetailList(index) {
			this.operateContractDetailList.splice(index, 1);
		},
		// 计算价格
		getTotalCount(index, paymentAmount = 0, discountAmount = 0) {
			let totalCount = 0;
			totalCount = (paymentAmount - discountAmount).toFixed(2);
			this.operateContractDetailList[index].finallyAmount = totalCount;
			return totalCount;
		},
		// 选择预算回调
		handlePropertyChange(data) {
			this.operationContract = {
				...this.operationContract,
				...data,
				totalAmount: data.approvalAmount,
				contractName: data.engineeringName + "施工工程"
			};
		},
		// 合同名称校验
		checkContractName(rule, value, callback) {
			let params = { vType: "1", value, id: this.operationContract.contractId };
			checkContractRepeat(params).then(res => {
				res.data.data == 1 ? callback(new Error("合同名称重复")) : callback();
			});
		},
		// 合同编号
		checkContractNumber(rule, value, callback) {
			let params = { vType: "2", value, id: this.operationContract.contractId };
			checkContractRepeat(params).then(res => {
				res.data.data == 1 ? callback(new Error("合同编号重复")) : callback();
			});
		}
	}
};
