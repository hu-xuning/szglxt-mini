import { getDePartment, deleteDepartment } from "@/views/wy-operate/api/repair.js";
import { mapGetters } from "vuex"
import departmentDialog from './components/dialog'
import dialogUpload from '@/views/wy-operate/components/dialogUpload';

export default {
    components: {
        departmentDialog,
        dialogUpload
    },
    provide() {
			return {
  	          //初始化数据
				initList: this.queryData,
			};
		},
    computed: {
        ...mapGetters(['permissions'])
    },
    data() {
        return {
            // 表格列表数据
            tableData: [],
            // 请求参数
            params: {
                current: 1,
                size: 20,
                clientProviderName: ''
            },
            // 选中的数据
            selectDataArr: [],
            // 分页
            pager: {
                total: 0, // 总页数
								current: 1, // 当前页数
								size: 20 // 每页显示多少条
            },
            // 页面loading
            loading: false,
        }
    },
    mounted() {
        this.queryData();
    },
    methods: {
        // 数据请求
				queryData (params = {}) {
					this.loading = true;
					let requestData = { ...this.params, ...params };
					getDePartment(requestData)
						.then(res => {
							const { current, size, total, records } = res.data.data;
							this.tableData = records;
							this.params = requestData;
							this.pager = { total, current, size };
						})
						.finally(() => {
							this.loading = false;
						});
				},
        // 数据删除
        deleteData() {
            if (this.selectDataArr.length == 0) {
                this.$alert("请勾选要删除的数据！");
                return;
            }
            this.$confirm("此操作将永久删除该数据, 是否继续?", {
                confirmButtonText: "确定",
                cancelButtonText: "取消",
                type: "warning",
            })
                .then(() => {
                    let ids = this.selectDataArr.map((sd) => sd.supplierId);
                    deleteDepartment(ids)
                        .then((rep) => {
                            this.$message.success("删除成功!");
                            this.queryData();
                        })
                        .catch((e) => {
                            this.$message.fail("删除失败!");
                        });
                })
                .catch(() => {
                    this.$message({
                        type: "info",
                        message: "已取消删除",
                    });
                });
        },
        //表格按钮点击事件监听
        handleBtnClick(type, row = {}){
            switch (type) {
                case 'preview':
                case 'edit':
                    this.$refs.editDialog.show({
                        type: type,
                        data: JSON.parse(JSON.stringify(row))
                    })
                    break;
                case 'delete':
                    this.$confirm("此操作将永久删除该数据, 是否继续?", {
                        confirmButtonText: "确定",
                        cancelButtonText: "取消",
                        type: "warning",
                    }).then(() => {
                        deleteDepartment([row.supplierId]).then((rep) => {
                            this.$message.success("删除成功!")
                            this.queryData({current: 1});
                        }).catch((e) => {
                            this.$message.fail("删除失败!")
                        });
                    }).catch(() => {
                        this.$message({
                            type: "info",
                            message: "已取消删除",
                        });
                    });
                    break;
                case 'upload':
                    this.$refs.dialogUpload.show({
                        data: row
                    })
                    break;
                default:
                    break;
            }
        },
        // 添加
        toAdd() {
            this.$refs.editDialog.show({
                type: 'add'
            })
        },
			// 菜单点击监听
			handleMenuClick (type) {
				switch (type) {
					case "add":
						this.toAdd();
						break;
					case "delete":
						this.deleteData();
						break;
					default:
						this.$message(type);
						break;
				}
			}
    },
};
