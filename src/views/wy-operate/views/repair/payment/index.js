import {
    getPaymentInfo,
    deletePaymentInfo,
    exportData
} from "@/views/wy-operate/api/repair.js";
import { mapGetters, mapState } from "vuex";
import departmentDialog from "./components/dialog";
import { downloadBlob } from "@/utils";

export default {
    components: {
        departmentDialog
    },
    provide() {
        return {
            initList: this.queryData
        };
    },
    computed: {
        ...mapGetters(["permissions"]),
        ...mapState({
            // 公司类型列表
            companyDict: state =>
                state.dict.find(item => item.type === "provider_type") || {
                    children: []
                }
        })
    },
    data() {
        return {
            // 表格列表数据
            tableData: [],
            // 请求参数
            params: {
                current: 1,
                size: 20,
                keyword: ""
            },
            // 选中的数据
            selectDataArr: [],
            // 分页
            pager: {
                total: 0, // 总页数
								current: 1, // 当前页数
								size: 20 // 每页显示多少条
            },
            // 页面loading
            loading: false
        };
    },
    mounted() {
        this.queryData();
    },
    methods: {
        // 数据请求
				queryData (params = {}) {
					this.loading = true;
					let requestData = { ...this.params, ...params };
					getPaymentInfo(requestData)
						.then(res => {
							const { current, size, total, records } = res.data.data;
							this.tableData = records;
							this.params = requestData;
							this.pager = { total, current, size };
						})
						.finally(() => {
							this.loading = false;
						});
				},
        // 数据删除
        deleteData() {
            if (this.selectDataArr.length == 0) {
                this.$alert("请勾选要删除的数据！");
                return;
            }
            this.$confirm("此操作将永久删除该数据, 是否继续?", {
                confirmButtonText: "确定",
                cancelButtonText: "取消",
                type: "warning"
            })
                .then(async () => {
                    let payIds = this.selectDataArr.map(item => item.payId);
                    let itemIds = this.selectDataArr.map(item => item.itemId);
                    try {
                        await deletePaymentInfo(itemIds, payIds);
                        this.$message.success("删除成功!");
                        this.queryData({ current: 1 });
                    } catch (error) {
                        this.$message.fail("删除失败!");
                    }
                })
                .catch(() => {
                    this.$message.info("已取消删除");
                });
        },
        //表格按钮点击事件监听
        handleBtnClick(type, row = {}) {
            switch (type) {
                case "preview":
                case "edit":
                    this.$refs.editDialog.show({
                        type,
                        data: JSON.parse(JSON.stringify(row))
                    });
                    break;
                case "delete":
                    this.$confirm("此操作将永久删除该数据, 是否继续?", {
                        confirmButtonText: "确定",
                        cancelButtonText: "取消",
                        type: "warning"
                    })
                        .then(async () => {
                            try {
                                await deletePaymentInfo(
                                    [row.itemId],
                                    [row.payId]
                                );
                                this.$message.success("删除成功!");
                                this.queryData({ current: 1 });
                            } catch (error) {
                                this.$message.fail("删除失败!");
                            }
                        })
                        .catch(() => {
                            this.$message.info("已取消删除");
                        });
                    break;
                default:
                    break;
            }
        },
        // 添加
        toAdd() {
            this.$refs.editDialog.show({
                type: "add"
            });
        },
        // 获取公司单位类型
        getCompanyName(unitType) {
            let companyType = this.companyDict.children.find(
                item => item.value == unitType
            );
            return companyType ? companyType.label : "";
        },
        // 导出
        async exportData() {
            const loading = this.$loading({
                lock: true,
                text: "数据处理中"
            });
            const response = await exportData(
                "payment",
                this.tableData.map(item => item.itemId)
            );
            loading.close();
            var time = new Date();
            let name =
                time.getFullYear() +
                "-" +
                (time.getMonth() + 1) +
                "-" +
                time.getDate() +
                " " +
                time.getHours() +
                ":" +
                time.getMinutes() +
                ":" +
                time.getSeconds() +
                "-" +
                "付款登记.xlsx";
            downloadBlob(response.data, name);
        },
				// 菜单点击监听
				handleMenuClick (type) {
					switch (type) {
						case "add":
							this.toAdd();
							break;
						case "delete":
							this.deleteData();
							break;
						case "export":
							this.exportData();
							break;
						default:
							this.$message(type);
							break;
					}
				}
    }
};
