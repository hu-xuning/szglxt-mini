import request from '@/plugins/axios'

//分页
export function query(query){
    return request({
        url: '/api/epcproject/page',
        method: 'get',
        params: query
    })
}
//新增
export function save(data){
    return request({
        url: '/api/epcproject',
        method: 'post',
        data: data
    })
}
//修改
export function update(data){
    return request({
        url: '/api/epcproject',
        method: 'put',
        data: data
    })
}
//查看
export function get(id){
    return request({
        url: '/api/epcproject/' + id,
        method: 'get'
    })
}
//验证中标通知书文号唯一
export function validateBidDocument(query){
    return request({
        url: '/api/epcproject/validateCode',
        method: 'get',
        params: query
    })
}
//验证工程名称唯一
export function validateName(query){
    return request({
        url: '/api/epcproject/validateName',
        method: 'get',
        params: query
    })
}
//验证工程是否可以删除
export function deleteValidate(id){
    return request({
        url: '/api/epcproject/deleteValidate',
        method: 'get',
        params: {"projectId": id}
    })
}
//通过id批量删除工程登记
export function batchRemove(data){
    return request({
        url: '/api/epcproject/batchRemove',
        method: 'delete',
        data: data
    })
}
