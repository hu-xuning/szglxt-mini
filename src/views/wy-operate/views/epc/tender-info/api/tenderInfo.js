import request from '@/plugins/axios'

//分页
export function query(query){
    return request({
        url: '/api/epcprojecttender/page',
        method: 'get',
        params: query
    })
}
//验证是否可以删除
export function deleteValidate(id){
    return request({
        url: '/api/epcprojecttender/deleteValidate' + id,
        method: 'get'
    })
}
//通过id批量删除工程登记
export function batchRemove(data){
    return request({
        url: '/api/epcprojecttender/removeBatch',
        method: 'delete',
        data: data
    })
}
//查看
export function get(id){
    return request({
        url: '/api/epcprojecttender/' + id,
        method: 'get'
    })
}
//验证文号唯一
export function validateCode(query){
    return request({
        url: '/api/epcprojecttender/validateCode',
        method: 'get',
        params: query
    })
}
//新增
export function save(data){
    return request({
        url: '/api/epcprojecttender',
        method: 'post',
        data: data
    })
}
//修改
export function update(data){
    return request({
        url: '/api/epcprojecttender',
        method: 'put',
        data: data
    })
}
