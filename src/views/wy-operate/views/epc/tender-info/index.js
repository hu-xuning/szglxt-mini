import dataList from '@/components/dataList'
import { getDict } from '~/utils'
import editData from './components/edit-data'
import showActual from './components/show-actual'
import {query, batchRemove} from './api/tenderInfo'
export default {
    name: 'bidding-registration-box',
    components: {dataList, editData, showActual},
    provide() {
        return {
            initList: this.getListData,
        };
    },
    beforeMount() {
        this.getListData();
    },
    mounted() {
        this.projectTypes = getDict("epc_project_type")
    },
    data() {
        return {
            tableData: [],
            // 请求参数
            params: {
                current: 1,
                size: 10,
                projectName: ''
            },
            //筛选组
            selectDataArr: [],
            // 分页
            pager: {
                total: 0, // 总页数
                current: 1, // 当前页数
                size: 10 // 每页显示多少条
            },
            // 页面loading
            loading: false,
            filterList: [
                { label: '录入日期', inputType: 'daterange', name: ['createTimeStart', 'createTimeEnd']},
                { label: '工程类型', inputType: 'dict', dictName: 'epc_project_type', name: 'projectType', multiple: true}
            ],
            ids: [],
            projectTypes: [],
            editDataShow: false,
            showActual: false
        }
    },
    methods: {
        //分页请求
        getListData(params = {}){
            this.loading = true
            let requestData = {...this.params, ...params}
            query(requestData).then(res => {
                const  {current, size, total, records} = res.data.data
                this.tableData = records
                this.params = requestData
                this.pager = {total, current, size}
            }).finally(() => {
                this.loading = false
            })
        },
        // 菜单点击监听
        handleMenuClick(type){
            switch (type) {
                case 'add':
                    this.handleToAdd();
                    break;
                case 'delete':
                    this.handleToMultipleDelete();
                    break;
                default:
                    this.$message(type)
                    break;
            }
        },
        // 新增
        handleToAdd() {
            this.editDataShow = true
            this.$nextTick(() => {
                this.$refs.editData.show()
            })
        },
        //新增编辑关闭弹框
        handleClose() {
            this.editDataShow = false
        },
        //查看关闭弹框
        handleShowClose() {
            this.showActual = false
        },
        // 编辑
        handleToEdit(item) {
            this.editDataShow = true
            this.$nextTick(() => {
                this.$refs.editData.show(item.projectTenderId)
            })
        },
        // 查看
        handleToLook(item) {
            this.showActual = true
            this.$nextTick(() => {
                this.$refs.showActual.show(item.projectTenderId)
            })
        },
        // 删除
        handleToDelete(item) {
            let ids = []
            ids.push(item.projectTenderId)

            if(item.projectStatus != "300"){
                this.$message.warning('该招标信息不可删除！')
                return false;
            }
            this.$confirm("此操作将永久删除该数据, 是否继续?", {
                confirmButtonText: "确定",
                cancelButtonText: "取消",
                type: "warning",
            }).then(() => {
                batchRemove(ids).then((res) => {
                    this.$message({
                        type: "success",
                        message: "删除成功!",
                    });
                    this.getListData();
                }).catch((e) => {
                    this.$message({
                        type: "success",
                        message: "删除失败!",
                    });
                });
            }).catch(() => {
                this.$message({
                    type: "info",
                    message: "已取消删除",
                });
            })
        },
        // 多选删除
        handleToMultipleDelete() {
            let nvData = []
            let selIds = [];
            this.selectDataArr.forEach(item => {
                selIds.push(item.projectId)
                if(item.projectStatus != '300'){
                    nvData.push(item)
                }
            })
            if(nvData){
                let nData = nvData.map(it => it.projectName)
                this.$message.warning('工程【'+nData.join("，")+'】不可删除')
                return false
            }

            if (selIds) {
                this.$confirm("此操作将永久删除该数据, 是否继续?", {
                    confirmButtonText: "确定",
                    cancelButtonText: "取消",
                    type: "warning",
                }).then(() => {
                    batchRemove(selIds).then((res) => {
                        this.$message({
                            type: "success",
                            message: "删除成功!",
                        });
                        this.getListData();
                    }).catch((e) => {
                        this.$message({
                            type: "success",
                            message: "删除失败!",
                        });
                    });
                }).catch(() => {
                    this.$message({
                        type: "info",
                        message: "已取消删除",
                    });
                })
            }
        },
        //对应value取label
        getCardTypeValue(num, sum){
            let arr = sum.filter(e=>e.value === num)
            if (arr.length > 0) {
                return arr[0].label
            } else {
                return num
            }
        },
        resetList() {
            this.editDataShow = false
            this.getListData()
        }
    }
}
