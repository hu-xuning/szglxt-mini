import request from '@/plugins/axios'

//分页
export function query(query){
    return request({
        url: '/api/epcprojectsetup/page',
        method: 'get',
        params: query
    })
}
//新增
export function save(data){
    return request({
        url: '/api/epcprojectsetup',
        method: 'post',
        data: data
    })
}
//修改
export function update(data){
    return request({
        url: '/api/epcprojectsetup',
        method: 'put',
        data: data
    })
}
//查看
export function get(id){
    return request({
        url: '/api/epcprojectsetup/' + id,
        method: 'get'
    })
}
//验证文号唯一
export function validateCode(query){
    return request({
        url: '/api/epcprojectsetup/validateCode',
        method: 'get',
        params: query
    })
}
//工程名称分页
export function queryProject(query){
    return request({
        url: '/api/epcprojectsetup/queryProject',
        method: 'get',
        params: query
    })
}
//验证是否可以删除
export function deleteValidate(id){
    return request({
        url: '/api/epcprojectsetup/deleteValidate' + id,
        method: 'get'
    })
}
//通过id批量删除工程登记
export function batchRemove(data){
    return request({
        url: '/api/epcprojectsetup/removeBatch',
        method: 'delete',
        data: data
    })
}
//验证工程名称是否可修改
export function validateProjectStatus(query){
	return request({
		url: '/api/epcproject/validateProjectStatus',
		method: 'get',
		params: query
	})
}
