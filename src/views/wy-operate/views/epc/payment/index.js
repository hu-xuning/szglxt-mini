import dataList from '@/components/dataList'
import { getDict } from '~/utils'
import editData from './components/edit-data'
import showActual from "./components/show-actual"
import {query, batchRemove} from './api/payment'
let moment = require('moment')
export default {
    name: 'payment',
    components: {dataList, editData, showActual},
    provide() {
        return {
            initList: this.getListData,
        };
    },
    beforeMount() {
        this.getListData();
    },
    mounted() {
        this.contractTypes = getDict("epc_contract_type")
    },
    data() {
        return {
            tableData: [],
            // 请求参数
            params: {
                current: 1,
                size: 10,
                projectName: ''
            },
            //筛选组
            selectDataArr: [],
            // 分页
            pager: {
                total: 0, // 总页数
                current: 1, // 当前页数
                size: 10 // 每页显示多少条
            },
            // 页面loading
            loading: false,
            filterList: [
                { label: '付款日期', inputType: 'daterange', name: ['payDateStart', 'payDateEnd']},
                { label: '工程类型', inputType: 'dict', dictName: 'epc_project_type', name: 'projectType', multiple: true},
                { label: '合同类型', inputType: 'dict', dictName: 'epc_contract_type', name: 'contractType', multiple: true}
            ],
            projectPaymentId: [],
            contractTypes: [],
            editDataShow: false,
            showActual: false
        }
    },
    methods: {
        //分页请求
        getListData(params = {}){
            this.loading = true
            let requestData = {...this.params, ...params}
            query(requestData).then(res => {
                const  {current, size, total, records} = res.data.data
                this.tableData = records
                this.params = requestData
                this.pager = {total, current, size}
            }).finally(() => {
                this.loading = false
            })
        },
        // 菜单点击监听
        handleMenuClick(type){
            switch (type) {
                case 'add':
                    this.handleToAdd();
                    break;
                case 'export':
                    this.handleToExport();
                    break;
                default:
                    this.$message(type)
                    break;
            }
        },
        // 新增
        handleToAdd() {
            this.editDataShow = true
            this.$nextTick(() => {
                this.$refs.editData.show()
            })
        },
        //新增编辑关闭弹框
        handleClose() {
            this.editDataShow = false
        },
        //查看关闭弹框
        handleShowClose() {
            this.showActual = false
        },
        // 导出
        handleToExport() {
            this.publicExport('付款登记','/api/epcprojectpayment/download',this.params)
        },
        // 编辑
        handleToEdit(item) {
            this.editDataShow = true
            this.$nextTick(() => {
                this.$refs.editData.show(item.projectPaymentId)
            })
        },
        // 查看
        handleToLook(item) {
            this.showActual = true
            this.$nextTick(() => {
                this.$refs.showActual.show(item.projectPaymentId)
            })
        },
        // 删除
        handleToDelete(item) {
            this.$confirm("此操作将永久删除该数据, 是否继续?", {
                confirmButtonText: "确定",
                cancelButtonText: "取消",
                type: "warning",
            }).then(() => {
                batchRemove(item.projectPaymentId).then((res) => {
                    this.$message({
                        type: "success",
                        message: "删除成功!",
                    });
                    this.getListData();
                }).catch((e) => {
                    this.$message({
                        type: "success",
                        message: "删除失败!",
                    });
                });
            }).catch(() => {
                this.$message({
                    type: "info",
                    message: "已取消删除",
                });
            })
        },
        //对应value取label
        getCardTypeValue(num, sum){
            let arr = sum.filter(e=>e.value === num)
            if (arr.length > 0) {
                return arr[0].label
            } else {
                return num
            }
        },
        //时间格式过滤
        dateFormat: function(row, column, cellValue){
            if(!cellValue){
                return ''
            }
            return moment(cellValue).format('YYYY-MM-DD')
        },
        resetList() {
            this.editDataShow = false
            this.getListData()
        }
    }
}
