import request from '@/plugins/axios'

//分页
export function query(query){
    return request({
        url: '/api/epcprojectpayment/page',
        method: 'get',
        params: query
    })
}
//通过id批量删除工程登记
export function batchRemove(id){
    return request({
        url: '/api/epcprojectpayment/' + id,
        method: 'delete'
    })
}
//新增
export function save(data){
    return request({
        url: '/api/epcprojectpayment',
        method: 'post',
        data: data
    })
}
//修改
export function update(data){
    return request({
        url: '/api/epcprojectpayment',
        method: 'put',
        data: data
    })
}
//查看
export function get(id){
    return request({
        url: '/api/epcprojectpayment/' + id,
        method: 'get'
    })
}
//验证金额是否正确
export function validateCode(query){
    return request({
        url: '/api/epcprojectpayment/validateCode',
        method: 'get',
        params: query
    })
}
