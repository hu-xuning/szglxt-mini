import request from '@/plugins/axios'

//分页
export function query(query){
    return request({
        url: '/api/epcprojectcontract/page',
        method: 'get',
        params: query
    })
}
//通过id批量删除工程登记
export function batchRemove(data){
    return request({
        url: '/api/epcprojectcontract/batchRemove',
        method: 'delete',
        data: data
    })
}
//新增
export function save(data){
    return request({
        url: '/api/epcprojectcontract',
        method: 'post',
        data: data
    })
}
//修改
export function update(data){
    return request({
        url: '/api/epcprojectcontract',
        method: 'put',
        data: data
    })
}
//查看
export function get(id){
    return request({
        url: '/api/epcprojectcontract/' + id,
        method: 'get'
    })
}
//验证合同编号唯一
export function validateCode(query){
    return request({
        url: '/api/epcprojectcontract/validateCode',
        method: 'get',
        params: query
    })
}
