import { get } from "../api/contract";
import subShow from "./sub-show.vue";

export default {
	name: "show-actual",
	created() {
		this.projectTypes = this.getDict("epc_project_type");
		this.contractTypes = this.getDict("epc_contract_type");
		this.projectForm.projectContract.projectContractId = this.$route.query.projectContractId;
		if (this.projectForm.projectContract.projectContractId) {
			this.handleInit(this.projectForm.projectContract.projectContractId);
		}
	},
	components: {
		subShow
	},
	data() {
		return {
			projectForm: {
				projectContract: {
					projectId: "",
					projectTenderId: "",
					projectName: "",
					projectAddress: "",
					projectType: "",
					projectContractId: "",
					contractNum: "",
					contractType: "",
					contractAmount: "",
					unitPrice: "",
					contractDate: "",
					startDate: "",
					endDate: "",
					partybUnit: "",
					partybContact: "",
					partybPhone: "",
					supplement: "",
					signDate: "",
					suppleStart: "",
					suppleEnd: "",
					suppleAmount: "",
					remarks: ""
				},
				subContracts: []
			},
			projectTypes: [],
			contractTypes: [],
			supplementaryFileList: [],
			fileList: [],
			supplements: [
				{
					value: "1",
					label: "是"
				},
				{
					value: "0",
					label: "否"
				}
			],
			showActual: false
		};
	},
	methods: {
		//对应value取label
		getCardTypeValue(num, sum) {
			let arr = sum.filter(e => e.value === num);
			if (arr.length > 0) {
				return arr[0].label;
			} else {
				return num;
			}
		},
		//补充协议附件回显过滤
		supplementaryLoadCompleted(data) {
			this.$refs.supplementaryUpload.loadFileList = data.filter(
				item => item.module === "supplementary"
			);
		},
		//附件回显过滤
		normalLoadCompleted(data) {
			this.$refs.normalUpload.loadFileList = data.filter(
				item => item.module === "normal"
			);
		},
		//子合同查看
		handleClickLook(item) {
			this.showActual = true;
			this.$nextTick(() => {
				this.$refs.subShow.showDialog(item);
			});
		},
		//子合同关闭弹框组件
		handleClose() {
			this.showActual = false;
		},
		//回显
		handleInit(id) {
			get(id).then(res => {
				if (res.data.code === 0) {
					this.projectForm = res.data.data;
					let obj,
						arr = [];
					this.projectForm.subContracts.map(e => {
						obj = { ...e.subContracts };
						obj.otherExpenses = e.otherExpenses;
						arr.push(obj);
					});
					this.projectForm.subContracts = arr;
				}
			});
		},
		// 关闭
		handleBack() {
			//关闭当前新增页，暂时没找到好办法
			this.$store.commit("delHistoryRoute", this.$route.fullPath);
			//跳转
			this.$router.push("/wy-operate/epc/contract");
		}
	}
};
