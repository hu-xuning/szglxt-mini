import dataList from "@/components/dataList";
import showActual from "./components/show-actual";
import { query, batchRemove } from "./api/contract";
import CacheParams from "@/assets/vue-mixin/CacheParams";
export default {
	name: "bidding-registration-box",
	components: { dataList, showActual },
	mixins: [CacheParams],
	provide() {
		return {
			initList: this.getListData
		};
	},
	mounted() {
		this.getListData();
	},
	data() {
		return {
			tableData: [],
			// 请求参数
			params: {
				current: 1,
				size: 10,
				projectName: ""
			},
			//筛选组
			selectDataArr: [],
			// 分页
			pager: {
				total: 0, // 总页数
				current: 1, // 当前页数
				size: 10 // 每页显示多少条
			},
			// 页面loading
			loading: false,
			filterList: [
				{
					label: "录入日期",
					inputType: "daterange",
					name: ["createTimeStart", "createTimeEnd"]
				},
				{
					label: "工程类型",
					inputType: "dict",
					dictName: "epc_project_type",
					name: "projectType",
					multiple: true
				},
				{
					label: "合同类型",
					inputType: "dict",
					dictName: "epc_contract_type",
					name: "contractType",
					multiple: true
				}
			],
			ids: [],
			showActual: false
		};
	},
	computed: {
		contractTypes() {
			return this.getDict("epc_contract_type");
		}
	},
	methods: {
		//分页请求
		getListData(params = {}) {
			this.loading = true;
			let requestData = { ...this.params, ...params };
			query(requestData)
				.then(res => {
					const { current, size, total, records } = res.data.data;
					this.tableData = records;
					this.params = requestData;
					this.pager = { total, current, size };
				})
				.finally(() => {
					this.loading = false;
				});
		},
		// 菜单点击监听
		handleMenuClick(type) {
			switch (type) {
				case "add":
					this.handleToAdd();
					break;
				case "export":
					this.handleToExport();
					break;
				case "delete":
					this.handleToMultipleDelete();
					break;
				default:
					this.$message(type);
					break;
			}
		},
		// 新增
		handleToAdd() {
			this.$router.push({ name: "contract_add" });
		},
		//查看关闭弹框
		handleShowClose() {
			this.showActual = false;
		},
		// 编辑
		handleToEdit(item) {
			this.$router.push({
				name: "contract_edit",
				params: { projectContractId: item.projectContractId }
			});
		},
		// 导出
		handleToExport() {
			this.publicExport(
				"合同录入",
				"/api/epcprojectcontract/download",
				this.params
			);
		},
		// 查看
		handleToLook(item) {
			this.$router.push({
				name: "contract_look",
				query: { projectContractId: item.projectContractId }
			});
		},
		// 删除
		handleToDelete(item) {
			if (item.contractStatus != "100") {
				this.$message({
					type: "error",
					message: "改合同已付款，不可删除！"
				});
				return false;
			}
			let ids = [];
			ids.push(item.projectContractId);
			this.$confirm("此操作将永久删除该数据, 是否继续?", {
				confirmButtonText: "确定",
				cancelButtonText: "取消",
				type: "warning"
			})
				.then(() => {
					batchRemove(ids)
						.then(res => {
							this.$message({
								type: "success",
								message: "删除成功!"
							});
							this.getListData();
						})
						.catch(e => {
							this.$message({
								type: "success",
								message: "删除失败!"
							});
						});
				})
				.catch(() => {
					this.$message({
						type: "info",
						message: "已取消删除"
					});
				});
		},
		// 多选删除
		handleToMultipleDelete() {
			let nData = [];
			this.selectDataArr.map(item => {
				this.ids.push(item.projectContractIds);
				if (item.contractStatus != "100") {
					nData.push(item);
				}
			});

			if ((nData != null) & (nData.length > 0)) {
				let cNo = [];
				nData.map(item => {
					cNo.push(item.contractNum);
				});

				this.$message({
					type: "error",
					message: "合同编号【" + cNo.join(",") + "】已付款，不能删除！"
				});
				return false;
			}

			if (this.ids) {
				this.$confirm("此操作将永久删除该数据, 是否继续?", {
					confirmButtonText: "确定",
					cancelButtonText: "取消",
					type: "warning"
				})
					.then(() => {
						batchRemove(this.ids)
							.then(res => {
								this.$message({
									type: "success",
									message: "删除成功!"
								});
								this.getListData();
							})
							.catch(e => {
								this.$message({
									type: "error",
									message: "删除失败!"
								});
							});
					})
					.catch(() => {
						this.$message({
							type: "info",
							message: "已取消删除"
						});
					});
			}
		},
		//对应value取label
		getCardTypeValue(num, sum) {
			let arr = sum.filter(e => e.value === num);
			if (arr.length > 0) {
				return arr[0].label;
			} else {
				return num;
			}
		}
	}
};
