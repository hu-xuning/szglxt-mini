/*
 * @Author: cqg
 * @Date: 2020-12-08 17:15:03
 * @LastEditors: cqg
 * @LastEditTime: 2020-12-22 09:40:10
 * @Description: file content
 */
import request from "@/plugins/axios";
// 分页查询
export function query(params) {
    return request({
        url: "/api/epcprojectfinal/page",
        method: "get",
        params
    });
}

// 新增或者修改
export function updateOrSave(type, data) {
    return request({
        url: "/api/epcprojectfinal",
        method: type == "add" ? "post" : "put",
        data
    });
}

// 批量删除
export function remove(ids) {
    return request({
        url: "/api/epcprojectfinal/batchRemove",
        method: "delete",
        data: ids
    });
}

//结算文号唯一校验
export function validateCode(params) {
    return request({
        url: `/api/epcprojectfinal/validateCode`,
        method: "get",
        params
    });
}
