/*
 * @Author: cqg
 * @Date: 2020-12-08 17:15:03
 * @LastEditors: cqg
 * @LastEditTime: 2020-12-14 15:38:29
 * @Description: file content
 */
import request from "@/plugins/axios";
// 验收分页查询
export function query(params) {
    return request({
        url: "/api/epcprojectoverview/page",
        method: "get",
        params
    });
}
// 工程分页查询
export function queryEngineering(projectId) {
    return request({
        url: `/api/epcprojectoverview/${projectId}`,
        method: "get"
    });
}
