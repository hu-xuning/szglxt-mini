/*
 * @Author: cqg
 * @Date: 2020-12-14 14:19:59
 * @LastEditTime: 2020-12-14 14:28:53
 * @Description: file content
 * @LastEditors: cqg
 */

export { default as project } from "./project.vue"; //工程登记信息
export { default as projectInfo } from "./projectInfo.vue"; //立项信息
export { default as tenderInfo } from "./tenderInfo.vue"; //招标信息
export { default as contract } from "./contract.vue"; //合同信息
export { default as payment } from "./payment.vue"; //付款信息
export { default as accept } from "./accept.vue"; //验收信息
export { default as finalAccounts } from "./finalAccounts.vue"; //结算/决算信息
export { default as finalTransfer } from "./finalTransfer.vue"; //财产决算/资产移交信息
