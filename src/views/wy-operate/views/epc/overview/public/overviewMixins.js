import { getDict } from "@/utils";
export default {
    data() {
        return {
            uploadFileList: []
        };
    },
    computed: {
        // 工程类型
        epcProjectTypes() {
            return getDict("epc_project_type");
        },
        // 合同类型
        epcContractTypes() {
            return getDict("epc_contract_type");
        },
        //验收结果
        epcAcceptResult() {
            return getDict("epc_accept_result");
        }
    }
};
