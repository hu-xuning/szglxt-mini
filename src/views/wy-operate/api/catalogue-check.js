//检查项功能接口·======================
import request from '~/plugins/axios'

// 新增物业巡查-检查项
export function addInspectionitem(obj) {
    return request({
      url: '/api/inspectionitem/add',
      method: 'post',
      data: obj
    })
  }

  //  删除物业巡查-检查项
  export function delInspectionitem(ids) {
    return request({
      url: '/api/inspectionitem/batchDelete',
      method: 'delete',
      data:ids
    })
  }

    //修改物业巡查-检查项
  export function putInspectionitem(obj) {
    return request({
      url: '/api/inspectionitem/update',
      method: 'put',
      data: obj
    })
  }
 //   通过左边物业类型ID值，获取该物业类型的列表
  export function getInspectionitem(query) {
    return request({
      url: '/api/inspectionitem/page/',
      method: 'get',
      params: query

    })
  }


