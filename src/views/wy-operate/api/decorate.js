import request from '~/plugins/axios'

/**
 * 查询装修申请列表
 * @param {Obj} params
 */
export const getdecorationList = params => {
  return request({
    url: '/api/repairbill/repair/page',
    method: 'get',
    params: params
  })
}

/**
 * 分页过程监督
 * @param {Obj} params
 */
export const getBillPage = params => {
	return request({
		url: '/isolate/page',
		method: 'get',
		params: params
	})
}

/**
 * 新增|修改装修申请
 * @param {Object} params
 */
export const changeDecorationApply = (params, type = 'add') => {
  let data = type === 'add' ? { ...params, billType: '11', reqChannel: '10' } : params
  return request({
    url: '/api/repairbill',
    method: type === 'add' ? 'post' : 'put',
    data
  })
}

/**
 * 删除装修申请
 * @param {Array} ids billId
 */
export const deleteDecoration = ids => {
  return request({
    url: `/api/repairbill/batchdeletebybillid`,
    method: 'delete',
    data: JSON.stringify(ids)
  })
}

/**
 * 获取物业名称列表
 * @param {Object} param0 请求参数
 */
export const getPropertyList = (params) => {
  return request({
    url: '/api/repairbill/getproperty',
    method: 'get',
    params
  })
}

/**
 * 获取验收记录
 * @param {Obj} params
 */
export const getAcceptList = params => {
  return request({
    url: '/api/operationdeliverable/decorativePage',
    method: 'get',
    params
  })
}

/**
 * 新增|修改验收申请
 * @param {Obj} data
 */
export const changeAcceptApply = (data, type = 'add') => {
  return request({
    url: type === 'add' ? '/api/operationdeliverable/saveDeliverable' : '/api/operationdeliverable',
    method:  type === 'add' ? 'post' : 'put',
    data
  })
}

/**
 * 删除装修申请
 * @param {Array} data
 */
export const deleteAccept = data => {
  return request({
    url: `/api/operationdeliverable/removeDeliverableByIds`,
    method: 'post',
    data: data
  })
}

/**
 * 获取审批相关数据
 * @param {String} id 业务id
 */
export const getApproveInfo = id => {
  return request({
    url: `/api/yyapproverecord/getByBillId/${id}`,
    method: 'get'
  })
}

/**
 * 审批装修申请
 * @param {Obj} params
 */
export const approvalDecoration = (params, result) => {
  return request({
    url: `/api/repairbill/approval/${result}`,
    method: 'put',
    data: params
  })
}
