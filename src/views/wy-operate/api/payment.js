/*
 * @Author: xjt
 * @Date: 2020-12-08 14:56:56
 * @LastEditTime: 2020-12-22 18:14:10
 * @Description: 日常运营模块-其他付款合同子模块接口
 */
import request from "~/plugins/axios";

/**
 * 获取单位列表
 */
export const getDepartmentList = params => {
	return request({
		url: "/api/payment/contract/unit/page",
		method: "get",
		params
	});
};

/**
 * 新增 & 编辑单位
 */
export const editDepartment = (data, type) => {
	return request({
		url: "/api/payment/contract/unit",
		method: type == "edit" ? "put" : "post",
		data
	});
};

/**
 * 批量删除付款合同单位
 */
export const deleteDepartment = arr => {
	return request({
		url: "/api/payment/contract/unit/batch/delete",
		method: "put",
		data: JSON.stringify(arr)
	});
};

/**
 * 校验社会信用码是否重复
 */
export const checkCreditCode = (params, type = "add") => {
	return request({
		url: `/api/payment/contract/unit/check/creditCode-${
			type === "add" ? "add" : "update"
		}`,
		method: "get",
		params
	});
};

/**
 * 校验单位名称是否重复
 */
export const checkUnitName = (params, type = "add") => {
	return request({
		url: `/api/payment/contract/unit/check/unitName-${
			type === "add" ? "add" : "update"
		}`,
		method: "get",
		params
	});
};

/**
 * 获取合同录入列表
 */
export const getContractList = params => {
	return request({
		url: "/api/payment/contract/page",
		method: "get",
		params
	});
};

/**
 * 获取单个合同信息
 */
export const getContractById = paymentContractId => {
	return request({
		url: `/api/payment/contract/${paymentContractId}`,
		method: "get"
	});
};

/**
 * 新增 & 编辑合同
 */
export const editContract = (data, type) => {
	return request({
		url: "/api/payment/contract",
		method: type == "edit" ? "put" : "post",
		data
	});
};

/**
 * 批量删除付款合同
 */
export const deleteContract = arr => {
	return request({
		url: "/api/payment/contract/batch/delete",
		method: "put",
		data: JSON.stringify(arr)
	});
};

/**
 * 校验合同编号重复
 */
export const checkContractNo = (params, type = "add") => {
	return request({
		url: `/api/payment/contract/check/contractNo-${
			type === "add" ? "add" : "update"
		}`,
		method: "get",
		params
	});
};

/**
 * 获取付款列表
 */
export const getPaymentList = params => {
	return request({
		url: "/api/payment/contract/registration/page",
		method: "get",
		params
	});
};

/**
 * 根据id获取付款数据
 */
export const getPaymentById = paymentRegistrationId => {
	return request({
		url: `/api/payment/contract/registration/${paymentRegistrationId}`,
		method: "get"
	});
};

/**
 * 新增 & 编辑付款记录
 */
export const editPayment = (data, type) => {
	return request({
		url: "/api/payment/contract/registration",
		method: type == "edit" ? "put" : "post",
		data
	});
};

/**
 * 删除付款记录
 */
export const deletePaymentList = (arr, type) => {
	if (arr.length > 1 || type == "menu") {
		return request({
			url: "/api/payment/contract/registration/batch/delete",
			method: "put",
			data: JSON.stringify(arr)
		});
	} else {
		return request({
			url: `/api/payment/contract/registration/${arr[0]}`,
			method: "delete"
		});
	}
};

/**
 * 根据合同ID获取最大付款次数
 */
export const getPaymentMaxNumber = paymentContractId => {
	return request({
		url: "/api/payment/contract/registration/getMaxNum",
		method: "get",
		params: {
			paymentContractId
		}
	});
};

/**
 * 根据合同ID获取付款记录
 */
export const getPaymentHistory = paymentContractId => {
	return request({
		url: "/api/payment/contract/registration/getPaymentRecord",
		method: "get",
		params: {
			paymentContractId
		}
	});
};

/**
 * 获取结算、决算记录
 */
export const getAccountList = params => {
	return request({
		url: "/api/payment/contract/settlement/registration/page",
		method: "get",
		params
	});
};

/**
 * 新增 & 编辑决算记录
 */
export const editAccount = (data, type) => {
	return request({
		url: "/api/payment/contract/settlement/registration",
		method: type == "edit" ? "put" : "post",
		data
	});
};

/**
 * 删除决算记录
 */
export const deleteAccount = arr => {
	return request({
		url: "/api/payment/contract/settlement/registration/batch/delete",
		method: "put",
		data: JSON.stringify(arr)
	});
};

/**
 * 结算时校验合同编号重复
 */
export const checkAccountContractNo = (params, type = "add") => {
	return request({
		url: `/api/payment/contract/settlement/registration/check/contract-${
			type === "add" ? "add" : "update"
		}`,
		method: "get",
		params
	});
};

/**
 * 通过id查询结算数据
 */
export const getAccountById = paymentSettlementId => {
	return request({
		url: `/api/payment/contract/settlement/registration/${paymentSettlementId}`,
		method: "get"
	});
};
