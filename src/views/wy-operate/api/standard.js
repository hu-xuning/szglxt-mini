import request from "~/plugins/axios";

export const pagingQuery = query => {
    return request({
        url: "/api/inspectionstandard/page/",
        method: "get",
        params: query
    });
};
//分页查询

export const getcheckItemList = () => {
    return request({
        url: "/api/inspectionstandard/getInspectionNameList/",
        method: "get"
    });
};
//新增物业巡查-检查标准
export const newStandard = obj => {
    return request({
        url: "/api/inspectionstandard/add",
        method: "post",
        data: obj
    });
};

//通过id批量删除物业巡查-检查标准
export const deleteStandard = arr => {
    return request({
        url: "/api/inspectionstandard/batchDelete",
        method: "delete",
        data: arr
    });
};
//修改物业巡查-检查标准 
export function editInspectionCriteria(obj) {
    return request({
        url: '/api/inspectionstandard/update',
        method: 'put',
        data: obj
    })
}
