/*
 * @Author: cqg 
 * @Date: 2020-09-05 18:11:46
 * @LastEditors: cqg
 * @LastEditTime: 2020-11-27 11:05:29
 * @Description: file content
 */
//检查方法功能接口·======================
import request from '~/plugins/axios'

// 新增物业巡查-检查项
export function addInspectionMethod (obj) {
    return request({
        url: '/api/inspectionway/add',
        method: 'post',
        data: obj
    })
}

//  删除物业巡查-检查项
export function delInspectionMethod (ids) {
    return request({
        url: ' /api/inspectionway/batchDelete',
        method: 'delete',
        data: ids
    })
}

//修改物业巡查-检查项
export function editInspectionMethod (obj) {
    return request({
        url: '/api/inspectionway/update',
        method: 'put',
        data: obj
    })
}
// 分页查询检查方法列表
export function getInspectionMethod (query) {
    return request({
        url: '/api/inspectionway/page/',
        method: 'get',
        params: query

    })
}
// 获取所有物业类型下的检查项名
export function getInspectionItems (query) {
    return request({
        url: '/api/inspectionway/getInspectionNameList',
        method: 'get',
        params: query
    })
}
//展示检查项的全部列表(用于模糊搜索)
export function getTinspectionName (projectType) {
    return request({
        url: `/api/inspectionway/getInspectionName`,
        method: 'get',
        params: { projectType }
    })
}



