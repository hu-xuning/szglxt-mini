/*
 * @Author: cqg
 * @Date: 2020-09-14 09:04:44
 * @LastEditors: cqg
 * @LastEditTime: 2021-06-02 10:50:22
 * @Description: file content
 */
import request from "~/plugins/axios";

//   巡查结果列表
export function inspectionList(query) {
	return request({
		url: "/api/inspectionresult/page",
		method: "get",
		params: query
	});
}
//   propertyName：物业名称（string）
export function propertyNameList(propertyName) {
	return request({
		url: "/api/inspectionresult/roomInfoPage",
		method: "get",
		params: {
			propertyName
		}
	});
}
//  获取物业相关信息
//   roomId：物业id（string）
export function getRoomInfo(roomId) {
	return request({
		url: `/api/inspectionresult/getRoomInfo/${roomId}`,
		method: "get"
	});
}
//  获取物业使用单位信息
//   roomId：物业id（string）
export function getRoomUnit(roomId) {
	return request({
		url: `/api/roominfo/getUnit/${roomId}`,
		method: "get"
	});
}
//   根据使用单位获取信息
export function useUnitList(useUnit) {
	return request({
		url: "/api/inspectionresult/getUseUnitInfo",
		method: "get",
		params: {
			useUnit
		}
	});
}
//   根据物业类型获取检查项和检查标准
export function getCheckItemList(projectType) {
	return request({
		url: "/api/inspectionresult/getReportDangerRecords",
		method: "get",
		params: {
			projectType
		}
	});
}
//   根据物业类型获取检查项和检查标准  editInformation
export function getEditInformation(resultId) {
	return request({
		url: `/api/inspectionresult/${resultId}`
		// method: 'get',
	});
}
//   修改巡查结果
export function getModifyInspectionResults(inspectionResultDto) {
	return request({
		url: "/api/inspectionresult",
		method: "put",
		data: inspectionResultDto
	});
}

// 删除巡查结果
export const deleteDeleteInspectionResults = ids => {
	return request({
		url: `/api/inspectionresult/batchDelete`,
		method: "delete",
		data: JSON.stringify(ids)
	});
};

//  *添加|修改巡查结果
export const changeModifyInspectionResults = (data, title = "新增巡查结果") => {
	return request({
		url: "/api/inspectionresult",
		method: title === "新增巡查结果" ? "post" : "put",
		data
	});
};
//  *添加|修改复查
export const InspectionLog = data => {
	return request({
		url: "/api/inspectionresult/InspectionLog",
		method: "post",
		data
	});
};

// 导出
export function downloadPost(url, params) {
	return request({
		url: url,
		method: "post",
		data: params,
		// Object.assign({},params),
		responseType: "blob"
	});
}
