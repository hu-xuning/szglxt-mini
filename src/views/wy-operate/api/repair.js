import request from "~/plugins/axios";

/**
 * 上传附件
 * @query {String} 绑定id
 * @formData {Array} files
 * @query {String} 自定义模块名称
 */
export const uploadFiles = data => {
	return request({
		url: "/sys-file/upLodeFileAdd",
		method: "post",
		data
	});
};

/**
 * 获取某条数据下的所有附件
 * @param {String} busId 业务id
 * @param {String} module 前端定义的模块名称
 */
export const getUploadFiles = (busId, module) => {
	return request({
		url: `/admin/sys-file/getFileList/${busId}/${module}`,
		method: "get"
	});
};

/**
 * 获取物业名称列表
 * @param {Object} param0 请求参数
 */
export const getPropertyList = (
	params,
	type = "question",
	dependentType = "repair"
) => {
	let url = {
		// 预算管理物业名称选择
		budget: "/api/repairbill/getproblem",
		// 修缮物业名称选择
		question: "/api/repairbill/getproperty",
		// 工程名称选择
		accept:
			dependentType == "repair"
				? "/api/operationContract/getConstructionPageByName"
				: "/api/repairbill/billPage",
		// 付款选择工程（合同）
		payment: "/api/operationpay/getByOperationPayInfoByName",
		// 决算选择合同
		"final-accounts": "/api/operationContract/getConstructionPageByName",
		// 已经有中标单位的工程
		contract: "/api/operationbudget/IsChoicePage"
	}[type];
	return request({
		url,
		method: "get",
		params
	});
};

/**
 * 获取决算工程名称列表
 * @param {Object} param0 请求参数
 */
export const getEngineeringNameList = (params, type = "final-accounts") => {
	return request({
		url: "/api/operationContract/getConstructionPageByName",
		method: "post",
		data: params
	});
};

/**
 * 获取单位列表
 * @param {Object} query
 */
export function getDePartment(query) {
	return request({
		url: "/api/clientprovider/page",
		method: "get",
		params: query
	});
}

/**
 * 添加|修改单位
 * @param {obj} data
 */
export const changeDepartment = (data, type = "add") => {
	return request({
		url: "/api/clientprovider",
		method: type === "add" ? "post" : "put",
		data
	});
};

/**
 * 删除单位
 * @param {Array} ids
 */
export const deleteDepartment = ids => {
	return request({
		url: `/api/clientprovider/batchdeletebyid`,
		method: "delete",
		data: JSON.stringify(ids)
	});
};

/**
 * 修缮问题列表
 * @param {Object} params
 */
export const getQuestionList = params => {
	return request({
		url: "/api/repairbill/page",
		method: "get",
		params
	});
};

/**
 * 新增|修改修缮申请
 * @param {Object} params
 */
export const changeQuestion = (params, type = "add") => {
	let data =
		type === "add" ? { ...params, billType: "10", reqChannel: "10" } : params;
	return request({
		url: type === "add" ? "/api/repairbill/repairPost":"/api/repairbill",
		method: type === "add" ? "post" : "put",
		data
	});
};

/**
 * 审批修缮申请
 * @param {Obj} params
 */
export const approvalQuestion = (params, result) => {
	return request({
		url: `/api/repairbill/approval/${result}`,
		method: "put",
		data: params
	});
};

/**
 * 删除修缮问题
 * @param {Array} ids billId
 */
export const deleteQuestion = ids => {
	return request({
		url: `/api/repairbill/batchdeletebybillid`,
		method: "delete",
		data: JSON.stringify(ids)
	});
};

/**
 * 获取付款信息列表
 * @param {Object} params
 */
export const getPaymentInfo = params => {
	return request({
		url: "/api/operationpay/getOperationPay/page",
		method: "GET",
		params
	});
};

/**
 * 添加/修改付款信息
 * @param {Object} params
 */
export const changeOperationpayitem = (data, type = "add") => {
	return request({
		url: "/api/operationpayitem",
		method: type === "add" ? "post" : "put",
		data
	});
};

/**
 * 删除付款信息
 * @param {Array} ids 支付id【payId】
 */
export const deletePaymentInfo = (itemIds, payIds) => {
	console.log(payIds);
	return request({
		url: `/api/operationpayitem/removeById`,
		method: "delete",
		data: JSON.stringify({ itemIds, payIds })
	});
};

/**
 * 查询付款明细列表
 * @param {String} payId 支付id
 */
export const getPaymentHistory = payId => {
	return request({
		url: `/api/operationpayitem/getByPayId/${payId}`,
		method: "get",
		params: {}
	});
};

/**
 *通过工程名称查询付款信息
 * @param 工程名称
 */
export const getByOperationPayInfoByName = params => {
	return request({
		url: "/api/operationpayitem/getByOperationPayInfoByName",
		method: "get",
		params
	});
};

/**
 * 获取预算列表
 * @param {Object} params
 */
export const getBudgetList = params => {
	return request({
		url: "/api/operationbudget/page",
		method: "GET",
		params
	});
};

/**
 * 添加|编辑预算信息
 * @param {Object} params
 * @type {String} 类型
 */
export const changeBudgetInfo = (params, type = "add") => {
	return request({
		url: "/api/operationbudget",
		method: type == "add" ? "post" : "put",
		data: params
	});
};

/**
 * 编辑预算信息
 * @param {Object} params
 */
export const editBudgetInfo = params => {
	return request({
		url: `/api/operationbudget`,
		method: "put",
		data: params
	});
};

/**
 * 删除预算信息
 * @param {Array} ids 支付id【payId】
 */
export const deleteBudgetInfo = ids => {
	return request({
		url: `/api/operationbudget/removeById`,
		method: "delete",
		data: JSON.stringify(ids)
	});
};

/**
 * 获取预算对应的报价列表
 * @param {String} obtId 预算id
 */
export const getQuoteList = obtId => {
	return request({
		url: `/api/operationbudget/getOperationBudgetDto/${obtId}`,
		method: "GET"
	});
};

/**
 * 获取预算详情
 * @param {String} obtId
 */
export const getBudgetDetails = obtId => {
	return request({
		url: "/api/operationbudget/" + obtId,
		method: "get"
	});
};

/**
 * 添加预算信息
 * @param {Object} params
 */
export const addAcceptInfo = params => {
	return request({
		url: "/api/operationbudget",
		method: "post",
		data: params
	});
};

/**
 * 删除预算信息
 * @param {Array} ids 支付id【payId】
 */
export const deleteAcceptInfo = ids => {
	return request({
		url: `/api/operationbudget/${ids.join(",")}`,
		method: "delete",
		params: {}
	});
};

/**
 * 编辑预算信息
 * @param {Object} params
 */
export const editAcceptInfo = params => {
	return request({
		url: `/api/operationbudget`,
		method: "put",
		params
	});
};

/**
 * 获取审批相关数据
 * @param {String} id 业务id
 */
export const getApproveInfo = id => {
	return request({
		url: `/api/yyapproverecord/getByBillId/${id}`,
		method: "get"
	});
};

/**
 * 根据工程名称或者物业名称或者施工单位名称分页查询
 * @param {Object} query
 */
export const getConstructionPageList = query => {
	return request({
		url: "/api/operationContract/getConstructionPageList",
		method: "get",
		params: query
	});
};

/**
 * 通过id查询合同信息和服务价目列表信息
 * @param  contractId
 */
export const getContractById = contractId => {
	return request({
		url: "/api/operationContract/" + contractId,
		method: "get"
	});
};
/**
 * 新增|修改运营类合同
 * @param {Object} params
 */
export const changeContract = (params, type = "add") => {
	return request({
		url: "/api/operationContract",
		method: type == "add" ? "post" : "put",
		data: params
	});
};

/**
 * 获取已经签订合同的预算列表
 * @engineeringName {String} params
 */
export const getChoicedBudgetList = engineeringName => {
	return request({
		url: "/api/operationbudget/IsChoicePage",
		method: "get",
		params: {
			engineeringName,
			current: 1,
			size: 30
		}
	});
};

/**
 * 批量删除合同
 * @param {Array} ids
 */
export const deleteContract = ids => {
	return request({
		url: `/api/operationContract/batchDelete`,
		method: "delete",
		data: JSON.stringify(ids)
	});
};

/**
 * 获取验收记录
 * @param {Obj} params
 */
export const getAcceptList = params => {
	return request({
		url: "/api/operationdeliverable/page",
		method: "get",
		params
	});
};

/**
 * 新增|修改验收申请
 * @param {Obj} data
 */
export const changeAcceptApply = (data, type = "add") => {
	return request({
		url: `/api/operationdeliverable`,
		method: type === "add" ? "post" : "put",
		data
	});
};

/**
 * 删除装修申请
 * @param {Array} ids
 */
export const deleteAccept = ids => {
	return request({
		url: `/api/operationdeliverable/batchDelete`,
		method: "DELETE",
		data: JSON.stringify(ids)
	});
};

/**
 * 获取物业决算列表
 * @param {Object} params
 */
export const getConclusionList = params => {
	return request({
		url: "/api/conclusion/page",
		method: "get",
		params
	});
};

/**
 * 新增/修改物业决算表
 * @param {Object} params
 */
export const changeConclusion = (data, type = "add") => {
	return request({
		url: "/api/conclusion",
		method: type === "add" ? "post" : "put",
		data
	});
};

/**
 * 删除物业决算表
 * @param {Array} ids billId
 */
export const deleteConclusion = ids => {
	return request({
		url: `/api/conclusion/batchConclusionId`,
		method: "delete",
		data: JSON.stringify(ids)
	});
};

export const exportData = (type, data) => {
	if (!type) return;
	let url = {
		question: "/api/repairbill/exportRepairs",
		contract: "/api/operationContract/export",
		payment: "/api/operationpay/exportOperationPays"
	}[type];
	return request({ url, method: "post", data, responseType: "blob" });
};

/**
 * 预算概览列表分页查询
 */
export const getOverviewList = params => {
	return request({
		url: "/api/operating/repair/overview/page",
		method: "get",
		params
	});
};

/**
 * 预算概览查看详情
 * @param {*} params
 */
export const getOverview = obtId => {
	return request({
		url: "/api/operating/repair/overview/detail",
		method: "get",
		params: { obtId }
	});
};

/**
 * 校验修缮-合同的名称和合同编号是否重复
 * @param {Obj} data
 */
export const checkContractRepeat = params => {
	return request({
		url: "/api/operationContract/validateCode",
		method: "get",
		params
	});
};

/**
 * 获取最大合同编号
 * @param params
 */
export const maxContractNumber = params => {
	return request({
		url: "/api/operationContract/maxContractNumber",
		method: "get",
		params
	});
};
