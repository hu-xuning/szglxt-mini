import request from '@/plugins/axios';

//房间信息
export function getpanelinfo(houseId) {
  return request({
    url: `/api/house/statical/panel/info/${houseId}`,
    method: 'get',
  });
}

//房间信息人数
export function getpanellist(params) {
  return request({
    url: `/api/person/statical/panel/list`,
    method: 'get',
    params,
  });
}

//房间人员具体信息
export function gettpaneldailypage(params) {
  return request({
    url: `/api/person/statical/panel/daily/page`,
    method: "get",
	params
  });
}

//一次性获取酒店所有楼层房间回显看板接口
export function getAllInfo(hotelId) {
  return request({
    url: `/api/house/statical/panel`,
    method: "get",
    params:{hotelId:hotelId}
  });
}