export const tableOption = {
  column: [],
  filterList: [
    {
      label: '房间所属楼层',
      dictName: 'shifou',
      name: 'roomFloor',
    },
    { label: '房间号', dictName: 'shifou', name: 'roomNum' },
    {
      label: '使用类型',
      inputType: 'dict',
      dictName: 'use_type',
      name: 'useType',
    },
  ],
};
