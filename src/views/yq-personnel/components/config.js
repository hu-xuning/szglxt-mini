export const hotelBaseInfoTitle = [
    { 'label': '隔离点名称', 'prop': 'hotelName' },
    { 'label': '详细地址', 'prop': 'hotelAddr', },
    { 'label': '楼层数量', 'prop': 'floorNum' },
    { 'label': '隔离人员居住楼层', 'prop': 'livingFloor' },
    { 'label': '楼道及周边是否安装视频监控', 'prop': 'isMonitor', 'dict': "shifou" },
    { 'label': '是否配备楼道安保人员', 'prop': 'isSecurity', 'dict': "shifou" },
    { 'type': 'input', 'label': '分配房间总数', 'prop': 'fjzs' },
    { 'label': '剩余房间数', 'prop': 'syfjs' },
    { 'label': '隔离点方联系人', 'prop': 'hotelContact' },
    { 'label': '隔离点方联系电话', 'prop': 'hotelPhone' },
    { 'label': '使用开始时间', 'prop': 'useStartTime', },
    { 'label': '使用终止时间', 'prop': 'useEndTime', }
]

export const hotelBookInfoTitle = [
    { 'label': '预订批次', 'prop': 'batchNum' },
    { 'label': '批次说明', 'prop': 'batchName', },
    { 'label': '预订人数', 'prop': 'bookPersonNum' },
    { 'label': '预订间数', 'prop': 'bookRoomNum' },
    { 'label': '隔离人员来源', 'prop': 'dataSource', },
    { 'label': '预入住时间', 'prop': 'bookMoveTime' },
    { 'type': 'input', 'label': '安排员', 'prop': 'arrangePerson' },
    { 'label': '安排时间', 'prop': 'arrangeTime' },
    { 'label': '备注', 'prop': 'remark' },
    { 'label': '人员清单附件', 'prop': 'file' },
]


//接收信息表单
export const receiveInfoList = [
    { label: '接收人数:', prop: 'receiveNum', type: 'input' },
    { label: '接收说明:', prop: 'receiveRemark', type: 'textarea' },
    { label: '接收人:', prop: 'receivePerson', type: 'input', disabled: true },
    { label: '接收时间:', prop: 'receiveTime', type: 'datetime', disabled: true },
]

