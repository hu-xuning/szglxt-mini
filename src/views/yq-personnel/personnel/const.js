
export const tableOption = {
      filterList:[
         {
          label: "房号",inputType: "input", name: 'roomNum',defaultValue:""
        },
         { label: '证件号码', inputType: 'input', name: 'cardId',defaultValue:""},
         { label: '联系方式',inputType: "input", name: 'telPhone',defaultValue:""},
         { label: '来源', inputType: 'dict', dictName: 'data_source', name: 'dataSource',defaultValue:""},
         { label: '风险等级', inputType: 'dict', dictName: 'risk_level', name: 'riskLevel',defaultValue:""},
         { label: '最大剩余天数', inputType: 'input', name: 'surplusDaysMax',defaultValue:""},
         { label: '最小剩余天数', inputType: 'input', name: 'surplusDaysMin',defaultValue:""},
         { label: '入住时间', inputType: 'datetimerange',  name: ["moveStartTimeMin","moveStartTimeMax"],defaultValue:["",""]},
         { label: '解除隔离时间', inputType: 'datetimerange', name: ["releaseIsolationDateMin","releaseIsolationDateMax"],defaultValue:["",""]},
         { label: '离点时间', inputType: 'datetimerange', dictName: 'shifou', name: ["leaveHotelDateMin","leaveHotelDateMax"],defaultValue:["",""]},
         { label: '人员状态', inputType: 'dict', dictName: 'person_state', name: 'personState',defaultValue:""},
         { label: '核酸检验情况', inputType: 'dict', dictName: 'nuclein_test_case', name: 'nucleinTestCase',defaultValue:""},
         { label: '是否为在校留学生', inputType: 'dict', dictName: 'is_valid', name: 'isOverStudent',defaultValue:""},
         { label: '是否有境外旅居史', inputType: 'dict', dictName: 'is_valid', name: 'isYjwLjs',defaultValue:""},
         { label: '是否机组人员', inputType: 'dict', dictName: 'is_valid', name: 'isAircrew',defaultValue:""},
         { label: '是否入境奔丧人员', inputType: 'dict', dictName: 'is_valid', name: 'isMourning',defaultValue:""},
         { label: '是否孕妇', inputType: 'dict', dictName: 'is_valid', name: 'isGravida',defaultValue:""},
         { label: '是否有明显的身体疾病', inputType: 'dict', dictName: 'is_valid', name: 'isBodyDisease',defaultValue:""},
         { label: '是否有基础疾病', inputType: 'dict', dictName: 'is_valid', name: 'isBasicDisease',defaultValue:""},
         { label: '是否属于前科人员', inputType: 'dict', dictName: 'is_valid', name: 'isCriminalPerson',defaultValue:""},
         { label: '是否豁免隔离', inputType: 'dict', dictName: 'is_valid', name: 'isImmunity',defaultValue:""},
         { label: '是否存在精神异常或心里异常', inputType: 'dict', dictName: 'is_valid', name: 'isSpiritHeartExce',defaultValue:""},
      ]
}