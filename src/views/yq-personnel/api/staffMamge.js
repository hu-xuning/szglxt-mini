import request from "~/plugins/axios";

export function checkID (query) {
    return request({
        url: "/admin/user/card/query",
        method: "get",
        params: query
    })
}

//   巡查结果列表
export function fetchList(query) {
    return request({
        url: "/admin/user/pagebyhotel",
        method: "get",
        params: query
    });
}

// 根据区域ID 获取区域名称
export function getAreaNameById(id) {
    return request({
        url: `/admin/division/getObjByCode/${id}`,
        method: "get",
    });
}

// 验证人手机号码
export function validatePhoneUnique(query) {
    return request({
        url: `/admin/user/phone/validate`,
        method: "get",
        params: query
    });
}

// 验证证件号码
export function validateIdCardUnique(query) {
    return request({
        url: `/admin/user/idCard/validate`,
        method: "get",
        params: query
    });
}

// 人员类型
export function getPersonType() {
    return request({
        url: `/admin/sysjob`,
        method: "get",
    });
}
//根据证件号查询用户信息

export function getUserInfoByCard(id) {
    return request({
        url: `/admin/user/card/query?cardId=${id}`,
        method: "get",
    });
}