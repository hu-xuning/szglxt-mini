import request from "~/plugins/axios";

export function addExConvict(data) {
  return request({
    url: "/JdglAgainstCriminalRecord",
    method: "post",
    data: data,
  });
}
//获得预订批次
export function getExConvict(personId) {
  return request({
    url: `/JdglAgainstCriminalRecord/${personId}`,
    method: "get",
});
}