import request from "~/plugins/axios";

//列表
export function getIsolatePage(query) {
  return request({
    url: "/isolate/page",
    params: query,
  });
}
//id查询
export function getDetails(sid) {
  return request({
    url: `/jdglPersonRiskAssessment/${sid}`,
  });
}
// //id查询该条历史评估信息
// export function getDetails(params) {
//   return request({
//     url: `/jdglPersonRiskAssessment/list`,
//     params
//   });
// }


//历史评估
export function page(query) {
  return request({
    url: "/jdglPersonRiskAssessment/page",
    method: "get",
    params: query,
  });
}
//心身风险评估新增或修改
export function addOrEdit(data, type) {
  return request({
    url: "/jdglPersonRiskAssessment",
    method: type == "add" ? "post" : "put",
    data,
  });
}
//心身风险评估删除
export function delObj(id) {
  return request({
    url: "/jdglPersonRiskAssessment/" + id,
    method: "delete",
  });
}
//心身风险评估通过id查询
export function getObj(id) {
  return request({
    url: "/jdglPersonRiskAssessment/"+id,
    method: "get",
  });
}
