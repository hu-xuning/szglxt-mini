import request from "~/plugins/axios";
export function editLiveInfo(data) {
  return request({
    url: "/jdglhotelmoveinto",
    method: "post",
    data: data,
  });
}
// 查询房间信息
export function listRoom(query) {
  return request({
    url: "/jdglhotelmoveinto/listRoom",
    method: "get",
    params: query,
  });
}
// 查询该房间人员信息
export function getLatestPersonIofo(personId) {
  return request({
    url: `/jdglhotelmoveinto/latest/${personId}`,
    method: "get",
  });
}
// 提交入住修改
export function houseEdit(data) {
  return request({
    url: `/jdglhotelmoveinto/houseEdit`,
    method: "post",
    data,
  });
}

// 查询人员关怀信息
export function getCare(personId) {
  return request({
    url: `/care/${personId}`,
    method: "get",
  });
}
