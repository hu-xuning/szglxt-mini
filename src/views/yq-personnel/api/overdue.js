import request from "~/plugins/axios";

//   值班管理列表
export function AddDutyInfo(data) {
    return request({
        url: "/jdglpersonoverdueregi",
        method: "post",
        data: data
    });
}
export function EditDutyInfo(data) {
    return request({
        url: "/jdglpersonoverdueregi",
        method: "put",
        data: data
    });
}
export function delDelayInfo(id) {
    return request({
        url: '/jdglpersonoverdueregi/' + id,
        method: 'delete',
    })
}