import request from "~/plugins/axios";

export function addAbnormal(data) {
    return request({
        url: "/jdglAbnormalScreeningRecord",
        method: "post",
        data
    });
}
//获得预订批次
export function getAbnormal(personId) {
    return request({
        url: `/jdglAbnormalScreeningRecord/${personId}`,
        method: "get",
    });
}