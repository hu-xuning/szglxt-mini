
import request from "~/plugins/axios";

//   每日健康填报 - 新增信息表
export function dailyhealth(query) {
    return request({
        url: "/isolate/daily-health",
        method: "post",
        data: query
    });
}

//   每日健康填报 - 修改信息表
export function updateDailyhealth(query) {
    return request({
        url: "/isolate/daily-health",
        method: "put",
        data: query
    });
}

//   医学监测 - 新增信息表
export function addisolate(query) {
    return request({
        url: "/isolate",
        method: "post",
        data: query
    });
}

//   医学监测 - 编辑信息表
export function putisolate(query) {
    return request({
        url: "/isolate",
        method: "put",
        data: query
    });
}

//   每日健康查询 - 分页查询
export function dailyhealthpage(query) {
    return request({
        url: "/isolate/daily-health/list",
        method: "get",
        params: query
    });
}

//   医学监测 - 新增信息表
export function getpagequery(query) {
    return request({
        url: "/jdglpersoninfo/page/query",
        method: "GET",
        params: query
    });
}

//   健康排查 - 新增
export function jdglPersonHealthInvestigate(query) {
    return request({
        url: "/jdglPersonHealthInvestigate",
        method: "POST",
        data: query
    });
}

//   健康排查 - 修改
export function putjdglPersonHealthInvestigate(query) {
    return request({
        url: "/jdglPersonHealthInvestigate",
        method: "put",
        data: query
    });
}

//   健康排查 - 修改
export function getjdglPersonHealthInvestigate(healthRegId) {
    return request({
        url: `/jdglPersonHealthInvestigate/${healthRegId}`,
        method: "GET",
    });
}

//   医学监测 - 通过id查询
export function isolateisolateId(pid) {
    return request({
        url: `/isolate/${pid}`,
        method: "GET",
    });
}

//   隔离人员信息表管理 - 手动一码通数据同步处理
export function synUpdatePersonInfoManual(personId) {
    return request({
        url: `/jdglpersoninfo/synUpdatePersonInfoManual/${personId}`,
        method: "POST",
    });
}