
import request from "~/plugins/axios";

//   巡查结果列表
export function getBookintoJspage(query) {
    return request({
        url: "/jdglhotelbookinto/jspage",
        method: "get",
        params: query
    });
}

//获得预订批次
export function getListBatchAll(query) {
    return request({
        url: "/jdglpersoninfo/listBatchAll",
        method: "get",
        params: query
    });
}

//通过ID获得隔离点基本信息
export function getHotelBaseById(query) {
    return request({
        url: "/jdglhotelbaseinfo/" + query,
        method: "get",
    });
}

//通过ID获得隔离点入住信息
export function getHotelBookById(query) {
    return request({
        url: "/jdglhotelbookinto/" + query,
        method: "get",
    });
}

//   隔离点消杀列表
export function getpageHouseInfo(query) {
    return request({
        url: "/jdglhotelhousexsqkinfo/pageHouseInfo",
        method: "get",
        params: query
    });
}

//   隔离点消杀新增
export function addjdglhotelhousexsqkinfo(query) {
    return request({
        url: "/jdglhotelhousexsqkinfo",
        method: "POST",
        data: query
    });
}

//   房屋入住记录
export function getpageMoveHouseRecordVO(query) {
    return request({
        url: "/jdglhotelhouseinfo/pageMoveHouseRecordVO",
        method: "GET",
        params: query
    });
}

//   消杀情况分页查询
export function gethousexsqkinfopage(query) {
    return request({
        url: "/jdglhotelhousexsqkinfo/page",
        method: "GET",
        params: query
    });
}

//   回收站分页查询
export function gettrashpage(query) {
    return request({
        url: "/jdglpersoninfo/page/trash",
        method: "GET",
        params: query
    });
}

//   人员信息还原
export function restroePerson(query) {
    return request({
        url: "/jdglpersoninfo/restroePerson",
        method: "POST",
        data: query
    });
}

//接收提交
export function putHotelBook(query) {
    return request({
        url: "/jdglhotelbookinto",
        method: "put",
        data: JSON.stringify(query)
    });
}

//完成提交
export function putBookComplete(query) {
    return request({
        url: "/jdglhotelbookinto/complete",
        method: "put",
        data: JSON.stringify(query)
    });
}

// 隔离人员综合管理列表

export function getList(query) {
    return request({
        url: "/jdglpersoninfo/page/manage",
        method: "get",
        params: query
    });
}

//分页
export function getHotelList(query) {
    return request({
        url: '/jdglhotelbaseinfo/page?',
        method: 'get',
        params: query
    })
}

// 离点登记新增
export function putLeaveInfo(obj) {
    return request({
        url: '/jdglpersonleaveinfo',
        method: 'put',
        data: obj
    })
}

//   回收站分页查询
export function getPersonInfoById(query) {
    return request({
        url: "/jdglpersoninfo/"+query,
        method: "GET",
    });
}

//国籍地区
export function getProvinceCity() {
    return request({
        url: "/admin/area/get_province_city",
        method: "GET",
    });
}

//获取码值
export function getDicts() {
    return request({
        url: "/jdgldict/getDicts",
        method: "GET",
    });
}

//目的地地区选择
export function getDestination() {
    return request({
        url: "/admin/area/get_destination",
        method: "GET",
    });
}

//提交隔离人员信息管理编辑
export function savePersonEditor(query) {
    return request({
        url: "/jdglpersoninfo/savePc",
        method: "POST",
        data: query
    });
}

//获取一码通数据
export function getSynPersonInfo(query) {
    return request({
        url: "/jdglpersoninfo/getSynPersonInfo",
        method: "POST",
        data: query
    });
}

//保存用户信息
export function savePerson(query) {
    return request({
        url: "/jdglpersoninfo/save/person",
        method: "POST",
        data: query
    });
}

// 获取联系人
export function getContact(pid) {
    return request({
        url:`/jdglPersonHealthInvestigate/person/${pid}`,
        method: "get",
    });
}