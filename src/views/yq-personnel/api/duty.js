import request from "~/plugins/axios";

//   值班管理列表
export function getDutyList(query) {
    return request({
        url: "/jdglhoteldutyinfo/page",
        method: "get",
        params: query
    });
}

export function addObj(obj) {
    return request({
      url: '/jdglhoteldutyinfo',
      method: 'post',
      data: obj
    })
  }
  // 获取值班详情
  export function getObj(id) {
    return request({
      url: '/jdglhoteldutyinfo/' + id,
      method: 'get'
    })
  }
  
  export function delObj(id) {
    return request({
      url: '/jdglhoteldutyinfo/' + id,
      method: 'delete'
    })
  }
  
  export function putObj(obj) {
    return request({
      url: '/jdglhoteldutyinfo',
      method: 'put',
      data: obj
    })
  }
  