import request from '@/plugins/axios'

//离点登记
 /**
  * 离点列表
  * @param {*} query 
  * @returns 
  */
export function fetchLeaveList(query) {
    return request({
        url: '/jdglpersonleaveinfo/pageld',
        method: 'get',
        params: query
    })
}
// 添加离点信息
export function putLeaveInfo(obj) {
    return request({
        url: '/jdglpersonleaveinfo',
        method: 'put',
        data: obj
    })
}
// 删除离点信息
export function delLeaveInfo(id) {
    return request({
        url: '/jdglpersonleaveinfo/' + id,
        method: 'delete',
    })
}

/**
 * 根据id查看离点信息
 * @param {*} leaveInfoId /离点信息ID
 * @returns 
 */
export function getLeaveInfoById(leaveInfoId) {
    return request({
        url: `/jdglpersonleaveinfo/${leaveInfoId}`,
        method: 'get',
    })
}

// 校验是否允许离点
export function isAllowedLeave(id) {
    return request({
        url: `/jdglpersoninfo/verify/${id}`,
        method: 'get'
    })
}

// 隔离人员信息

export function getPersonInfo(personId) {
    return request({
        url: `/jdglpersoninfo/${personId}`,
        method: 'get'
    })
}
// 获取行政区域
export function getProvinceCity(query) {
    return request({
        url: '/admin/area/get_province_city_pc',
        method: 'get',
        params: query
    })
}

