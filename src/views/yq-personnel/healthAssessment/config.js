
export const assessments = [
    { key: '0', label: '0', title: '完全没有' },
    { key: '1', label: '1', title: '几天时间' },
    { key: '2', label: '2', title: '一半以上时间' },
    { key: '3', label: '3', title: '几乎每天' },
];

//心理健康问卷
export const mentalHealthList = [
    { label: '1做事时没兴趣或乐趣', prop: 'a', type: 'radio', options: assessments },
    { label: '2感到心情低落、沮丧或绝望', prop: 'b', type: 'radio', options: assessments },
    { label: '3入睡困难、易醒或睡眠过多', prop: 'c', type: 'radio', options: assessments },
    { label: '4感到疲倦或没有精力', prop: 'd', type: 'radio', options: assessments },
    { label: '5食欲不振或吃的过多', prop: 'e', type: 'radio', options: assessments },
    { label: '6觉得自己很糟糕或自己很失败，或让自己或家人失望', prop: 'f', type: 'radio', options: assessments },
    { label: '7做事情难以专注、例如读报纸或看电视', prop: 'g', type: 'radio', options: assessments },
    { label: '8行动或说话速度缓慢到别人已经察觉到，或刚好相反一变得比平日更烦躁或坐立不安，以至于走来走去比平常多', prop: 'h', type: 'radio', options: assessments },
    { label: '9有不如死掉的想法或以某种方式伤害自己的念头', prop: 'i', type: 'radio', options: assessments }
];

//失眠问卷
export const insomniaList = [
    { label: '1做事时没兴趣或乐趣', prop: 'a', type: 'radio', options: assessments },
    { label: '2感到心情低落、沮丧或绝望', prop: 'b', type: 'radio', options: assessments },
    { label: '3入睡困难、易醒或睡眠过多', prop: 'c', type: 'radio', options: assessments },
    { label: '4感到疲倦或没有精力', prop: 'd', type: 'radio', options: assessments },
    { label: '5食欲不振或吃的过多', prop: 'e', type: 'radio', options: assessments },
    { label: '6觉得自己很糟糕或自己很失败，或让自己或家人失望', prop: 'f', type: 'radio', options: assessments },
    { label: '7做事情难以专注、例如读报纸或看电视', prop: 'g', type: 'radio', options: assessments },
    { label: '8行动或说话速度缓慢到别人已经察觉到，或刚好相反一变得比平日更烦躁或坐立不安，以至于走来走去比平常多', prop: 'h', type: 'radio', options: assessments },
    { label: '9有不如死掉的想法或以某种方式伤害自己的念头', prop: 'i', type: 'radio', options: assessments }
];

//焦虑问卷
export const anxiouList = [
    { label: '1感到紧张、焦虑或快要奔溃', prop: 'a', type: 'radio', options: assessments },
    { label: '2不能停止或控制担忧', prop: 'b', type: 'radio', options: assessments },
    { label: '3过多的担忧各种各样的事情', prop: 'c', type: 'radio', options: assessments },
    { label: '4很难放松下来', prop: 'd', type: 'radio', options: assessments },
    { label: '5不安得难以静坐', prop: 'e', type: 'radio', options: assessments },
    { label: '6变得容易气恼或易怒', prop: 'f', type: 'radio', options: assessments },
    { label: '7感到似乎要发生不好的事情而担心', prop: 'g', type: 'radio', options: assessments },
]