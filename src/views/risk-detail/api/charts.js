/*
 * @Description: echarts图表的数据请求
 * @Author: xiaofang lan
 * @Date: 2021-07-12 16:11:56
 * @LastEditors: xiaofang lan
 * @LastEditTime: 2021-07-13 10:15:37
 */
import request from "@/plugins/axios";
import { formatDate } from 'board/utils/date'

// 十大症状
export function getSymptomData(query) {
	return request({
		url: "/api/ten/statical",
		method: "get",
		params: query
	});
}

// 十大症状趋势
export function getHealthySymptomData(query) {
	return request({
		url: '/healthy/symptom',
		method: 'get',
		params: query
	})
}

// 心理疾病隔离人数
export function getPsyDiseaseData(query) {
	return request({
		url: "/api/heart/statical",
		method: "get",
		params: query
	});
}

// 心理疾病隔离人数
export function getHealthyPsyDiseaseData(query) {
	return request({
		url: "/healthy/psychology",
		method: "get",
		params: query
	});
}

// 基础疾病
export function getBaseDiseaseData(query) {
	return request({
		url: "/api/basic/statical",
		method: "get",
		params: query
	});
}

// 基础疾病趋势
export function getHealthyBaseDiseaseData(query) {
	return request({
		url: '/healthy/basics',
		method: 'get',
		params: query
	})
}

function getTwoWeekDate () {
	const now = new Date()
	const endDate = formatDate(now, 'YYYY-mm-dd 00:00:00')
	const startDate = formatDate(now.setDate(now.getDate() - 14), 'YYYY-mm-dd 00:00:00')
	return [startDate, endDate]
}
// 加载趋势图数据
export function loadChartDate (dataFn, params, fieldPrefix='') {
	const type = params ? params.type : null
	const [startDate, endDate] = getTwoWeekDate()
	return dataFn({
		begin: startDate,
		end: endDate
	}).then(rs => {
		const data = rs.data.data
		const field = type === null ? 'total' : `${fieldPrefix}${type}`
		return data.reduce((rs, v) => {
			rs.push({
				date: v.date.split(/\s/)[0],
				count: v[field] || 0
			})
			return rs
		}, [])
	})	
}

// 十大症状趋势新的
export function loadHealthySymptomData(params) {
	return loadChartDate((nextParams) => {
		return request({
			url: '/apis/bmsoft/statistics/top-symptoms',
			method: 'get',
			params: nextParams
		})
	}, params, 'type')
}

// 基础疾病
export function loadBaseDiseaseData(params) {
	return loadChartDate((nextParams) => {
		return request({
			url: '/apis/bmsoft/statistics/health-investigate',
			method: 'get',
			params: nextParams
		})
	}, params)
}

// 心理疾病
export function loadBaseAssessmentData() {
	const [startDate, endDate] = getTwoWeekDate()
	return request({
		url: '/apis/bmsoft/statistics/assessment-risk-level',
		method: 'get',
		params: {
			begin: startDate,
			end: endDate
		}
	}).then(rs => {
		return rs.data.data.reduce((rs, v) => {
			rs.push({
				date: v.date.split(/\s/)[0],
				hight: v.type2 || 0,
				middle: v.type1 || 0
			})
			return rs
		}, [])
	})
}
