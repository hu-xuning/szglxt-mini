import request from '@/plugins/axios'
import {dealPhoneNum} from '@/utils/crypto'

export function getuserbyuserid(userid) {
    // return request({
    //     url: '/ebus/org/getuserbyuserid',
    //     method: 'post',
    //     data: {
    //         userid
    //     }
    // })
    return Promise.resolve({
        "account": "feng11",
        "accountalias": "sam2",
        "birthday": "19780923",
        "certificatetypeid": "5",
        "city": "长沙",
        "createtime": "2019-05-24T02:46:06.967Z",
        "displayname": "四月三十",
        "district": "南湖",
        "email": "zhangsan11188888@gmail.com",
        "engname": "sam2",
        "errcode": 0,
        "errmsg": "ok",
        "extend": {"工号": "001"},
        "gender": "1",
        "nation": "中国",
        "province": "湖南",
        "status": 0,
        "telephonenumber": 13500000000,
        "units": [{
            "order": 1,
            "position": "委员长",
            "displayposition": "局长、厅长",
            "priority": 1,
            "unitid": "uytvmf1j9dsi2cho7julv3",
            "unitleader": true
        }],
        "updatetime": "2019-05-24T02:46:06.967Z",
        "userid": "spaamgz7l333ztvc54zz8q",
        "username": "四月三十"
    })
}


export function login(username, password, code, uuid) {
    return request({
        url: '/auth/login',
        method: 'post',
        data: {
            username,
            password,
            code,
            uuid
        }
    })
}

export function Phonelogin(telnumber, password, code, uuid) {
    return request({
        url: '/auth/Phonelogin',
        method: 'post',
        data: {
            telnumber,
            password,
            code,
            uuid
        }
    })
}

/**
 * 自动登录，无感登录
 * 目前是临时这么处理，后面对接政务微信跳转
 */
export function autoLogin(telphone) {
    return request({
        url: '/auth/loginByEncodePhone',  // 登陆接口
        method: 'get',
        params: {
            phone: dealPhoneNum(telphone)
        }
    })
}

export function getInfo() {
    return request({
        url: '/auth/info',
        method: 'get'
    })
}

export function getMenu() {
    return request({
        url: '/admin/menu',
        method: 'get'
    })
}

export function UnLock(id) {
    return request({
        url: '/auth/UnLock/' + id,
        method: 'put',
    })
}

export function getCodeImg() {
    return request({
        url: '/auth/code',
        method: 'get'
    })
}

export function getPhoneCodeImg(telnumber) {
    return request({
        url: '/auth/PhoneCode/' + telnumber,
        method: 'get'
    })
}

export function logout() {
    return request({
        url: '/auth/logout',
        method: 'delete'
    })
}

export function passwordIsRight(username, password) {
    return request({
        url: '/auth/passwordIsRight',
        method: 'post',
        data: {
            username,
            password,
        }
    })
}

export function setIsolation(isolationVo) {
    return request({
        url: '/auth/setIsolation',
        method: 'post',
        data: isolationVo
    })
}

export function getIsolations(userId) {
    return request({
        url: '/auth/getIsolations/' + userId,
        method: 'get',
    })
}
