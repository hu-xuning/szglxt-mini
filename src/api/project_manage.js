import request from '@/plugins/axios'

export function query(query){
    return request({
        url: '/api/propertybasic/page',
        method: 'get',
        params: query
    })
}

export function save(data){
    return request.post('/api/propertybasic', data)
}

export function remove(ids){
    return request({
        url: '/api/propertybasic/delete',
        method: 'post',
        data: ids
    })
}
export function validateProjectCode(param){
    return request({
        url: '/api/propertybasic/validateProjectCode',
        method: 'get',
        params: param
    })
}

export function get(projectId){
    return request({
        url: '/api/propertybasic/' + projectId,
        method: 'get'
    })
}

export function update(data){
    return request({
        url: '/api/propertybasic',
        method: 'put',
        data: data
    })
}