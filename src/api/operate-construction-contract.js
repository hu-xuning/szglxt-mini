import request from '~/plugins/axios'

export function fetchList(query) {
  return request({
    url: '/api/operateconstructioncontract/page',
    method: 'get',
    params: query
  })
}
export function propertyPageList(query) {
    return request({
        url: '/api/operateconstructioncontract/toPropertyPage',
        method: 'get',
        params: query
    })
}

export function addObj(obj) {
  return request({
    url: '/api/operateconstructioncontract',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/api/operateconstructioncontract/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/api/operateconstructioncontract/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/api/operateconstructioncontract',
    method: 'put',
    data: obj
  })
}

//hxf-0803-文件列表接口
export function getFiles(id, moduleId) {
    return request({
        url: '/admin/sys-file/getFileList/' + id + '/' + moduleId,
        method: 'get'
    })
}
//hxf-0803-判断文件是否存在
export function checkFiles(id) {
    return request({
        url: '/admin/sys-file/checkFileIsNot/' + id,
        method: 'get'
    })
}//hxf-0803-判断文件是否存在
export function downloadFiles(id) {
    return request({
        url: '/admin/sys-file/download/' + id,
        method: 'get'
    })
}
//hxf-0803-删除文件
export function delFiles(id, moduleId) {
    return request({
        url: '/admin/sys-file/delFileByFileId/' + id + '/' + moduleId,
        method: 'delete'
    })
}
//hxf-0803-删除文件
export function delFile(id) {
    return request({
        url: '/admin/sys-file/delFileByFileId/' + id,
        method: 'delete'
    })
}
