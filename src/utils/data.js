export function getSymptomOptions () {
    return [{
        text: '发热',
        value: 1
    }, {
        text: '干咳',
        value: 2
    }, {
        text: '乏力',
        value: 3
    }, {
        text: '鼻塞',
        value: 4
    }, {
        text: '流涕',
        value: 5
    }, {
        text: '咽痛',
        value: 6
    }, {
        text: '嗅觉/味觉减退',
        value: 7
    }, {
        text: '结膜炎',
        value: 8
    }, {
        text: '肌痛',
        value: 9
    }, {
        text: '腹泻',
        value: 10
    }/*, {
        text: '其他',
        value: 11
    }*/]
}

export function getDiseaseOptions () {
    return [{
        text: '高血压',
        value: 'hypertension'
    }, {
        text: '高血糖',
        value: 'hyperglycemia'
    }, {
        text: '高血脂',
        value: 'hyperlipidemia'
    }, {
        text: '心脏病',
        value: 'heartDisease'
    }, {
        text: '癫痫病',
        value: 'epilepsy'
    }, {
        text: '精神类疾病',
        value: 'mentalIllness'
    }, {
        text: '安装支架',
        value: 'bracket'
    }, {
        text: '有搭桥',
        value: 'bridging'
    }, {
        text: '脑梗病',
        value: 'historyOfCerebralInfarction'
    }, {
        text: '肺结核',
        value: 'pulmonaryTuberculosis'
    }, {
        text: '慢阻肺',
        value: 'copd'
    }, {
        text: '肺气肿',
        value: 'emphysema'
    }, {
        text: '哮喘',
        value: 'asthma'
    }, {
        text: '其他',
        value: 'other'
    }]
}

export function getPsychicOptions () {
    return [{
        text: '低风险',
        value: 0
    }, {
        text: '中风险',
        value: 1
    }, {
        text: '高风险',
        value: 2
    }]
}
