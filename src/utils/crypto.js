import CryptoJS from 'crypto-js' 

/**
 * @description 加密手机号（格式：手机号码:时间戳  -->  再转base64）
 * @param {*} phone
 * @return {*} 输出加密过的手机号码
 */
 export function dealPhoneNum(phone) {
    const composeText = `${phone}`;
    // return encode(composeText);
    return encrypt(composeText)
  }
  
  // AES加密方法
  function encrypt(word){ 
    let keyStr = 'Hcdipgaf38gFPg1z';
    var key  = CryptoJS.enc.Utf8.parse(keyStr);
    var srcs = CryptoJS.enc.Utf8.parse(word);
    var encrypted = CryptoJS.AES.encrypt(srcs, key, {mode:CryptoJS.mode.ECB,padding: CryptoJS.pad.Pkcs7});
    return encrypted.toString();
  }