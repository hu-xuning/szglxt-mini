import Vue from "vue";
import Vuex from "vuex";
import actions from "./actions";
import getters from "./getters";
import mutations from "./mutations";
import user from "./modules/user";
import settings from "./modules/settings";
import areaDict from "./modules/areaDict";
import getAddress from "./modules/getAddress";

Vue.use(Vuex);

let state = {
	theme: "dark", // 主题风格 [light|dark]
	expandMenu: true, //左侧菜单是否展开
	fullscreenLoading: false, //是否显示全屏loading动画
	isMobile: false, //是否是移动端
	isWebView: false, //是否是内嵌页面(内嵌页面隐藏顶部和左侧)
	currentMenu: {
		children: []
	},
	title: "",
	//字典
	dict: [],
	menus: [],
	// 部门列表
	depts: [],
	userInfo: {},
	unreadMsg: [],
	historyRoutes: [],
	//页面顶部的导航
	headerMenu: [],
	//当前激活的顶部菜单
	currentHeaderMenu: {
		label: "控制台",
		value: "dashboard"
	},
	//当前激活的菜单  控制台：dashboard   应用：app  系统管理：sysManage
	menuType: "dashboard",
	// 系统菜单
	sysManage: [],
	//控制台菜单
	dashboardMenu: [],
	//应用菜单
	appMenu: [],
	//消息中心
	messagesTotal: {
		unReadNotice: "0"
	},
	// 控制物业信息查看
	propertyViewFlag: "",
	// 允许上传的附件后缀名称
	// prettier-ignore
	canUploadFile: ["jpg","jpeg","png","gif","bmp","wmv","mpeg","mp4","doc","docx","xls","xlsx","ppt","pptx","txt","pdf","c4d","dem","dxf","iob","lwo","lws","3dm","3dmf","ai","ps","wrl","3ds","obj","mon","fbx","zip", "rar", "7z"]
};

export default new Vuex.Store({
	state,
	mutations,
	actions,
	getters,
	modules: {
		user,
		settings,
		areaDict,
		getAddress
	}
});
