import { getDivision } from "@/admin/api/division";
const areaDict = {
	state: {},

	mutations: {},

	actions: {
		// 获取省
		queryProvince({ commit }, val) {
			// 为空查询省
			if (val === null || val === "" || val === undefined) {
				val = "000000";
			}
			return new Promise((resolve, reject) => {
				getDivision({ lazy: true, parentId: val }).then(rep => {
					let rep_province = rep.data.data;
					let provinces = rep_province.map(p => {
						return { label: p.divisionName, value: p.divisionCode };
					});
					resolve(provinces);
				});
			});
		}
	}
};

export default areaDict;
