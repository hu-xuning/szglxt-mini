const getAddress = {
	state: {
		cityAssembly: [
			{
				label: "深圳市",
				value: "440300"
			}
		],
		districtCollection: [
			// {
			//     label: '罗湖区',
			//     value: '440303',
			//     pid: '440300',
			// },
			{
				label: "福田区",
				value: "440304",
				pid: "440300"
			},
			{
				label: "龙华区",
				value: "440309",
				pid: "440300"
			},
			{
				label: "龙岗区",
				value: "440307",
				pid: "440300"
			}
			// ,
			// {
			//     label: '南山区',
			//     value: '440305',
			//     pid: '440300',
			// },
			// {
			//     label: '宝安区',
			//     value: '440306',
			//     pid: '440300',
			// },
			// {
			//     label: '盐田区',
			//     value: '440308',
			//     pid: '440300',
			// },
			// {
			//     label: '坪山区',
			//     value: '4403010',
			//     pid: '440300',
			// },
			// {
			//     label: '大鹏新区',
			//     value: '4403011',
			//     pid: '440300',
			// },
			// {
			//     label: '光明新区',
			//     value: '4403012',
			//     pid: '440300',
			// }
		]
	},
	getters: {
		getCityAssembly: state => {
			return state.cityAssembly || [];
		},
		getDistrictCollection: state => {
			return state.districtCollection || [];
		}
	},
	mutations: {},
	actions: {
		/*incrementCityAssembly ({ state, commit }) {
            if (state.cityAssembly) {
                commit('getCityAssembly')
            }
        },
        incrementDistrictCollection ({ state, commit }) {
            if (state.districtCollection) {
                commit('getDistrictCollection')
            }
        }*/
	}
};

export default getAddress;
