
export default {
    init (context) {
        context.commit('init')
        context.commit('getDict')
        context.commit('getDepts')
        context.commit('getCity')
        context.commit('getUnreadMsg')
    },
    setPropertyViewFlag (commit, val) {
        commit.commit('changePropertyViewFlag', val)
    }

}
