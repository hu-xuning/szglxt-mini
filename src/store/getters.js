export default {
    permissions: state => state.user.roles || {},
    getDict: state => state.dict || [],
    getPropertyViewFlag: state => state.propertyViewFlag
}
