import Vue from 'vue'
/**
   * 设置url 参数
  */
let setUrlParams  = (url, params, isHashMode) =>{
    function isObject (value) {
      return Object(value) === value
    }

    if (typeof url === 'undefined' || !isObject(params)) {
      return url
    }

    for (var name in params) {
      if (params.hasOwnProperty(name)) {
        let value = params[name]
        let reg = new RegExp('(^|&|\\?|#)' + name + '=([^&]*?)(&|#|$)')
        let tempHash = url.match(/#.*/) ? url.match(/#.*/)[0] : ''

        url = url.replace(/#.*/, '')
        if (isHashMode === true) {
          if (reg.test(tempHash)) {
            tempHash = tempHash.replace(reg, function (m, r1, r2, r3) {
              return r1 + name + '=' + encodeURIComponent(value) + r3
            })
          } else {
            var separator = tempHash.indexOf('#') === -1 ? '#' : '&'
            tempHash = tempHash + separator + name + '=' +
              encodeURIComponent(value)
          }
          tempHash = tempHash.replace(reg, function (m, r1, r2, r3) {
            return r1 + name + '=' + encodeURIComponent(value) + r3
          })
        } else if (reg.test(url)) {
          url = url.replace(reg, function (m, r1, r2, r3) {
            return r1 + name + '=' + encodeURIComponent(value) + r3
          })
        } else {
          let separator = url.indexOf('?') === -1 ? '?' : '&'
          url = url + separator + name + '=' + encodeURIComponent(value)
        }
        url += tempHash
      }
    }
    return url
  }
  let replaceParamSkip = (params) =>{
      location.replace(setUrlParams(window.location.href,params,false))
  }
  Vue.prototype.replaceParamSkip = replaceParamSkip;
  Vue.prototype.setUrlParams = setUrlParams;
