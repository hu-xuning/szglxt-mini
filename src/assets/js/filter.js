// Created by lifei on 2020/6/12--15:58.
import Vue from "vue";
import dateFormate from "./date";
import store from "@/store/index";
import { formatThousandMoney } from "@/utils";

Vue.filter("dict", (value, type) => {
	let dictItem = store.state.dict.find(item => item.type === type) || {};
	let list = dictItem.children || [];
	let data = list.find(item => item.value == value) || {};
	return data.label || "";
});

Vue.filter("shortDate", date => {
	let res = "";
	if (date) {
		let time = new Date(date).getTime() / 1000;
		res = dateFormate.datasng(time, "yyyy-MM-dd");
	} else {
		res = "";
	}
	return res;
});

Vue.filter("shortMonth", date => {
	let res = "";
	if (date) {
		let time = new Date(date).getTime() / 1000;
		res = dateFormate.datasng(time, "yyyy-MM");
	} else {
		res = "";
	}
	return res;
});

//千本位金额格式化
Vue.filter("thousandMoney", money => {
	return money == 0 ? money : formatThousandMoney(money, 2);
});
