export default {
    /* 格式化时间戳
     * 调用：let tt = datasng(shijianchuo, 'yyyy-MM-dd hh:mm:ss') */

    datasng: function (shijianchuo, fmat) {
        let sng = shijianchuo
        // if (typeof sng == 'object') {
            sng = (new Date(sng)).getTime()
            sng = Math.floor(sng / 1000)
        // }
        let week_list = ['周一',
            '周二',
            '周三',
            '周四',
            '周五',
            '周六',
            '周日']
        Date.prototype.format = function (format) {
            let day = this.getDay()
            let week = week_list[day - 1]
            let o = {
                'M+': this.getMonth() + 1, // month
                'd+': this.getDate(), // day
                'h+': this.getHours(), // hour
                'm+': this.getMinutes(), // minute
                's+': this.getSeconds(), // second
                'w+': week, // week
                'q+': Math.floor((this.getMonth() + 3) / 3), // quarter
                S: this.getMilliseconds() // millisecond
            }
            if (/(y+)/.test(format)) {
                format = format.replace(
                    RegExp.$1,
                    (this.getFullYear() + '').substr(4 - RegExp.$1.length)
                )
            }
            for (let k in o) {
                if (new RegExp('(' + k + ')').test(format)) {
                    format = format.replace(
                        RegExp.$1,
                        RegExp.$1.length === 1
                            ? o[k]
                            : ('00' + o[k]).substr(('' + o[k]).length)
                    )
                }
            }
            return format
        }
        return new Date(parseInt(sng) * 1000).format(fmat)
    },
    timeFormat: function (date) {
        if (!date || typeof date === "string") {
            this.error("参数异常，请检查...");
        }
        var y = date.getFullYear(); //年
        var m = date.getMonth() + 1; //月
        var d = date.getDate(); //日
        if(m<10){
            m = '0'+m
        }
        if(d<10){
            d = '0'+d
        }
        return y + "-" + m + "-" + d;
    },

    //获取这周的周一
    getFirstDayOfWeek: function (date) {
        var weekday = date.getDay() || 7; //获取星期几,getDay()返回值是 0（周日） 到 6（周六） 之间的一个整数。0||7为7，即weekday的值为1-7
        date.setDate(date.getDate() - weekday + 1); //往前算（weekday-1）天，年份、月份会自动变化
        return this.timeFormat(date);
    },

    //获取当月第一天
    getFirstDayOfMonth: function (date) {
        date.setDate(1);
        return this.timeFormat(date);
    },

    //获取当季第一天
    getFirstDayOfSeason: function (date) {
        var month = date.getMonth();
        if (month < 3) {
            date.setMonth(0);
        } else if (2 < month && month < 6) {
            date.setMonth(3);
        } else if (5 < month && month < 9) {
            date.setMonth(6);
        } else if (8 < month && month < 11) {
            date.setMonth(9);
        }
        date.setDate(1);
        return this.timeFormat(date);
    },

    //获取当年第一天
    getFirstDayOfYear: function (date) {
        date.setDate(1);
        date.setMonth(0);
        return this.timeFormat(date);
    }
}
