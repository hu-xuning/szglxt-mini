// Created by lifei on 2020/7/17--17:09.
export default {
    data() {
        return {
            selectionList: [],
            pager: {
                total: 0, // 总页数
                currentPage: 1, // 当前页数
                pageSize: 10 // 每页显示多少条
            }
        }
    },
    computed: {
        pageParams: function () {
            return {
                currentPage: this.pager.currentPage,
                pageSize: this.pager.pageSize,
            }
        }
    },
    methods: { 
        changePageSize(val) {
            this.pager.pageSize = val
            this.getListData()
        },
        changeCurrentPage(val) {
            this.pager.currentPage = 1
            this.pager.currentPage = val 
            this.getListData()
        }, 
        selectionChange(val) {
            this.selectionList = val
        }
    }
}
