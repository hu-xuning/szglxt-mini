import Vue from 'vue'
import {MessageBox, Message, Loading} from 'element-ui'

Vue.prototype.suc = function (data, duration) {
    Message({
        message: data,
        type: 'success',
        duration: duration || 3000,
        showClose: duration === 0
    })
}

Vue.prototype.warning = function (data, duration) {
    Message({
        message: data,
        type: 'warning',
        duration: duration || 3000,
        showClose: duration === 0
    })
}
Vue.prototype.err = function (data, duration) {
    Message({
        message: data,
        type: 'error',
        duration: duration || 3000,
        showClose: duration === 0
    })
}
Vue.prototype.info = function (data, duration) {
    Message({
        message: data,
        type: 'info',
        duration: duration || 3000,
        showClose: duration === 0
    })
}
Vue.prototype.confirm = function (title, data, success, cancel) {
    MessageBox.confirm(data, title, {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
    }).then(() => {
        success && success()
    }).catch(() => {
        cancel && cancel()
    })
}

Vue.prototype.alert = function (title, data, confirm) {
    MessageBox.alert(data, title, {
        confirmButtonText: '确定',
        callback: () => {
            confirm && confirm()
        }
    })
}
