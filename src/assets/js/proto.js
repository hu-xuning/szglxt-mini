// Created by lifei on 2020/5/27--9:16.
import Vue from "vue";
import request from "@/plugins/axios";
import store from "@/store/index";
import { downloadBlob } from "@/utils";
import { download, downloadPost } from "@/admin/api/common";

// 页面跳转
const link = function(url) {
	this.$router.push(url);
};

// 返回上一页
const goBack = function() {
	history.go(-1);
};

// 通过id获取图片url
const getPicById = async function(id) {
	let resp = await request({
		url: "/admin/sys-file/fileById/" + id,
		method: "get",
		responseType: "blob"
	});

	let data = await window.URL.createObjectURL(resp.data);

	return data;
};

// 获取音频url
const getAudioById = async function(id) {
	let resp = await request({
		url: "/admin/sys-file/fileById/" + id,
		method: "get",
		responseType: "blob"
	});

	return resp.data;
};

// 通过url获取图片url临时地址
const getPicByUrl = async function(url) {
	let resp = await request({
		url: url,
		method: "get",
		responseType: "blob"
	});

	let data = await window.URL.createObjectURL(resp.data);
	return data;
};

// 通过名称获取url
const getPicByUrlName = async function(url) {
	let resp = await request({
		url: "/admin/sys-file/entry/" + url,
		method: "get",
		responseType: "blob"
	});

	let data = await window.URL.createObjectURL(resp.data);

	return data;
};

const getComparePicByUrlName = async function(url, type = "compare") {
	let resp = await request({
		url: `/admin/sys-file/compareImg/${type}/${url}`,
		method: "get",
		responseType: "blob"
	});

	let data = await window.URL.createObjectURL(resp.data);

	return data;
};

// 下载资源
const downLoadFile = async function(data) {
	let msg = this.$message({
		showClose: true,
		duration: 0,
		message: "正在准备下载内容，请稍等……"
	});
	let url = await getPicByUrl("/admin/sys-file/entry/" + data.fileName);
	let a = document.createElement("a");
	a.href = url;
	a.download = data.original;
	a.innerText = data.original;
	a.target = "_blank";
	a.click();
	msg.close();
};

// 深拷贝
const deepCopy = obj => {
	var copy = Object.create(Object.getPrototypeOf(obj));
	var propNames = Object.getOwnPropertyNames(obj);

	propNames.forEach(function(name) {
		var desc = Object.getOwnPropertyDescriptor(obj, name);
		Object.defineProperty(copy, name, desc);
	});

	return copy;
};

/**
 * 根据字典名称获取字典项
 * @param {String} typeName
 */
export const getDict = function(typeName) {
	let data = store.state.dict.find(item => item.type === typeName) || {};
	return data.children || [];
};

/**
 * 根据权限键名获取权限
 * @param {String} key 权限键名
 */
export const getPermissions = key => {
	const roles = store.state.user.roles || {};
	return roles[key] || false;
};

/**
 * 公共导出表格方法
 * @param {String} exportName 导出文件名称
 * @param {String} url 导出api地址
 * @param {Object} params 导出接口参数
 * @param {String} format 导出格式
 */
export function publicExport(
	exportName,
	url,
	params = {},
	format = "xlsx",
	type = "get"
) {
	if (!url) {
		return new Error("导出api地址不能为空");
	}
	const time = new Date();
	const name = `${time.getFullYear()}-${time.getMonth() +
		1}-${time.getDate()} ${time.getHours()}:${time.getMinutes()}:${time.getSeconds()}${exportName}.${format}`;
	if (type === "get") {
		download(url, params).then(response => {
			downloadBlob(response.data, name);
		});
	} else {
		downloadPost(url, params).then(response => {
			downloadBlob(response.data, name);
		});
	}
}

Vue.prototype.link = link;
Vue.prototype.goBack = goBack;
Vue.prototype.deepCopy = deepCopy;
Vue.prototype.getPicById = getPicById;
Vue.prototype.getPicByUrl = getPicByUrl;
Vue.prototype.downLoadFile = downLoadFile;
Vue.prototype.getPicByUrlName = getPicByUrlName;
Vue.prototype.getAudioById = getAudioById;
Vue.prototype.getComparePicByUrlName = getComparePicByUrlName;
Vue.prototype.getDict = getDict;
Vue.prototype.getPermissions = getPermissions;
Vue.prototype.publicExport = publicExport;
