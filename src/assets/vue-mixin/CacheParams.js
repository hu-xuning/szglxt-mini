/*
 * @Author: xjt
 * @Date: 2021-03-30 09:11:00
 * @LastEditTime: 2021-07-02 16:14:49
 * @Description: 缓存页面跳转后筛选的参数
 * 实现思路：
 * 1.通过组件路由守卫判断跳转前的path和跳转后的path是否属于同一个目录下【通过/字符切片】
 * 2.如果是列表页跳转其他页面，判断to的路由路由地址是否是add、edit、preview、look等关键字，如果是的话，在session中保存当前筛选项的参数，
 * 在beforeRouteEnter路由中，判断session中是否有缓存当前页面的path和筛选参数，有的话就修改筛选参数;
 * 3.替换筛选参数后，再对dataList组件进行筛选项回显处理
 * 4.替换筛选参数时，保留之前列表页的current参数
 * @LastEditors: cqg
 */
export default {
  beforeRouteEnter(to, from, next) {
    const sence = from.path.split("/").pop();
    const saveScens = [
      "look",
      "edit",
      "preview",
      "add",
      "import",
      "change",
      "view",
      "assess",
    ];
    let params = sessionStorage.getItem(to.path);
    if (sence && saveScens.includes(sence) && from.path.includes(to.path)) {
      next((vm) => {
        // 定义手动替换请求参数方法
        if (
          vm.restoreRequestParams &&
          typeof vm.restoreRequestParams === "function"
        ) {
          vm.restoreRequestParams(params, next);
        } else {
          if (params) {
            // 提取出有效的筛选参数，通过ref的方式插入进去，通过默认值的方式无法回显搜索框的文本内容
            vm.params = { ...vm.params, ...JSON.parse(params) };
            // 对于某些tab类型的页面进行判断
            vm.$refs.dataList && vm.$refs.dataList.echoParams(vm.params);
          }
        }
      });
    } else {
      params && sessionStorage.removeItem(to.path);
      next();
    }
  },
  beforeRouteLeave(to, from, next) {
    const sence = to.path.split("/").pop();
    const saveScens = [
      "look",
      "edit",
      "preview",
      "add",
      "import",
      "change",
      "view",
      "assess",
    ];
    if (sence && saveScens.includes(sence) && to.path.includes(from.path)) {
      let params = this.getCacheParams ? this.getCacheParams() : this.params;
      sessionStorage.setItem(from.path, JSON.stringify(params));
    }
    next();
  },
};
