
const EditBaseMixins = {
  data() {
    return {
      editForm: Object.create(null)
    };
  },
  mounted(){
      this.reflectQueryData()
  },
  methods: {
      reflectQueryData: function() {
          if (this.editSettings) {
              this.editSettings.map(item => {
                  if (item && item.children) {
                      item.children.map(config => {
                          if (config.datePickerRange) {
                              this.$set(this.editForm, config.model, []);
                          } else {
                              this.$set(this.editForm, config.model, "");
                          }
                      });
                  }

              })
              this.$set(this.editForm, this.busIdPropertyName, '');
          }
      }
  }
};
export default EditBaseMixins;
