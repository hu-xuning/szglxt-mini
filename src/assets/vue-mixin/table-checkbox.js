// Created by lifei on 2020/6/17--9:42.

export default {
    data() {
        return {
            checkedItem: {}
        };
    },
    methods: {
        handleRowClick(list, row) {
            this.toggleSelectTable(list, row);
        },
        toggleSelectTable(list, data) {
            let item = data;

            if (!Array.isArray(list)) {
                item = list;
            }

            let key = "";
            if (item.id) {
                key = "id";
            } else if (item.accentId) {
                key = "accentId";
            } else if (item.personId) {
                key = "personId";
            } else if (item.customId) {
                key = "customId";
            } else {
                key = "uuid";
            }
            if (this.checkedItem[key] === item[key]) {
                this.checkedItem = {};
                this.$refs.crud.selectClear();
            } else {
                this.checkedItem = item;

                this.$refs.crud.selectClear();

                let data = this.tableData[item.$index];

                this.$refs.crud.toggleSelection([data]);
            }
        }
    },
    watch: {
        tableData() {
            this.checkedItem = {};
            this.$refs.crud.selectClear();
        }
    },
    created() {
        let param = this.$route.query;
        if (this.searchForm) {
            this.searchForm.inStationNumber = param.inStationNumber || "";

            if (this.searchForm.inStationNumber) {
                this.tableOption.column.map(item => {
                    if (item.prop == "inStationNumber") {
                        item.searchValue = this.searchForm.inStationNumber;
                    }
                });
            }
        }
    }
};
