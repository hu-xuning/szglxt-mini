// 预算管理以及工作流管理，涉及预算这块的逻辑复用
import { Loading } from "element-ui";
import moment from "moment";
import { formatMoney, ToCDB, formatThousandMoney } from "@/utils";
import { mapState } from "vuex";

export default {
	data() {
		return {
			datatime: moment().format("yyyy-MM-DD hh:mm:ss"), // 当前时间
			//loading
			loadService: Object.create(null),
			commitDisabled: false //提交等待期间按钮禁用
		};
	},
	computed: {
		...mapState({ user: state => state.user.user })
	},
	methods: {
		//科目表头的单独样式
		headerCellStyle({ row, column, rowIndex, columnIndex }) {
			if (rowIndex === 0 && columnIndex === 0) {
				//指定坐标
				return `
        font-size: 1.5em;
        width: 25 %;
        font - weight: normal;
        padding - left: 20px;`;
			}
		},
		// 监听科目禁用改变
		handleIsNeadChange(e, row) {
			if (row.isLeaf == 1) {
				let list = this.form.itemValueList.map(item => {
					if (row.itemId == item.itemId) {
						item.itemValuePre = 0;
					}
					return item;
				});
				this.form.itemValueList = list;
			} else {
				let index = this.form.itemValueList.findIndex(
					item => item.itemId == row.parentId
				);
				this.form.itemValueList[index].children = this.form.itemValueList[
					index
				].children.map(item => {
					if (row.itemId == item.itemId) {
						item.itemValuePre = 0;
					}
					return item;
				});
			}
			this.budgetChange();
		},
		// 监听两列的科目禁用改变
		handleSimpleIsNeadChange(e, row) {
			if (row.isLeaf == 1) {
				let list = this.form.itemValueList.map(item => {
					if (row.itemId == item.itemId) {
						item.itemValue = 0;
					}
					return item;
				});
				this.form.itemValueList = list;
			} else {
				let index = this.form.itemValueList.findIndex(
					item => item.itemId == row.parentId
				);
				this.form.itemValueList[index].children = this.form.itemValueList[
					index
				].children.map(item => {
					if (row.itemId == item.itemId) {
						item.itemValue = 0;
					}
					return item;
				});
			}
			this.simpleBudgetChange();
		},
		// 校验必输
		checkWillBe() {
			const reg = /^\d{0,15}(\.\d{0,2})?$/;
			let list = this.form.itemValueList.reduce((total, item) => {
				return item.children ? total.concat(item.children) : [...total, item];
			}, []);

			let tips = list.find(
				item =>
					item.itemValuePre == null ||
					item.itemValuePre === "" ||
					!reg.test(`${item.itemValuePre}`)
			);
			if (tips) {
				if (tips.itemValuePre === "" || tips.itemValuePre == null) {
					this.$message({
						message: `科目"${tips.itemName} " 调整后金额为空，请输入完整调整后金额`,
						type: "warning"
					});
					return false;
				} else {
					this.$message({
						message: ` 科目"${tips.itemName} " 调整后金额不正确，请输入合理金额`,
						type: "warning"
					});
					return false;
				}
			}
			return true;
		},
		// 两列的计算金额
		simpleBudgetChange() {
			let result = 0;
			this.form.itemValueList.forEach((item, index) => {
				if (item.children != null) {
					let total = item.children.reduce(
						(total, i) => (total += this.changeHalfCode(i.itemValue)),
						0
					);
					let itemValue =
						String(total).indexOf(".") > 0 ? total.toFixed(2) : total;
					this.$set(this.form.itemValueList[index], "itemValue", itemValue);
				}
				result += this.changeHalfCode(item.itemValue);
			});
			this.form.applyData.budgetSum =
				String(result).indexOf(".") > 0 ? result.toFixed(2) : result;
		},
		// 计算调整后金额和变化金额
		budgetChange() {
			// 调整后金额统计
			let result = 0;
			// 变化金额统计
			let changeTheAmountResult = 0;
			const itemValueList = this.form.itemValueList.map(item => {
				// 没有子集时
				if (!item.children) {
					let itemValuePre = this.changeHalfCode(item.itemValuePre);
					let itemValue = this.changeHalfCode(item.itemValue);
					item.changeAmount =
						item.itemValuePre != null || itemValuePre
							? formatMoney(itemValuePre - itemValue)
							: 0;
				} else {
					//有子集时
					let num = 0;
					let changeTheAmount = 0;
					item.children.forEach(i => {
						let itemValuePre = this.changeHalfCode(i.itemValuePre);
						let itemValue = this.changeHalfCode(i.itemValue);
						let changeAmount = itemValuePre - itemValue;
						i.changeAmount =
							i.itemValuePre != null || itemValuePre
								? formatMoney(changeAmount)
								: 0;
						num += itemValuePre;
						changeTheAmount += +i.changeAmount;
					});
					changeTheAmount =
						String(item.itemValuePre).indexOf(".") > 0
							? formatMoney(changeTheAmount)
							: changeTheAmount;
					item.itemValuePre =
						String(item.itemValuePre).indexOf(".") > 0 ? formatMoney(num) : num;
					item.changeAmount =
						String(changeTheAmount).indexOf(".") > 0
							? formatMoney(changeTheAmount)
							: changeTheAmount;
				}
				result += +item.itemValuePre;
				changeTheAmountResult += +item.changeAmount;
				return item;
			});

			let resultDotIndex = String(result).indexOf(".");
			let amountDotIndex = String(changeTheAmountResult).indexOf(".");
			this.form.itemValueList = itemValueList;
			this.form.applyData.changeAmount =
				amountDotIndex > 0
					? changeTheAmountResult.toFixed(2)
					: changeTheAmountResult;
			this.form.applyData.budgetSum2 =
				resultDotIndex > 0
					? formatMoney(this.form.applyData.budgetSum + changeTheAmountResult)
					: formatMoney(this.form.applyData.budgetSum + changeTheAmountResult);
		},
		loadAction: function(val, target) {
			if (target) {
				this.loadService = Loading.service({ target: target, text: val });
			} else {
				this.loadService = Loading.service({ text: val });
			}
			this.loadService = Loading.service({ text: val });
		},
		loadClose: function() {
			try {
				this.loadService.close();
			} catch (e) {}
		},
		changeHalfCode(str) {
			let re = /[\uff00-\uffff]/g;
			return re.test(str) ? +ToCDB(str) : +str;
		},
		formatterMoney(money) {
			if (!money) {
				return "暂无数据";
			}
			return formatThousandMoney(money);
		},
		formatterThousandMoney(money) {
			return formatThousandMoney(money);
		}
	}
};
