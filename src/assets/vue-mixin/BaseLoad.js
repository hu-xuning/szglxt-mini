export default {
	data() {
		return {
			//loading
			loadService: Object.create(null)
		};
	},
	methods: {
		loadAction(val, target) {
			if (target) {
				this.loadService = this.$loading({ target: target, text: val });
			} else {
				this.loadService = this.$loading({ text: val });
			}
		},
		loadClose() {
			try {
				this.loadService.close();
			} catch (e) {}
		}
	}
};
