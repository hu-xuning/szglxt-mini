
import Vue from 'vue';
function onInput(el, ele, binding, vnode) {
    function handle() {
        // 只保留数字
        ele.value = ele.value.replace(/[^\d]/g, '')
    } 
    return handle
}
Vue.directive('number-input', {
    bind(el, binding, vnode) {
        const ele = el.tagName === 'INPUT' ? el : el.querySelector('input')
        ele.addEventListener('input', onInput(el, ele, binding, vnode), false)
    },
})