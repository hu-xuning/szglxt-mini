// Created by lifei on 2020/7/17--17:09.
export default {
    data() {
        return {
            tableLoading: true,
            selectionList: [],
            pager: {
                total: 0, // 总页数
                currentPage: 1, // 当前页数
                pageSize: 10 // 每页显示多少条
            },
            engineeringName: ''
        }
    },
    computed: {
        pageParams: function () {
            return {
                current: this.pager.currentPage,
                size: this.pager.pageSize,
            }
        }
    },
    mounted() {
        this.getListData()
    },
    methods: {
        setListData(resp) {
            let data = resp.data
            this.pager.total = data.total
            this.tableData = data.records
            this.tableLoading = false
        },
        changePageSize(val) {
            this.pager.currentPage = 1
            this.pager.pageSize = val
            this.tableLoading = true
            this.getListData()
        },
        changeCurrentPage(val) {
            this.pager.currentPage = val
            this.tableLoading = true
            this.getListData()
        },
        selectionChange(val) {
            this.selectionList = val

        },
        resetList() {
            this.pager.currentPage = 1
            this.tableLoading = true
            this.getListData()
        }
    }
}
