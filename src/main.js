import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import bus from "@/store/bus";
import 'amfe-flexible'

// 全局组件
import "@/plugins/components";

// directive 指令
import "@/directive";

// 全局插件
import "@/plugins";

// 全局样式
import "@/assets/style/reset.scss";
const FastClick = require('fastclick')
FastClick.attach(document.body)

import Vant from 'vant';
import 'vant/lib/index.css';

Vue.use(Vant);
// bus
Vue.prototype.$bus = bus;

Vue.config.productionTip = false;

new Vue({
	router,
	store,
	render: h => h(App)
}).$mount("#app");
