/*
 * @Author: xjt
 * @Date: 2021-06-24 23:03:01
 * @LastEditTime: 2021-06-24 23:37:48
 * @Description: 安排预定管理
 * @LastEditors: xjt
 */
export default {
	path: "/yq-reserve",
	component: () => import("~/layout/index"),
	children: [
		{
			path: "hotelReserve",
			name: "hotelReserve",
			meta: { title: "隔离点预定" },
			component: () => import("~/views/yq-reserve/hotelReserve/index.vue")
		},
		{
			path: "personReceive",
			name: "personReceive",
			meta: { title: "人员接收跟踪" },
			component: () =>
				import("~/views/yq-reserve/personReceive/index.vue")
		},
	]
};
