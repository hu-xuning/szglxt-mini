/*
 * @Author: xjt
 * @Date: 2021-06-24 23:03:01
 * @LastEditTime: 2021-06-30 20:52:30
 * @Description: 隔离点基本信息模块
 * @LastEditors: cqg
 */
export default {
  path: "/yq-hotel",
  component: () => import("~/layout/index"),
  children: [
    {
      path: "baseInfo",
      name: "baseInfo",
      meta: { title: "隔离点基本信息" },
      component: () => import("~/views/yq-hotel/baseInfo/index.vue"),
    },
    {
      path: "room",
      name: "room",
      meta: { title: "隔离点房间信息" },
      component: () => import("~/views/yq-hotel/room/index.vue"),
    },
    {
      path: "room/edit",
      name: "roomEdit",
      meta: { title: "隔离点房间编辑" },
      component: () => import("~/views/yq-hotel/room/edit.vue"),
    },
    {
      path: "room/preview",
      name: "roomPreview",
      meta: { title: "隔离点房间查看" },
      component: () => import("~/views/yq-hotel/room/preview.vue"),
    },
    {
      path: "roomOverview",
      name: "roomPreview",
      meta: { title: "隔离点房间看板" },
      component: () => import("~/views/yq-hotel/roomKanban/index.vue"),
    },
  ],
};
