/*
 * @Author: xjt
 * @Date: 2021-06-24 23:18:21
 * @LastEditTime: 2021-06-24 23:20:07
 * @Description: 统计查询
 * @LastEditors: xjt
 */
export default {
	path: "/yq-totalSearch",
	component: () => import("~/layout/index"),
	children: [
		{
			path: "searchUser",
			name: "searchUser",
			meta: { title: "全市集中隔离点责任人及隔离情况动态" },
			component: () => import("~/views/yq-totalSearch/searchUser/index.vue")
		},
		{
			path: "personReceive",
			name: "personReceive",
			meta: { title: "全市集中隔离点人员动态情况" },
			component: () =>
				import("~/views/yq-totalSearch/personReceive/index.vue")
		},
		{
			path: "abnormalPersonnel",
			name: "abnormalPersonnel",
			meta: { title: "全市集中隔离人员中表现异常人员" },
			component: () =>
				import("~/views/yq-totalSearch/abnormalPersonnel/index.vue")
		},
		{
			path: "workingPersonnel",
			name: "workingPersonnel",
			meta: { title: "全市集中隔离场所工作人员" },
			component: () =>
				import("~/views/yq-totalSearch/workingPersonnel/index.vue")
		},
		{
			path: "IsolationRoom",
			name: "IsolationRoom",
			meta: { title: "全市各区集中隔离点房间动态" },
			component: () =>
				import("~/views/yq-totalSearch/IsolationRoom/index.vue")
		},
		{
			path: "lawbreakers",
			name: "lawbreakers",
			meta: { title: "全市集中隔离人员中有犯罪前科和违法记录人员" },
			component: () =>
				import("~/views/yq-totalSearch/lawbreakers/index.vue")
		},
		{
			path: "personsLarge",
			name: "personsLarge",
			meta: { title: "全市集中隔离人员中在逃人员" },
			component: () =>
				import("~/views/yq-totalSearch/personsLarge/index.vue")
		}
	]
};