/*
 * @Author: xjt
 * @Date: 2021-06-24 23:15:22
 * @LastEditTime: 2021-06-28 15:17:43
 * @Description: 隔离人员管理
 * @LastEditors: cqg
 */
export default {
  path: "/yq-personnel",
  component: () => import("~/layout/index"),
  children: [
    {
      path: "ReceivRegistration",
      name: "ReceivRegistration",
      meta: { title: "信息登记核实" },
      component: () =>
        import("~/views/yq-personnel/ReceivRegistration/index.vue"),
    },
    {
      path: "user",
      name: "user",
      meta: { title: "隔离点工作人员管理" },
      component: () => import("~/views/yq-personnel/staffManage/index.vue"),
    },
    {
      path: "dutyMange",
      name: "dutyMange",
      meta: { title: "隔离点工作人员排班" },
      component: () => import("~/views/yq-personnel/dutyManage/index.vue"),
    },
    {
      path: "himultipleManage",
      name: "himultipleManage",
      meta: { title: "历史隔离人员列表", isHistory: true },
      component: () =>
        import("~/views/yq-personnel/multipleManage/himultipleManage.vue"),
    },
    {
      path: "multipleManage",
      name: "multipleManage",
      meta: { title: "在管隔离人员列表", isHistory: false },
      component: () => import("~/views/yq-personnel/multipleManage/index.vue"),
    },
    // {
    // 	path: "infoRegistra",
    // 	name: "infoRegistra",
    // 	meta: { title: "信息登记核实" },
    // 	component: () => import("~/views/yq-personnel/infoRegistra/index.vue")
    // },
    // {
    // 	path: "healthAssessment",
    // 	name: "healthAssessment",
    // 	meta: { title: "心身风险评估" },
    // 	component: () =>
    // 		import("~/views/yq-personnel/healthAssessment/index.vue")
    // },
    {
      path: "register",
      name: "register",
      meta: { title: "隔离点消杀情况登记" },
      component: () => import("~/views/yq-personnel/register/index.vue"),
    },
    {
      path: "personnel",
      name: "personnel",
      meta: { title: "人员信息回收站" },
      component: () => import("~/views/yq-personnel/personnel/index.vue"),
    },
    {
      path: "checkIn",
      name: "checkIn",
      meta: { title: "办理入住" },
      component: () => import("~/views/yq-personnel/checkIn/index.vue"),
    },
    {
      path: "medicalMonitor",
      name: "medicalMonitor",
      meta: { title: "健康评估" },
      component: () => import("~/views/yq-personnel/medicalMonitor/index.vue"),
    },
    {
      path: "health",
      name: "health",
      meta: { title: "医学观察" },
      component: () => import("~/views/yq-personnel/health/index.vue"),
    },
    {
      path: "medicalMonitor/assess",
      name: "assess",
      meta: { title: "评估" },
      component: () => import("~/views/yq-personnel/medicalMonitor/assess.vue"),
    },
    {
      path: "overdueManage",
      name: "overdueManage",
      meta: { title: "隔离延期管理" },
      component: () => import("~/views/yq-personnel/overdue/index.vue"),
    },
    {
      path: "checkOutManage",
      name: "checkOutManage",
      meta: { title: "离点管理" },
      component: () => import("~/views/yq-personnel/checkOutManage/index.vue"),
    },
    {
      path: "abnormalScreening",
      name: "abnormalScreening",
      meta: { title: "异常排查" },
      component: () => import("~/views/yq-personnel/abnormalScreening/index.vue"),
    },
    {
      path: "exConvict",
      name: "exConvict",
      meta: { title: "前违逃管理" },
      component: () => import("~/views/yq-personnel/exConvict/index.vue"),
    },
  ],
};
