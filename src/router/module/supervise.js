/*
 * @Author: xjt
 * @Date: 2021-06-24 23:18:21
 * @LastEditTime: 2021-06-24 23:22:16
 * @Description: 监测预警
 * @LastEditors: xjt
 */
export default {
	path: "/yq-supervise",
	component: () => import("~/layout/index"),
	children: [
		{
			path: "expirationAlert",
			name: "expirationAlert",
			meta: { title: "隔离到期预警" },
			component: () =>
				import("~/views/yq-supervise/expirationAlert/index.vue")
		},
		{
			path: "userSupervise",
			name: "userSupervise",
			meta: { title: "中高危人员监测" },
			component: () => import("~/views/yq-supervise/userSupervise/index.vue")
		},
		{
			path: "todoList",
			name: "todoList",
			meta: { title: "待办事项查询" },
			component: () =>
				import("~/views/yq-supervise/todoList/index.vue")
		},
	]
};