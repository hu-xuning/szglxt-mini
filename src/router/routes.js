// 系统管理
import systemManage from '@/admin/views/system-manage/router';
// 应用首页
import dashboard from '@/views/wy-home/router';
// 隔离点基本信息
import hotel from './module/hotel';
// 安排预定管理
import reserve from './module/reserve';
// 隔离点登记管理
import checkIn from './module/checkIn';
// 统计查询
import totalSearch from './module/totalSearch';
// 监测预警
import supervise from './module/supervise';

const routes = [
  {
    path: '/error403',
    component: () => import('~/layout/index'),
    children: [
      {
        path: '',
        meta: { title: '权限不足' },
        component: () => import('@/views/_error/403'),
      },
    ],
  },
  // systemManage,
  // dashboard,
  // hotel,
  // reserve,
  // checkIn,
  // totalSearch,
  // supervise,
  {
    path: '/',
    redirect: '/risk',
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('@/views/wy-login/index'),
  },
  {
    path: '/risk',
    name: 'risk',
    component: () => import('@/views/risk/add'),
  },
  {
    path: '/riskList',
    name: 'riskList',
    component: () => import('@/views/risk/list'),
  },
  {
    path: '/flightTopic',
    name: 'risk',
    component: () => import('@/views/risk/flightTopic'),
  },
  {
    path: '/gl/bulletinboard',
    component: () => import('@/views/gl-bulletinBoard/index'),
  },
  {
    path: '/riskDetail',
    name: 'riskDetail',
    component: () => import('@/views/risk-detail/index'),
  },
  // {
  //   path: '/roomNews',
  //   name: 'roomNews',
  //   component: () => import('@/views/roomNews/index.vue'),
  // },
  // {
  //   path: '/roomKanban',
  //   name: 'roomKanban',
  //   component: () => import('@/views/wy-login/roomKanban'),
  // },
  // {
  //   path: '*',
  //   component: () => import('~/layout/index'),
  //   children: [
  //     {
  //       path: '',
  //       component: () => import('@/views/wy-home/index'),
  //     },
  //   ],
  // },
  
];

export default routes;
