export default {
    77: {
        text: '密接次密接隔离点',
        icon: require('./icons/77.svg'),
        color: '#FF8B7C'
    },
    33: {
        text: '航班入境隔离点',
        icon: require('./icons/33.svg'),
        color: '#FFAC1E'
    },
    44: {
        text: '香港入境隔离点',
        icon: require('./icons/44.svg'),
        color: '#4D9CFD'
    },
    11: {
        text: '一般隔离点',
        icon: require('./icons/11.svg'),
        color: '#61C39D'
    },
    99: {
        text: '休眠隔离点',
        icon: require('./icons/99.svg'),
        color: '#999999'
    }
}