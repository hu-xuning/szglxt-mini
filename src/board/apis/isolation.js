/**
 * 加载隔离点类型数据 
*/
export function loadIsolationTypes () {
    return Promise.resolve([{
        text: "密接次密接隔离点",
        value: 77,
    }, {
        text: "航班入境隔离点",
        value: 33,
    }, {
        text: "香港入境隔离点",
        value: 44,
    }, {
        text: "一般隔离点",
        value: 11,
    }, {
        text: '休眠隔离点',
        value: 99
    }])
}