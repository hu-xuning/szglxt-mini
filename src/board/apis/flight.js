import { get } from 'board/utils/request'

import { formatDate } from 'board/utils/date'
/**
 * 获取当前隔离人员健康检测和心理评估统计数据
 */
export function loadHealthInvestigateAndRiskAssessment () {
    return get('/apis/bmsoft/statistics/today/health-investigate-and-risk-assessment')
}
/**
 * 获取指定日期医学观察统计数据
 */
export function loadDailyHealth () {
    const now = new Date()
    const date = formatDate(now, 'YYYY-mm-dd')
    return get('/apis/bmsoft/statistics/daily-health', {
        date
    })
}
/**
 * 隔离点统计信息
 */
export function loadCheckinInformation () {
    return get('/apis/bmsoft/statistics/check-in-information')
}
/**
 * 当前隔离人员类型统计
 */
export function loadPersonClassify () {
    return get('/apis/bmsoft/statistics/person-classify')
}

