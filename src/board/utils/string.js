export const unique = (() => {
    let number = 162703055217
    return () => {
        number++
        return 'unique_' + number
    }
})()