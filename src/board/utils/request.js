import request from '@/plugins/axios'

function ajax (method, url, params, options) {
    options = options || {}
    if (method === 'get') {
        options.params = params
    } else {
        options.data = params
    }
    return request({
        url,
        method,
        ...options
    }).then(rs => {
        return rs.data.data
    })
}

export function get (url, params, options) {
    return ajax('get', url, params, options)
}

export function post (url, data, options) {
    return ajax('post', url, data, options)
}