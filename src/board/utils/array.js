import lodashUniq from 'lodash.uniq'

export function uniq (arr) {
    return lodashUniq(arr)
}