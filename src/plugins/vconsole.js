if (process.env.NODE_ENV === 'development') {
    import('vconsole').then(rs => {
        new rs.default({ maxLogNumber: 1000 })
    })
}