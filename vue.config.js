let webpack = require("webpack");
const path = require("path");
const CompressionWebpackPlugin = require("compression-webpack-plugin");
const productionGzipExtensions = ["js", "css", "woff", "ttf"];

// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

function resolve(dir) {
  return path.join(__dirname, dir);
}

// let baseUrl = "http://192.168.1.20:8002"; //个人地址
// let baseUrl = "http://10.226.48.13:8002"; //服务器地址
// let baseUrl = "http://10.226.48.14:8001"; //测试
let baseUrl = "https://218.17.84.73:443/test"; //测试

console.log(process.env.VUE_APP_BUILD_TYPE)

module.exports = {
  lintOnSave: false,
  productionSourceMap: false,
  parallel: true,
  publicPath: process.env.VUE_APP_BUILD_TYPE === 'test' ? '/wxjzgl/' : '/wxfxkb/',
  devServer: {
    proxy: {
      "/jzgl": {
        target: baseUrl,
        changeOrigin: true,
        logLevel: "debug",
        secure: false,
        headers: {
          Referer: 'https://218.17.84.73:443'
        },
        pathRewrite: {
          "^/wxfxkb/jzgl": "/jzgl",
        },
      },
      "/user/login": {
        target: 'https://gl.sz.bmdigitech.cn',
        changeOrigin: true,
      },
      '/apis': {
        target: "https://218.17.84.73/pc-test-apis",
        changeOrigin: true,
        logLevel: "debug",
        secure: false,
        headers: {
          Referer: 'https://218.17.84.73'
        },
        pathRewrite: {
          "^/wxfxkb/apis/bmsoft": "/",
        },
      }
    },
    overlay: {
      warnings: true,
      errors: true,
    },
    port: 8091,
    https: false,
    open: true,
    disableHostCheck: true
  },
  chainWebpack: (config) => {
    config.plugins.delete("prefetch");
    config.resolve.alias
      .set("~", resolve("src"))
      .set("board", resolve("src/board"))
      .set("assets", resolve("src/assets"));
  },
  configureWebpack: {
    plugins: [
      // 代码size分析
      //new BundleAnalyzerPlugin(),
      // 去除moment的s其他语言包
      new webpack.ContextReplacementPlugin(/moment[/\\]locale$/, /ja|it/),

      new webpack.ProvidePlugin({
        utils: [resolve("./src/utils/index.js"), "default"],
      }),
      // gzip
      new CompressionWebpackPlugin({
        algorithm: "gzip",
        test: new RegExp("\\.(" + productionGzipExtensions.join("|") + ")$"),
        threshold: 10240,
        minRatio: 0.8,
      }),
    ],
  },
  css: {
    loaderOptions: {
      scss: {
        prependData: `
                    @import "@/assets/style/common.scss";
                    @import "@/assets/style/public.scss";
                `,
      },
    },
  },
};
